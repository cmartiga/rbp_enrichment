#!/usr/bin/env python

import csv
import itertools
import multiprocessing
from subprocess import check_call
import sys
from time import ctime

import numpy
from pandas import read_table, DataFrame
import pathos

from motif.matrix import read_matrix, write_matrix
from utils.settings import EVENTS_INDEX


GENETIC_CODE = {'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
                'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
                'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
                'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
                'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
                'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
                'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
                'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
                'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
                'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
                'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
                'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
                'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
                'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
                'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
                'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}

BASIC_AA = 'Basic'
ACID_AA = 'Acid'
POLAR_AA = 'Polar'
SPECIAL_AA = 'Special'
HYDROPHOBIC_AA = 'Hydrophobic'
STOP = 'Stop codon'
AA_PROPERTIES = {'R': BASIC_AA, 'H': BASIC_AA, 'K': BASIC_AA,
                 'D': ACID_AA, 'E': ACID_AA, 'S': POLAR_AA,
                 'T': POLAR_AA, 'N': POLAR_AA, 'Q': POLAR_AA,
                 'C': SPECIAL_AA, 'P': SPECIAL_AA, 'U': SPECIAL_AA,
                 'G': SPECIAL_AA, 'A': HYDROPHOBIC_AA, 'V': HYDROPHOBIC_AA,
                 'I': HYDROPHOBIC_AA, 'L': HYDROPHOBIC_AA, 'M': HYDROPHOBIC_AA,
                 'F': HYDROPHOBIC_AA, 'Y': HYDROPHOBIC_AA, 'W': HYDROPHOBIC_AA,
                 '_': STOP}

BIOTYPES = {'Pseudogene': ['pseudogene', 'tRNA_pseudogene',
                           'polymorphic_pseudogene', 'scRNA_pseudogene',
                           'rRNA_pseudogene', 'snoRNA_pseudogene',
                           'Mt_rRNA_pseudogene', 'IG_V_pseudogene',
                           'disrupted_domain', 'IGC_pseudogene',
                           'IGJ_pseudogene', 'IG_pseudogene',
                           'IGV_pseudogene', 'processed_pseudogene',
                           'transcribed_processed_pseudogene',
                           'transcribed_unitary_pseudogene',
                           'transcribed_unprocessed_pseudogene',
                           'translated_processed_pseudogene',
                           'TRJ_pseudogene', 'unprocessed_pseudogene',
                           'unitary_pseudogene'],
            'lncRNA': ['processed_transcript', 'lincRNA', 'antisense',
                       'sense_intronic', 'sense_overlapping',
                       '3prime_overlapping_ncrna', 'ncrna_host',
                       'ambiguous_orf', 'antisense_RNA', 'macro_lncRNA',
                       'non_coding', 'bidirectional_promoter_lncrna'],
            'sncRNA': ['miRNA', 'rRNA', 'snoRNA', 'misc_RNA',
                       'miscRNA_pseudogene', 'Mt_rRNA', 'Mt_tRNA', 'scRNA',
                       'snlRNA', 'snRNA', 'tRNA', 'tRNA_pseudogene',
                       'miRNA_pseudogene', 'miscRNA_pseudogene', 'vaultRNA',
                       'sRNA', 'scaRNA'],
            'protein_coding': ['protein_coding', 'TR_J_gene',
                               'IG_V_gene', 'IG_C_gene',
                               'IG_C_gene', 'IG_D_gene', 'IG_gene', 'IG_J_gene',
                               'IG_LV_gene', 'IG_M_gene', 'IGZ_gene',
                               'nonsense_mediated_decay', 'nontranslating_CDS',
                               'non_stop_decay', 'polymorphic_pseudogene',
                               'TR_C_gene', 'TR_D_gene', 'TR_V_gene']}

BIOTYPES_CLASSES = {}
for biotype_class, biotypes in BIOTYPES.items():
    for biotype in biotypes:
        BIOTYPES_CLASSES[biotype] = biotype_class


class AppendableDict(dict):
    def append(self, key, value):
        try:
            self[key].append(value)
        except KeyError:
            self[key] = [value]

    def extend(self, key, values):
        try:
            self[key].extend(values)
        except KeyError:
            self[key] = values

    def add(self, key, value):
        try:
            self[key].add(value)
        except KeyError:
            self[key] = set([value])


def get_length(list_or_array):
    try:
        return list_or_array.shape[0]
    except AttributeError:
        return len(list_or_array)


def calc_set_distance(set1, set2):
    total = len(set1.union(set2))
    common = len(set1.intersection(set2))
    return (total - common) / float(total)


def calc_set_precision(true, test):
    common = len(true.intersection(test))
    positive = len(test)
    return common / float(positive)


def calc_set_recall(true, test):
    common = len(true.intersection(test))
    n_true = len(true)
    return common / float(n_true)


def hamming_distance(array1, array2):
    sel_idxs = numpy.logical_or(array1 == 1, array2 == 1)
    a1 = array1[sel_idxs]
    a2 = array2[sel_idxs]
    return (a1 != a2).sum() / float(a1.shape[0])


def translate_seq(seq, genetic_code):
    seq = seq.replace('U', 'T')
    seq_len = len(seq)
    protein_seq = []
    for idx in xrange(0, seq_len, 3):
        if idx + 3 > seq_len:
            break
        codon = seq[idx: idx + 3]
        aa = genetic_code.get(codon, None)
        if aa is None:
            msg = 'Unrecognized characters for translating sequences'
            raise ValueError(msg)
        protein_seq.append(aa)
    return ''.join(protein_seq)


def calc_aa_spectra(seq, reading_frame=None, codon_table=GENETIC_CODE,
                    map_to_aa_properties=None, count_after_stop=True):
    if reading_frame is None:
        reading_frames = [0, 1, 2]
    else:
        reading_frames = [reading_frame]

    aa_spectra = {}
    for reading_frame in reading_frames:
        protein_seq = translate_seq(seq[reading_frame:], codon_table)
        for aa in protein_seq:
            if map_to_aa_properties is not None:
                aa = map_to_aa_properties[aa]
            try:
                aa_spectra[aa] += 1
            except KeyError:
                aa_spectra[aa] = 1
            if aa == '_' and not count_after_stop:
                break
    return aa_spectra


def take_chunks(iterable, chunk_size):
    chunk = []
    for item in iterable:
        if len(chunk) < chunk_size:
            chunk.append(item)
        else:
            yield chunk
            chunk = []
    if chunk:
        yield chunk


def run_parallel(func, iterable, threads=1, use_pathos=False):
    '''pathos library allows multiprocessing with methods from classes whereas
       standard library multiprocessing can only work with functions'''
    if threads == 1:
        map_func = itertools.imap
    else:
        if use_pathos:
            workers = pathos.multiprocessing.ProcessingPool(threads)
        else:
            workers = multiprocessing.Pool(threads)
        map_func = workers.imap
    return map_func(func, iterable)


def complete_cases(matrix):
    return numpy.logical_not(numpy.any(numpy.isnan(matrix), axis=1))


def get_groups_from_vast_out(fhand, index_col=1):
    df = read_table(fhand, index_col=index_col)
    group = numpy.full(df.shape[0], 'not_significant', dtype='|S15')
    abs_dpsi = df.columns[-2]
    dpsi_prob = df.columns[-1]
    included = numpy.logical_and(df[dpsi_prob] > 0, df[abs_dpsi] > 0)
    group[numpy.array(included)] = 'included'
    excluded = numpy.logical_and(df[dpsi_prob] > 0, df[abs_dpsi] < 0)
    group[numpy.array(excluded)] = 'excluded'
    result = DataFrame()
    result['Group'] = group
    result.index = df.index
    return result


def get_mic_groups_from_vast_out(fhand, length_fhand, mic_lenght=27,
                                 index_col=1):
    df = read_table(fhand, index_col=index_col)
    length_df = read_table(length_fhand, index_col=1)
    length = numpy.array(length_df.ix[df.index, 2])
    group = numpy.full(df.shape[0], 'not_significant', dtype='|S15')
    dpsi_prob = df.columns[-1]
    reg_mic = numpy.logical_and(df[dpsi_prob] > 0, length <= mic_lenght)
    group[numpy.array(reg_mic)] = 'regulated_microexons'
    reg = numpy.logical_and(df[dpsi_prob] > 0, length > mic_lenght)
    group[numpy.array(reg)] = 'regulated_exons'
    mic = numpy.logical_and(df[dpsi_prob] == 0, length < mic_lenght)
    group[numpy.array(mic)] = 'not_reg_mic'
    result = DataFrame()
    result['Group'] = group
    result.index = df.index
    return result


class LogTrack(object):
    def __init__(self, fhand=None):
        if fhand is None:
            fhand = sys.stderr
        self.fhand = fhand

    def write(self, msg, add_time=True):
        if add_time:
            msg = '[ {} ] {}\n'.format(ctime(), msg)
        else:
            msg += '\n'
        self.fhand.write(msg)
        self.fhand.flush()


class ProcessCall(object):
    def __init__(self, log_track, run_cmd=True, write_cmds=True):
        self.log_track = log_track
        self.run_cmd = run_cmd
        self.write_cmds = write_cmds

    def __call__(self, cmd):
        if self.write_cmds:
            self.log_track.write('\t{}'.format(' '.join(cmd)),
                                 add_time=False)
        if self.run_cmd:
            check_call(cmd)


def parse_vast_output(dict_reader):
    for record in dict_reader:
        gene_id = record['GENE']
        event_id = record['EVENT']
        length = record['LENGTH']
        event_type = record['COMPLEX']
        samples = {}
        for key, value in record.items():
            if key in ['GENE', 'EVENT', 'COORD', 'LENGTH',
                       'FullCO', 'COMPLEX']:
                continue
            sample = key
            if sample.split('-')[-1] == 'Q':
                sample = '-'.join(sample.split('-')[:-1])
                reads = [float(x) if x != 'NA' else float('nan')
                         for x in value.split('@')[-1].split(',')]
                try:
                    samples[sample]['reads'] = reads
                except KeyError:
                    samples[sample] = {'reads': reads}
            else:
                value = float(value) if value != 'NA' else float('nan')
                try:
                    samples[sample]['psi'] = value
                except KeyError:
                    samples[sample] = {'psi': value}

        yield {'gene_id': gene_id, 'event_id': event_id, 'length': length,
               'samples': samples, 'event_type': event_type, 'record': record}


def filter_vast_output(parsed_vast_output, min_total_coverage=None,
                       min_samples=None, event_type=None, min_psi=None):
    for record in parsed_vast_output:
        if (event_type is not None and
                EVENTS_INDEX[record['event_type']] != event_type):
            continue
        if min_total_coverage is not None:
            n_pass = 0
            skip = False
            for sample in record['samples'].values():
                total_reads = sum(sample['reads'])
                if total_reads < min_total_coverage:
                    if min_samples is None:
                        skip = True
                        break
                    else:
                        n_pass += 1
            if skip or (min_samples is not None and n_pass < min_samples):
                continue
        yield record


def get_psi_groups_from_vast_out(parsed_vast_output, min_psi=None,
                                 min_samples=None, length_fhand=None,
                                 mic_length=27):
    groups = []
    events_ids = []
    for record in parsed_vast_output:
        group = 'present'
        if min_psi is not None:
            n_pass = 0
            for sample in record['samples'].values():
                psi = sample['psi']
                if psi < min_psi:
                    if min_samples is None:
                        group = 'absent'
                        break
                    else:
                        n_pass += 1
            if min_samples is not None and n_pass < min_samples:
                group = 'absent'
        events_ids.append(record['event_id'])
        groups.append(group)

    if length_fhand is not None:
        length_df = read_matrix(length_fhand, rownames_col=1)
        mic = numpy.array(length_df.ix[events_ids, 2] <= 27)
        groups = [group + '_mic' if is_mic else group
                  for group, is_mic in zip(groups, mic)]
    return DataFrame({'Group': groups}, index=events_ids)


def _is_first_exon(parsed_exon):
    return parsed_exon['rank'] == 1


def _is_last_exon(parsed_exon, exon_idx):
    gene_id = parsed_exon['gene_id']
    transcript_id = parsed_exon['transcript_id']
    rank = parsed_exon['rank']
    is_last = True
    for alternative in [True, False]:
        if alternative not in exon_idx[gene_id]:
            continue
        if transcript_id not in exon_idx[gene_id][alternative]:
            continue
        transcript_exons = exon_idx[gene_id][alternative][transcript_id]
        for exon_data in transcript_exons.values():
            if exon_data['rank'] > rank:
                is_last = False
                break
        if not is_last:
            break
    return is_last


def get_exon_type(exon_record, exon_idx):
    if _is_first_exon(exon_record):
        return 'first_exon'
    elif _is_last_exon(exon_record, exon_idx):
        return 'last_exon'
    elif exon_record['alternative']:
        return 'alternative'
    else:
        return 'constitutive'


def get_gene_exon_ids(gene_id, exon_idx):
    exon_ids = set()
    for alternative in [True, False]:
        if alternative not in exon_idx[gene_id]:
            continue
        for transcript in exon_idx[gene_id][alternative].values():
            for exon in transcript.values():
                exon_ids.add(exon['exon_id'])
    return exon_ids


def group_exon_ids_by_gene(ensembl_fhand):
    exon_idx = {}
    for line in ensembl_fhand:
        items = line.strip().split()
        parsed_exon = parse_ensembl_exon(items)
        exon_id = parsed_exon['exon_id']
        gene_id = parsed_exon['gene_id']
        transcript_id = parsed_exon['transcript_id']
        alternative = parsed_exon['alternative']
        try:
            try:
                try:
                    exon_idx[gene_id][alternative][transcript_id][exon_id] = parsed_exon
                except KeyError:
                    exon_idx[gene_id][alternative][transcript_id] = {exon_id: parsed_exon}
            except KeyError:
                exon_idx[gene_id][alternative] = {transcript_id: {exon_id: parsed_exon}}
        except KeyError:
            exon_idx[gene_id] = {alternative: {transcript_id: {exon_id: parsed_exon}}}
    return exon_idx


if __name__ == '__main__':
    full_fpath = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/inclusion_full.all.tab'
    dict_reader = csv.DictReader(open(full_fpath), delimiter='\t')
    fieldnames = dict_reader.fieldnames

    parsed_vast = parse_vast_output(dict_reader)
    filtered_vast = filter_vast_output(parsed_vast, min_total_coverage=20,
                                       min_samples=4)

    out_full = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/inclusion_full.sel.tab'
    out_full_fhand = open(out_full, 'w')
    dict_writer = csv.DictWriter(out_full_fhand, fieldnames=fieldnames,
                                 delimiter='\t')
    dict_writer.writeheader()
    events_ids = []
    for record in filtered_vast:
        events_ids.append(record['event_id'])
        dict_writer.writerow(record['record'])
    out_full_fhand.flush()
#
#     filtered_fpath = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/inclusion_filtered.all.tab'
#     filtered_matrix = read_matrix(open(filtered_fpath), rownames_col=1)
#     out_filtered = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/inclusion_filtered.sel.tab'
#     out_filtered_fhand = open(out_filtered, 'w')
#     events_ids = numpy.intersect1d(events_ids, filtered_matrix.index)
#     filtered_matrix = filtered_matrix.loc[events_ids, :]
#     write_matrix(filtered_matrix, out_filtered_fhand)
#
#     grouping1 = get_groups_from_vast_out(open(out_filtered), index_col=0)
#     out_fhand = open('/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/grouping.sel.tab', 'w')
#     write_matrix(grouping1, out_fhand)
#
#     length_fhand = open(out_full)
#     grouping2 = get_mic_groups_from_vast_out(open(out_filtered), length_fhand,
#                                              index_col=0)
#     out_fhand = open('/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/grouping.mic.sel.tab', 'w')
#     write_matrix(grouping2, out_fhand)

    length_fhand = open(out_full)
    dict_reader = csv.DictReader(open(out_full), delimiter='\t')
    fieldnames = dict_reader.fieldnames
    parsed_vast = parse_vast_output(dict_reader)
    grouping3 = get_psi_groups_from_vast_out(parsed_vast, min_psi=0.1,
                                             min_samples=None,
                                             length_fhand=length_fhand)
    out_fhand = open('/home/cmarti/microexons_analysis/human_hf/hf_vs_control/vast_out/grouping.present_all_samples.mic.sel.tab', 'w')
    write_matrix(grouping3, out_fhand)
#
#
#
#     filtered_fpath = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/inclusion_filtered_reduced_samples.tab'
#     filtered_fhand2 = open(filtered_fpath + '2', 'w')
#     for line in open(filtered_fpath):
#         if line == '\n':
#             continue
#         if 'GENE' in line:
#             filtered_fhand2.write(line)
#             continue
#         items = line.strip().split()
#         if len(items) == 6:
#             filtered_fhand2.write(line)
#         elif len(items) > 6:
#             items1 = items[:5]
#             item5 = items[5]
#             i = 0
#             value = ''
#             while True:
#                 i += 1
#                 try:
#                     value = float(item5[:i])
#                 except ValueError:
#                     break
#             value = str(value)
#             items1 += [value]
#             items2 = [item5[i:]] + items[6:]
#             filtered_fhand2.write('\t'.join(items1) + '\n')
#             filtered_fhand2.write('\t'.join(items2) + '\n')
#
#     filtered_fhand2.flush()
#
#     filtered_matrix = read_matrix(open(filtered_fhand2.name), rownames_col=1)
#     out_filtered = '/home/cmarti/microexons_analysis/human_hf/hf_vs_control/inclusion_filtered_reduced_samples.sel.tab'
#     out_filtered_fhand = open(out_filtered, 'w')
#     events_ids = numpy.intersect1d(events_ids, filtered_matrix.index)
#     filtered_matrix = filtered_matrix.loc[events_ids, :]
#     write_matrix(filtered_matrix, out_filtered_fhand)
