#!/usr/bin/env python
import os
from os.path import dirname, abspath, join

HOME = os.getenv("HOME")

BIN_DIR = abspath(join(dirname(__file__), '..', 'bin'))
TEST_DATA_DIR = abspath(join(dirname(__file__), '..', 'test', 'test_data'))
EXT_DIR = abspath(join(dirname(__file__), '..', 'external_tools'))
SS_SCRIPTS_DIR = join(EXT_DIR, 'score_splice_site')
GSEA_FPATH = join(EXT_DIR, 'gsea2-2.2.2.jar')
SCORE_5SS = os.path.join(SS_SCRIPTS_DIR, 'score5.pl')
SCORE_3SS = os.path.join(SS_SCRIPTS_DIR, 'score3.pl')
SVM_BPFINDER = join(EXT_DIR, 'svm-bpfinder', 'svm_bpfinder.py')
CAPR = join(EXT_DIR, 'CapR', 'src', 'CapR')
PRUNET_DIR = join(EXT_DIR, 'PRUNET')
PRUNET_CMD = ['perl', join(PRUNET_DIR, 'PRUNET_command_line.pl')]
CLUSTALO = join(EXT_DIR, 'clustalo')

INTRON_WINDOW_SIZE = 250
EXON_WINDOW_SIZE = 50
ALT_WINDOW_SIZE = 200

# IO tags
RNAMOTIF = 'rnamotif'
CISBP_RNA = 'cisbp_rna'
REGEXP = 'reg_exp'
MOTIF_FIELDNAMES = {RNAMOTIF: ['gene_name', 'organism', 'seq'],
                    CISBP_RNA: ['RBP_name', 'PWM']}
MOTIF_TYPES = {RNAMOTIF: 'seq', CISBP_RNA: 'PWM'}
MATS_FORMAT = 'MATS'
VAST_FORMAT = 'vast'
BED_FORMAT = 'BED'
ENSEMBL_FORMAT = 'ensembl'

# Tags for regions considered in exon cassettes
A_tag = 'Alternative_exon'
I1_tag = 'Upstream_intron'
I2_tag = 'Downstream_intron'
C1_tag = 'Upstream_exon'
C2_tag = 'Downstream_exon'

# Tags for regions considered in retained introns
I_tag = 'Retained_intron'

# Tags for regions considered in alternative splice sites
ALT5_tag = 'Alternative_splice_site_exon'


# Junctions tags
C1I1 = 'C1I1'
I1A = 'I1A'
AI2 = 'AI2'
I2C2 = 'I2C2'
JUNCTIONS_TAGS = [C1I1, I1A, AI2, I2C2]

# Settings
N_SAMPLES = 10000
N_SHUFFLING = 1000
FISHER_TEST = 'fisher_test'
SAMPLING_TEST = 'sampling_test'
INTRON_WINDOW_SIZE = 250
SUPPORTED_CORRECTION_METHODS = ['holm', 'hochberg', 'hommel', 'bonferroni',
                                'BH', 'BY', 'fdr', 'none']

# Biomart tags
GENE_ID = 'ensembl_gene_id'
EXON_START = 'exon_chrom_start'
EXON_END = 'exon_chrom_end'
STRAND = 'strand'
CHROM = 'chromosome_name'
PHASE = 'phase'
MOUSE_DATABASE = 'mmusculus_gene_ensembl'

# Features settings
MAX_K = 3
GET_PU_SCORES = 'perl -w  ' + HOME + '/Scripts/GetSecondaryStructureValues'
GET_PU_SCORES += '.perl -method PU -f {} -o {}'
INTRONS_EXTENSION = 50
JUNCTION_WINDOW_SIZE = 20
PU_INTRON_WINDOW_SIZE = 70
SLIDING_WINDOW_SIZE = 50
RNAPLFOLD_WS = 500

GTF = 'gtf'
GFF = 'gff'
FASTA = 'fasta'
END3 = '3end'
END5 = '5end'
MCM = 'mcm'
MULTINOMIAL = 'multinomial'

# Vast-tools events types
S = 'S'
C1 = 'C1'
C2 = 'C2'
C3 = 'C3'
MIC = 'MIC'
ALT3 = 'Alt3'
ALT5 = 'Alt5'
IR_C = 'IR-C'
IR_S = 'IR-S'
EX = 'Exon_cassette'
INT = 'Intron_retention'
ALTD = 'Alternative_donor'
ALTA = 'Alternative_acceptor'
EVENT = 'Splicing_event'
BED = 'bed_interval'
EVENTS_TYPES = [EX, INT, ALTD, ALTA, BED, EVENT]
EVENTS_INDEX = {S: EX, C1: EX, C2: EX, C3: EX, MIC: EX,
                IR_C: INT, IR_S: INT, ALT3: ALTA, ALT5: ALTD}
MIC_LENGTH = 27

# Enrichment methods available
FISHER = 'fisher'
GLM = 'poisson_glm'
GSEA = 'gsea'
ENRICHMENT_METHODS = [FISHER, GLM, GSEA]
