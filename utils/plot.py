#!/usr/bin/env python

import itertools
import os
import sys

import hiveplot
from matplotlib import cm
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy
import pandas
from pandas.core.frame import DataFrame
import seaborn

import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from motif.matrix import change_ref_coords_profile
from motif.stats import calc_cum_distib
from statistics.base import padjust, EmpiricalPvalueCalculator
from utils.misc import run_parallel, AppendableDict
from utils.plot_utils import repel_labels, create_patches_legend


plt.style.use('bmh')
seaborn.set_style('white')


def get_new_figure_and_canvas(figsize=None):
    fig = Figure(figsize=figsize)
    canvas = FigureCanvas(fig)
    return fig, canvas


def volcano_plot(results_dfs, axes, palette=None, xlim=None, ylim=None,
                 threshold=None, legend=False, only_over=False, sep='.',
                 set_ylabel=True, set_xlabel=True):

    if palette is None:
        def_colors = seaborn.color_palette(n_colors=len(results_dfs))
        palette = dict(zip(sorted(results_dfs.keys()), def_colors))

    for label, df in results_dfs.items():
        df['Label'] = label

    df = pandas.concat(results_dfs.values())
    df = df.loc[numpy.isfinite(df['estimate']), :]
    df['pvalue'].replace(0.0, df['pvalue'][df['pvalue'] != 0.0].min(),
                         inplace=True)
    df['logOR'] = numpy.log2(df['estimate'])
    df['logpval'] = -numpy.log10(df['pvalue'])
    df['tag'] = [str(motif).split(sep)[0] for motif in df.index]

    # Filter enrichment tags
    if threshold is not None:
        df.loc[df['pvalue'] > threshold, 'tag'] = ''

    if only_over:
        df.loc[df['logOR'] <= 0, 'tag'] = ''

    # Make scatterplot
    for label, color in palette.items():
        sub_df = df.loc[df['Label'] == label, :]
        axes.scatter(sub_df['logOR'], sub_df['logpval'], c=color, s=40)
        sub_df = sub_df.loc[sub_df['tag'] != '', :]
        if sub_df.shape[0] > 0:
            repel_labels(axes, sub_df['logOR'], sub_df['logpval'],
                         labels=sub_df['tag'], fontsize=12)

    # Plots adjustments
    if set_ylabel:
        axes.set_ylabel('$-log_{10}(p-value)$', fontsize=18)
    else:
        axes.set_ylabel('')
    if set_xlabel:
        axes.set_xlabel('$-log_{2}(OddsRatio)$', fontsize=18)
    else:
        axes.set_xlabel('')
    if ylim is not None:
        axes.set_ylim(ylim)
    if xlim is not None:
        axes.set_xlim(xlim)
    if legend:
        create_patches_legend(axes, palette, loc=2, kwargs={'fontsize': 12})
    if threshold is not None:
        y = -numpy.log10(threshold)
        xlim = axes.get_xlim()
        ylim = axes.get_ylim()
        axes.plot(xlim, (y, y), linewidth=0.5, color='grey')
        axes.plot((0, 0), ylim, linewidth=0.5, color='grey')
        axes.set_xlim(xlim)
        axes.set_ylim(ylim)

    sig_motifs = numpy.unique(df.index[df['tag'] != ''])
    return axes, sig_motifs


def _get_regions(results_dfs, sep='.'):
    regions = set()
    for df in results_dfs:
        for rowname in df.index:
            region = '_'.join(rowname.split(sep)[1:])
            regions.add(region)
    return regions


def _group_by_region(results_dfs, regions, sep='.'):
    for region in sorted(regions):
        region_dfs = []
        for df in results_dfs:
            regions = numpy.array(['_'.join(rowname.split(sep)[1:])
                                   for rowname in df.index])
            sel_rows = df.index[numpy.array(regions == region)]
            region_dfs.append(df.loc[sel_rows, :])
        yield region, region_dfs


def _prep_dfs_fisher(dfs):
    for df in dfs:
        df.columns = [col if col != 'OR' else 'estimate' for col in df.columns]


def _prep_dfs_glm(df, params):
    dfs = []
    for param in params:
        param_df = DataFrame()
        param_df['estimate'] = df['{}.estimate'.format(param)]
        param_df['pvalue'] = df['{}.pvalue'.format(param)]
        param_df.index = df.index
        dfs.append(param_df)
    return dfs


def volcano_plot_fisher_by_region(results_dfs, fhand, dfs_labels=None,
                                  figsize=None, region_sep='.',
                                  replace_inf=False, only_over=False):
    seaborn.set_style('white')
    regions = _get_regions(results_dfs, sep=region_sep)
    n_cols = len(regions) / 2 + len(regions) % 2
    if figsize is None:
        figsize = (4 * n_cols, 8)
    fig, canvas = get_new_figure_and_canvas(figsize)
    gs = gridspec.GridSpec(2, n_cols)
    n_tests = sum([df.shape[0] for df in results_dfs])
    if n_tests == 0:
        sys.stderr.write('Total tests are 0. No plot could be done\n')
        return
    bonferroni_threshold = 0.05 / n_tests

    sig_motifs = {}
    for i, (region, dfs) in enumerate(_group_by_region(results_dfs, regions,
                                                       sep=region_sep)):
        axes = fig.add_subplot(gs[i % 2, i / 2])
        if i == len(regions) - 1:
            legend = True
        else:
            legend = False
        _prep_dfs_fisher(dfs)
        dfs_dict = dict(zip(dfs_labels, dfs))
        axes, sig = volcano_plot(dfs_dict, axes,
                                 threshold=bonferroni_threshold,
                                 legend=legend, only_over=only_over, sep='.',
                                 set_xlabel=(i % 2 == 1),
                                 set_ylabel=(i / 2 == 0))
        axes.set_title(region.replace('_', ' ').replace('.', ' '))
        seaborn.despine(ax=axes)
        sig_motifs[region] = sig

    fig.tight_layout()
    canvas.print_figure(fhand)
    return sig_motifs


def volcano_plot_glm_by_region(df, fhand, params, figsize=None,
                               region_sep='_', replace_inf=False):
    regions = _get_regions([df], sep=region_sep)
    n_cols = len(regions) / 2 + len(regions) % 2
    if figsize is None:
        figsize = (5 * n_cols, 10)
    fig, canvas = get_new_figure_and_canvas(figsize)
    gs = gridspec.GridSpec(2, n_cols)
    n_tests = df.shape[0] * len(params)
    bonferroni_threshold = 0.05 / n_tests

    sig_motifs = {}
    for i, (region, dfs) in enumerate(_group_by_region([df], regions,
                                                       sep=region_sep)):
        axes = fig.add_subplot(gs[i % 2, i / 2])
        if i == len(regions) - 1:
            legend = True
        else:
            legend = False
        dfs = _prep_dfs_glm(dfs[0], params)
        axes, sig = volcano_plot(dfs, axes, params, xlabel='Estimate',
                                 bonferroni_threshold=bonferroni_threshold,
                                 legend=legend, replace_inf=replace_inf,
                                 sep=region_sep, remove_first_char=True)
        axes.set_title(region)
        sig_motifs[region] = sig
    canvas.print_figure(fhand)
    return sig_motifs


def _prep_dfs_gsea(dfs):
    for df in dfs:
        df.columns = [col if col != 'rho' else 'estimate'
                      for col in df.columns]


def volcano_plot_gsea_by_region(df, fhand, figsize=None,
                                region_sep='.', replace_inf=False):
    regions = _get_regions([df], sep=region_sep)
    n_cols = len(regions) / 2 + len(regions) % 2
    if figsize is None:
        figsize = (5 * n_cols, 10)
    fig, canvas = get_new_figure_and_canvas(figsize)
    gs = gridspec.GridSpec(2, n_cols)
    n_tests = df.shape[0]
    bonferroni_threshold = 0.05 / n_tests

    sig_motifs = {}
    for i, (region, dfs) in enumerate(_group_by_region([df], regions,
                                                       sep=region_sep)):
        axes = fig.add_subplot(gs[i % 2, i / 2])
        _prep_dfs_gsea(dfs)
        axes, sig = volcano_plot(dfs, axes, xlabel='Rho',
                                 bonferroni_threshold=bonferroni_threshold,
                                 legend=False, replace_inf=replace_inf,
                                 sep=region_sep, remove_first_char=False)
        axes.set_title(region)
        sig_motifs[region] = sig
    canvas.print_figure(fhand)
    return sig_motifs


def plot_line_series(x, ys, axes, labels=None, colors=None, linestyle=None):
    if linestyle is None:
        linestyle = ['solid'] * len(ys)
    elif type(linestyle) == str:
        linestyle = [linestyle] * len(ys)
    elif not isinstance(linestyle, list):
        msg = 'linestyle value not valid: None, str or list allowed'
        raise ValueError(msg)

    for i, y in enumerate(ys):
        if labels is not None:
            if colors is not None:
                axes.plot(x, y, linestyle=linestyle[i], label=labels[i],
                          color=colors[i])
            else:
                axes.plot(x, y, linestyle=linestyle[i], label=labels[i])
        else:
            axes.plot(x, y, linestyle=linestyle[i])
    return axes


def plot_regions_series(xs, ys_series, plot_labels, series_labels, fhand,
                        figsize=None, colors=None, ylabel=None,
                        ylims=None, xticks_size=10, negative_ticks=None,
                        linestyle=None, ys_series2=None, series_labels2=None,
                        ylabel2=None, ylims2=None):
    if figsize is None:
        figsize = (5 * len(xs), 7)
    fig, canvas = get_new_figure_and_canvas(figsize)
    xsums = sum([len(x) for x in xs])
    spacing = int(0.01 * xsums)
    gs = gridspec.GridSpec(1, xsums + spacing * (len(xs) - 1))
    if negative_ticks is None:
        negative_ticks = [False] * len(xs)

    xstart = 0
    j = 0
    for x, ys, label, negative_tick in zip(xs, ys_series, plot_labels,
                                           negative_ticks):
        xend = xstart + len(x)
        axes = fig.add_subplot(gs[0, xstart:xend])
        plot_line_series(x, ys, axes=axes, labels=series_labels, colors=colors,
                         linestyle=linestyle)
        axes.set_title(label)
        axes.set_xlabel('Coordinates')
        if xstart == 0:
            axes.legend()
        if ylims:
            axes.set_ylim(ylims)
        if ylabel and (xstart == 0 or ylims is None):
            axes.set_ylabel(ylabel)
        if xstart != 0 and ylims is not None:
            axes.set_yticklabels([])
        xstart = xend + spacing
        xticks = [i for i in range(1, len(x) + 1) if i % xticks_size == 0]
        if negative_tick:
            xticks = [-i for i in xticks[::-1]]
        axes.set_xticks(xticks)
        axes.set_xticklabels(xticks)

        if ys_series2 is not None:
            axes = axes.twinx()
            ys = ys_series2[j]
            plot_line_series(x, ys, axes=axes, labels=series_labels2,
                             colors=colors, linestyle='dashed')
            if ylims2:
                axes.set_ylim(ylims2)
            if ylabel and (xstart == 0 or ylims2 is None):
                axes.set_ylabel(ylabel2)
        j += 1

    canvas.print_figure(fhand)


def _calc_series(seqs_regions, regions_labels, groups_idxs, cumulative=False,
                 window_size=None, ylims=None, seqs_regions2=None):
    series_values = []
    ymax = []
    ymin = []
    if seqs_regions2 is not None:
        seqs_regions = seqs_regions * seqs_regions2
    for seqs, label in zip(seqs_regions, regions_labels):
        seqs = numpy.array(seqs)
        series = []

        for group_idx in groups_idxs:
            if cumulative:
                just = label.split('.')[1]
                if just == 'r':
                    reverse = True
                elif just == 'l':
                    reverse = False
                else:
                    msg = 'Aligning could not be detected in region label'
                    raise ValueError(msg)
                serie = calc_cum_distib(seqs[group_idx, :], reverse=reverse)
            else:
                serie = numpy.nanmean(seqs[group_idx, :], axis=0)
            series.append(serie)

        series_values.append(series)

        if window_size is not None:
            new_series = []
            for serie in series:
                new_serie = []
                for i in range(0, serie.shape[0] - window_size + 1):
                    new_serie.append(serie[i:i + window_size].mean())
                new_series.append(new_serie)
            series = new_series

        ymax.append(max([max(i) for i in series]))
        ymin.append(min([min(i) for i in series]))

    if ylims is None:
        ylims = (min(ymin), max(ymax) + 0.2 * max(ymax))
    return series_values, ylims


def _get_groups_indxs(grouping, shuffle=False):
    groups_idxs = []
    seq_groups = numpy.unique(grouping.iloc[:, 0])
    for group in seq_groups:
        is_in_group = grouping.iloc[:, 0].as_matrix() == group
        if shuffle:
            numpy.random.shuffle(is_in_group)
        groups_idxs.append(is_in_group)
    return groups_idxs


def _calc_series_stat(series_values, seqs_regions, regions_labels, grouping,
                      cumulative=False, window_size=None, ylims=None,
                      n=1000, alternative='two-sided', log_scale=False,
                      verbose=False, n_threads=1, statistic='pvalue',
                      seqs_regions2=None):

    def get_null_distrib(i):
        if verbose:
            sys.stderr.write('Permutation number: {}\n'.format(i))
        groups_idxs = _get_groups_indxs(grouping, shuffle=True)
        i_series_values = _calc_series(seqs_regions, regions_labels,
                                       groups_idxs, cumulative=cumulative,
                                       window_size=window_size, ylims=ylims,
                                       seqs_regions2=seqs_regions2)[0]
        return i_series_values

    null_distribs = list(run_parallel(get_null_distrib, xrange(n),
                                      threads=n_threads))

    series_stats = []
    for i, series in enumerate(series_values):
        null_distrib = [i_series_values[i]
                        for i_series_values in null_distribs]
        serie_stats = []
        for group_idx, group_series in enumerate(series):
            region_stats = []
            group_null = [i_series_values[group_idx]
                          for i_series_values in null_distrib]
            for j, value in enumerate(group_series):
                null = numpy.array([j_serie[j] for j_serie in group_null])
                if statistic == 'pvalue':
                    calc_pvalue = EmpiricalPvalueCalculator(null, alternative)
                    stat_value = calc_pvalue(value)
                    if log_scale:
                        stat_value = -numpy.log10(stat_value)

                elif statistic == 'zscore':
                    stat_value = (value - null.mean()) / null.std()

                else:
                    msg = 'only zscore or pvalue statistics are available'
                    raise ValueError(msg)

                region_stats.append(stat_value)
            serie_stats.append(numpy.array(region_stats))
        series_stats.append(serie_stats)
    ylims = [numpy.nanmin([numpy.nanmin(x) for x in series_stats]),
             numpy.nanmax([numpy.nanmax(x) for x in series_stats])]
    ylims[1] = ylims[1] + 0.2 * ylims[1]
    return series_stats, ylims


def plot_regions_track_values(seqs_regions, seq_names, regions_labels,
                              grouping, out_fhand, figsize=None, xs=None,
                              ylabel=None, ylims=None, xticks_size=20,
                              window_size=None, cumulative=False,
                              calc_pvalues=False, null_size=1000,
                              verbose=False, n_threads=1, calc_zscore=False,
                              seqs_regions2=None):
    if numpy.any(seq_names != grouping.index):
        raise ValueError('Seq names must match rownames of grouping matrix')
    if figsize is None:
        figsize = (5 * len(seqs_regions), 8)

    if calc_zscore and calc_pvalues:
        raise ValueError('Only one of zscore or pvalue can be calculated')

    stat = 'pvalue'
    if calc_zscore:
        stat = 'zscore'

    groups_idxs = _get_groups_indxs(grouping, shuffle=False)
    seq_groups = numpy.unique(grouping.iloc[:, 0])
    series_values, ylims = _calc_series(seqs_regions, regions_labels,
                                        groups_idxs, cumulative=cumulative,
                                        window_size=window_size, ylims=ylims,
                                        seqs_regions2=seqs_regions2)
    series_p, ylims_p, ylabel2 = None, None, None
    if calc_pvalues or calc_zscore:
        series_p, ylims_p = _calc_series_stat(series_values, seqs_regions,
                                              regions_labels, grouping,
                                              cumulative=cumulative,
                                              window_size=window_size,
                                              ylims=ylims, log_scale=True,
                                              n=null_size, verbose=verbose,
                                              n_threads=n_threads,
                                              statistic=stat,
                                              seqs_regions2=seqs_regions2)
        if stat == 'pvalue':
            ylabel2 = '-log10(p-value)'
        else:
            series_values = series_p
            ylims = ylims_p
            if ylabel is None:
                ylabel = ''
            ylabel = '{} z-score'.format(ylabel)

    negative_xticks = []
    if xs is None:
        xs = []
        for label, series in zip(regions_labels, series_values):
            negative_xtick = False
            x = range(1, series[0].shape[0] + 1)
            if label.split('.')[1] == 'r':
                x = [-i for i in x][::-1]
                negative_xtick = True
            xs.append(x)
            negative_xticks.append(negative_xtick)

    plot_regions_series(xs, series_values, regions_labels, seq_groups,
                        out_fhand, figsize=figsize, ylabel=ylabel,
                        ylims=ylims, xticks_size=xticks_size,
                        negative_ticks=negative_xticks,
                        ys_series2=series_p, ylims2=ylims_p, ylabel2=ylabel2)


def plot_bp_centered_profiles(profiles, seq_names, regions_labels,
                              grouping, out_fhand, bp_dists, figsize=None,
                              xs=None, len_left=120, len_right=60,
                              ylabel=None, ylims=None, xticks_size=20,
                              window_size=None, cumulative=False,
                              kword='intron.r'):
    new_profiles = []
    new_regions = []
    negative_xticks = []
    for profile, region in zip(profiles, regions_labels):
        if kword not in region:
            continue
        region = region.split('.')[0]
        bp_pos = profile.shape[1] - bp_dists[region]
        left, right = change_ref_coords_profile(profile, bp_pos, len_left,
                                                len_right)
        new_profiles.extend([left, right])
        new_regions.extend(['{}_bp.r'.format(region),
                            '{}_bp.l'.format(region)])
        negative_xticks.extend([False, True])

    plot_regions_track_values(new_profiles, seq_names, new_regions,
                              grouping, out_fhand, figsize=figsize, xs=xs,
                              ylabel=ylabel, ylims=ylims, xticks_size=20,
                              window_size=window_size, cumulative=cumulative)


def plot_regions_pos_dependent_enrichment(pos_dependent_enrichments,
                                          plot_labels, fhand, plot_or=False):
    xs = []
    ys_series = []
    ys_series2 = []
    for enrichment in pos_dependent_enrichments:
        series_labels, x, pvalues, odds_ratios = enrichment
        xs.append(x)
        ys_series.append([-numpy.log10(p) for p in pvalues])
        ys_series2.append([numpy.log2(_or) for _or in odds_ratios])

    ylims1 = [numpy.min([numpy.min(y) for y in ys_series]),
              numpy.max([numpy.max(y) for y in ys_series])]
    ylims1 = [ylims1[0] - ylims1[0] * 0.1, ylims1[1] + 0.1 * ylims1[1]]
    ylims2 = [numpy.min([numpy.min(y) for y in ys_series2]),
              numpy.max([numpy.max(y) for y in ys_series2])]
    ylims2 = [ylims2[0] - ylims2[0] * 0.1, ylims2[1] + 0.1 * ylims2[1]]

    labels1 = []
    labels2 = []
    for label in series_labels:
        labels1.append('{} -log10(pvalue)'.format(label))
        labels2.append('{} log2(OR)'.format(label))

    negative_xticks = []
    new_xs = []
    for label, x in zip(plot_labels, xs):
        negative_xtick = False
        if label.split('.')[1] == 'r':
            x = [-i for i in x][::-1]
            negative_xtick = True
        new_xs.append(x)
        negative_xticks.append(negative_xtick)
    xs = new_xs

    kwargs = {}
    if plot_or:
        kwargs = {'ys_series2': ys_series2, 'ylabel2': 'log2(OR)',
                  'series_labels2': labels2, 'ylims2': ylims2}
    plot_regions_series(xs, ys_series, plot_labels, labels1, fhand,
                        ylabel='-log10(pvalue)', ylims=ylims1, xticks_size=10,
                        negative_ticks=negative_xticks, **kwargs)


def plot_distance_to_ss_bloxplots(distance_df, grouping_df, fhand,
                                  title=None, ylabel=None):
    df = distance_df.copy()
    group_col = list(grouping_df.columns)[0]
    df[group_col] = grouping_df.loc[df.index, group_col]

    fig, canvas = get_new_figure_and_canvas(figsize=(20, 7))
    axes = fig.add_subplot(111)
    melted_df = pandas.melt(df, id_vars=group_col)
    melted_df.columns = [group_col, 'Regions', 'Distance to splice site']
    axes = seaborn.boxplot(x='Regions', y='Distance to splice site',
                           hue=group_col, data=melted_df, ax=axes,
                           showfliers=False)
    if title is not None:
        axes.set_title(title)
    if ylabel is not None:
        axes.set_ylabel(ylabel)
    canvas.print_figure(fhand)
    fhand.flush()


def plot_motif_ss_score(profiles_dfs, regions, ss_scores_df, grouping_df,
                        splice_site, fhand):
    figsize = (5 * len(profiles_dfs), 6)
    fig, canvas = get_new_figure_and_canvas(figsize)
    gs = gridspec.GridSpec(1, len(profiles_dfs))

    group_col = list(grouping_df.columns)[0]
    for i, (region, profile_df) in enumerate(zip(regions, profiles_dfs)):
        common_ids = numpy.intersect1d(profile_df.index, ss_scores_df.index)
        common_ids = numpy.intersect1d(common_ids, grouping_df.index)
        presence = profile_df.loc[common_ids, :].sum(axis=1) > 0
        presence_df = DataFrame({'RBP': presence}, index=common_ids)

        df = pandas.concat([presence_df,
                            ss_scores_df.loc[common_ids, splice_site],
                            grouping_df.loc[common_ids, :]], axis=1)
        axes = fig.add_subplot(gs[0, i])
        axes = seaborn.boxplot(x=('RBP'), y=splice_site, hue=group_col,
                               data=df, ax=axes, showfliers=False)
        axes.set_title(region)
    canvas.print_figure(fhand)
    fhand.flush()


def plot_motif_conservation(motif_cons_dfs, regions, grouping_df, fhand):
    figsize = (5 * len(motif_cons_dfs), 6)
    fig, canvas = get_new_figure_and_canvas(figsize)
    gs = gridspec.GridSpec(1, len(motif_cons_dfs))

    for i, (region, motif_cons_df) in enumerate(zip(regions, motif_cons_dfs)):
        common_ids = numpy.intersect1d(motif_cons_df.index, grouping_df.index)
        df = DataFrame({'Motif': motif_cons_df.loc[common_ids, 'Motif'],
                        'No motif': motif_cons_df.loc[common_ids, 'No motif'],
                        'Group': grouping_df.ix[common_ids, 0]},
                       index=common_ids)
        melted_df = pandas.melt(df, id_vars='Group')
        melted_df.columns = ['Group', 'RBP', 'Mean cons']
        axes = fig.add_subplot(gs[0, i])
        axes = seaborn.violinplot(x='Group', y='Mean cons', hue='RBP',
                                  data=melted_df, ax=axes, split=True,
                                  inner='quartile')
        axes.set_title(region)
    canvas.print_figure(fhand)
    fhand.flush()


def _update_ppt_feat_names(df):
    map_names = {'agez': 'AG exclusion zone', 'bp_scr': 'Branch poing score',
                 'ppt_len': 'PPT length', 'ppt_scr': 'PPT score',
                 'ppt_off': 'PPT offset relative to the BP adenine',
                 'ss_dist': 'Distance of BP to splice site',
                 'y_cont': 'Pyrimidin content between BP and 3\'SS'}
    return [map_names.get(colname, colname) for colname in df.columns]


def plot_ppt_features(features_dfs, grouping_df, out_fhand,
                      motif_profile=None, showfliers=False):
    features_dfs_list = []
    for region, features_df in features_dfs.items():
        features_df['Intron'] = region.split('_')[0]
        features_df['Group'] = grouping_df.ix[features_df.index, 0]
        if 'bp_seq' in features_df.columns:
            features_df.drop('bp_seq', inplace=True, axis=1)
        features_df.columns = _update_ppt_feat_names(features_df)
        if motif_profile is not None:
            present = motif_profile.sum(axis=1) > 0
            features_df['RBP'] = present
        features_dfs_list.append(features_df)
    features_df = pandas.concat(features_dfs_list, axis=0)

    features = [x for x in features_df.columns
                if x not in ['Intron', 'Group', 'RBP']]
    n_feat = len(features)
    fig, canvas = get_new_figure_and_canvas(figsize=(n_feat * 6, 7))

    gs = gridspec.GridSpec(1, n_feat)
    for i, feat in enumerate(features):
        axes = fig.add_subplot(gs[0, i])
        if motif_profile is not None:
            axes = seaborn.violinplot(x='Group', y=feat, ax=axes, hue='RBP',
                                      data=features_df, split=True,
                                      inner='quartile')
        else:
            axes = seaborn.boxplot(x='Intron', y=feat, ax=axes, hue='Group',
                                   data=features_df, showfliers=showfliers)
        axes.set_title(feat)
    canvas.print_figure(out_fhand)
    out_fhand.close()


def plot_functional_enrichment(df, title, out_fhand, fdr_threshold=0.01,
                               figsize=(30, 7), sel_top=None,
                               estimate='OR', take_estimate_log2=True):
    fig, canvas = get_new_figure_and_canvas(figsize)

    df['Gene set'] = df.index
    if 'FDR' not in df.columns:
        df['FDR'] = padjust(df['pvalue'])
    df.sort(columns=['FDR'], ascending=True, inplace=True)
    sel_rows = numpy.array(df['FDR'] < fdr_threshold)
    df = df.iloc[sel_rows, :]
    if df.shape[0] == 0:
        return

    pvalues = numpy.array(df['FDR'])
    pvalues[pvalues == 0] = numpy.min(pvalues[pvalues > 0])
    df['1/FDR'] = 1 / pvalues

    if sel_top is not None:
        df = df.iloc[:sel_top, :]
    axes = fig.add_subplot(142)
    axes = seaborn.barplot(y='Gene set', x='1/FDR', estimator=lambda x: x,
                           data=df, orient='horizontal', ax=axes,
                           order=df.index)
    axes.set_xscale('log')
    axes.set_title(title + ' FDR')
    axes.set_xlabel('1 / FDR')

    axes = fig.add_subplot(144)
    xfield = estimate
    if take_estimate_log2:
        df['log2({})'.format(estimate)] = numpy.log2(df[estimate])
        xfield = 'log2({})'.format(estimate)
    axes = seaborn.barplot(y='Gene set', x=xfield, estimator=lambda x: x,
                           data=df, orient='horizontal', ax=axes,
                           order=df.index)
    axes.set_title(title + ' {}'.format(estimate))
    axes.set_xlabel(xfield)

    canvas.print_figure(out_fhand)


def qqplot(x, y, axes, n=100):
    percentiles = numpy.linspace(0, 100, n)
    x_perc = numpy.percentile(x, percentiles)
    y_perc = numpy.percentile(y, percentiles)
    df = DataFrame({'x': x_perc, 'y': y_perc})
    axes = seaborn.regplot(x='x', y='y', data=df, ax=axes, scatter=True,
                           reg_fit=False)
    return axes


def qqqplot(x, y, z, axes3d, n=1000, add_expected=False, label=None):
    if label is None:
        label = 'Observed'
    percentiles = numpy.linspace(0, 100, n)
    x_perc = numpy.percentile(x, percentiles)
    y_perc = numpy.percentile(y, percentiles)
    z_perc = numpy.percentile(z, percentiles)
    axes3d.scatter(x_perc, y_perc, z_perc, label=label)
    if add_expected:
        exp = numpy.linspace(0, numpy.max(numpy.stack([z_perc, y_perc, x_perc])), n)
        axes3d.plot(exp, exp, exp, label='Expected')
    return axes3d
