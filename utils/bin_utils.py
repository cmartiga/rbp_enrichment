
import argparse
from utils.settings import (EVENTS_TYPES, EX, BED_FORMAT, MATS_FORMAT,
                            VAST_FORMAT, EXON_WINDOW_SIZE, INTRON_WINDOW_SIZE,
                            ALT_WINDOW_SIZE)


def init_parser(description):
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    options_group = parser.add_argument_group('Options')
    output_group = parser.add_argument_group('Output')

    return parser, input_group, options_group, output_group


def add_input_arg(input_group, help_msg):
    input_group.add_argument('input', help=help_msg)


def add_read_events(input_group):
    help_msg = 'File containing alternative splicing events'
    add_input_arg(input_group, help_msg)
    help_msg = 'Input file format: {} (def), {} or {} supported'
    input_group.add_argument('-f', '--format', default=MATS_FORMAT,
                             help=help_msg.format(MATS_FORMAT, VAST_FORMAT,
                                                  BED_FORMAT))
    help_msg = 'Type of event to select {}'.format(EVENTS_TYPES)
    input_group.add_argument('-e', '--sel_event_type', default=EX,
                             help=help_msg)


def add_grouping_arg(input_group, required=True):
    help_msg = 'Tab separated file with additional covariates. The first '
    help_msg += ' column should contain the information about grouping'
    input_group.add_argument('-g', '--groups_fpath', required=required,
                             help=help_msg)


def add_windows_options(options_group):
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    options_group.add_argument('-ew', '--exon_window_size', type=int,
                               default=EXON_WINDOW_SIZE, help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    options_group.add_argument('-iw', '--intron_window_size', type=int,
                               default=INTRON_WINDOW_SIZE, help=help_msg)
    help_msg = 'Other window size({})'.format(ALT_WINDOW_SIZE)
    options_group.add_argument('-aw', '--alternative_window_size', type=int,
                               default=ALT_WINDOW_SIZE, help=help_msg)


def add_output_arg(output_group, output_type='fpath', required=True):
    output_group.add_argument('-o', '--output', required=required,
                              help='Output {}'.format(output_type))
