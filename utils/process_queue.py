#!/usr/bin/env python

from subprocess import Popen
from tempfile import NamedTemporaryFile


def launch_queued_processes(cmds, n_simultaneous=1, stdout=None,
                            verbose=False):
    if stdout is None:
        stdout = NamedTemporaryFile()
    running = []
    while len(cmds) > 0:
        cmd = cmds.pop(0)
        if verbose:
            print ' '.join(cmd)
        process = Popen(cmd, stdout=stdout)
        running.append(process)
        if len(running) == n_simultaneous or len(cmds) == 0:
            process = running.pop(0)
            process.wait()
