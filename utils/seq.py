#!/usr/bin/env python

import numpy


def fasta_to_numpy(seqs):
    names = []
    new_seqs = []
    seq_len = None
    _dtype = float
    for name, seq in seqs:
        if seq_len is not None and seq_len != len(seq):
            msg = 'All seqs must have the same length to store them in a'
            msg += ' matrix'
            raise ValueError(msg)
        names.append(name)
        if isinstance(seq, str):
            seq = [seq[i] for i in range(len(seq))]
            _dtype = str
        new_seqs.append(seq)
    return numpy.array(names), numpy.array(new_seqs, dtype=_dtype)


def just_seqs(seqs, length, just_right=False, fill_empty=True,
              fill_char='-'):
    for name, seq in seqs:
        if just_right:
            seq = seq[-length:]
            fill = fill_char * (length - len(seq))
            seq = fill + seq if fill_empty else seq
        else:
            seq = seq[:length]
            fill = fill_char * (length - len(seq))
            seq = seq + fill if fill_empty else seq
        yield name, seq
