#!/usr/bin/env python

from scipy.stats.stats import pearsonr

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import seaborn as sns


def create_patches_legend(axes, colors_dict, loc=1, kwargs={}):
    axes.legend(handles=[mpatches.Patch(color=color, label=label)
                         for label, color in sorted(colors_dict.items())],
                loc=loc, **kwargs)


def _make_2d_plot(x, y, df, cmap, axes, add_scatter, sample,
                  xlim=None, ylim=None, sample_size=20000):
    color = cmap.lower()[:-1]

    # Filter values out of lims
    if xlim is not None:
        df = df.loc[np.logical_and(df[x] >= xlim[0], df[x] <= xlim[1]), :]
    if ylim is not None:
        df = df.loc[np.logical_and(df[y] >= ylim[0], df[y] <= ylim[1]), :]

    # Sample big datasets to speed up kde computation
    if df.shape[0] == 0:
        return axes
    elif df.shape[0] > sample_size and sample and y is not None:
        sel_rows = np.random.choice(df.shape[0], sample_size)
        df = df.iloc[sel_rows, :]
    xs = df[x]

    if y is None:
        axes = sns.kdeplot(xs, ax=axes, color=color, shade=True)
        axes.set_yticks([])
        axes.legend_.set_visible(False)
    else:
        ys = df[y]
        if add_scatter:
            axes.scatter(xs, ys, alpha=0.1, color=color, s=4)
        axes = sns.kdeplot(xs, ys, shade=False, ax=axes, cmap=cmap)
    return axes


def make_2d_plot_axes(data, x, y, axes, hue=None, xlim=None, ylim=None,
                      add_scatter=True, sample=True,
                      cmaps=['Reds', 'Blues', 'Greens', 'Greys'],
                      xlabel=None, ylabel=None, hue_order=None, legend_loc=1,
                      add_cor=False, cor_func=pearsonr, sample_size=20000):
    # Selected columns
    sel_cols = [x]
    if y is not None:
        sel_cols.append(y)
    if hue is not None:
        sel_cols.append(hue)

    tmp = data[sel_cols].replace([np.inf, -np.inf], np.nan).dropna().copy()

    if tmp.shape[0] == 0:
        return axes

    if hue is None:
        axes = _make_2d_plot(x, y, tmp, cmap=cmaps[0], axes=axes,
                             add_scatter=add_scatter, sample=sample,
                             xlim=xlim, ylim=ylim, sample_size=sample_size)
        if add_cor and y is not None:
            rho, p = cor_func(tmp[x], tmp[y])
            label = r'$r={:.2f}\ p={:.2f}$'.format(rho, p)
            axes.text(axes.get_xlim()[0] + 0.1 * abs(axes.get_xlim()[0]),
                      0.9 * axes.get_ylim()[1], label)
    else:
        if hue_order is None:
            hue_order = tmp[hue].unique()

        colors_dict = {}
        for hue_value, cmap in zip(hue_order, cmaps):
            sel_df = tmp.loc[tmp[hue] == hue_value, :].copy()

            axes = _make_2d_plot(x, y, sel_df, cmap=cmap, axes=axes,
                                 add_scatter=add_scatter, sample=sample,
                                 xlim=xlim, ylim=ylim, sample_size=sample_size)
            if add_cor and y is not None:
                rho, p = cor_func(sel_df[x], sel_df[y])
                label = '{} r={:.2f} p={:.2f}'.format(hue_value, rho, p)
            else:
                label = hue_value
            colors_dict[label] = cmap.lower()[:-1]

        create_patches_legend(axes, colors_dict, loc=legend_loc)

    # Add additional arguments if required
    if ylabel is not None:
        axes.set_ylabel(ylabel)

    if xlabel is not None:
        axes.set_xlabel(xlabel)

    if ylim is not None:
        axes.set_ylim(ylim)

    if xlim is not None:
        axes.set_xlim(xlim)

    sns.despine(ax=axes)
    return axes


def savefig(fig, fpath, fmt='png'):
    fig.tight_layout()
    fig.savefig(fpath, format=fmt, dpi=180)
    plt.close()


def make_2d_plot(data, x, y, fpath, hue=None, xlim=None, ylim=None,
                 axes_size=(4, 3.5), add_scatter=True, sample=True,
                 cmaps=['Reds', 'Blues', 'Greens', 'Greys'],
                 xlabel=None, ylabel=None, hue_order=None, legend_loc=1,
                 add_cor=False, cor_func=pearsonr,
                 col_var=None, row_var=None, sharex=True, sharey=True,
                 sample_size=20000):

    if col_var is None:
        col_var = 'None'
        col_values = ['']
        n_col = 1
        data[col_var] = ''
    else:
        col_values = data[col_var].unique()
        n_col = col_values.shape[0]

    if row_var is None:
        row_values = ['']
        n_row = 1
        row_var = 'None'
        data[row_var] = ''
    else:
        row_values = data[row_var].unique()
        n_row = row_values.shape[0]

    sns.set_style('white')
    figsize = (n_col * axes_size[0], n_row * axes_size[1])
    fig, subplots = plt.subplots(n_row, n_col, figsize=figsize,
                                 sharex=sharex, sharey=sharey)

    if row_var == 'None':
        subplots = [subplots]

    if col_var == 'None':
        subplots = [[axes] for axes in subplots]

    for i, row_value in enumerate(row_values):
        row_data = data.loc[data[row_var] == row_value, :]
        row_axes = subplots[i]

        for j, col_value in enumerate(col_values):
            axes_data = row_data.loc[row_data[col_var] == col_value, :].copy()
            axes = row_axes[j]

            if axes_data.shape < 20:
                continue

            axes = make_2d_plot_axes(axes_data, x, y, axes, hue=hue, xlim=xlim,
                                     ylim=ylim, add_scatter=add_scatter,
                                     sample=sample, cmaps=cmaps, xlabel=xlabel,
                                     ylabel=ylabel, hue_order=hue_order,
                                     legend_loc=legend_loc, add_cor=add_cor,
                                     cor_func=pearsonr,
                                     sample_size=sample_size)
            if row_var != 'None':
                if col_var != 'None':
                    axes.set_title('{} - {}'.format(row_value, col_value))
                else:
                    axes.set_title('{}'.format(row_value))
            elif col_var != 'None':
                axes.set_title('{}'.format(col_value))
            if i < n_row - 1:
                axes.set_xlabel('')
            if j != 0:
                axes.set_ylabel('')

    savefig(fig, fpath)


def make_boxplot_axes(data, x, y, axes, hue=None, xorder=None, ylim=None,
                      palette=None, xlabel=None, ylabel=None, hue_order=None,
                      legend_loc=1):
    # Selected columns
    sel_cols = [x]
    if y is not None:
        sel_cols.append(y)
    if hue is not None:
        sel_cols.append(hue)

    tmp = data[sel_cols].replace([np.inf, -np.inf], np.nan).dropna().copy()

    if tmp.shape[0] == 0:
        return axes

    axes = sns.boxplot(x, y, hue, data=data, order=xorder, showfliers=False,
                       palette=palette, hue_order=hue_order, ax=axes)
    if hue is not None:
        axes.legend(loc=legend_loc)
    elif axes.legend_ is not None:
        axes.legend_.set_visible(False)

    # Add additional arguments if required
    if ylabel is not None:
        axes.set_ylabel(ylabel)

    if xlabel is not None:
        axes.set_xlabel(xlabel)

    if ylim is not None:
        axes.set_ylim(ylim)

    return axes


def make_boxplot(data, x, y, fpath, hue=None, ylim=None, xorder=None,
                 axes_size=(4, 3.5), palette=None,
                 xlabel=None, ylabel=None, hue_order=None, legend_loc=1,
                 col_var=None, row_var=None, sharex=True, sharey=True):

    if col_var is None:
        col_var = 'None'
        col_values = ['']
        n_col = 1
        data[col_var] = ''
    else:
        col_values = data[col_var].unique()
        n_col = col_values.shape[0]

    if row_var is None:
        row_values = ['']
        n_row = 1
        row_var = 'None'
        data[row_var] = ''
    else:
        row_values = data[row_var].unique()
        n_row = row_values.shape[0]

    sns.set_style('whitegrid')
    figsize = (n_col * axes_size[0], n_row * axes_size[1])
    fig, subplots = plt.subplots(n_row, n_col, figsize=figsize,
                                 sharex=sharex, sharey=sharey)

    if n_row == 1:
        subplots = [subplots]

    if n_col == 1:
        subplots = [[axes] for axes in subplots]

    for i, row_value in enumerate(row_values):
        row_data = data.loc[data[row_var] == row_value, :]
        row_axes = subplots[i]

        for j, col_value in enumerate(col_values):
            axes_data = row_data.loc[row_data[col_var] == col_value, :].copy()
            axes = row_axes[j]

            axes = make_boxplot_axes(axes_data, x, y, axes, hue=hue,
                                     xorder=xorder, ylim=ylim, palette=palette,
                                     xlabel=xlabel, ylabel=ylabel,
                                     hue_order=hue_order,
                                     legend_loc=legend_loc)
            if row_var != 'None':
                if col_var != 'None':
                    axes.set_title('{} - {}'.format(row_value, col_value))
                else:
                    axes.set_title('{}'.format(row_value))
            elif col_var != 'None':
                axes.set_title('{}'.format(col_value))
            if i < n_row - 1:
                axes.set_xlabel('')
            if j != 0:
                axes.set_ylabel('')

    savefig(fig, fpath)


def repel_labels(ax, x, y, labels, k=0.01, color='black', fontsize=8):
    G = nx.DiGraph()
    data_nodes = []
    init_pos = {}
    for xi, yi, label in zip(x, y, labels):
        data_str = 'data_{0}'.format(label)
        G.add_node(data_str)
        G.add_node(label)
        G.add_edge(label, data_str)
        data_nodes.append(data_str)
        init_pos[data_str] = (xi, yi)
        init_pos[label] = (xi, yi)

    pos = nx.spring_layout(G, pos=init_pos, fixed=data_nodes, k=k)

    # undo spring_layout's rescaling
    pos_after = np.vstack([pos[d] for d in data_nodes])
    pos_before = np.vstack([init_pos[d] for d in data_nodes])
    scale, shift_x = np.polyfit(pos_after[:, 0], pos_before[:, 0], 1)
    scale, shift_y = np.polyfit(pos_after[:, 1], pos_before[:, 1], 1)
    shift = np.array([shift_x, shift_y])
    for key, val in pos.iteritems():
        pos[key] = (val * scale) + shift

    for label, data_str in G.edges():
        ax.annotate(label,
                    xy=pos[data_str], xycoords='data',
                    xytext=pos[label], textcoords='data',
                    arrowprops=dict(arrowstyle="->",
                                    shrinkA=0, shrinkB=0,
                                    connectionstyle="arc3",
                                    color='red'),
                    fontsize=fontsize, color=color)
