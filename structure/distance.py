# !/usr/bin/env python

from itertools import combinations

import RNA
from scipy.cluster.hierarchy import linkage
from scipy.spatial.distance import squareform


def calc_pairwise_dist(structures):
    for strc1, strc2 in combinations(structures.items(), 2):
        strc1_name, strc1 = strc1
        strc2_name, strc2 = strc2
        distance = RNA.hamming_distance(strc1, strc2)
        yield strc1_name, strc2_name, distance


def perform_hc(distance_matrix):
    if len(distance_matrix.shape) != 1:
        distance_matrix = squareform(distance_matrix)
    hc = linkage(distance_matrix)
    return hc
