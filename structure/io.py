# !/usr/bin/env python
from itertools import groupby, product
import os
import sys

import numpy
from pandas import DataFrame
from scipy.spatial.distance import squareform

from motif.matrix import write_matrix
from seq.parsers import read_fasta_file


def _get_fasta_records(lines):
    name = None
    group_lines = []
    for line in lines:
        if line == '\n':
            continue
        if line.startswith('>'):
            if group_lines and name is not None:
                yield name, group_lines
            name = line[1:].strip()
            group_lines = []
        else:
            group_lines.append(line.strip())
    if group_lines and name is not None:
        yield name, group_lines


def parse_capr_output(fhand, check_len=True):
    for name, lines in _get_fasta_records(fhand):
        if name.endswith('length : 1'):
            name = name[:-10]
        chrom, start, end, strand = name.split()
        start, end = int(start), int(end)
        profile = {}
        for line in lines:
            items = line.strip().split()
            profile[items[0]] = [float(x) for x in items[1:] if x != '-1']
            profile_len = len(profile[items[0]])
            if profile_len != end - start:
                msg = 'Seq length does not match profile length {}: {} vs {}'
                msg = msg.format(name, end - start, profile_len)
                if check_len:
                    raise ValueError(msg)
                else:
                    sys.stderr.write(msg)
        yield {'chrom': chrom, 'start': start, 'end': end,
               'strand': strand, 'profile': profile}


def _capr_output_to_bedgraph(parsed_capr):
    for capr_record in parsed_capr:
        chrom, start = capr_record['chrom'], capr_record['start']
        end, strand = capr_record['end'], capr_record['strand']
        profiles = capr_record['profile']
        positions = range(start, end)
        bg_lines = {}
        for struc_type, profile in profiles.items():
            if strand == '-':
                profile = profile[::-1]
            for pos, value in zip(positions, profile):
                line = '{}\t{}\t{}\t{}'.format(chrom, pos, pos + 1, value)
                try:
                    bg_lines[struc_type].append(line)
                except KeyError:
                    bg_lines[struc_type] = [line]
        yield bg_lines


def _group_continuous_records(in_records):
    '''Records must be of the same strand'''
    prev_record = None
    records = []
    for record in in_records:
        if prev_record is None:
            records.append(record)
            prev_record = record
            continue
        assert record['strand'] == prev_record['strand']
        if (record['chrom'] == prev_record['chrom'] and
                record['start'] == prev_record['end']):
            records.append(record)
        else:
            yield records
            records = [record]
            prev_record = record
    if records:
        yield records


def _merge_records(records):
    if len(records) == 1:
        return records[0]
    chrom = records[0]['chrom']
    start = records[0]['start']
    end = records[-1]['end']
    strand = records[0]['strand']
    profiles = None
    for record in records:
        assert record['strand'] == strand
        if profiles is None:
            profiles = record['profile']
        else:
            for key, value in record['profile'].items():
                profiles[key].extend(value)
    return {'start': start, 'end': end, 'chrom': chrom, 'strand': strand,
            'profile': profiles}


def _capr_output_to_wig(parsed_capr):
    '''It requires the result to be sorted by genome coordinates'''
    for _, capr_records in groupby(parsed_capr, key=lambda x: x['chrom']):
        for grouped_records in _group_continuous_records(capr_records):
            record = _merge_records(grouped_records)
            line = 'fixedStep chrom={} start={} step=1'.format(record['chrom'],
                                                               record['start'])
            res = {}
            for key, value in record['profile'].items():
                values = [str(x) for x in value]
                if record['strand'] == '-':
                    values = values[::-1]
                res[key] = [line] + values
            yield res


def write_capr(capr_out_fhand, out_prefix, file_format='bedgraph',
               check_len=True):
    parsed_capr = parse_capr_output(capr_out_fhand, check_len=check_len)
    if file_format == 'bedgraph':
        lines = _capr_output_to_bedgraph(parsed_capr)
        suffix = 'bg'
    elif file_format == 'wig':
        lines = _capr_output_to_wig(parsed_capr)
        suffix = 'wig'
    fhands = {}
    for record in lines:
        for strc_type, profile in record.items():
            for line in profile:
                try:
                    fhands[strc_type].write(line + '\n')
                except KeyError:
                    fpath = '{}.{}.{}'.format(out_prefix, strc_type, suffix)
                    fhands[strc_type] = open(fpath, 'w')
                    fhands[strc_type].write(line + '\n')
    for fhand in fhands.values():
        fhand.close()


def read_rnaz(fhand):
    is_header = False
    record = {}
    line = None
    for line in fhand:
        if line == '\n':
            continue
        if '#' in line:
            is_header = not is_header
            continue
        if is_header:
            key, value = line.strip().split(':')
            value = value.strip()
            try:
                value = float(value)
            except ValueError:
                pass
            record[key] = value
        else:
            break
    if not line:
        return None
    fhand = [line] + list(fhand)
    strcs = {name: seq.split(' ')[0] for name, seq in read_fasta_file(fhand)}
    return record, strcs


def parse_rnaz_files(dirname):
    for fname in os.listdir(dirname):
        items = fname.split('.')
        if items[-1] != 'rnaz':
            continue
        event_id = items[0]
        region = '.'.join(items[1:-1])
        fhand = open(os.path.join(dirname, fname))
        record = read_rnaz(fhand)
        if record is None:
            continue
        yield event_id, region, record
        fhand.close()


def rnaz_data_to_df(parsed_rnaz):
    records = []
    for event_id, region, record in parsed_rnaz:
        record = record[0]
        record['event_id'] = event_id
        record['region'] = region
        records.append(record)

    return DataFrame.from_records(records)


def read_pairwise_distance(fhand, header=True):
    if header:
        try:
            fhand.next()
        except StopIteration:
            pass

    for line in fhand:
        id1, id2, distance = line.strip().split()
        yield id1, id2, int(distance)


def pairwise_distances_to_matrix2(pairwise_distances):
    idx = {}
    print 'reading'
    prev_id = None
    i = 0
    for id1, id2, distance in pairwise_distances:
        if prev_id is None or id1 != prev_id:
            i += 1
            print id1, i
            prev_id = id1
        try:
            idx[id1][id2] = distance
        except KeyError:
            idx[id1] = {id2: distance}

    print 'done reading'
    ids = idx.keys()
    matrix = []
    row_distances = []
    prev_id1 = None
    for id1, id2 in product(ids, 2):
        if id1 == id2:
            distance = 0
        else:
            try:
                distance = idx[id1][id2]
            except KeyError:
                distance = idx[id2][id1]
        if prev_id1 is None or prev_id1 == id1:
            row_distances.append(distance)
        else:
            matrix.append(row_distances)
            row_distances = [distance]
        prev_id1 = id1
    if row_distances:
        matrix.append(row_distances)
    matrix = numpy.array(matrix)
    df = DataFrame(matrix, index=ids, columns=ids)
    return df


def pairwise_distances_to_matrix(pairwise_distances):
    distances = []
    ids = []
    sel_ids = set()
    for id1, id2, distance in pairwise_distances:
        distances.append(distance)
        if id1 not in sel_ids:
            sel_ids.add(id1)
            ids.append(id1)
    ids.append(id2)
    return DataFrame(squareform(distances), index=ids, columns=ids,
                     dtype=int)
