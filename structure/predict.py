# !/usr/bin/env python

import random
from subprocess import check_call
from tempfile import NamedTemporaryFile

import zscore
import RNA
import numpy

from utils.settings import CAPR


def calc_rnafold(seq):
    return RNA.fold(seq.replace('T', 'U'))


def _calc_mfe_zscore(seq, mfe, n_shuffle=100):
    seq = list(seq)
    mfes = []
    for _ in xrange(n_shuffle):
        random.shuffle(seq)
        mfes.append(calc_rnafold(''.join(seq))[1])
    return (mfe - numpy.mean(mfes)) / numpy.std(mfes)


def calc_rnazfold_zscore(seq, mfe=None, shuffle=False, use_mononc_freq=False,
                         use_python=False, n_shuffle=100):
    if shuffle:
        if use_mononc_freq:
            calc_type = 1
        else:
            calc_type = 3
    else:
        if use_mononc_freq:
            calc_type = 0
        else:
            calc_type = 2

    if mfe is None:
        mfe = 1
    if use_python:
        mfe = calc_rnafold(seq)[1]
        return _calc_mfe_zscore(seq, mfe, n_shuffle=n_shuffle)
    else:
        return zscore.calc_mfe_zscore(seq, mfe, calc_type)


def predict_capr(genome_intervals, ref_fastafile, out_fhand, max_span=200):
    fasta_fhand = NamedTemporaryFile('w', suffix='.fasta')
    lengths_fhand = NamedTemporaryFile('w', suffix='.len')
    pos_fhand = NamedTemporaryFile('w', suffix='.pos')
    seq_lengths = []
    for interval in genome_intervals:
        name = '{} {} {} {}'.format(interval.chrom, interval.start,
                                    interval.end, interval.strand)
        seq = interval.get_sequence_from_fastafile(ref_fastafile)
        if len(seq) == 0:
            continue
        seq_lengths.append(len(seq))

        fasta_fhand.write('>{}\n{}\n'.format(name, seq))

    lengths_fhand.write('1\n')
    lengths_fhand.flush()
    fasta_fhand.flush()
    positions = range(1, max(seq_lengths) + 1)
    pos_fhand.write(' '.join([str(position) for position in positions]) + '\n')
    pos_fhand.flush()

    cmd = [CAPR, fasta_fhand.name, out_fhand.name, str(max_span),
           lengths_fhand.name, pos_fhand.name]
    check_call(cmd)
