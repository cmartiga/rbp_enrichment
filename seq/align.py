#!/usr/bin/env python

from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile

from seq.parsers import read_fasta_file
from utils.settings import CLUSTALO


def clustalo(seqs):
    fhand = NamedTemporaryFile()
    for i, seq in enumerate(seqs):
        fhand.write('>seq{}\n{}\n'.format(i, seq))
    fhand.flush()

    cmd = [CLUSTALO, '-i', '-']
    p = Popen(cmd, stdout=PIPE, stdin=open(fhand.name))
    return [seq for _, seq in read_fasta_file(p.stdout)]
