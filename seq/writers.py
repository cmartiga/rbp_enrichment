#!/usr/bin/env python
import numpy


def write_fasta_file(seqs, fhand):
    for name, seq in seqs:
        fhand.write('>{}\n{}\n'.format(name, seq))
    fhand.flush()


def write_splicing_events(splicing_events, out_prefix, only_id=False):
    out_fhands = {}
    tracks_fhands = {}
    for event in splicing_events:
        if not out_fhands:
            for region_name, region in event.regions.items():
                if region.seq:
                    out_fpath = out_prefix + '.' + region_name + '.fasta'
                    out_fhands[region_name] = open(out_fpath, 'w')

        if not tracks_fhands:
            for region_name, region in event.regions.items():
                track_names = region.tracks.keys() + region.annotations.keys()
                for track_name in track_names:
                    track_fpath = '.'.join([out_prefix, region_name,
                                            track_name])
                    tracks_fhand = open(track_fpath, 'w')
                    try:
                        tracks_fhands[region_name][track_name] = tracks_fhand
                    except KeyError:
                        tracks_fhands[region_name] = {track_name: tracks_fhand}

        for region_name, region in event.regions.items():
            if only_id:
                name = '>{}\n'.format(event.id)
            else:
                name = '>{}_{}|{}\n'.format(event.gene_id, event.id,
                                            region_name)
            if region.seq:
                seq = region.seq + '\n'
                out_fhands[region_name].write(name + seq)
            tracks = region.tracks.items() + region.annotations.items()
            for track_name, track_values in tracks:
                track_mean = str(region.tracks_mean[track_name][0])
                name = name.strip() + ' mean=' + track_mean + '\n'
                track_values = ' '.join([str(x) for x in track_values]) + '\n'
                line = name + track_values
                tracks_fhands[region_name][track_name].write(line)

    for out_fhand in out_fhands.values() + tracks_fhands.values():
        try:
            out_fhand.flush()
        except AttributeError:
            for fhand in out_fhand.values():
                fhand.flush()


def write_profiles(profiles, out_prefix, sep='\t', unique=True):
    fhands = {}
    event_ids = set()
    for event_id, event_profiles in profiles:
        if unique and event_id in event_ids:
            continue
        event_ids.add(event_id)
        for region_name, profile in event_profiles.items():
            if region_name not in fhands:
                fpath = '{}.{}'.format(out_prefix, region_name)
                fhands[region_name] = open(fpath, 'w')
                items = ['EventId'] + [str(x) for x in range(profile.shape[0])]
                fhands[region_name].write(sep.join(items) + '\n')
            items = [event_id] + [str(x) if not numpy.isnan(x) else ''
                                  for x in profile]
            fhands[region_name].write(sep.join(items) + '\n')
    for fhand in fhands.values():
        fhand.flush()
