#!/usr/bin/env python

import csv
import itertools
import random
from subprocess import check_call, PIPE, Popen
import sys
from tempfile import NamedTemporaryFile

from pandas import DataFrame

from seq.intervals import GenomeInterval
from seq.parsers import parsers, parse_gtf
from utils.misc import GENETIC_CODE, calc_aa_spectra
from utils.settings import (A_tag, C1_tag, C2_tag, I1_tag, I2_tag, I_tag, S,
                            C1, C2, C3, MIC, IR_S, IR_C, EX, INT, VAST_FORMAT,
                            INTRON_WINDOW_SIZE, EXON_WINDOW_SIZE, SCORE_5SS,
                            SCORE_3SS, SVM_BPFINDER, BED, ALT_WINDOW_SIZE,
                            BED_FORMAT, EVENT, ALTD, ALTA, ALT5, ALT3)


class SplicingEvent(object):
    def __init__(self, idx={}, liftover=None):
        self.regions = {}
        self.id = None
        for region_name, data in idx.items():
            interval = GenomeInterval(data['chrom'], data['start'],
                                      data['end'], data['strand'])
            interval._windows_taken = True
            self.chromosome = data['chrom']
            self.strand = data['strand']
            if 'id' in data:
                self.id = data['id']
            self.regions[region_name] = interval

    def take_windows(self, exon_window_size=EXON_WINDOW_SIZE,
                     intron_window_size=INTRON_WINDOW_SIZE,
                     alternative_window_size=200, force=False):
        self.exon_ws = exon_window_size
        self.intron_ws = intron_window_size
        new_regions = {}
        for region_name, region in self.regions.items():
            if not force and region._windows_taken:
                new_regions[region_name] = region
                continue
            if 'exon' in region_name:
                window_size = exon_window_size
            elif 'intron' in region_name:
                window_size = intron_window_size
            else:
                window_size = alternative_window_size
            left, right = region.take_ends(window_size=window_size)
            new_regions['{}.l.{}'.format(region_name, window_size)] = left
            new_regions['{}.r.{}'.format(region_name, window_size)] = right
        self.regions = new_regions

    def get_regions_sequences(self, ref_fastafile):
        for region in self.regions.values():
            region.get_sequence_from_fastafile(ref_fastafile)

    def get_bigwig_profile(self, bigwigfile, tag='cons_scores',
                           rev_bw_file=None):
        for region in self.regions.values():
            region.get_bigwig_profile(bigwigfile, tag,
                                      rev_bw_file=rev_bw_file)

    def get_annotation_values(self, annotation_idx, tag, annotation_types=[]):
        for region in self.regions.values():
            region.get_base_annotations(annotation_idx, tag, annotation_types)

    def find_tabix_intervals(self, tabix_index, use_start=True, sel_rbps=None,
                             cols_filters=None):
        for region in self.regions.values():
            region.find_tabix_intervals(tabix_index, use_start=use_start,
                                        sel_rbps=sel_rbps,
                                        cols_filters=cols_filters)

    def get_tabix_profile(self, tabix_index, sel_rbps, use_start=False,
                          cols_filters=None):
        for region in self.regions.values():
            region.find_tabix_intervals(tabix_index, use_start=use_start,
                                        sel_rbps=sel_rbps,
                                        cols_filters=cols_filters)
            for rbp in sel_rbps:
                region.get_tabix_profile(rbp)

    def get_tabix_counts(self, tabix_index, cols_filters=None):
        for region in self.regions.values():
            region.get_tabix_counts(tabix_index, cols_filters=cols_filters)

    def get_exp_het(self, vcf_tabix, only_snps=True, only_biallelic=True,
                    af_field='MAF', variant_type_field='TSA'):
        for region in self.regions.values():
            region.calc_exp_het(vcf_tabix, only_snps=only_snps,
                                only_biallelic=only_biallelic,
                                af_field=af_field,
                                variant_type_field=variant_type_field)


class ExonCassette(SplicingEvent):
    '''It assigns the corresponding sequences to the regions defined for an
    exon cassette taking into account the strand in which the gene is coded.
    It can work also in cases where the order of the exons is already given.
    In case the limits of the flaking exons are not defined, such as in
    vast-tools output, a exon window of given size will be used to fetch the
    sequence and conservation scores'''

    def __init__(self, parsed_exon, liftover=None,
                 intron_ws=INTRON_WINDOW_SIZE):
        self.chromosome = parsed_exon['chrom']
        self.gene_id = parsed_exon['gene_id']
        self.strand = parsed_exon['strand']
        self.pu_scores_process = None
        self.id = parsed_exon['id']
        self.A_phases = parsed_exon['phase']
        self.intron_ws = intron_ws

        A = GenomeInterval(self.chromosome, parsed_exon['start'],
                           parsed_exon['end'], self.strand, liftover)
        if 'up_exon_start' in parsed_exon and 'dw_exon_start' in parsed_exon:
            if self.strand == '+':
                C1 = GenomeInterval(self.chromosome,
                                    parsed_exon['up_exon_start'],
                                    parsed_exon['up_exon_end'], self.strand,
                                    liftover)
                C2 = GenomeInterval(self.chromosome,
                                    parsed_exon['dw_exon_start'],
                                    parsed_exon['dw_exon_end'], self.strand,
                                    liftover)
                if C1.start > C2.start:
                    msg = 'Invalid coordinates for negative strand: C1 > C2'
                    raise ValueError(msg)
                I1 = GenomeInterval(self.chromosome, C1.end,
                                    A.start, self.strand, liftover)
                I2 = GenomeInterval(self.chromosome, A.end,
                                    C2.start, self.strand, liftover)
            else:
                C1 = GenomeInterval(self.chromosome,
                                    parsed_exon['dw_exon_start'],
                                    parsed_exon['dw_exon_end'], self.strand,
                                    liftover)
                C2 = GenomeInterval(self.chromosome,
                                    parsed_exon['up_exon_start'],
                                    parsed_exon['up_exon_end'], self.strand,
                                    liftover)
                if C1.start < C2.start:
                    msg = 'Invalid coordinates for negative strand: C1 < C2'
                    raise ValueError(msg)
                I1 = GenomeInterval(self.chromosome, A.end,
                                    C1.start, self.strand, liftover)
                I2 = GenomeInterval(self.chromosome, C2.end,
                                    A.start, self.strand, liftover)
            regions = {A_tag: A, C1_tag: C1, C2_tag: C2, I1_tag: I1,
                       I2_tag: I2}
        else:
            I1 = GenomeInterval(self.chromosome, A.start - intron_ws,
                                A.start, self.strand, liftover)
            I1._windows_taken = True
            I2 = GenomeInterval(self.chromosome, A.end,
                                A.end + intron_ws, self.strand, liftover)
            I2._windows_taken = True
            if self.strand == '-':
                I1, I2 = I2, I1
            regions = {A_tag: A, '{}.r.{}'.format(I1_tag, intron_ws): I1,
                       '{}.l.{}'.format(I2_tag, intron_ws): I2}
        self.regions = regions
        self.features = {}

    def get_splice_sites_seqs(self, extra_length=0):
        if C1_tag in self.regions:
            up_5ss = self.regions[C1_tag].seq[-3 - extra_length:]
            up_5ss = up_5ss + self.regions[I1_tag].seq[:6 + extra_length]
            _3ss = self.regions[I1_tag].seq[-20 - extra_length:]
            _3ss += self.regions[A_tag].seq[:3 + extra_length]
            _5ss = self.regions[A_tag].seq[-3 - extra_length:]
            _5ss += self.regions[I2_tag].seq[:6 + extra_length]
            dw_3ss = self.regions[I2_tag].seq[-20 - extra_length:]
            dw_3ss = dw_3ss + self.regions[C2_tag].seq[:3 + extra_length]
            self.splice_sites = {'up_5ss': up_5ss, '5ss': _5ss, '3ss': _3ss,
                                 'down_3ss': dw_3ss}
        else:
            tag_i1 = '{}.r.{}'.format(I1_tag, self.intron_ws)
            _3ss = self.regions[tag_i1].seq[-20 - extra_length:]
            _3ss += self.regions[A_tag].seq[:3 + extra_length]
            tag_i2 = '{}.l.{}'.format(I2_tag, self.intron_ws)
            _5ss = self.regions[A_tag].seq[-3 - extra_length:]
            _5ss += self.regions[tag_i2].seq[:6 + extra_length]
            self.splice_sites = {'5ss': _5ss, '3ss': _3ss}

    @property
    def rfp(self):
        return self.regions[A_tag].length % 3 == 0

    def calc_aa_spectra(self, ref_fastafile=None, codon_table=GENETIC_CODE,
                        map_to_aa_properties=None,
                        shuffle=False, count_after_stop=True):
        '''Returns the aminoacid composition of the sequence. If no reading
           frame is given then all three reading frames will be examined and
           all potential encoded aa will be returned'''
        phases_to_rf = {0: 2, 1: 0, 2: 1, None: None}
        if not hasattr(self, 'phase'):
            self.phase = None
        reading_frame = phases_to_rf[self.phase]

        if not hasattr(self.regions[A_tag], 'seq'):
            if ref_fastafile is None:
                msg = 'ref_fastafile is required when interval does not '
                raise ValueError(msg + ' have seq attribute')
            self.get_regions_sequences(ref_fastafile)
        exon_seq = self.regions[A_tag].seq
        c1_seq = self.regions[C1_tag].seq[-2:]
        c2_seq = self.regions[C2_tag].seq[:2]
        seq = c1_seq + exon_seq + c2_seq
        if shuffle:
            seq_items = [nc for nc in seq]
            random.shuffle(seq_items)
            seq = ''.join(seq_items)

        return calc_aa_spectra(seq, codon_table=codon_table,
                               reading_frame=reading_frame,
                               map_to_aa_properties=map_to_aa_properties,
                               count_after_stop=count_after_stop)


class RetainedIntron(SplicingEvent):
    def __init__(self, parsed_ret_intron, liftover=None):
        self.chromosome = parsed_ret_intron['chrom']
        self.gene_id = parsed_ret_intron['gene_id']
        self.strand = parsed_ret_intron['strand']
        self.pu_scores_process = None
        self.id = parsed_ret_intron['id']
        intron = GenomeInterval(self.chromosome, parsed_ret_intron['start'],
                                parsed_ret_intron['end'], self.strand,
                                liftover)
        if self.strand == '+':
            C1 = GenomeInterval(self.chromosome,
                                parsed_ret_intron['up_exon_start'],
                                parsed_ret_intron['up_exon_end'],
                                self.strand, liftover)
            C2 = GenomeInterval(self.chromosome,
                                parsed_ret_intron['dw_exon_start'],
                                parsed_ret_intron['dw_exon_end'],
                                self.strand, liftover)
            if C1.start > C2.start:
                msg = 'Invalid coordinates for positive strand: C1 > C2'
                raise ValueError(msg)
        else:
            C1 = GenomeInterval(self.chromosome,
                                parsed_ret_intron['dw_exon_start'],
                                parsed_ret_intron['dw_exon_end'],
                                self.strand, liftover)
            C2 = GenomeInterval(self.chromosome,
                                parsed_ret_intron['up_exon_start'],
                                parsed_ret_intron['up_exon_end'],
                                self.strand, liftover)
            if C1.start < C2.start:
                msg = 'Invalid coordinates for negative strand: C1 < C2'
                raise ValueError(msg)
        self.regions = {I_tag: intron, C1_tag: C1, C2_tag: C2}
        self.features = {}

    def get_splice_sites_seqs(self):
        if I_tag in self.regions:
            region_names = C1_tag, I_tag, I_tag, C2_tag
            tag_c1, tag_i1, tag_i2, tag_c2 = region_names
        else:
            tag_c1 = '{}.r.{}'.format(C1_tag, self.exon_ws)
            tag_i1 = '{}.l.{}'.format(I_tag, self.intron_ws)
            tag_i2 = '{}.r.{}'.format(I_tag, self.intron_ws)
            tag_c2 = '{}.r.{}'.format(C1_tag, self.exon_ws)
        _3ss = self.regions[tag_i2].seq[-20:] + self.regions[tag_c2].seq[:3]
        _5ss = self.regions[tag_c1].seq[-3:] + self.regions[tag_i1].seq[:6]
        self.splice_sites = {'5ss': _5ss, '3ss': _3ss}


def fetch_splicing_events(in_fpath, file_format='vast', ref_fastafile=None,
                          bigwigfiles=[], rev_bw_files=None, liftover=None,
                          bw_tags=[], tabix_index=None, sel_event_type=EX,
                          take_windows=False, use_tabix_start=True,
                          exon_window_size=EXON_WINDOW_SIZE,
                          intron_window_size=INTRON_WINDOW_SIZE,
                          alternative_window_size=ALT_WINDOW_SIZE,
                          verbose=False, tabix_cols_filter=None,
                          vcf_index=None, af_field='MAF',
                          variant_type_field='TSA',
                          only_exons=False):
    if rev_bw_files is None:
        rev_bw_files = [None] * len(bigwigfiles)
    splicing_class = {EX: ExonCassette, INT: RetainedIntron,
                      BED: GeneInverval, EVENT: SplicingEvent,
                      ALTD: RetainedIntron, ALTA: RetainedIntron}
    available_events = {EX: [S, C1, C2, C3, MIC], INT: [IR_C, IR_S],
                        BED: [BED], EVENT: [EVENT], ALTD: [ALT5],
                        ALTA: [ALT3]}
    for parsed_exon in parsers[file_format](in_fpath):
        if file_format in [VAST_FORMAT, BED_FORMAT]:
            event_type, parsed_exon = parsed_exon
            if event_type not in available_events[sel_event_type]:
                continue
        try:
            splicing_event = splicing_class[sel_event_type](parsed_exon,
                                                            liftover=liftover)
        except ValueError:
            sys.stderr.write('Error fetching {}\n'.format(sel_event_type))
            continue

        # Filter event regions if required
        splicing_event.regions = {name: r
                                  for name, r in splicing_event.regions.items()
                                  if not only_exons or 'exon' in name}
        if take_windows:
            alt_ws = alternative_window_size
            splicing_event.take_windows(exon_window_size=exon_window_size,
                                        intron_window_size=intron_window_size,
                                        alternative_window_size=alt_ws)

        if ref_fastafile:
            splicing_event.get_regions_sequences(ref_fastafile)
            if hasattr(splicing_event, 'get_splice_sites_seqs'):
                splicing_event.get_splice_sites_seqs()
        for bigwigfile, rev_bw_file, bw_tag in zip(bigwigfiles, rev_bw_files,
                                                   bw_tags):
            splicing_event.get_bigwig_profile(bigwigfile, bw_tag,
                                              rev_bw_file=rev_bw_file)
        if vcf_index is not None:
            splicing_event.get_exp_het(vcf_index, af_field=af_field,
                                       variant_type_field=variant_type_field)

        if tabix_index is not None:
            splicing_event.find_tabix_intervals(tabix_index,
                                                use_start=use_tabix_start,
                                                sel_rbps=None,
                                                cols_filters=tabix_cols_filter)
        yield splicing_event


class GeneInverval(SplicingEvent):
    def __init__(self, parsed_bed_line, liftover=None):
        self.chromosome = parsed_bed_line['chrom']
        self.strand = parsed_bed_line['strand']
        self.gene_id = parsed_bed_line['id']
        self.id = parsed_bed_line['id']
        self.transcript_id = parsed_bed_line['extra_fields'][0]
        interval = GenomeInterval(self.chromosome, parsed_bed_line['start'],
                                  parsed_bed_line['end'], self.strand,
                                  liftover=liftover)
        self.regions = {'interval': interval}
        self.features = {}


def calc_splice_site_scores(splicing_events, out_prefix=None):
    splice_sites = {}
    event_ids = []
    for splicing_event in splicing_events:
        event_ss = splicing_event.splice_sites
        skip = False
        for key, value in event_ss.items():
            if (('5ss' in key and len(value) != 9) or
                    ('3ss' in key and len(value) != 23) or
                    ('n' in value.lower())):
                skip = True
                break
        if skip:
            continue

        event_ids.append(splicing_event.id)
        for key, value in event_ss.items():
            try:
                splice_sites[key].append(value)
            except KeyError:
                splice_sites[key] = [value]

    ss_scores = {}
    for site, seqs in splice_sites.items():
        if out_prefix is None:
            fhand = NamedTemporaryFile(mode='w')
        else:
            fhand = open('{}.{}'.format(out_prefix, site), 'w')
        for seq in seqs:
            fhand.write(seq.lower() + '\n')
        fhand.flush()
        score_bin = SCORE_5SS if '5ss' in site else SCORE_3SS
        cmd = ['perl', score_bin, fhand.name]
        scores_fhand = NamedTemporaryFile(mode='w')
        check_call(cmd, stdout=scores_fhand)
        scores_fhand.flush()
        scores = []
        for line in open(scores_fhand.name):
            scores.append(float(line.strip().split()[-1]))
        ss_scores[site] = scores
    return DataFrame(ss_scores, index=event_ids)


def calc_ppt_features(splicing_events, out_prefix=None, species='Mmus',
                      intron_ws=INTRON_WINDOW_SIZE, kword='intron.r'):
    ppts = {}
    event_ids = []
    for splicing_event in splicing_events:
        event_ids.append(splicing_event.id)
        for region_name, region in splicing_event.regions.items():
            if kword not in region_name:
                continue
            try:
                ppts[region_name].append((splicing_event.id, region.seq))
            except KeyError:
                ppts[region_name] = [(splicing_event.id, region.seq)]

    ppts_features = {}
    for region_name, seqs in ppts.items():
        if out_prefix is None:
            fhand = NamedTemporaryFile(mode='w', suffix='.fasta')
        else:
            fhand = open('{}.{}'.format(out_prefix, region_name), 'w')
        for event_id, seq in seqs:
            fhand.write('>{}\n{}\n'.format(event_id, seq))
        fhand.flush()
        cmd = [sys.executable, SVM_BPFINDER, fhand.name, species,
               str(intron_ws)]
        process = Popen(cmd, stdout=PIPE)

        feat_reader = csv.DictReader(process.stdout, delimiter='\t')
        records = itertools.groupby(feat_reader, key=lambda x: x['seq_id'])
        event_ids = []
        features = {}
        for event_id, event_records in records:
            record = max(list(event_records), key=lambda x: x['svm_scr'])
            event_ids.append(event_id)
            for key, value in record.items():
                if key == 'seq_id' or key == 'svm_scr':
                    continue
                try:
                    features[key].append(value)
                except KeyError:
                    features[key] = [value]
        ppts_features[region_name] = DataFrame(features, index=event_ids)
    return ppts_features


def calc_rfp(exon_cassettes):
    rfp = {exon.id: exon.rfp for exon in exon_cassettes}
    return DataFrame.from_dict(rfp, orient='index')


def calc_regions_length(splicing_events):
    lengths = {}
    event_ids = []
    for splicing_event in splicing_events:
        event_ids.append(splicing_event.id)
        for region_name, region in splicing_event.regions.items():
            try:
                lengths[region_name].append(region.length)
            except KeyError:
                lengths[region_name] = [region.length]
    return DataFrame(lengths, index=event_ids)


def get_alternative_exons(exon_cassettes, indexed_gtf, check_id=True):
    exon_ids = []
    is_alternative = []
    for exon_cassette in exon_cassettes:
        exon = exon_cassette.regions[A_tag]
        transcripts = set()
        gtf = exon.get_overlapping_intervals(indexed_gtf)
        if gtf is None:
            continue
        exon_ids.append(exon_cassette.id)
        for record in parse_gtf(gtf):
            if exon.strand != record['strand']:
                continue
            if check_id and exon_cassette.gene_id != record['gene_id']:
                continue
            if exon.start < record['start'] or exon.end > record['end']:
                continue
            try:
                transcripts.add(record['attributes']['transcript_id'])
            except KeyError:
                continue
        if len(transcripts) > 1:
            is_alternative.append(True)
        else:
            is_alternative.append(False)
    return DataFrame({'Alternative': is_alternative}, index=exon_ids)
