#!/usr/bin/env python

from csv import DictReader
import sys

import numpy
import pandas

from utils.settings import (MATS_FORMAT, VAST_FORMAT, S, C1, C2, C3, MIC, IR_C,
                            IR_S, ALT3, ALT5, BED, BED_FORMAT, ENSEMBL_FORMAT,
                            EVENT, EX, INT, ALTA, ALTD)


def parse_exon(record):
    try:
        if record['phase'] == '-':
            phase = []
        else:
            phase = [int(phase) for phase in record['phase'].split(',')]
    except KeyError or ValueError:
        phase = []
    return {'chrom': record['chr'], 'start': int(record['exonStart_0base']),
            'end': int(record['exonEnd']), 'strand': record['strand'],
            'gene_id': record['GeneID'], 'phase': phase,
            'dw_exon_start': int(record['downstreamES']),
            'dw_exon_end': int(record['downstreamEE']),
            'up_exon_start': int(record['upstreamES']),
            'up_exon_end': int(record['upstreamEE']),
            'exon_id': record['ID']}


def read_MATS_output(in_fpath, min_fdr=None, min_inc_level_dif=None):
    '''Reads MATS output. It allows filtering the input, but do it beforehand
    because the final script does not allow it and it is easier this way'''

    reader = DictReader(open(in_fpath), delimiter='\t')
    for record in reader:
        if ((min_fdr is not None and float(record['FDR']) < min_fdr) or
            (min_inc_level_dif is not None and
             float(record['IncLevelDifference']) < min_inc_level_dif)):
            continue
        yield parse_exon(record)


def parse_vast_psi_data(psi_items, samples=None):
    data = []
    psi_items = iter(psi_items)
    for i, sample_item in enumerate(psi_items):
        psi = float(sample_item) if sample_item != 'NA' else None
        try:
            depth_data = psi_items.next()
        except StopIteration:
            msg = 'Error parsing PSI data. No coverage information found'
            raise ValueError(msg)
        depth = numpy.nansum([float(rc) if rc != 'NA' else numpy.nan
                              for rc in depth_data.split('@')[1].split(',')])
        psi_data = {'psi': psi, 'depth': depth}
        if samples is not None:
            psi_data['sample'] = samples[i]
        data.append(psi_data)
    return data


def parse_vast_exon(exon_line_items, samples=None, flanking_exon_window=50):
    chrom, coord = exon_line_items[4].split(':')
    chrom = chrom.lstrip('chr')
    up_exon, _, down_exon = coord.split(',')
    up_exon = [int(x) for x in up_exon.split('+')]
    down_exon = [int(x) for x in down_exon.split('+')]
    start, end = [int(x) for x in exon_line_items[2].split(':')[1].split('-')]
    exon_id = exon_line_items[1]
    if ((up_exon[0] < start and down_exon[0] < start) or
            (up_exon[0] > start and down_exon[0] > start)):
        msg = 'Unexpected coordinates found for {}. It will '.format(exon_id)
        msg += 'not be taken into account'
        print Warning(msg)
        return None
    if max(up_exon) < min(down_exon) and min(up_exon) < start < max(down_exon):
        strand = '+'
        up_exon_end, down_exon_start = max(up_exon), min(down_exon)
    elif (min(up_exon) > max(down_exon) and
            max(up_exon) > start > min(down_exon)):
        strand = '-'
        down_exon_start, up_exon_end = min(up_exon), max(down_exon)
    else:
        msg = 'Unexpected coordinates found for {}. It will '.format(exon_id)
        msg += 'not be taken into account'
        print Warning(msg)
        return None
    up_exon_start = up_exon_end - flanking_exon_window
    down_exon_end = down_exon_start + flanking_exon_window
    psi_data = parse_vast_psi_data(exon_line_items[6:], samples)
    return {'chrom': chrom, 'start': start - 1, 'end': end, 'strand': strand,
            'gene_id': exon_line_items[0], 'phase': None,
            'id': exon_id, 'dw_exon_start': down_exon_start - 1,
            'dw_exon_end': down_exon_end, 'up_exon_start': up_exon_start - 1,
            'up_exon_end': up_exon_end, 'psi_data': psi_data,
            'length': int(exon_line_items[3])}


def parse_ensembl_exon(line_items):
    start = int(line_items[2])
    end = int(line_items[3])
    alternative = line_items[4] != '1'
    strand = '+' if line_items[8] == '1' else '-'
    chrom = line_items[9]
    gene_name = line_items[10]
    return {'chrom': chrom, 'start': start - 1, 'end': end, 'strand': strand,
            'gene_id': line_items[0], 'phase': line_items[6],
            'exon_id': line_items[7], 'rank': int(line_items[5]),
            'alternative': alternative, 'gene_name': gene_name,
            'transcript_id': line_items[1]}


def parse_ensembl_exons(fpath):
    for line in open(fpath):
        yield parse_ensembl_exon(line.strip().split())


def parse_vast_intron_retention(intron_line_items, samples=None):
    chrom, coord, strand = intron_line_items[4].split(':')
    chrom = chrom.strip('chr')
    up_exon, down_exon = [[int(x) for x in exon.split('-')]
                          for exon in coord.split('=')]
    if strand == '-':
        up_exon, down_exon = down_exon, [up_exon[0] + 1, up_exon[1]]
    intron = [up_exon[1], down_exon[0] - 1]
    psi_data = parse_vast_psi_data(intron_line_items[6:], samples)
    return {'chrom': chrom, 'start': intron[0], 'end': intron[1],
            'strand': strand, 'gene_id': intron_line_items[0], 'phase': None,
            'id': intron_line_items[1],
            'dw_exon_start': down_exon[0] - 1,
            'dw_exon_end': down_exon[1], 'up_exon_start': up_exon[0] - 1,
            'up_exon_end': up_exon[1], 'psi_data': psi_data,
            'length': int(intron_line_items[3])}


def parse_vast_alt3(alt3_line_items, samples=None, flanking_exon_window=50):
    chrom, coord = alt3_line_items[4].split(':')
    chrom = chrom.strip('chr')
    alt_coord = alt3_line_items[2].split(':')[1].split('-')
    up_exon, down_exon = [x.split('+') for x in coord.split('-')]
    if len(up_exon) > 1:
        strand = '-'
    elif len(down_exon) > 1:
        strand = '+'
    else:
        raise ValueError('Format error parsing ALTD event')

    if '' in alt_coord:
        return None
    up_exon_end, dw_exon_start = [int(x) for x in alt_coord]
    up_exon_start = up_exon_end - flanking_exon_window
    dw_exon_end = dw_exon_start + flanking_exon_window

    psi_data = parse_vast_psi_data(alt3_line_items[6:], samples)

    return {'chrom': chrom, 'start': up_exon_end - 1, 'end': dw_exon_start,
            'strand': strand, 'gene_id': alt3_line_items[0], 'phase': None,
            'id': alt3_line_items[1],
            'dw_exon_start': dw_exon_start - 1,
            'dw_exon_end': dw_exon_end, 'up_exon_start': up_exon_start - 1,
            'up_exon_end': up_exon_end, 'psi_data': psi_data,
            'length': int(alt3_line_items[3])}


def parse_vast_alt5(alt5_line_items, samples=None, flanking_exon_window=50):
    chrom, coord = alt5_line_items[4].split(':')
    chrom = chrom.strip('chr')
    alt_coord = alt5_line_items[2].split(':')[1].split('-')
    up_exon, down_exon = [x.split('+') for x in coord.split('-')]
    if len(up_exon) > 1:
        strand = '+'
    elif len(down_exon) > 1:
        strand = '-'
    else:
        raise ValueError('Format error parsing ALTD event')

    if '' in alt_coord:
        return None
    up_exon_end, dw_exon_start = [int(x) for x in alt_coord]
    up_exon_start = up_exon_end - flanking_exon_window
    dw_exon_end = dw_exon_start + flanking_exon_window

    psi_data = parse_vast_psi_data(alt5_line_items[6:], samples)

    return {'chrom': chrom, 'start': up_exon_end - 1, 'end': dw_exon_start,
            'strand': strand, 'gene_id': alt5_line_items[0], 'phase': None,
            'id': alt5_line_items[1],
            'dw_exon_start': dw_exon_start - 1,
            'dw_exon_end': dw_exon_end, 'up_exon_start': up_exon_start - 1,
            'up_exon_end': up_exon_end, 'psi_data': psi_data,
            'length': int(alt5_line_items[3])}


def read_vasttools_output(fpath):
    '''Parses all splicing events in the output results from Vast-tools,
    returning a dictionary with different features for the different types of
    events. Result coordinates are 1-indexed as given by vast-tools, so
    they are transformed to 0-based coordinates to use fastafiles or
    tabixfiles from pysam'''

    fhand = open(fpath)
    samples = None
    for line in fhand:
        if 'GENE' in line:
            samples = [item for item in line.strip().split()[6:]
                       if not item.endswith('-Q')]
        items = line.strip().split()
        if len(items) < 6:
            continue

        event_type = items[5]
        if event_type not in [S, C1, C2, C3, MIC, IR_C, IR_S, ALT5, ALT3]:
            continue
        if event_type in [S, C1, C2, C3, MIC]:
            event = parse_vast_exon(items, samples)
        elif event_type in [IR_C, IR_S]:
            event = parse_vast_intron_retention(items, samples)
        elif event_type == ALT3:
            event = parse_vast_alt3(items, samples)
        elif event_type == ALT5:
            event = parse_vast_alt5(items, samples)
        else:
            msg = 'Splicing event not supported: {}'.format(event_type)
            raise ValueError(msg)
        if event is not None:
            yield event_type, event


def vast_output_to_psi_dfs(parsed_vast_output, sample_names=None):
    psi = {}
    depth = {}
    data = {}
    for event_type, parsed_event in parsed_vast_output:
        event_psi = {}
        event_depth = {}
        event_data = {'event_type':  event_type,
                      'gene_id': parsed_event['gene_id'],
                      'length': parsed_event['length']}
        if sample_names is None:
            sample_names = range(len(parsed_event['psi_data']))
        for sample_name, sample_data in zip(sample_names,
                                            parsed_event['psi_data']):
            event_psi[sample_name] = sample_data['psi']
            event_depth[sample_name] = sample_data['depth']
        psi[parsed_event['id']] = event_psi
        depth[parsed_event['id']] = event_depth
        data[parsed_event['id']] = event_data
    psi = pandas.DataFrame.from_dict(psi, orient='index')
    depth = pandas.DataFrame.from_dict(depth, orient='index')
    data = pandas.DataFrame.from_dict(data, orient='index')
    return psi, depth, data


def filter_psi_by_depth(psi_df, depth_df, data_df, min_depth=20,
                        min_samples=None):
    if min_samples is None:
        min_samples = depth_df.shape[1]
    sel_rows = numpy.nansum(depth_df >= min_depth, axis=1) >= min_samples
    return (psi_df.loc[sel_rows, :], depth_df.loc[sel_rows, :],
            data_df.loc[sel_rows, :])


def filter_by_event_type(psi_df, depth_df, data_df,
                         sel_event_type=EX):
    if sel_event_type == EX:
        sel_rows = numpy.logical_or(data_df['event_type'] == S,
                                    data_df['event_type'] == C1)
        sel_rows = numpy.logical_or(sel_rows, data_df['event_type'] == C2)
        sel_rows = numpy.logical_or(sel_rows, data_df['event_type'] == C3)
        sel_rows = numpy.logical_or(sel_rows, data_df['event_type'] == MIC)
    elif sel_event_type == INT:
        sel_rows = numpy.logical_or(data_df['event_type'] == IR_C,
                                    data_df['event_type'] == IR_S)
    elif sel_event_type == ALTD or sel_event_type == ALTA:
        sel_rows = numpy.logical_or(data_df['event_type'] == ALT3,
                                    data_df['event_type'] == ALT5)
    else:
        raise ValueError('Not supported event')
    return (psi_df.loc[sel_rows, :], depth_df.loc[sel_rows, :],
            data_df.loc[sel_rows, :])


def read_bed_file(fpath):
    fhand = open(fpath)
    for line in fhand:
        items = line.strip().split()
        parsed_line = {'chrom': items[0], 'start': int(items[1]),
                       'end': int(items[2]), 'strand': items[3]}
        if len(items) > 4:
            parsed_line['id'] = items[4]
        if len(items) > 5:
            parsed_line['extra_fields'] = items[5:]
        yield BED, parsed_line


def parse_bed_events_idx(bed_dict):
    events_idx = {}
    for region_name, bed_fpath in bed_dict.items():
        for line in open(bed_fpath):
            items = line.strip().split()
            chrom = items[0]
            start = int(items[1])
            end = int(items[2])
            strand = items[5]
            event_id = items[6]
            values = (chrom, start, end, strand)
            try:
                events_idx[event_id][region_name] = values
            except KeyError:
                events_idx[event_id] = {region_name: values}
    return events_idx


def parse_bed_events(fpath):
    bed_dict = {}
    for line in open(fpath):
        items = line.strip().split()
        bed_dict[items[0]] = items[1]
    events_idx = parse_bed_events_idx(bed_dict)
    key_values = ['chrom', 'start', 'end', 'strand']
    for event_id, regions_dict in events_idx.items():
        event_dict = {}
        for region, coords in regions_dict.items():
            event_dict[region] = {'id': event_id}
            for key, value in zip(key_values, coords):
                event_dict[region][key] = value
        yield event_dict


parsers = {MATS_FORMAT: read_MATS_output, VAST_FORMAT: read_vasttools_output,
           BED_FORMAT: read_bed_file, ENSEMBL_FORMAT: parse_ensembl_exons,
           EVENT: parse_bed_events}


def read_fasta_file(fhand, numerical=False):
    name = ''
    seq = [] if numerical else ''
    for line in fhand:
        if line == '\n':
            continue
        if line.startswith('>'):
            if seq:
                yield name, seq
            name = line.strip()[1:]
            seq = [] if numerical else ''
        else:
            if numerical:
                seq.extend([float(x) for x in line.strip().split(' ')])
            else:
                seq += line.strip()
    if seq:
        yield name, seq


def parse_gtf(fhand):
    for line in fhand:
        if line.startswith('#'):
            continue
        items = line.strip().split('\t')
        chrom = items[0]
        feature = items[2]
        start = int(items[3]) - 1
        end = int(items[4])
        strand = items[6]
        frame = items[7]
        attributes = {}
        gene_id = None
        for attribute in items[8].split(';'):
            att_items = attribute.split()
            if len(att_items) == 0:
                continue
            key = att_items[0]
            value = ' '.join(att_items[1:])
            value = value.strip('"')
            if key == 'gene_id':
                gene_id = value
            attributes[key] = value
        yield {'chrom': '{}'.format(chrom), 'start': start, 'end': end,
               'strand': strand, 'frame': frame, 'gene_id': gene_id,
               'feature': feature, 'attributes': attributes}


def parse_vcf_line(line):
    items = line.strip().split()
    if len(items) < 8:
        msg = 'Error parsing line. Not enough fields:\n{}\n'.format(line)
        sys.stderr.write(msg)
        return None
    attributes = {}
    for attribute in items[7].split(';'):
        attr_items = attribute.split('=')
        if len(attr_items) == 1:
            attributes[attr_items[0]] = None
        else:
            try:
                value = float(attr_items[1])
            except ValueError:
                value = attr_items[1]
            attributes[attr_items[0]] = value

    return {'chrom': items[0], 'pos': int(items[1]), 'id': items[2],
            'ref': items[3], 'alt': items[4].split(','),
            'attributes': attributes}
