#!/usr/bin/env python

import itertools
import sys

import numpy
from pandas import DataFrame
from scipy.stats.stats import spearmanr

from seq.intervals import GenomeInterval
from utils.settings import INT, IR_C, IR_S


def _group_gtf_by_gene_id(gtf, add_chr=False):
    gene_idx = {}
    for gtf_record in gtf:
        gene_id = gtf_record['gene_id']
        if (add_chr and 'chr' not in gtf_record['chrom'] and
                len(gtf_record['chrom']) < 3):
            gtf_record['chrom'] = 'chr' + gtf_record['chrom']
        try:
            gene_idx[gene_id].append(gtf_record)
        except KeyError:
            gene_idx[gene_id] = [gtf_record]
    return gene_idx.items()


def _group_gtf_by_gene_id_sorted(gtf, add_chr=False):
    prev_gene_id = None
    gtf_records = []
    for gtf_record in gtf:
        gene_id = gtf_record['gene_id']
        if prev_gene_id is None or gene_id == prev_gene_id:
            gtf_records.append(gtf_record)
        else:
            if gtf_records:
                yield prev_gene_id, gtf_records
                gtf_records = [gtf_record]
        prev_gene_id = gene_id
    if gtf_records:
        yield prev_gene_id, gtf_records


def group_gtf_by_gene_id(gtf, add_chr=False, is_sorted=False):
    if is_sorted:
        return _group_gtf_by_gene_id_sorted(gtf, add_chr=add_chr)
    else:
        return _group_gtf_by_gene_id(gtf, add_chr=add_chr)


def _get_gene_coords(gene_gtf_records):
    chrom = gene_gtf_records[0]['chrom']
    strand = gene_gtf_records[0]['strand']
    start = min(gene_gtf_records, key=lambda x: x['start'])['start']
    end = max(gene_gtf_records, key=lambda x: x['end'])['end']
    return {'chrom': chrom, 'start': start, 'end': end, 'strand': strand}


def get_gene_coord(gtf, add_chr=False, is_sorted=False):
    gene_coords = {}
    for gene_id, gtf_records in group_gtf_by_gene_id(gtf, add_chr, is_sorted):
        gene_coords[gene_id] = _get_gene_coords(gtf_records)
    return gene_coords


def _are_overlapping(interval1, interval2):
    if interval1[0] >= interval2[1] or interval1[1] <= interval2[0]:
        return False
    else:
        return True


def _remove_interval(interval1, interval2):
    if interval1[0] < interval2[0]:
        if interval1[1] > interval2[1]:
            return [(interval1[0], interval2[0], interval1[2]),
                    (interval2[1], interval1[1], interval1[2])]
        else:
            return [(interval1[0], interval2[0], interval1[2])]
    else:
        if interval1[0] >= interval2[0] and interval1[1] <= interval2[1]:
            return []
        else:
            return [(interval2[1], interval1[1], interval1[2])]


def _get_gene_regions_feature(start, end, gtf_records, gene_features=None):
    if gene_features is None:
        gene_features = ['exon', 'CDS']
    intervals = [(start, end, 'Intron')]
    for feature in gene_features:
        for gtf_record in gtf_records:
            if gtf_record['feature'] != feature:
                continue
            new_interval = (gtf_record['start'], gtf_record['end'], feature)
            for i, interval in enumerate(intervals):
                if _are_overlapping(interval, new_interval):
                    intervals.pop(i)
                    for interval in _remove_interval(interval, new_interval):
                        intervals.append(interval)
            intervals.append(new_interval)
    return sorted(intervals, key=lambda x: x[0])


def _get_gene_type(gtf_records):
    return gtf_records[0]['attributes']['gene_biotype']


def get_gene_features_intervals(gtf, add_chr=False,
                                gene_features=['exon', 'CDS'],
                                is_sorted=False):
    '''Returns a generator of genes with non overlapping intervals for the
       different gene features: exon and CDS by default'''

    for gene_id, gtf_records in group_gtf_by_gene_id(gtf, add_chr,
                                                     is_sorted):
        gene_coords = _get_gene_coords(gtf_records)
        intervals = _get_gene_regions_feature(gene_coords['start'],
                                              gene_coords['end'], gtf_records,
                                              gene_features)
        gene_type = _get_gene_type(gtf_records)
        yield (gene_id, gene_type, gene_coords['chrom'],
               gene_coords['strand'], intervals)


class GeneEvent(object):
    def __init__(self, gene_id, chrom, strand, feat_intervals,
                 features_ws={}, gene_type=None):
        self.id = gene_id
        self.regions = {}
        self.gene_type = gene_type
        for start, end, feature in feat_intervals:
            interval = GenomeInterval(chrom, start, end, strand)
            try:
                self.regions[feature].append(interval)
            except KeyError:
                self.regions[feature] = [interval]
            if feature in features_ws:
                window_size = features_ws[feature]
                ends = interval.take_ends(window_size)
                for end_type, interval in zip(['l', 'r'], ends):
                    new_feature = '{}.{}.{}'.format(feature, end_type,
                                                    window_size)
                    try:
                        self.regions[new_feature].append(interval)
                    except KeyError:
                        self.regions[new_feature] = [interval]


def fetch_gene_events(gtf, add_chr=False, is_sorted=False, features_ws={}):
    for gene_features in get_gene_features_intervals(gtf, add_chr=add_chr,
                                                     gene_features=['exon',
                                                                    'CDS'],
                                                     is_sorted=is_sorted):
        gene_id, gene_type, chrom, strand, feat_intervals = gene_features
        yield GeneEvent(gene_id, chrom, strand, feat_intervals,
                        features_ws=features_ws, gene_type=gene_type)


def calc_gene_features_values(gtf, add_chr=False, tabix_index=None,
                              bw_file=None, rev_bw_file=None, tag=None,
                              features_ws={}, vcf_index=None, verbose=False,
                              is_sorted=False, af_field='MAF',
                              variant_type_field='TSA'):
    vt_field = variant_type_field
    given_files = numpy.sum([tabix_index is not None, bw_file is not None,
                             vcf_index is not None])
    if given_files > 1:
        msg = 'Only one of tabix_index and bw_file can be used'
        raise ValueError(msg)
    elif given_files == 0:
        msg = 'At least one of tabix_index and bw_file must be used'
        raise ValueError(msg)

    data = {}
    i = 0
    for gene_event in fetch_gene_events(gtf, add_chr=add_chr,
                                        is_sorted=is_sorted,
                                        features_ws=features_ws):
        i += 1
        if verbose:
            if i % 100 == 0:
                sys.stderr.write('Processed genes: {}\n'.format(i))
        feat_values = {}
        for feature, intervals in gene_event.regions.items():
            for interval in intervals:
                if bw_file is not None:
                    interval.get_bigwig_profile(bw_file, tag, expand=True,
                                                rev_bw_file=rev_bw_file)
                    values = interval.profiles[tag]
                elif tabix_index is not None:
                    if tag is None:
                        msg = 'A tag is required to use tabix_index'
                        raise ValueError(msg)
                    interval.find_tabix_intervals(tabix_index, sel_rbps=[tag])
                    values = interval.get_tabix_profile(tag)
                else:
                    values = interval.calc_exp_het(vcf_index,
                                                   maf_field=af_field,
                                                   variant_type_field=vt_field)

                try:
                    feat_values[feature] = numpy.append(feat_values[feature],
                                                        values)
                except KeyError:
                    feat_values[feature] = values
        for feature_name, values in feat_values.items():
            feat_values[feature_name] = numpy.nanmean(values)
        data[gene_event.id] = feat_values
    return DataFrame.from_dict(data, orient='index')


def calc_tabix_counts_promoter(gene_coords, upstream_ws, downstream_ws,
                               tabix_index, tf_ids, verbose=False):
    counts = {}
    for gene_id, record in gene_coords.items():
        chrom = record['chrom']
        start = record['start']
        end = record['end']
        strand = record['strand']
        if strand == '+':
            promoter = GenomeInterval(chrom, start - upstream_ws,
                                      start + downstream_ws, strand)
        else:
            promoter = GenomeInterval(chrom, end - downstream_ws,
                                      end + upstream_ws, strand)
        binding_sites = promoter.get_overlapping_intervals(tabix_index,
                                                           verbose=verbose)
        if binding_sites is None:
            continue

        counts[gene_id] = {tf_id: 0 for tf_id in tf_ids}
        for bed_line in binding_sites:
            items = bed_line.strip().split()
            tf = items[3]
            try:
                counts[gene_id][tf] += 1
            except KeyError:
                pass

    return DataFrame.from_dict(counts, orient='index', dtype='int')


def calc_gene_splicing_efficiency_params(psi_df, data_df, min_introns=2):
    ir_events = numpy.logical_or(data_df['event_type'] == IR_C,
                                 data_df['event_type'] == IR_S)
    data_df = data_df.loc[ir_events, :]
    data_df['event_id'] = data_df.index
    data_df.index = data_df['gene_id']
    median_psi = []
    sd_psi = []
    mean_pcorr = []
    gene_ids = []
    for gene_id in numpy.unique(data_df['gene_id']):
        event_ids = data_df.loc[gene_id, 'event_id']
        gene_psi = psi_df.loc[event_ids, :]
        if gene_psi.shape[0] < min_introns or len(gene_psi.shape) < 2:
            continue
        gene_ids.append(gene_id)
        median_psi.append(numpy.nanmedian(gene_psi, axis=0))
        sd_psi.append(numpy.nanstd(gene_psi, axis=0))
        rhos = []
        pvalues = []
        for idx1, idx2 in itertools.combinations(range(event_ids.shape[0]), 2):
            psi1 = gene_psi.iloc[idx1, :]
            psi2 = gene_psi.iloc[idx2, :]
            rho, pvalue = spearmanr(psi1, psi2)
            rhos.append(rho)
            pvalues.append(pvalue)
        mean_pcorr.append([numpy.nanmean(rhos), numpy.nanmean(pvalues),
                           len(list(event_ids))])
    mean_pcorr = DataFrame(numpy.vstack(mean_pcorr), index=gene_ids,
                           columns=['mean_rho', 'mean_pvalue', 'n_introns'])
    median_psi = DataFrame(numpy.vstack(median_psi), index=gene_ids,
                           columns=psi_df.columns)
    sd_psi = DataFrame(numpy.vstack(sd_psi), index=gene_ids,
                       columns=psi_df.columns)
    return median_psi, sd_psi, mean_pcorr
