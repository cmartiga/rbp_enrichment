#!/usr/bin/env python
import re
from subprocess import check_call
import sys
from tempfile import NamedTemporaryFile

import RNA
import numpy
from pyliftover.liftover import LiftOver

from seq.align import clustalo
from seq.parsers import parse_vcf_line
from structure.predict import calc_rnazfold_zscore


change_strand = {'+': '-', '-': '+'}


def reverse_complement(seq):
    seq_dict = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G', 'N': 'N'}
    return ''.join([seq_dict[base] for base in reversed(seq)])


class GenomeInterval(object):
    def __init__(self, chrom, start, end, strand, liftover=None,
                 min_lo_score=None):
        assert start <= end
        self.defined = True
        if liftover:
            new_coords = self.liftover(liftover, min_score=min_lo_score)
            if new_coords is None:
                self.defined = False
                chrom, start, end, strand = [None] * 4
            else:
                chrom, start, end, strand = new_coords
        if start > end:
            start, end = end, start
            strand = change_strand[strand]
        self.chrom = chrom
        self.start = start
        self.end = end
        self.strand = strand
        self.length = None if end is None or start is None else end - start
        self.seq = None
        self.tracks = {}
        self.tracks_mean = {}
        self.annotations = {}
        self.id = 'chr{}:{}-{}{}'.format(chrom, start, end, strand)
        self._windows_taken = False

    def get_coverage(self, samfile, strand_specific=False):
        try:
            reads = samfile.fetch(self.chrom, self.start, self.end)
        except ValueError:
            reads = []
        if not strand_specific:
            return len(list(reads))
        else:
            filtered_reads = []
            for read in reads:
                strand = '-' if read.is_reverse else '+'
                if strand == self.strand:
                    filtered_reads.append(read)
            return len(filtered_reads)

    def pos_liftover(self, liftover, chrom, pos, strand, min_score=None):
        res = liftover.convert_coordinate(chrom, pos, strand)
        if res is None and 'chr' not in chrom:
            res = liftover.convert_coordinate('chr{}'.format(chrom),
                                              pos, strand)
        if res is None or len(res) != 1:
            return None
        chrom, pos, strand, score = res[0]
        if min_score is not None and min_score > score:
            return None
        return chrom, pos, strand

    def _liftover(self, liftover_fpath, min_score=None):
        fhand = NamedTemporaryFile(suffix='.bed')
        line = '{}\t{}\t{}\t.\t.\t{}\n'.format(self.chrom, self.start,
                                               self.end, self.strand)
        fhand.write(line)
        fhand.flush()

        out_fhand = NamedTemporaryFile(suffix='.bed')
        unmapped_fhand = NamedTemporaryFile(suffix='.bed')

        cmd = ['liftOver', fhand.name, liftover_fpath, out_fhand.name,
               unmapped_fhand.name]
        if min_score is not None:
            cmd.append('-minMatch={}'.format(min_score))
        check_call(cmd)
        lines = list(open(out_fhand.name))
        if len(lines) == 0:
            return None
        elif len(lines) == 1:
            items = lines[0].strip().split()
            return items[0], int(items[1]), int(items[2]), items[5]
        else:
            return None

    def _pyliftover(self, liftover, min_score=None):
        start = self.pos_liftover(liftover, self.chrom, self.start,
                                  self.strand, min_score=min_score)
        if start is None:
            return None
        chrom_start, start_pos, start_strand = start

        end = self.pos_liftover(liftover, self.chrom, self.end,
                                self.strand, min_score=min_score)
        if end is None:
            return None
        chrom_end, end_pos, end_strand = end

        if chrom_end != chrom_start or start_strand != end_strand:
            return None
        if start > end:
            start_pos, end_pos = end_pos + 1, start_pos + 1
        return chrom_start, start_pos, end_pos, end_strand

    def _liftover_dict(self, liftover, min_score=None):
        try:
            return liftover[self.event_id][self.region]
        except KeyError:
            return None

    def liftover(self, liftover, min_score=None):
        '''If a chain liftover file is given pyliftover will be used to
        transform the coordinates. Loading the whole liftover file in memory
        is not memory efficient. Alternatively, the path for the liftover chain
        file can be given and python will use liftOver external tool, but
        it has to create temporary files and it is slow. Finally, if
        coordinates have been previously been lifted over, a dictionary
        containing the new coordinates for each event id and region is used'''

        if isinstance(liftover, str):
            return self._liftover(liftover, min_score=min_score)
        elif isinstance(liftover, dict):
            if self.id is None or self.region is None:
                msg = 'GenomeInterval requires id and region attributes to '
                msg += 'use liftover_dict method'
                raise ValueError(msg)
            return self._liftover_dict(liftover)
        elif isinstance(liftover, LiftOver):
            return self._pyliftover(liftover, min_score=min_score)
        else:
            msg = 'Invalid liftover object'
            raise ValueError(msg)

    def expand(self, size=150):
        self.start = self.start - size
        self.end = self.end + size
        if self.start < 0:
            self.start = 0

    @property
    def bed(self):
        return '{}\t{}\t{}\t{}'.format(self.chrom, self.start,
                                       self.end, self.strand)

    def take_ends(self, window_size):
        start1 = self.start
        end1 = self.start + window_size
        if end1 > self.end:
            end1 = self.end
        end2 = self.end
        start2 = self.end - window_size
        if start2 < self.start:
            start2 = self.start
        interval1 = GenomeInterval(chrom=self.chrom, start=start1,
                                   end=end1, strand=self.strand)
        interval2 = GenomeInterval(chrom=self.chrom, start=start2,
                                   end=end2, strand=self.strand)
        interval1._windows_taken = True
        interval2._windows_taken = True
        if self.strand == '+':
            return interval1, interval2
        elif self.strand == '-':
            return interval2, interval1
        else:
            raise ValueError('Strand information is required')

    def get_sequence_from_fastafile(self, ref_fastafile):
        chrom = self.chrom
        if self.start < 0:
            self.start = 0
        if self.end < 0:
            self.end = 0
        if chrom not in ref_fastafile.references:
            if 'chr' in chrom:
                chrom = chrom[3:]
            else:
                chrom = 'chr' + chrom

        if (self.start == self.end or not self.defined or
                chrom not in ref_fastafile.references):
            seq = ''
            self.seq = seq
            return seq
        seq = ref_fastafile.fetch(reference=bytes(chrom), start=self.start,
                                  end=self.end).upper()
        if self.strand == '-':
            seq = reverse_complement(seq)
        self.seq = seq
        return seq

    def find_motifs(self, motif_db_by_seq, max_motif_len=None,
                    min_motif_len=None, is_regexp=False):

        if max_motif_len is None:
            max_motif_len = max([len(motif_seq)
                                 for motif_seq in motif_db_by_seq])
        if min_motif_len is None:
            min_motif_len = min([len(motif_seq)
                                 for motif_seq in motif_db_by_seq])

        if not hasattr(self, 'seq'):
            msg = 'Fetch first the sequence usign get_sequence_from_fastafile'
            raise ValueError(msg)

        seq = self.seq
        motifs = {}
        motif_profile = {}
        if is_regexp:
            for motif_name, motif in motif_db_by_seq.items():
                for match in re.finditer(motif, seq):
                    start, end = match.span()
                    try:
                        motifs[(start, end)].add(motif_name)
                    except KeyError:
                        motifs[(start, end)] = set([motif_name])
                    try:
                        motif_profile[motif_name].append((start, end))
                    except KeyError:
                        motif_profile[motif_name] = [(start, end)]
        else:
            for pos in range(len(seq)):
                start = pos
                for motif_len in range(min_motif_len, max_motif_len + 1):
                    end = pos + motif_len
                    motif_seq = seq[start: end]
                    try:
                        rbps = motif_db_by_seq[motif_seq]['gene_name']
                    except KeyError:
                        continue
                    try:
                        motifs[(start, end)].union(rbps)
                    except KeyError:
                        motifs[(start, end)] = rbps
                    for rbp in rbps:
                        try:
                            motif_profile[rbp].append((start, end))
                        except KeyError:
                            motif_profile[rbp] = [(start, end)]
        self.motifs = motifs
        self.motif_profile = motif_profile
        return motifs

    def motifs_to_bed(self):
        if not hasattr(self, 'motifs'):
            msg = 'No motifs annotated in this interval. Call find motifs'
            raise ValueError(msg)

        for pos, motif in sorted(self.motifs.items()):
            start, end = pos
            if hasattr(self, 'profiles'):
                bw_profile = self.profiles['bw']
                if bw_profile is None:
                    value = numpy.nan
                else:
                    value = bw_profile[start: end].mean()
            else:
                value = None
            rbps = ','.join(list(motif))
            start = self.relative_to_genomic_coordinates(start)
            end = self.relative_to_genomic_coordinates(end)
            if self.strand == '-':
                start, end = end, start
            assert start < end
            fields = [self.chrom, start, end, self.strand, rbps]
            if value is not None:
                fields.append(value)
            yield '{}\n'.format('\t'.join([str(x) for x in fields]))

    def relative_to_genomic_coordinates(self, pos):
        if self.strand == '+':
            return self.start + pos
        else:
            return self.end - pos

    def genomic_to_relative_coordinates(self, pos):
        if pos < self.start or pos > self.end:
            msg = 'This position is not included in the genomic interval'
            raise ValueError(msg)
        if self.strand == '+':
            return pos - self.start
        else:
            return self.end - pos

    def get_bigwig_profile(self, bigwigfile, tag, expand=True,
                           rev_bw_file=None):
        if not hasattr(self, 'profiles'):
            self.profiles = {}
        if self.start == self.end or not self.defined:
            self.profiles[tag] = []
        chrom = self.chrom
        if 'chr' not in chrom and len(chrom) < 2:
            chrom = 'chr' + self.chrom
        if self.start < 0:
            pre_values = [None] * abs(self.start)
            start = 0
        else:
            pre_values = []
            start = self.start

        if rev_bw_file is not None and self.strand == '-':
            values = rev_bw_file.get(chrom, start, self.end)
        else:
            values = bigwigfile.get(chrom, start, self.end)
        if values is None:
            self.profiles[tag] = numpy.array([])
            return
        values = pre_values + values
        if self.strand == '-':
            values.reverse()
        if expand:
            self.profiles[tag] = []
            for items in values:
                if items is None:
                    items_values = [None]
                else:
                    items_values = [float(items[2])] * (int(items[1]) -
                                                        int(items[0]))
                self.profiles[tag].extend(items_values)
        else:
            self.profiles[tag] = [float(items[2]) for items in values]
        for tag, profile in self.profiles.items():
            self.profiles[tag] = numpy.array(profile)

    def get_overlapping_intervals(self, tabix_index, verbose=False):
        chrom = self.chrom
        if chrom not in tabix_index.contigs:
            if chrom.startswith('chr'):
                chrom = chrom[3:]
            else:
                chrom = 'chr{}'.format(chrom)
        if (self.start == self.end or not self.defined or
                chrom not in tabix_index.contigs):
            coordinates = '{}:{}-{}{}'.format(chrom, self.start, self.end,
                                              self.strand)
            if verbose:
                msg = 'Coordinates not found {}\n'.format(coordinates)
                sys.stderr.write(msg)
            self.motif_profile = {}
            if not hasattr(self, 'counts'):
                self.counts = {}
            return None

        return tabix_index.fetch(chrom, self.start, self.end)

    def find_tabix_intervals(self, tabix_index, use_start=False, sel_rbps=None,
                             cols_filters=None):
        motifs = self.get_overlapping_intervals(tabix_index)
        if motifs is None:
            return {}
        motif_profile = {}
        for motif in motifs:
            items = motif.strip().split()

            if cols_filters is not None:
                skip = False
                for col, filter_func in cols_filters.items():
                    if not filter_func(items[col]):
                        skip = True
                        break
                if skip:
                    continue

            start, end, strand, rbps = items[1:5]
            if strand != '.' and strand != self.strand:
                continue
            start, end = int(start), int(end)
            if use_start:
                pos = [self.genomic_to_relative_coordinates(start)]
            else:
                if start < self.start:
                    start = self.start
                if end > self.end:
                    end = self.end
                if strand == '-':
                    start, end = end, start
                start = self.genomic_to_relative_coordinates(start)
                end = self.genomic_to_relative_coordinates(end)
                pos = range(start, end)

            rbps = rbps.split(',')
            for rbp in rbps:
                if sel_rbps is not None and rbp not in sel_rbps:
                    continue
                try:
                    motif_profile[rbp].extend(pos)
                except KeyError:
                    motif_profile[rbp] = pos
        self.motif_profile = motif_profile
        return motif_profile

    def get_tabix_profile(self, motif_name):
        if hasattr(self, 'profiles') and motif_name in self.profiles:
            return self.profiles[motif_name]
        if not hasattr(self, 'motif_profile'):
            msg = 'Motif profile is not calculated. Use find_motifs or '
            msg += 'get_tabix_profile to find motif positions'
            raise ValueError(msg)
        motif_pos = self.motif_profile.get(motif_name, [])
        # TODO: find out the real solution
        motif_pos = [i - 1 for i in motif_pos]

        profile = numpy.zeros(self.length)
        if motif_pos:
            profile[motif_pos] = 1
        if hasattr(self, 'profiles'):
            self.profiles[motif_name] = profile
        else:
            self.profiles = {motif_name: profile}
        return profile

    def get_tabix_counts_from_profile(self, motif_name):
        if hasattr(self, 'counts') and motif_name in self.counts:
            return self.counts[motif_name]
        if not hasattr(self, 'motif_profile'):
            msg = 'Motif profile is not calculated. Use find_motifs or '
            msg += 'get_tabix_profile to find motif positions'
            raise ValueError(msg)
        counts = len(self.motif_profile.get(motif_name, []))
        if hasattr(self, 'counts'):
            self.counts[motif_name] = counts
        else:
            self.counts = {motif_name: counts}
        return counts

    def get_tabix_counts(self, tabix_index, cols_filters=None,
                         rbps_col=4, same_strand=True):

        motif_counts = {}
        motifs = self.get_overlapping_intervals(tabix_index)
        if motifs is None:
            return {}
        for motif in motifs:
            items = motif.strip().split()

            if same_strand and items[3] != self.strand:
                continue

            if cols_filters is not None:
                skip = False
                for col, filter_func in cols_filters.items():
                    if not filter_func(items[col]):
                        skip = True
                        break
                if skip:
                    continue
            for rbp in items[rbps_col].split(','):
                try:
                    motif_counts[rbp] += 1
                except KeyError:
                    motif_counts[rbp] = 1
        if hasattr(self, 'counts'):
            self.counts.update(motif_counts)
        else:
            self.counts = motif_counts
        return self.counts

    def get_base_annotations(self, annotation_idx, tag, annotation_types=[]):
        self.annotations[tag] = {}
        base_annotations = []
        feature_types = set()
        for i in xrange(self.start, self.end):
            pos_annotations = annotation_idx.fetch(self.chrom, i, i + 1)
            annotations = set([annot.split()[2] for annot in pos_annotations])
            feature_types = feature_types.union(annotations)
            base_annotations.append(annotations)
        if self.strand == '-':
            base_annotations.reverse()
        if not annotation_types:
            annotation_types = feature_types
        for annotation_type in annotation_types:
            value = [1 if annotation_type in base_ann else 0
                     for base_ann in base_annotations]
            self.annotations[tag][annotation_type] = value

    def get_clipseq_counts(self, clip_idx, clip_tag, verbose=False):
        chrom = 'chr{}'.format(self.chrom)
        if (self.start == self.end or not self.defined or
                chrom not in clip_idx.contigs):
            n_peaks = 0
            if verbose:
                coordinates = '{}:{}-{}{}'.format(chrom, self.start, self.end,
                                                  self.strand)
                msg = 'Coordinates not found {}\n'.format(coordinates)
                sys.stderr.write(msg)
        else:
            peaks = list(clip_idx.fetch(chrom, self.start, self.end + 1))
            n_peaks = 0
            for peak in peaks:
                strand = peak.strip().split()[3]
                if strand == self.strand:
                    n_peaks += 1
        self.tracks[clip_tag] = n_peaks
        return n_peaks

    def get_clipseq_presence_profile(self, annotation_idx, tag):
        chrom = 'chr{}'.format(self.chrom)
        if self.start == self.end or not self.defined:
            self.tracks[tag] = []
            self.tracks_mean[tag] = [None]
            coordinates = '{}:{}-{}{}'.format(chrom, self.start, self.end,
                                              self.strand)
            sys.stderr.write('Coordinates not found {}\n'.format(coordinates))
            return
        self.annotations[tag] = {}
        clipseq_presence = []
        if chrom not in annotation_idx.contigs:
            self.tracks[tag] = []
            self.tracks_mean[tag] = [None]
            return
        for i in xrange(self.start, self.end):
            pos_annotations = list(annotation_idx.fetch(chrom, i, i + 1))
            pos_annotations = [x for x in pos_annotations
                               if x.strip().split()[-1] == self.strand]
            clipseq_presence.append(1 if len(pos_annotations) > 0 else 0)
        if self.strand == '-':
            clipseq_presence.reverse()
        self.tracks[tag] = clipseq_presence
        self.tracks_mean[tag] = [sum(clipseq_presence) /
                                 float(len(clipseq_presence))]

    def get_tabix_score(self, annotation_idx, tag, score_col=5):
        self.tracks[tag] = {}
        if self.start == self.end:
            self.tracks[tag] = [0]
            self.tracks_mean[tag] = [0]
            sys.stderr.write('Interval has length 0: no annotation found\n')
            return
        try:
            pos_annotations = annotation_idx.fetch(self.chrom, self.start,
                                                   self.end)
            peak_heights = 0
            peak_heights_list = []
            for peak_line in pos_annotations:
                items = peak_line.split()
                start = max(int(items[1]), self.start)
                end = min(int(items[2]), self.end)
                peak_height = float(items[score_col])
                peak_heights_list.append(peak_height)
                peak_heights += peak_height * (end - start)
            mean_peak_height = peak_heights / float(self.end - self.start)
        except ValueError:
            peak_heights_list = [0]
            mean_peak_height = 0
        if len(peak_heights_list) == 0:
            peak_heights_list = [0]
            mean_peak_height = 0
        peak_height = max(peak_heights_list)
        self.tracks[tag] = [peak_height]
        self.tracks_mean[tag] = [mean_peak_height]

    def rnafold(self, shuffle=False, use_mononc_freq=False,
                use_python=False, calc_zscore=True, n_shuffle=100):
        if self.seq is None:
            msg = 'Sequence information is required to predict structure'
            raise ValueError(msg)
        seq = self.seq.replace('T', 'U')
        self.strc, self.mfe = RNA.fold(seq)
        self.mfe_zscore = None
        if calc_zscore:
            mononc_freq = use_mononc_freq
            self.mfe_zscore = calc_rnazfold_zscore(seq, shuffle=shuffle,
                                                   use_mononc_freq=mononc_freq,
                                                   use_python=use_python,
                                                   n_shuffle=n_shuffle)
        return self.strc, self.mfe, self.mfe_zscore

    def find_snps(self, vcf_tabix, only_snps=True, only_biallelic=True,
                  min_maf=None, af_field='MAF', variant_type_field='TSA'):
        if not only_snps or not only_biallelic:
            msg = 'It is only implemented of only_snps and only_biallelic True'
            raise NotImplementedError(msg)
        variants = self.get_overlapping_intervals(vcf_tabix)
        snps = []
        if variants is not None:
            for variant_line in variants:
                vcf_record = parse_vcf_line(variant_line)
                if vcf_record is None:
                    continue
                if vcf_record['attributes'][variant_type_field] not in ['SNV',
                                                                        'SNP']:
                    continue
                if len(vcf_record['alt']) > 2:
                    continue
                if af_field not in vcf_record['attributes']:
                    continue
                maf = vcf_record['attributes'][af_field]
                if type(maf) == str:
                    maf = min([float(x) for x in maf.split(',')])
                if min_maf is not None and (maf < min_maf or
                                            1 - maf < min_maf):
                    continue
                if self.strand == '-':
                    vcf_record['ref'] = reverse_complement(vcf_record['ref'])
                    vcf_record['alt'] = [reverse_complement(alt)
                                         for alt in vcf_record['alt']]
                snps.append(vcf_record)
        return snps

    def calc_exp_het(self, vcf_tabix, only_snps=True, only_biallelic=True,
                     min_maf=None, af_field='MAF', variant_type_field='TSA'):
        exp_het_array = numpy.full(self.length, 0.0)
        variants = self.find_snps(vcf_tabix, only_snps=only_snps,
                                  only_biallelic=only_biallelic,
                                  min_maf=min_maf, af_field=af_field,
                                  variant_type_field=variant_type_field)
        for snp in variants:
            maf = snp['attributes'][af_field]
            pos = snp['pos']
            rel_pos = self.genomic_to_relative_coordinates(pos)
            if rel_pos == exp_het_array.shape[0]:
                continue
            if type(maf) == str:
                maf = min([float(x) for x in maf.split(',')])
            exp_het = 1 - maf ** 2 - (1 - maf) ** 2
            exp_het_array[rel_pos] = exp_het
        if hasattr(self, 'profiles'):
            self.profiles['exp_het'] = exp_het_array
        else:
            self.profiles = {'exp_het': exp_het_array}
        return exp_het_array

    def find_polymorphism_changes(self, vcf_tabix, only_snps=True,
                                  only_biallelic=True, min_maf=None,
                                  af_field='MAF', variant_type_field='TSA'):
        changes = [None] * self.length
        variants = self.find_snps(vcf_tabix, only_snps=only_snps,
                                  only_biallelic=only_biallelic,
                                  min_maf=min_maf, af_field=af_field,
                                  variant_type_field=variant_type_field)
        for snp in variants:
            pos = snp['pos']
            rel_pos = self.genomic_to_relative_coordinates(pos)
            if rel_pos == len(changes):
                continue
            changes[rel_pos] = (snp['ref'], snp['alt'][0],
                                snp['attributes'][af_field])
        return changes

    def find_divergence_seqs(self, liftover, ref_fastafile,
                             outgroup_liftover, outgroup_fastafile,
                             min_score=None):
        if self.seq is None:
            msg = 'Genome interval has no sequence information. '
            msg += 'Call get_sequence_from_fastafile to fetch it'
            raise ValueError(msg)

        new_coords = self.liftover(liftover, min_score=min_score)
        if new_coords is None:
            return None
        chrom, start, end, strand = new_coords
        new_interval = GenomeInterval(chrom, start, end, strand)
        if new_interval.length != self.length:
            return None

        outgroup_coords = self.liftover(outgroup_liftover, min_score=min_score)
        if outgroup_coords is None:
            return None
        ochrom, ostart, oend, ostrand = outgroup_coords
        outgroup_interval = GenomeInterval(ochrom, ostart, oend, ostrand)
        if outgroup_interval.length != self.length:
            return None

        new_seq = new_interval.get_sequence_from_fastafile(ref_fastafile)
        out_f = outgroup_fastafile
        outgroup_seq = outgroup_interval.get_sequence_from_fastafile(out_f)

        return self.seq, new_seq, outgroup_seq

    def find_divergence_changes(self, divergence_seqs):
        if not all(divergence_seqs):
            return []

        # Realign if sequence length is not the same
#         if len(set([len(seq) for seq in divergence_seqs])) > 1:
        divergence_seqs = clustalo(divergence_seqs)

        changes = []
        for nc1, nc2, nc3 in zip(*divergence_seqs):
            if nc1 == nc2 or '-' in [nc1, nc2, nc3]:
                change = None
            else:
                if nc2 == nc3:
                    change = (nc3, nc1)
                else:
                    change = None
            changes.append(change)
        return changes


def rebuild_ancestral_seq(seq, changes):
    new_seq = ''
    for change, nc in zip(changes, seq):
        if change is None:
            new_seq += nc
        else:
            new_seq += change[0]
    return new_seq
