#!/usr/bin/env python
from subprocess import check_call
import sys
from tempfile import NamedTemporaryFile


def liftover_events_to_bed_dict(splicing_events, liftover_fpaths,
                                verbose=False):

    '''Returns a dictionary containing region names as keys and bedfiles
       fpaths of the transformed coordinates of the events regions as values.
       Takes a dictionary with the species as keys and liftover chain file
       paths as values.'''

    bed_lines = {}
    prev_events = set()
    for event in splicing_events:
        if event.id in prev_events:
            continue
        prev_events.add(event.id)
        for region_name, region in event.regions.items():
            chrom = region.chrom
            if 'chr' not in chrom and len(chrom) <= 2:
                chrom = 'chr' + chrom
            line = '{}\t{}\t{}\t.\t.\t{}\t{}\n'
            line = line.format(chrom, region.start, region.end, region.strand,
                               event.id)
            try:
                bed_lines[region_name].append(line)
            except KeyError:
                bed_lines[region_name] = [line]

    fpaths = {}
    for out_prefix, liftover_fpath in liftover_fpaths.items():
        for region_name, bedlines in bed_lines.items():
            fpath = '{}.bed'.format(region_name)
            fhand = open(fpath, 'w')
            for line in bedlines:
                fhand.write(line)
            fhand.close()

            out_fpath = '{}.{}.bed'.format(out_prefix, region_name)
            unmapped_fhand = NamedTemporaryFile(suffix='.bed')

            cmd = ['liftOver', fhand.name, liftover_fpath, out_fpath,
                   unmapped_fhand.name]
            if verbose:
                sys.stderr.write(' '.join(cmd) + '\n')
            check_call(cmd)
            try:
                fpaths[out_prefix].append(out_fpath)
            except KeyError:
                fpaths[out_prefix] = [out_fpath]
    return fpaths
