#!/usr/bin/env python
import re

import RNA


def count_changes_types(changes, ancestral_seq, classify_change_funct):
    effects = {}
    change_pos = 0
    for change, ancestral_nc in zip(changes, ancestral_seq):
        if (change is None or
                (change[0] != ancestral_nc and change[1] != ancestral_nc)):
            continue
        if change[0] != ancestral_nc:
            change = change[1], change[0]
        effect = classify_change_funct(change, ancestral_seq, change_pos)
        try:
            effects[effect] += 1
        except KeyError:
            effects[effect] = 1
        change_pos += 1
    return effects


def classify_ag_decrease(change, ancestral_seq, change_pos):
    new_seq = ''.join([nc if j != change_pos else change[1]
                       for j, nc in enumerate(ancestral_seq)])
    if ancestral_seq.count('AG') < new_seq.count('AG'):
        return 'decreaseAG'
    else:
        return 'otherAG'


def classify_pairing_decrease(change, ancestral_seq, change_pos):
    pre_free = RNA.fold(ancestral_seq.replace('T', 'U'))[0].count('.')
    new_seq = ''.join([nc if j != change_pos else change[1]
                       for j, nc in enumerate(ancestral_seq)])
    post_free = RNA.fold(new_seq.replace('T', 'U'))[0].count('.')
    if pre_free < post_free:
        return 'decPair'
    elif pre_free > post_free:
        return 'incPair'
    else:
        return 'eqPair'


def classify_gc_change(change, ancestral_seq, change_pos):
    if change[0] in 'GC':
        first = 'GC'
    else:
        first = 'AT'
    if change[1] in 'GC':
        last = 'GC'
    else:
        last = 'AT'
    return '{}->{}'.format(first, last)


def classify_to_gc_change(change, ancestral_seq, change_pos):
    return 'toGC' if change[1] in 'GC' else 'otherGC'


def classify_to_t_change(change, ancestral_seq, change_pos):
    return 'toT' if change[1] == 'T' else 'otherT'


def classify_pyrimidine_change(change, ancestral_seq, change_pos):
    if change[1] in 'UCT':
        return 'toPyr'
    else:
        return 'otherPyr'


def classify_mfe_change(change, ancestral_seq, change_pos):
    pre_mfe = RNA.fold(ancestral_seq.replace('T', 'U'))[1]
    new_seq = ''.join([nc if j != change_pos else change[1]
                       for j, nc in enumerate(ancestral_seq)])
    post_mfe = RNA.fold(new_seq.replace('T', 'U'))[1]
    if post_mfe < pre_mfe:
        return 'stabilizing'
    elif post_mfe > pre_mfe:
        return 'de-stabilizing'
    else:
        return 'stable'


GQUAD_REGEXP = '(G{3,}[ATGCU]{1,7}){3,}G{3,}|(G{2,}[ATGCU]{1,7}){3,}G{2,}'
GQUADRUPLEX = re.compile(GQUAD_REGEXP)


def _get_Gquad_counts(seq):
    matches = [m.start() for m in re.finditer(GQUADRUPLEX, seq)]
    return len(matches)


def classify_Gquad_change(change, ancestral_seq, change_pos):
    gquad_counts = _get_Gquad_counts(ancestral_seq)
    new_seq = ''.join([nc if j != change_pos else change[1]
                       for j, nc in enumerate(ancestral_seq)])
    new_gquad_counts = _get_Gquad_counts(new_seq)
    if new_gquad_counts > gquad_counts:
        return 'Gquad+'
    elif new_gquad_counts < gquad_counts:
        return 'Gquad-'
    else:
        return 'Gquad='
