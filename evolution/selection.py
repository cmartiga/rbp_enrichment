import os
from subprocess import check_call
import sys

from evolution.changes import count_changes_types
from seq.intervals import GenomeInterval, rebuild_ancestral_seq


def fetch_polymorphism_changes(interval, vcf_index, ref_fastafile,
                               classify_changes_functions=[],
                               only_snps=True, only_biallelic=True,
                               min_maf=None, af_field='MAF',
                               variant_type_field='TSA', save_seq=True,
                               window_size=None):
    if not hasattr(interval, 'seq') or interval.seq is None:
        interval.get_sequence_from_fastafile(ref_fastafile)
    ob = only_biallelic
    vtf = variant_type_field
    pol_changes = interval.find_polymorphism_changes(vcf_index,
                                                     only_snps=only_snps,
                                                     only_biallelic=ob,
                                                     min_maf=min_maf,
                                                     af_field=af_field,
                                                     variant_type_field=vtf)
    ancestor = interval.rebuild_ancestral_seq(pol_changes)
    for pos, pol_change in enumerate(pol_changes):
        if window_size is None:
            pol_ancestor = ancestor
        else:
            start = pos - window_size
            if start < 0:
                start = 0
            end = pos + window_size
            if end > len(ancestor):
                end = len(ancestor)
            pos = pos - start
            pol_ancestor = ancestor[start:end]

        if pol_change is None:
            continue
        change_data = {'change_type': 'polymorphism', 'AF': pol_change[2],
                       'change': (pol_change[0], pol_change[1])}
        if save_seq:
            change_data['seq'] = pol_ancestor
            change_data['pos'] = pos
        else:
            change_data['seq'] = None
            change_data['pos'] = None
        for i, classify_funct in enumerate(classify_changes_functions):
            name = 'change{}'.format(i)
            change_data[name] = classify_funct(pol_change, pol_ancestor,
                                               pos)
        yield change_data


def fetch_divergence_changes(interval, liftover, ref_fastafile, alt_fastafile,
                             outgroup_liftover, outgroup_fastafile,
                             classify_changes_functions=[], min_lo_score=None,
                             verbose=False, save_seq=True, window_size=None):
    # TODO: split function for this to work only with sequences and another function to extract sequences before
    if not hasattr(interval, 'seq') or interval.seq is None:
        interval.get_sequence_from_fastafile(ref_fastafile)

    div_seqs = interval.find_divergence_seqs(liftover, alt_fastafile,
                                             outgroup_liftover,
                                             outgroup_fastafile, min_lo_score)
    div_changes = interval.find_divergence_changes(div_seqs)
    if div_changes is None or div_changes == []:
        if verbose:
            msg = 'Divergence could not be calculated from liftOver'
            sys.stderr.write(msg)
        return None

    ancestor = rebuild_ancestral_seq(div_seqs[0], div_changes)
    changes = []
    for pos, div_change in enumerate(div_changes):
        if div_change is None:
            continue

        if window_size is None:
            div_ancestor = ancestor
        else:
            start = pos - window_size
            if start < 0:
                start = 0
            end = pos + window_size
            if end > len(ancestor):
                end = len(ancestor)
            pos = pos - start
            div_ancestor = ancestor[start:end]

        change_data = {'change_type': 'divergence', 'AF': None,
                       'change': (div_change[0], div_change[1])}
        if save_seq:
            change_data['seq'] = div_ancestor
            change_data['pos'] = pos
        else:
            change_data['seq'] = None
            change_data['pos'] = None
        for i, classify_funct in enumerate(classify_changes_functions):
            name = 'change{}'.format(i)
            change_data[name] = classify_funct(div_change, div_ancestor,
                                               pos)
        changes.append(change_data)
    return changes


def find_polymorphism_divergence_changes(event, vcf_index, liftover,
                                         ref_fastafile, alt_fastafile,
                                         outgroup_liftover,
                                         outgroup_fastafile,
                                         classify_changes_functions,
                                         only_snps=True, only_biallelic=True,
                                         min_maf=None, min_lo_score=None,
                                         verbose=False, af_field='MAF',
                                         variant_type_field='TSA',
                                         skip_div=False, save_seq=True,
                                         window_size=None):

    '''It is meant to return iteratively al the changes found in the different
       regions of a gene or splicing event. However if there is no change in a
       sequence no change will be returned for it, the same way as if liftover
       failed to return a compatible sequence for divergence calculations.
       Thus it is useful when the total number of changes is not important,
       such as when comparing allele frequencies of different types rather than
       ratios polymorphism/divergence'''

    if verbose:
        sys.stderr.write('Analysing event {}\n'.format(event.id))

    vt_field = variant_type_field
    for region_name, region in event.regions.items():
        if not isinstance(region, list):
            region = [region]
        for interval in region:
            pol_changes = fetch_polymorphism_changes(interval, vcf_index,
                                                     ref_fastafile,
                                                     classify_changes_functions,
                                                     only_snps=only_snps,
                                                     only_biallelic=only_biallelic,
                                                     min_maf=min_maf,
                                                     af_field=af_field,
                                                     variant_type_field=vt_field,
                                                     save_seq=save_seq,
                                                     window_size=window_size)
            for change_data in pol_changes:
                change_data['event_id'] = event.id
                change_data['region'] = region_name
                yield change_data
            if skip_div:
                continue
            div_changes = fetch_divergence_changes(interval, liftover,
                                                   ref_fastafile,
                                                   alt_fastafile,
                                                   outgroup_liftover,
                                                   outgroup_fastafile,
                                                   classify_changes_functions,
                                                   min_lo_score=None,
                                                   verbose=False,
                                                   save_seq=save_seq,
                                                   window_size=window_size)
            if div_changes is None:
                continue
            for change_data in div_changes:
                change_data['event_id'] = event.id
                change_data['region'] = region_name
                yield change_data


def get_changes_index(changes_records, additional_change_type=None):
    changes_idx = {}
    for record in changes_records:
        event_id = record['event_id']
        region = record['region']
        change_type = record['change_type']
        try:
            af = float(record['AF'].split(',')[0])
        except:
            print record
            continue
        if additional_change_type is not None:
            change_type2 = record[additional_change_type]
            try:
                try:
                    try:
                        changes_idx[event_id][region][change_type][change_type2].append(af)
                    except KeyError:
                        changes_idx[event_id][region][change_type] = {change_type2: [af]}
                except KeyError:
                    changes_idx[event_id][region] = {change_type: {change_type2: [af]}}
            except KeyError:
                    changes_idx[event_id] = {region: {change_type: {change_type2: [af]}}}
        else:
            try:
                try:
                    changes_idx[event_id][region][change_type].append(af)
                except KeyError:
                    changes_idx[event_id][region] = {change_type: [af]}
            except KeyError:
                    changes_idx[event_id] = {region: {change_type: [af]}}
    return changes_idx


def compare_polymophism_divergence_changes(event, vcf_index, ref_fastafile,
                                           liftover=None, alt_fastafile=None,
                                           outgroup_liftover=None,
                                           outgroup_fastafile=None,
                                           classify_changes_functions=[],
                                           only_snps=True, only_biallelic=True,
                                           min_maf=None, min_lo_score=None,
                                           verbose=False, af_field='MAF',
                                           variant_type_field='TSA'):

    '''This function is similar to the previous ones, but it is necessary for
       another strategy. The others return directly a generator of changes
       with the region where they were found. However, there is no way to tell
       whether a region was not considered or if no changes were found. This
       event centered approach directly counts the different changes and skips
       events with no homologous sequences found'''

    for region_name, region in event.regions.items():
        if verbose:
            sys.stderr.write('Analyzing event {}\n'.format(event.id))
        record_data = {'event_id': event.id, 'region': region_name,
                       'length': region.length}
        region.get_sequence_from_fastafile(ref_fastafile)
        region.event_id = event.id
        region.region = region_name
        ob = only_biallelic
        vtf = variant_type_field
        pol_changes = region.find_polymorphism_changes(vcf_index,
                                                       only_snps=only_snps,
                                                       only_biallelic=ob,
                                                       min_maf=min_maf,
                                                       af_field=af_field,
                                                       variant_type_field=vtf)
        pol_ancestor = rebuild_ancestral_seq(region.seq, pol_changes)

        if liftover is not None:
            div_seqs = region.find_divergence_seqs(liftover, alt_fastafile,
                                                   outgroup_liftover,
                                                   outgroup_fastafile,
                                                   min_lo_score)
        else:
            if (not hasattr(region, 'closest_seq') or
                    not hasattr(region, 'outgroup_seq')):
                msg = 'No seqs from other species stored in region. '
                msg += 'Try to use liftover tool or either provide '
                msg += 'closest_seq and outgroup_seq as attribute'
                raise ValueError(msg)

            div_seqs = region.seq, region.closest_seq, region.outgroup_seq

        div_changes = region.find_divergence_changes(div_seqs)

        if div_changes is None:
            if verbose:
                msg = 'Divergence could not be calculated from liftOver'
                msg += ' for {} in {}\n'.format(event.id, region_name)
                sys.stderr.write(msg)
            continue
        if div_changes == []:
            if verbose:
                msg = 'No sequence could be fetched for {} in {} after'
                msg += ' liftover\n'
                sys.stderr.write(msg.format(event.id, region_name))
            continue
        div_ancestor = rebuild_ancestral_seq(region.seq, div_changes)
        record_data['pol.changes'] = {i: change
                                      for i, change in enumerate(pol_changes)
                                      if change is not None}
        record_data['div.changes'] = {i: change
                                      for i, change in enumerate(div_changes)
                                      if change is not None}
        record_data['pol.total'] = len([x for x in pol_changes
                                        if x is not None])
        record_data['div.total'] = len([x for x in div_changes
                                        if x is not None])
        for classify_changes_funct in classify_changes_functions:
            div_effects = count_changes_types(div_changes, div_ancestor,
                                              classify_changes_funct)
            for key, value in div_effects.items():
                record_data['div.{}'.format(key)] = value
            pol_effects = count_changes_types(pol_changes, pol_ancestor,
                                              classify_changes_funct)
            for key, value in pol_effects.items():
                record_data['pol.{}'.format(key)] = value
        yield record_data


def find_conserved_strc_rnaz(splicing_events, liftover_idxs, ref_fastafile,
                             liftover_fastafiles, species_names,
                             merge_regions=None, sel_regions=None,
                             min_species=3):
    for event in splicing_events:
        event_id = event.id
        event_seqs = {}
        print 'event_id', event_id
        for region_name, region in event.regions.items():
            if sel_regions is not None and region_name not in sel_regions:
                continue
            seqs = {}
            seq = region.get_sequence_from_fastafile(ref_fastafile)
            if seq:
                seqs['ref'] = seq
            region.region = region_name
            region.event_id = event_id
            for liftover_prefix, liftover_idx in liftover_idxs.items():
                coords = region.liftover(liftover_idx)
                if coords is None:
                    continue
                chrom, start, end, strand = coords
                interval = GenomeInterval(chrom, start, end, strand)
                fasta_file = liftover_fastafiles[liftover_prefix]
                seq = interval.get_sequence_from_fastafile(fasta_file)
                if not seq:
                    continue
                seqs[liftover_prefix] = seq
            event_seqs[region_name] = seqs

        if merge_regions is not None:
            new_event_seqs = {}
            for region1, region2 in merge_regions:
                new_region = '{}|{}'.format(region1, region2)
                new_region_seqs = {}
                for species in species_names:
                    try:
                        new_seq = event_seqs[region1][species]
                        new_seq += event_seqs[region2][species]
                    except KeyError:
                        continue
                    new_region_seqs[species] = new_seq
                if len(new_region_seqs) < min_species:
                    continue
                new_event_seqs[new_region] = new_region_seqs
            if not new_event_seqs:
                continue
        else:
            new_event_seqs = event_seqs

        for region_name, seqs in new_event_seqs.items():
            if len(seqs) < min_species:
                continue
            # Fetch sequences from coordinates
            fasta_fpath = '{}.{}.fasta'.format(event_id, region_name)
            fasta_fhand = open(fasta_fpath, 'w')
            for name, seq in seqs.items():
                fasta_fhand.write('>{}\n{}\n'.format(name, seq))
            fasta_fhand.flush()

            # Realign using clustalW
            clustal_out = '{}.{}.clustal.stdout'.format(event_id, region_name)
            clustal_out_fhand = open(clustal_out, 'w')
            cmd = ['clustalw', fasta_fhand.name]
            check_call(cmd, stdout=clustal_out_fhand, stderr=clustal_out_fhand)
            clustal_out_fhand.close()
            clustal_fpath = '{}.{}.aln'.format(event_id, region_name)

            # Run RNAz
            rnaz_fpath = '{}.{}.rnaz'.format(event_id, region_name)
            cmd = ['RNAz', clustal_fpath, '-o', rnaz_fpath, '-d', '-n']
            check_call(cmd)
            os.remove(fasta_fpath)
            os.remove(clustal_fpath)
            os.remove(clustal_out)
            os.remove(clustal_fpath.rstrip('aln') + 'dnd')
