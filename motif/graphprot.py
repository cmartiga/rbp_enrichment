#!/usr/bin/env python

import itertools
from subprocess import check_call
from tempfile import NamedTemporaryFile

from pybedtools.bedtool import BedTool
from pysam import FastaFile

from motif.matrix import read_matrix
from seq.events import fetch_splicing_events, GenomeInterval


def train_database(db_bed_fhand, gene_coords, chrom_sizes_fpath,
                   ref_fastafile, n_shuffle=1000, verbose=False,
                   out_prefix=None, expand_size=150):
    if out_prefix is None:
        out_prefix = 'graphprot'
    db = {}
    for line in db_bed_fhand:
        items = line.strip().split()
        chrom, start, end, strand = items[:4]
        start, end = int(start), int(end)
        names = items[4].split(',')
        for name in names:
            try:
                db[name].append(GenomeInterval(chrom, start, end, strand))
            except KeyError:
                db[name] = [GenomeInterval(chrom, start, end, strand)]

    models = {}
    for name, intervals in db.items():
        if verbose:
            print 'RBP', name
        if name in ['AGO', 'SRSF1', 'TDP-43']:
            continue
        pos_intervals = intervals
        neg_intervals = list(shuffle_intervals(intervals, gene_coords,
                                               chrom_sizes_fpath,
                                               n_intervals=n_shuffle))
        prefix = out_prefix + '.' + name
        model_fpath = train(pos_intervals, neg_intervals, ref_fastafile,
                            expand_size, out_prefix=prefix)
        models[name] = model_fpath


def shuffle_intervals(intervals, gene_coords, chrom_sizes_fpath,
                      seed=None, n_intervals=1):
    gene_coords = gene_coords.values()
    genes_bedlines = '\n'.join(['{}\t{}\t{}\t{}'.format(x['chrom'], x['start'],
                                                        x['end'], x['strand'])
                                for x in gene_coords])
    genes_bed = BedTool(genes_bedlines, from_string=True)

    bedlines = '\n'.join([interval.bed for interval in intervals])
    intervals_bed = BedTool(bedlines, from_string=True)

    genes_with_intervals = genes_bed.intersect(intervals_bed, wa=True, s=True)
    genes_with_intervals_fhand = NamedTemporaryFile(mode='w')
    for interval in genes_with_intervals:
        line = '{}\t{}\t{}\t{}\n'.format(interval.chrom, interval.start,
                                         interval.end, interval.strand)
        genes_with_intervals_fhand.write(line)
    genes_with_intervals_fhand.flush()

    i = 0
    stop = False
    while True:
        incl_fpath = genes_with_intervals_fhand.name
        if seed is None:
            shuffled = intervals_bed.shuffle(incl=incl_fpath,
                                             g=chrom_sizes_fpath)
        else:
            shuffled = intervals_bed.shuffle(incl=incl_fpath,
                                             g=chrom_sizes_fpath, seed=seed)
        for bedline in shuffled.subtract(intervals_bed, A=True, s=True):
            items = bedline.__str__().strip().split()
            yield GenomeInterval(items[0], int(items[1]), int(items[2]),
                                 items[3])
            i += 1
            if i >= n_intervals:
                stop = True
                break
        if stop:
            break


def shuffle_intervals2(intervals, gene_coords, chrom_sizes_fpath,
                       seed=None, n_iter=1):

    # Write bedfiles
    gene_coords = gene_coords.values()
    genes_bedlines = '\n'.join(['{}\t{}\t{}\t{}'.format(x['chrom'], x['start'],
                                                        x['end'], x['strand'])
                                for x in gene_coords])
    genes_fhand = NamedTemporaryFile(mode='w', suffix='.bed')
    genes_fhand.write(genes_bedlines)
    genes_fhand.flush()

    bedlines = '\n'.join([interval.bed for interval in intervals])
    intervals_fhand = NamedTemporaryFile(mode='w', suffix='.bed')
    intervals_fhand.write(bedlines)
    intervals_fhand.flush()

    # Find genes with at least one overlapping interval
    genes_with_intervals_fhand = NamedTemporaryFile(mode='w')
    cmd = ['bedtools', 'intersect', '-a', genes_fhand.name,
           '-b', intervals_fhand.name, '-wa']
    check_call(cmd, stdout=genes_with_intervals_fhand)

    # Shuffle file
    shuffled_fhand = NamedTemporaryFile(mode='w')
    cmd = ['bedtools', 'shuffle', '-i', intervals_fhand.name,
           '-incl', genes_with_intervals_fhand.name,
           '-g', chrom_sizes_fpath]
    if seed is not None:
        cmd.extend(['-seed', str(seed)])
    check_call(cmd, stdout=shuffled_fhand)

    # Remove shuffled intervals overlapping with the original
    final_fhand = NamedTemporaryFile(mode='w')
    cmd = ['bedtools', 'subtract', '-a', shuffled_fhand.name,
           '-b', intervals_fhand.name, '-A']
    check_call(cmd, stdout=final_fhand)

    for line in open(final_fhand.name):
        yield line


def train(pos_intervals, neg_intervals, ref_fastafile, expand_size=150,
          out_prefix=None):
    if out_prefix is None:
        out_prefix = 'out_graphprot'

    seqs = {'pos': [], 'neg': []}
    for group, intervals in [('pos', pos_intervals), ('neg', neg_intervals)]:
        for interval in intervals:
            if expand_size > 0:
                interval.expand(expand_size)
            seq = interval.get_sequence_from_fastafile(ref_fastafile)
            if seq:
                seqs[group].append(seq)

    pos_fasta = NamedTemporaryFile(mode='w', suffix='.fasta')
    for i, seq in enumerate(seqs['pos']):
        pos_fasta.write('>seq{}\n{}\n'.format(i, seq))

    neg_fasta = NamedTemporaryFile(mode='w', suffix='.fasta')
    for i, seq in enumerate(seqs['neg']):
        neg_fasta.write('>seq{}\n{}\n'.format(i, seq))

    cmd = ['GraphProt.pl', '-mode', 'classification', '-action', 'train',
           '-prefix', out_prefix, '-fasta', pos_fasta.name, '-negfasta',
           neg_fasta.name]
    check_call(cmd)
    return out_prefix + '.model'


def train_splicing_events(splicing_events, grouping, ref_fastafile, out_prefix,
                          expand_size=150):
    intervals = {}
    groups = {}
    for splicing_event in splicing_events:
        event_id = splicing_event.id
        for region_name, region in splicing_event.regions.items():
            try:
                intervals[region_name].append(region)
                groups[region_name].append(grouping.ix[event_id, 0])
            except KeyError:
                intervals[region_name] = [region]
                groups[region_name] = [grouping.ix[event_id, 0]]

    models_fpaths = []
    for region_name, intervals in intervals.items():
        region_groups = groups[region_name]
        for group1, group2 in itertools.combinations(set(region_groups), 2):
            prefix = out_prefix + '.{}.{}_vs_{}'.format(region_name, group1,
                                                        group2)
            pos_intervals = []
            neg_intervals = []
            for group, interval in zip(region_groups, intervals):
                if group == group1:
                    pos_intervals.append(interval)
                elif group == group2:
                    neg_intervals.append(interval)
            model = train(pos_intervals, neg_intervals,
                          ref_fastafile=ref_fastafile, expand_size=expand_size,
                          out_prefix=prefix)
            models_fpaths.append(model)


if __name__ == '__main__':
    events_fpath = '/home/cmarti/new_pipeline_test/inclusion.tab.sel.sorted'
    fasta_fpath = '/home/cmarti/genomes_and_annotations/mm9.genome/mm9.genome.fa'
    grouping_fpath = '/home/cmarti/new_pipeline_test/grouping.sorted.sel'
    out_prefix = '/home/cmarti/new_pipeline_test/graphprot/test2'

    ref_fastafile = FastaFile(fasta_fpath)
    grouping = read_matrix(open(grouping_fpath))
    events = fetch_splicing_events(events_fpath, file_format='vast',
                                   exon_window_size=40, intron_window_size=50,
                                   take_windows=True)

    train_splicing_events(events, grouping, ref_fastafile, out_prefix)





