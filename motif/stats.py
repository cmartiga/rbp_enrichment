#!/usr/bin/env python

from _functools import partial
import itertools

import numpy
from pandas import DataFrame
from scipy.spatial.distance import pdist, squareform
from scipy.stats import mannwhitneyu

from statistics.base import padjust


def calc_distance_to_end(motif_profiles_matrix, reverse=False):
    if reverse:
        motif_profiles_matrix = numpy.fliplr(motif_profiles_matrix)

    without_motifs = numpy.nansum(motif_profiles_matrix, axis=1) == 0
    distance = numpy.argmax(motif_profiles_matrix, axis=1).astype(float)
    distance[without_motifs] = numpy.nan
    return distance


def calc_distances_to_ends_df(motif_profiles_dfs, regions_labels):
    distances_df = DataFrame()
    for df, label in zip(motif_profiles_dfs, regions_labels):
        just = label.split('.')[1]
        if just == 'r':
            reverse = True
        elif just == 'l':
            reverse = False
        else:
            msg = 'Aligning information could not be guessed from region label'
            raise ValueError(msg)
        distances = calc_distance_to_end(df.as_matrix(), reverse=reverse)
        distances_df[label] = distances
    distances_df.index = motif_profiles_dfs[0].index
    return distances_df


def pairwise_mannwhitneyu(df, grouping_df):
    group_col = list(grouping_df.columns)[0]
    if grouping_df is None and group_col not in df.columns:
        raise ValueError('Grouping information is required')
    grouping = grouping_df.loc[df.index, :].as_matrix().flatten()
    matrix = df.as_matrix()
    colnames = df.columns

    res = DataFrame()
    groups = numpy.unique(grouping)
    for i in xrange(matrix.shape[1]):
        column_pvalues = []
        comparisons = []
        for group1, group2 in itertools.combinations(groups, 2):
            comparisons.append('{}_vs_{}'.format(group1, group2))
            sel_rows1 = grouping == group1
            values1 = matrix[sel_rows1, i]
            sel_rows2 = grouping == group2
            values2 = matrix[sel_rows2, i]
            if values1.shape[0] > 0 and values2.shape[0] > 0:
                _, pvalue = mannwhitneyu(values1, values2)
            else:
                pvalue = numpy.nan
            column_pvalues.append(pvalue)
        res[colnames[i]] = column_pvalues
    res.index = comparisons
    return res


def calc_cum_distib(motif_profiles_matrix, reverse=False):
    if reverse:
        motif_profiles_matrix = numpy.fliplr(motif_profiles_matrix)

    xs = calc_distance_to_end(motif_profiles_matrix)
    cum_distrib = numpy.zeros(motif_profiles_matrix.shape[1])
    for row_number in range(motif_profiles_matrix.shape[0]):
        idx = xs[row_number]
        if not numpy.isnan(idx):
            cum_distrib[idx] += 1
    cum_distrib = numpy.cumsum(cum_distrib)
    cum_distrib = cum_distrib / motif_profiles_matrix.shape[0]
    if reverse:
        cum_distrib = numpy.fliplr([cum_distrib])[0]
    return cum_distrib


def calc_motif_conservation(motif_profile_df, cons_profile_df):
    events_ids = numpy.intersect1d(motif_profile_df.index,
                                   cons_profile_df.index)
    motif_numpy = motif_profile_df.loc[events_ids, :].as_matrix()
    cons_numpy = cons_profile_df.loc[events_ids, :].as_matrix()

    with_motifs = motif_numpy.sum(axis=1) > 0
    with_cons = numpy.all(numpy.logical_not(numpy.isnan(cons_numpy)), axis=1)
    select = numpy.logical_and(with_motifs, with_cons)

    motif_numpy = motif_numpy[select, :]
    cons_numpy = cons_numpy[select, :]
    events_ids = events_ids[select]

    motif_cons = cons_numpy.copy()
    no_motif_cons = cons_numpy.copy()
    missing = numpy.logical_or(numpy.isnan(cons_numpy),
                               numpy.isnan(motif_numpy))

    motif_cons[motif_numpy == 0] = numpy.nan
    motif_cons[missing] = numpy.nan

    no_motif_cons[motif_numpy > 0] = numpy.nan
    no_motif_cons[missing] = numpy.nan

    motif_cons = numpy.nanmean(motif_cons, axis=1)
    no_motif_cons = numpy.nanmean(no_motif_cons, axis=1)

    res = DataFrame({'Motif': motif_cons, 'No motif': no_motif_cons},
                    index=events_ids)
    return res


def weigthed_hamming(array1, array2, weights=None):
    if weights is None:
        weights = 1
        total = array1.shape[0]
    else:
        total = float(weights.sum())
    distance = (array1 != array2).astype('float') * weights
    result = numpy.nansum(distance) / total
    return result


def calc_motifs_hamming_similarity(enrichment_df, counts_df,
                                   padj_threshold=None, weighted=True,
                                   by_regulator=False,
                                   expression_df=None,
                                   sel_events=None, combine=False):
    enrichment_df['fdr'] = padjust(enrichment_df['pvalue'])
    if padj_threshold is not None:
        plim = padj_threshold
        enrichment_df = enrichment_df.loc[enrichment_df['fdr'] < plim, :]
        sel_regs = enrichment_df.index
        counts_df = counts_df.loc[:, sel_regs]

    if sel_events is not None:
        if by_regulator:
            sel_events = set(sel_events)
            sel_events = [colname.split('.') in sel_events
                          for colname in counts_df.columns]
            counts_df = counts_df.loc[:, sel_events]
        else:
            common_events = numpy.intersect1d(sel_events, counts_df.index)
            counts_df = counts_df.loc[common_events, :]

    if by_regulator:
        counts_df = counts_df.transpose()

    presence = (counts_df > 0).as_matrix()
    names = counts_df.index

    weights = None
    if weighted:
        if by_regulator:
            msg = 'Option to calculate weighted distance for enriched motifs'
            msg += ' is yet to be implemented. It will require a z-score for '
            msg += 'dPSI as event weights'
            raise NotImplementedError(msg)
        else:
            weights = -numpy.log10(enrichment_df['fdr']).as_matrix()
            rbp_names = numpy.array([rowname.split('.')[0]
                                     for rowname in enrichment_df.index])
            if expression_df is not None:
#                 weights_2 = -numpy.log(expression_df.ix[rbp_names, 'padj'])
                zscore = expression_df.ix[:, 'log2FoldChange'] / numpy.std(expression_df.ix[:, 'log2FoldChange'])
                weights_2 = zscore.loc[rbp_names]

                weights_2 = numpy.abs(weights_2.as_matrix())
                if combine:
                    weights = weights * weights_2
                else:
                    weights = weights_2
                weights[numpy.isnan(weights)] = 0

    _weigthed_hamming = partial(weigthed_hamming, weights=weights)
    similarity = 1 - squareform(pdist(presence, metric=_weigthed_hamming))
    return DataFrame(similarity, index=names, columns=names)
