#!/usr/bin/env python

import numpy
from pandas import DataFrame

from seq.events import GenomeInterval
from statistics.base import padjust, EmpiricalPvalueCalculator
from utils.settings import CISBP_RNA, RNAMOTIF


def _motif_in_seq_with_missmatches(motif, seq, max_missmatches=0):
    if max_missmatches == 0:
        return motif in seq
    motif_len = len(motif)
    for i in range(len(seq) - motif_len + 1):
        sub_seq = seq[i:i + motif_len]
        n_missmatches = 0
        for base1, base2 in zip(motif, sub_seq):
            if base1 != base2:
                n_missmatches += 1
            if n_missmatches > max_missmatches:
                break
        else:
            return True
    return False


class MotifCounter(object):
    def __init__(self, db_type, max_missmatches=0, pval_cutoff=0.05,
                 pwm_random_scores=None, multiple_test_correction=False):
        self.motif_type = db_type
        self.max_missmatches = max_missmatches
        self.pval_cutoff = pval_cutoff
        self.pwm_random_scores = pwm_random_scores
        if db_type == CISBP_RNA and pwm_random_scores is None:
            raise ValueError('A distribution of scores is required')
        self.multiple_test_correction = multiple_test_correction

    def _rnamotif_count(self, motif, seq):
        counts = 0
        motif_seqs = motif['seq']
        if self.max_missmatches == 0:
            for motif_seq in motif_seqs:
                counts += seq.count(motif_seq)
            return counts
        for motif_seq in motif_seqs:
            motif_len = len(motif_seq)
            for i in xrange(len(seq) - motif_len + 1):
                window = seq[i: i + motif_len]
                if _motif_in_seq_with_missmatches(motif_seq, window,
                                                  self.max_missmatches):
                    counts += 1
        return counts

    def get_cisbprna_pvalues(self, motif, seq):
        motif_name, pwm = motif
        motif_scores = self.pwm_random_scores[motif_name]
        calc_empirical_pvalue = EmpiricalPvalueCalculator(motif_scores)
        motif_len = len(pwm)
        pvalues_seq = []
        for i in range(len(seq) - motif_len + 1):
            kmer = seq[i: i + motif_len]
            score = sum([pwm[j].get(nc, 0) for j, nc in enumerate(kmer)])
            pvalue = calc_empirical_pvalue(score)
            pvalues_seq.append(pvalue)
        if self.multiple_test_correction:
            pvalues_seq = padjust(pvalues_seq, 'fdr')
        return pvalues_seq

    def _cisbprna_count(self, motif, seq):
        pvalues_seq = self.get_cisbprna_pvalues(motif, seq)
        counts = len([pvalue for pvalue in pvalues_seq
                      if pvalue < self.pval_cutoff])
        return counts

    def __call__(self, motif, seq):
        if self.motif_type == RNAMOTIF:
            return self._rnamotif_count(motif, seq)
        elif self.motif_type == CISBP_RNA:
            return self._cisbprna_count(motif, seq)
        else:
            msg = 'Motif type not supported: {}'.format(self.motif_type)
            raise ValueError(msg)


class MotifInSeq(object):
    '''Class that generates a functions to find a motif in a given sequence. It
    can look either for literal consensus in the sequence using the RNAMOTIF
    database type, or use Position Weighted Matrices to look for hits. In the
    last case, it can use a given minimum score to call for motifs, or if a
    random distribution of scores is given it can either return the p-values
    or call presence or absence according to a p-value cutoff'''
    # TODO: add multiple test correction if we want to use it to really find
    # motifs with statistical significance

    def __init__(self, db_type, max_missmatches=0, pval_cutoff=0.05,
                 pwm_random_scores=None, min_score=None, return_pvalues=False,
                 multiple_test_correction=False):
        if return_pvalues and pwm_random_scores is None:
            msg = 'To calculate p-values, scores from random seqs must be '
            msg += 'given'
            raise AttributeError(msg)
        self.db_type = db_type
        self.max_missmatches = max_missmatches
        self.pwm_random_scores = pwm_random_scores
        self.return_pvalues = return_pvalues
        self.pval_cutoff = pval_cutoff
        self.multiple_test_correction = multiple_test_correction
        if db_type == CISBP_RNA:
            if pwm_random_scores is None:
                self.min_score = min_score
                if min_score is None:
                    msg = 'Provide either a minimum score or '
                    msg += 'scores distribution'
                    raise ValueError(msg)
            else:
                if min_score is not None:
                    raise ValueError(msg)
                self.min_score = None

    def _rnamotifs_in_seq(self, motif, seq):
        motifs_seqs = motif['seq']
        for motif_seq in motifs_seqs:
            motif_seq = motif_seq.replace('U', 'T')
            if _motif_in_seq_with_missmatches(motif_seq, seq,
                                              self.max_missmatches):
                return True
        return False

    def _pwm_in_seq(self, motif, seq):
        adjust = self.multiple_test_correction
        motif_count = MotifCounter(db_type=CISBP_RNA,
                                   pwm_random_scores=self.pwm_random_scores,
                                   multiple_test_correction=adjust)
        pvalues = motif_count.get_cisbprna_pvalues(motif, seq)
        for pvalue in pvalues:
            if pvalue <= self.pval_cutoff:
                return True
        return False

    def __call__(self, motif, seq):
        if self.db_type == RNAMOTIF:
            return self._rnamotifs_in_seq(motif, seq)
        elif self.db_type == CISBP_RNA:
            return self._pwm_in_seq(motif, seq)
        else:
            msg = 'Motif database type not supported:{}'.format(self.db_type)
            raise ValueError(msg)


def calc_motif_matrix(parsed_db, seqs, function, db_type=RNAMOTIF):
    matrix = []
    motifs_names = parsed_db.keys()
    seq_names = []
    seq_lengths = []
    for name, seq in seqs:
        seq_result = []
        seq_lengths.append(len(seq.replace('-', '')))
        seq_names.append(name)
        for motif_name, motif in parsed_db.items():
            if db_type == CISBP_RNA:
                motif = (motif_name, motif['PWM'])
            seq_result.append(function(motif, seq))
        matrix.append(seq_result)
    df = DataFrame(matrix, index=seq_names, columns=motifs_names)
    df.seq_length = seq_lengths
    return df


def calc_tabix_counts_matrices(splicing_events, sel_rbps, tabix_index=None,
                               cols_filters=None, rbps_col=4, by_region=True,
                               same_strand=True):
    counts_matrices = {}
    for i, splicing_event in enumerate(splicing_events):
        if i % 1000 == 0:
            print 'Events processed: {}'.format(i)
        event_id = splicing_event.id
        for name, region_intervals in splicing_event.regions.items():

            if not isinstance(region_intervals, list):
                region_intervals = [region_intervals]

            for region in region_intervals:
                if tabix_index is None:
                    region_counts = region.counts
                else:
                    region_counts = region.get_tabix_counts(tabix_index,
                                                            cols_filters,
                                                            rbps_col=rbps_col,
                                                            same_strand=True)
                for rbp in sel_rbps:
                    counts = region_counts.get(rbp, 0)
                    if by_region:
                        try:
                            try:
                                counts_matrices[name][event_id][rbp] = counts
                            except KeyError:
                                counts_matrices[name][event_id] = {rbp: counts}
                        except KeyError:
                            counts_matrices[name] = {event_id: {rbp: counts}}
                    else:
                        try:
                            try:
                                counts_matrices[event_id][rbp] += counts
                            except KeyError:
                                counts_matrices[event_id][rbp] = counts
                        except KeyError:
                            counts_matrices[event_id] = {rbp: counts}
                if by_region and event_id in counts_matrices[name]:
                    counts_matrices[name][event_id]['seq_length'] = region.length
                elif not by_region and event_id in counts_matrices:
                    try:
                        counts_matrices[event_id]['seq_length'] += region.length
                    except KeyError:
                        counts_matrices[event_id]['seq_length'] = region.length

    if not by_region:
        counts_matrices = {'all_regions': counts_matrices}
    dfs = []
    regions = []
    for region, region_counts in counts_matrices.items():
        df = DataFrame.from_dict(data=region_counts, orient='index')
        df['total'] = df.sum(axis=1) - df['seq_length']
        dfs.append(df)
        regions.append(region)
    return regions, dfs


def _calc_regions_max_lengths(splicing_events):
    max_lengths = {}
    for splicing_event in splicing_events:
        for name, region in splicing_event.regions.items():
            try:
                if max_lengths[name] < region.length:
                    max_lengths[name] = region.length
            except KeyError:
                max_lengths[name] = region.length
    return max_lengths


def get_profiles(splicing_events, profile_name, max_lengths=None):
    profiles = {}
    if max_lengths is None:
        splicing_events = list(splicing_events)
        max_lengths = _calc_regions_max_lengths(splicing_events)

    for splicing_event in splicing_events:
        event_id = splicing_event.id
        profiles = {}
        for region_name, region in splicing_event.regions.items():
            max_len = max_lengths[region_name]
            profile = region.get_tabix_profile(profile_name)
            fill_number = max_len - profile.shape[0]
            if fill_number > 0:
                align = region_name.split('.')[1]
                fill = numpy.full(fill_number, numpy.nan, dtype=float)
                if align == 'l':
                    profile = numpy.append(profile, fill)
                elif align == 'r':
                    profile = numpy.append(fill, profile)
                else:
                    msg = 'Error detecting aligning of incomplete profiles'
                    msg += '. Check if it is specified in region name. '
                    msg += 'Use take windows options otherwise'
                    raise ValueError(msg)
            profiles[region_name] = profile
        yield event_id, profiles


def calc_tabix_profiles(splicing_events, rbp):
    profiles = {}
    max_lengths = {}
    for splicing_event in splicing_events:
        event_id = splicing_event.id
        for name, region in splicing_event.regions.items():
            profile = region.get_tabix_profile(rbp)
            try:
                profiles[name][event_id] = profile
            except KeyError:
                profiles[name] = {event_id: profile}
            try:
                if max_lengths[name] < region.length:
                    max_lengths[name] = region.length
            except KeyError:
                max_lengths[name] = region.length

    dfs = []
    regions = []
    for region_name, events_profiles in profiles.items():
        region_profiles = []
        events_ids = []
        max_len = max_lengths[region_name]
        for event_id, event_profile in events_profiles.items():
            fill_number = max_len - event_profile.shape[0]
            if fill_number > 0:
                align = region_name.split('.')[1]
                fill = numpy.full(fill_number, numpy.nan, dtype=float)
                if align == 'l':
                    event_profile = numpy.append(event_profile, fill)
                elif align == 'r':
                    event_profile = numpy.append(fill, event_profile)
                else:
                    msg = 'Error detecting aligning of incomplete profiles'
                    msg += '. Check if it is specified in region name. '
                    msg += 'Use take windows options otherwise'
                    raise ValueError(msg)
            region_profiles.append(event_profile)
            events_ids.append(event_id)
        region_profiles = numpy.vstack(region_profiles)
        region_profiles_df = DataFrame(region_profiles, index=events_ids)
        dfs.append(region_profiles_df)
        regions.append(region_name)

    return regions, dfs


def find_gene_motifs(gene_coords, ref_fastafile, motif_db_by_seq,
                     liftover=None, max_motif_len=None, min_motif_len=None,
                     bw_file=None, verbose=False, is_regexp=False):
    for i, (gene_id, coords) in enumerate(gene_coords.items()):
        if verbose:
            print gene_id, i
        interval = GenomeInterval(coords['chrom'], coords['start'],
                                  coords['end'], coords['strand'],
                                  liftover=liftover)
        interval.get_sequence_from_fastafile(ref_fastafile)
        if bw_file is not None:
            interval.get_bigwig_profile(bw_file, tag='bw', expand=True)
        interval.find_motifs(motif_db_by_seq, max_motif_len, min_motif_len,
                             is_regexp=is_regexp)
        for bed_line in interval.motifs_to_bed():
            yield bed_line
