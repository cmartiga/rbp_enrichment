#!/usr/bin/env python

import itertools

import numpy
from pandas import read_table, concat, DataFrame
from scipy.stats.stats import fisher_exact


def read_matrix(fhand, rownames_col=0, sep='\t', dtype=None):
    df = read_table(fhand, index_col=rownames_col, sep=sep)
    if 'seq_length' in df.columns:
        seq_length = df['seq_length']
        df.drop('seq_length', inplace=True, axis=1)
        df.seq_length = seq_length
    if dtype is not None:
        df = df.astype(dtype=dtype)
    return df


def write_matrix(matrix, fhand, sep='\t', index=True):
    if hasattr(matrix, 'seq_length'):
        matrix['seq_length'] = matrix.seq_length
    matrix.to_csv(fhand, sep=sep, index=index)
    fhand.flush()


def _update_rownames(dfs, labels):
    new_dfs = []
    for df, label in zip(dfs, labels):
        new_df = df.copy()
        new_df.columns = ['{}.{}'.format(col, label) for col in df.columns]
        if hasattr(df, 'seq_length'):
            new_df.seq_length = df.seq_length
        new_dfs.append(new_df)
    return new_dfs


def _fill_lengths(lengths_df):
    if len(lengths_df.shape) == 1:
        return lengths_df
    elif lengths_df.shape[1] == 1:
        return lengths_df[:, 0]
    missing_lengths = numpy.isnan(lengths_df[:, 0])
    lengths_df[missing_lengths, 0] = lengths_df[missing_lengths, 1]
    remainin_cols = [0] + range(2, lengths_df.shape[1])
    return _fill_lengths(lengths_df[:, remainin_cols])


def merge_matrices(matrices, labels=None, axis=1):
    if len(matrices) == 1:
        return matrices[0]
    if labels is not None:
        matrices = _update_rownames(matrices, labels)

    for matrix in matrices:
        if hasattr(matrix, 'seq_length'):
            matrix['seq_length'] = matrix.seq_length

    merged = concat(matrices, axis=axis)
    if 'seq_length' in merged.columns:
        seq_lengths = merged['seq_length']
        seq_lengths = _fill_lengths(numpy.array(seq_lengths))
        merged.drop('seq_length', axis=1, inplace=True)
        merged.seq_length = seq_lengths

    for matrix in matrices:
        if hasattr(matrix, 'seq_length'):
            matrix.drop('seq_length', inplace=True, axis=1)

    return merged


def _combine_colnames(colname1, colname2):
    if '.' in colname1:
        rbp1, region1 = colname1.split('.', 1)
        rbp2, region2 = colname2.split('.', 1)
        return '{}|{}.{}|{}'.format(rbp1, rbp2, region1, region2)
    else:
        return '{}|{}'.format(colname1, colname2)


def _filter_combinations(combinations, colnames, sel_region_pairs,
                         keep_same_regions=False):
    for i, j in combinations:
        region1 = colnames[i].split('.', 1)[1]
        region2 = colnames[j].split('.', 1)[1]
        if sel_region_pairs is None:
            if not keep_same_regions and region1 == region2:
                continue
            yield i, j
        elif ([region1, region2] in sel_region_pairs or
                [region2, region1] in sel_region_pairs):
            yield i, j


def calc_combination_matrix(matrix, chunk_size=None, sel_region_pairs=None,
                            keep_same_regions=True):
    colnames = list(matrix.columns)
    rownames = matrix.index
    matrix_numpy = matrix.as_matrix()
    combi_cols = []
    combi_colnames = []

    combinations = itertools.combinations(range(matrix_numpy.shape[1]), 2)
    combinations = _filter_combinations(combinations, colnames,
                                        sel_region_pairs, keep_same_regions)

    n_combis = 0
    for i, j in combinations:
        n_combis += 1
        col = numpy.logical_and(matrix_numpy[:, i] > 0, matrix_numpy[:, j] > 0)
        combi_cols.append(col)
        combi_colnames.append(_combine_colnames(colnames[i], colnames[j]))

        if chunk_size is not None and n_combis == chunk_size:
            combi_matrix = numpy.vstack(combi_cols).transpose()
            combi_matrix = combi_matrix.astype(int)
            res = DataFrame(combi_matrix, index=rownames,
                            columns=combi_colnames)
            if hasattr(matrix, 'seq_length'):
                res.seq_length = matrix.seq_length

            combi_cols = []
            combi_colnames = []
            n_combis = 0
            yield res

    if chunk_size is None or n_combis != 0:
        combi_matrix = numpy.vstack(combi_cols).transpose()
        combi_matrix = combi_matrix.astype(int)
        res = DataFrame(combi_matrix, index=rownames,
                        columns=combi_colnames)
        if hasattr(matrix, 'seq_length'):
            res.seq_length = matrix.seq_length
        yield res


def test_pairwise_association(matrix, sel_region_pairs=None,
                              keep_same_regions=True):
    colnames = list(matrix.columns)
    matrix_numpy = matrix.as_matrix()

    combinations = itertools.combinations(range(matrix_numpy.shape[1]), 2)
    combinations = _filter_combinations(combinations, colnames,
                                        sel_region_pairs, keep_same_regions)

    for i, j in combinations:
        series1 = matrix.iloc[:, i]
        series2 = matrix.iloc[:, j]
        present1 = numpy.nansum(numpy.logical_and(series1 > 0, series2 > 0))
        absent1 = numpy.nansum(numpy.logical_and(series1 > 0, series2 == 0))
        present2 = numpy.nansum(numpy.logical_and(series1 == 0, series2 > 0))
        absent2 = numpy.nansum(numpy.logical_and(series1 == 0, series2 == 0))
        contingency_table = [[present1, absent1], [present2, absent2]]
        oddsratio, pvalue = fisher_exact(contingency_table,
                                         alternative='greater')

        yield {'id': _combine_colnames(colnames[i], colnames[j]),
               'present1': present1, 'absent1': absent1,
               'present2': present2, 'absent2': absent2,
               'pvalue': pvalue, 'OR': oddsratio}


def rows_are_uniform(matrix):
    return numpy.all(matrix == numpy.transpose([matrix[:, 0]]), axis=1)


def cols_are_uniform(matrix):
    matrix = matrix.transpose()
    return numpy.all(matrix == numpy.transpose([matrix[:, 0]]),
                     axis=1)


def change_ref_coords_profile(profile_numpy, ref_pos_array, sel_len_left,
                              sel_len_right):
    left_profile = []
    right_profile = []
    for row, pos in zip(profile_numpy, ref_pos_array):
        if numpy.isnan(pos):
            left_profile.append(numpy.full(sel_len_left, numpy.nan))
            right_profile.append(numpy.full(sel_len_right, numpy.nan))
            continue
        left, right = row[:pos], row[pos:]
        len_left, len_right = left.shape[0], right.shape[0]

        if len_left < sel_len_left:
            fill_left = numpy.full(sel_len_left - len_left, numpy.nan)
            left = numpy.append(fill_left, left)
        elif len_left > sel_len_left:
            left = left[-sel_len_left:]
        left_profile.append(left)

        if len_right < sel_len_right:
            fill_right = numpy.full(sel_len_right - len_right, numpy.nan)
            right = numpy.append(right, fill_right)
        elif len_right > sel_len_right:
            right = right[:sel_len_right]
        right_profile.append(right)

    left_profile = numpy.vstack(left_profile)
    right_profile = numpy.vstack(right_profile)

    return left_profile, right_profile
