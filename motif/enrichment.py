#!/usr/bin/env python

import itertools

import numpy
from pandas import DataFrame, get_dummies
from statsmodels.genmod.families.links import identity

from statistics.enrichment import ContingencyTable
import statsmodels.api as sm
import statsmodels.formula.api as smf


def _calc_position_dependent_enrichment(profiles_df, grouping_df,
                                        window_size=1, reverse=False):
    grouping = numpy.array(grouping_df.ix[profiles_df.index, 0])
    groups = numpy.unique(grouping)
    profiles = profiles_df.as_matrix()

    or_series = []
    pvalue_series = []
    series_labels = []
    for group1, group2 in itertools.combinations(groups, 2):
        series_labels.append('{}_vs_{}'.format(group1, group2))
        combination_or = []
        combination_pvalue = []
        combination_pos = []
        for idx in range(1, profiles.shape[1], window_size):
            if reverse:
                pos_range = range(profiles.shape[1] - idx, profiles.shape[1])
            else:
                pos_range = range(idx)
            pos_profiles = profiles[:, pos_range]
            sel_idxs = numpy.logical_or(grouping == group1,
                                        grouping == group2)

            sel_counts = pos_profiles.sum(axis=1)[sel_idxs]
            sel_grouping = grouping[sel_idxs]

            contingency_table = ContingencyTable(sel_counts,
                                                 sel_grouping == group1)
            contingency_table.fisher_test()

            combination_or.append(contingency_table.odds_ratio)
            combination_pvalue.append(contingency_table.pvalue)
            combination_pos.append(idx)
        if reverse:
            combination_pvalue = numpy.fliplr([combination_pvalue])[0]
            combination_or = numpy.fliplr([combination_or])[0]
        pvalue_series.append(combination_pvalue)
        or_series.append(combination_or)
    return series_labels, combination_pos, pvalue_series, or_series


def calc_pos_dependent_enrichment_regions(dfs, labels, grouping_df):
    enrichments = []
    for profile_df, region in zip(dfs, labels):
        if region.split('.')[1] == 'r':
            reverse = True
        elif region.split('.')[1] == 'l':
            reverse = False
        enrichment = _calc_position_dependent_enrichment(profile_df,
                                                         grouping_df,
                                                         reverse=reverse)
        enrichments.append(enrichment)
    return enrichments


def poisson_regression(matrix, grouping, is_categorical=True,
                       grouping_name='grouping', additional_covariates=None,
                       by_region=False):
    has_length = False
    for colname in matrix.columns:
        if 'seq_length' in colname:
            has_length = True
            break
    if not has_length:
        msg = 'Seq length field is required to run the model'
        raise ValueError(msg)

    if hasattr(matrix, 'seq_length'):
        matrix['seq_length'] = matrix.seq_length
    covariates = []
    if is_categorical:
        grouping = get_dummies(grouping)
        for column in grouping.columns:
            matrix[column] = numpy.array(grouping[column])
            covariates.append(column)
        # If they are linearly dependent we remove the last dummy variable
        # this state will be represented as the background
        if numpy.sum(grouping).sum() == matrix.shape[0]:
            matrix.drop(column, inplace=True, axis=1)
            covariates.pop()

    else:
        matrix[grouping_name] = grouping
        covariates.append(grouping_name)

    if additional_covariates is not None:
        for column in additional_covariates.columns:
            matrix[column] = additional_covariates[column]
            covariates.append(column)

    result = {}
    matrix.columns = ['m' + x.replace('.', '_') for x in matrix.columns]
    matrix.columns = [x.replace('-', '') for x in matrix.columns]
    covariates = ['m' + cov for cov in covariates]
    for motif in matrix.columns:
        if motif in covariates or 'seq_length' in motif:
            continue
        length_field = 'mseq_length'
        if by_region:
            region_name = motif.split('_', 1)[-1]
            length_field = '{}_{}'.format(length_field, region_name)
        formula = '{}~{}'.format(motif, '+'.join(covariates + [length_field]))
        try:
            model = smf.glm(formula=formula, data=matrix,
                            family=sm.families.Poisson(link=identity)).fit()
        except (ValueError, numpy.linalg.linalg.LinAlgError) as error:
            print '"{}" error ocurred with {}'.format(error, motif)
            continue
        motif_result = {}
        params = ['intercept'] + covariates
        for param, estimate, pvalue in zip(params, model.params,
                                           model.pvalues):
            motif_result['{}.estimate'.format(param)] = estimate
            motif_result['{}.pvalue'.format(param)] = pvalue
        result[motif] = motif_result

    df = DataFrame.from_dict(result, orient='index')
    return df[sorted(df.columns)]
