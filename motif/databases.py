#!/usr/bin/env python

from csv import DictReader
import os

from utils.settings import MOTIF_FIELDNAMES


def read_attract_database(db_fpath, reading_option='gene_name',
                          selected_organisms=None, selected_ids=None):
    '''Returns a dictionary that can use as keys the motif sequence or the gene
    name of the RBP, and as values a list of motifs with such key'''

    if reading_option not in ['gene_name', 'seq']:
        msg = 'Reading option not allowed: {}'.format(reading_option)
        raise ValueError(msg)
    fieldnames = MOTIF_FIELDNAMES['rnamotif']
    db_reader = DictReader(open(db_fpath))
    parsed_db = {}
    for record in db_reader:
        record = {key.split('.')[-1]: value for key, value in record.items()}
        no_organism = (selected_organisms is not None and
                       record['organism'] not in selected_organisms)
        no_motif_ids = (selected_ids is not None and
                        record['id'] not in selected_ids)
        if no_organism or no_motif_ids:
            continue
        key = record[reading_option]
        if reading_option == 'seq':
            key = key.upper().replace('U', 'T')

        try:
            for fieldname in fieldnames:
                if fieldname == 'seq':
                    seq = record[fieldname]
                    parsed_db[key][fieldname].add(seq.replace('U', 'T'))
                else:
                    parsed_db[key][fieldname].add(record[fieldname])
        except KeyError:
            parsed_db[key] = {}
            for fieldname in fieldnames:
                if fieldname == 'seq':
                    seq = record[fieldname]
                    parsed_db[key][fieldname] = set([seq.replace('U', 'T')])
                else:
                    parsed_db[key][fieldname] = set([record[fieldname]])

    return parsed_db


def read_pwm_fhand(fhand):
    reader = DictReader(fhand, delimiter='\t')
    return [{key: float(value) for key, value in record.items()
             if key != 'Pos'} for record in reader]


def read_cisbp_rna_db(database_dir):
    motif_db = {}
    for fname in os.listdir(database_dir):
        fpath = os.path.join(database_dir, fname)
        fhand = open(fpath)
        motif_name = fname.strip('.txt')
        pwm = read_pwm_fhand(fhand)
        if len(pwm) <= 1:
            continue
        motif_db[motif_name] = {'RBP_name': motif_name,
                                'PWM': pwm}
    return motif_db


def read_pwm_background_scores(fhand):
    scores = {}
    for line in fhand:
        items = line.strip().split()
        scores[items[0]] = [float(score) for score in items[1:]]
    return scores
