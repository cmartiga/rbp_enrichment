#!/usr/bin/env python

from os.path import join
import unittest

from pyliftover.liftover import LiftOver
from pysam import FastaFile, Tabixfile

from evolution.selection import compare_polymophism_divergence_changes
from seq.align import clustalo
from seq.events import SplicingEvent
from seq.intervals import GenomeInterval
from utils.settings import TEST_DATA_DIR


class AlignmentTests(unittest.TestCase):
    def test_clustalo(self):
        seqs = ['ATGGCA', 'ATGCCA', 'AGGCA']
        assert clustalo(seqs) == ['ATGGCA', 'ATGCCA', '-AGGCA']


class SelectionTests(unittest.TestCase):
    def test_compare_pol_div_changes(self):
        # splicing event
        event = SplicingEvent()
        event.id = 'event1'
        event.chromosome = 'chr1'
        event.strand = '+'
        event.regions = {'region1': GenomeInterval('chr1', 10, 20, '+')}

        # create genomes and liftover files
        ref_fastafile = FastaFile(join(TEST_DATA_DIR, 'sel1.fasta'))
        alt_fastafile = FastaFile(join(TEST_DATA_DIR, 'sel2.fasta'))
        out_fastafile = FastaFile(join(TEST_DATA_DIR, 'sel3.fasta'))
        vcf = Tabixfile(join(TEST_DATA_DIR, 'sel.vcf.gz'))
        alt_liftover = LiftOver(join(TEST_DATA_DIR, 'Sel1ToSel2.chain.gz'))
        out_liftover = LiftOver(join(TEST_DATA_DIR, 'Sel1ToSel3.chain.gz'))

        res = list(compare_polymophism_divergence_changes(event, vcf,
                                                          alt_liftover,
                                                          ref_fastafile,
                                                          alt_fastafile,
                                                          out_liftover,
                                                          out_fastafile,
                                                          []))[0]
        expected = {'div.changes': {2: ('G', 'C'), 6: ('G', 'C')},
                    'div.total': 2, 'event_id': 'event1', 'region': 'region1',
                    'length': 10, 'pol.changes': {8: ('T', 'C', 0.5)},
                    'pol.total': 1}
        assert res == expected


if __name__ == '__main__':
    import sys;sys.argv = ['', 'AlignmentTests.test_clustalo']
    unittest.main()
