#!/usr/bin/env python

import os
import unittest

from seq.genes import get_gene_features_intervals
from seq.parsers import parse_gtf
from utils.settings import TEST_DATA_DIR


class GeneTests(unittest.TestCase):
    def test_get_gene_features_intervals(self):
        gtf_fpath = os.path.join(TEST_DATA_DIR, 'sample.gtf')
        gtf_fhand = open(gtf_fpath)
        gtf = parse_gtf(gtf_fhand)
        res = list(get_gene_features_intervals(gtf))
        exp_intervals = [(108006, 108028, 'exon'), (108028, 108247, 'CDS'),
                         (108247, 109883, 'Intron'), (109883, 110007, 'CDS'),
                         (110007, 118421, 'Intron'), (118421, 118588, 'CDS'),
                         (118588, 119628, 'Intron'), (119628, 119673, 'CDS'),
                         (119673, 121072, 'Intron'), (121072, 121143, 'CDS'),
                         (121143, 126647, 'Intron'), (126647, 126718, 'CDS'),
                         (126718, 129227, 'Intron'), (129227, 129365, 'CDS'),
                         (129365, 131063, 'Intron'), (131063, 131170, 'CDS'),
                         (131170, 133942, 'Intron'), (133942, 134116, 'CDS'),
                         (134116, 134275, 'Intron'), (134275, 134390, 'CDS'),
                         (134390, 138766, 'Intron'), (138766, 139287, 'CDS'),
                         (139287, 139339, 'exon')]
        assert res == [('ENSG00000237375', 'GL000213.1', '-', exp_intervals)]


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'GeneTests']
    unittest.main()
