#!/usr/bin/env python

from tempfile import NamedTemporaryFile
import unittest

from motif.graphprot import shuffle_intervals
from seq.events import GenomeInterval


class GraphProtTests(unittest.TestCase):
    def test_shuffle_intervals(self):
        intervals = [GenomeInterval('chr1', 10, 40, '+'),
                     GenomeInterval('chr2', 10, 40, '+')]
        gene_coords = {1: {'chrom': 'chr1', 'start': 0, 'end': 150,
                           'strand': '+'}, 2: {'chrom': 'chr3', 'start': 5,
                                               'end': 120, 'strand': '+'}}
        with NamedTemporaryFile('w') as genome_fhand:
            genome_fhand.write('chr1\t200\nchr2\t3000\nchr3\t200')
            genome_fhand.flush()
            res = list(shuffle_intervals(intervals, gene_coords,
                                         chrom_sizes_fpath=genome_fhand.name,
                                         seed=2, n_intervals=10))
            assert len(res) == 10
            assert res[0].chrom == 'chr1'
            assert res[0].start == 48
            assert res[0].end == 78
            assert res[0].strand == '+'


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'GraphProtTests.test_shuffle_intervals']
    unittest.main()
