#!/usr/bin/env python

from tempfile import NamedTemporaryFile
import unittest

import numpy
from pandas import DataFrame

from motif.matrix import read_matrix, write_matrix, merge_matrices, \
    rows_are_uniform, cols_are_uniform, calc_combination_matrix, \
    change_ref_coords_profile


class MotifsTests(unittest.TestCase):
    def test_matrix_io(self):
        counts_df = DataFrame([[2, 1], [0, 1]], index=['seq1', 'seq2'],
                              columns=['motif1', 'motif2'])
        with NamedTemporaryFile(mode='w') as fhand:
            write_matrix(counts_df, fhand)
            fhand = open(fhand.name)
            new_df = read_matrix(fhand)
            assert numpy.all(counts_df == new_df)

    def test_merge(self):
        matrix1 = DataFrame([[2, 1], [0, 1]], index=['seq1', 'seq2'],
                            columns=['motif1', 'motif2'])
        matrix2 = DataFrame([[2, 1], [0, 1]], index=['seq3', 'seq2'],
                            columns=['motif1', 'motif3'])
        merged = merge_matrices([matrix1, matrix2], labels=None)
        expected = [[2, 1, numpy.nan, numpy.nan],
                    [0, 1, 0, 1], [numpy.nan, numpy.nan, 2, 1]]
        assert numpy.allclose(merged, expected, equal_nan=True)
        assert numpy.all(merged.columns == ['motif1', 'motif2', 'motif1',
                                            'motif3'])

        merged = merge_matrices([matrix1, matrix2], labels=['exon', 'intron'])
        assert numpy.allclose(merged, expected, equal_nan=True)
        expected_cols = ['motif1.exon', 'motif2.exon',
                         'motif1.intron', 'motif3.intron']
        assert numpy.all(merged.columns == expected_cols)

        # With seq length information
        matrix1 = DataFrame([[2, 1], [0, 1]], index=['seq1', 'seq2'],
                            columns=['motif1', 'motif2'])
        matrix1.seq_length = [10, 20]
        matrix2 = DataFrame([[2, 1], [0, 1]], index=['seq3', 'seq2'],
                            columns=['motif1', 'motif3'])
        matrix2.seq_length = [5, 10]

        merged = merge_matrices([matrix1, matrix2], labels=None)
        expected = numpy.array([[2, 1, numpy.nan, numpy.nan],
                                [0, 1, 0, 1], [numpy.nan, numpy.nan, 2, 1]])
        assert numpy.allclose(merged, expected, equal_nan=True)
        assert numpy.all(merged.columns == ['motif1', 'motif2', 'motif1',
                                            'motif3'])

        merged = merge_matrices([matrix1, matrix2], labels=['exon', 'intron'])
        assert numpy.allclose(merged, expected, equal_nan=True)
        expected_cols = ['motif1.exon', 'motif2.exon',
                         'motif1.intron', 'motif3.intron']
        assert numpy.all(merged.columns == expected_cols)

    def test_rows_are_uniform(self):
        matrix = numpy.array([[1, 1, 1, 1], [1, 2, 1, 1], [0, 1, 2, 3]])
        assert numpy.all(rows_are_uniform(matrix) == [True, False, False])

    def test_cols_are_uniform(self):
        matrix = numpy.array([[1, 1, 1, 1], [1, 2, 1, 1], [0, 1, 2, 3]])
        assert numpy.all(cols_are_uniform(matrix.transpose()) == [True, False,
                                                                  False])

    def test_calc_combination_matrix(self):
        matrix = DataFrame([[2, 1, 0, 1], [0, 1, 0, 1]],
                           index=['seq1', 'seq2'],
                           columns=['motif1.a', 'motif2.a',
                                    'motif3.a', 'moitf4.a'])
        matrix.seq_length = numpy.array([10, 20])
        res = calc_combination_matrix(matrix).next()
        expected = numpy.array([[1, 0, 1, 0, 1, 0], [0, 0, 0, 0, 1, 0]])
        assert numpy.all(res == expected)
        assert numpy.all(res.seq_length == matrix.seq_length)
        exp_cols = numpy.array(['motif1|motif2.a|a', 'motif1|motif3.a|a',
                                'motif1|moitf4.a|a', 'motif2|motif3.a|a',
                                'motif2|moitf4.a|a', 'motif3|moitf4.a|a'])
        assert numpy.all(res.columns == exp_cols)

        matrix.columns = ['motif1', 'motif2', 'motif3', 'moitf4']
        expected = [numpy.array([[1, 0], [0, 0]]),
                    numpy.array([[1, 0], [0, 0]]),
                    numpy.array([[1, 0], [1, 0]])]
        exp_cols = [numpy.array(['motif1|motif2', 'motif1|motif3']),
                    numpy.array(['motif1|moitf4', 'motif2|motif3']),
                    numpy.array(['motif2|moitf4', 'motif3|moitf4'])]
        for res, exp, exp_col in zip(calc_combination_matrix(matrix,
                                                             chunk_size=2),
                                     expected, exp_cols):
            assert numpy.all(res == exp)
            assert numpy.all(res.seq_length == matrix.seq_length)
            assert numpy.all(res.columns == exp_col)


class ProfilesTests(unittest.TestCase):
    def test_change_ref_coords_profile(self):
        nan = numpy.nan
        profile = numpy.array([[0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                               [0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
                               [0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 1, 1, 1, 0, 0]])
        pos_array = [5, 6, 4, 5]
        len_right, len_left = 5, 4
        exp = numpy.array([[0, 0, 0, 1, 1, 1, 0, 0, 0],
                           [0, 0, 0, 1, 1, 1, 0, 0, nan],
                           [0, 0, 0, 1, 1, 1, 0, 0, 0],
                           [0, 0, 0, 0, 1, 1, 1, 0, 0]])
        res = change_ref_coords_profile(profile, pos_array, len_left,
                                        len_right)
        res = numpy.append(res[0], res[1], axis=1)
        assert numpy.allclose(res, exp, equal_nan=True)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'ProfilesTests']
    unittest.main()
