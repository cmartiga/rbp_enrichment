#!/usr/bin/env python

import unittest

import numpy
from pandas import DataFrame

from motif.enrichment import (poisson_regression,
                              _calc_position_dependent_enrichment)
from motif.stats import (calc_distance_to_end, calc_cum_distib,
                         calc_motif_conservation)


class MotifEnrichmentTests(unittest.TestCase):

    def test_poisson_regression(self):
        # TODO: Check why the effect of the sequence length changes so much
        # with the seed
        matrix = DataFrame()
        matrix['motif1'] = [0, 2, 0, 3, 1, 0, 0, 0, 1, 0,
                            2, 1, 0, 4, 2, 3, 2, 1, 2, 1]
        matrix['motif2'] = [0, 1, 0, 1, 1, 0, 0, 1, 0, 0,
                            0, 1, 0, 1, 1, 0, 0, 1, 0, 0]

        numpy.random.seed(14)
        matrix.seq_length = numpy.random.normal(50, 10, size=20)
        grouping = ['seqs1'] * 10 + ['seqs2'] * 10
        test = poisson_regression(matrix, grouping)
        assert numpy.all(test['mseq_length.pvalue'] > 0.05)
        assert test['mseqs1.pvalue'][0] < 0.05
        assert numpy.allclose(test['mseqs1.estimate'][0], -1.10425893938)

    def test_calc_pos_dependent_enrichment(self):
        profiles = [[0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]]
        event_ids = ['event1', 'event2', 'event3', 'event4']
        groups = ['group1'] * 2 + ['group2'] * 2
        profiles_df = DataFrame(profiles, index=event_ids)
        grouping_df = DataFrame({'Grouping': groups}, index=event_ids)
        res = _calc_position_dependent_enrichment(profiles_df, grouping_df)
        comparisons, pos, pvalues, odds_ratio = res
        assert comparisons == ['group1_vs_group2']
        assert numpy.all(pos == numpy.arange(1, 10))
        assert numpy.all(pvalues == [[1.0, 1.0, 1.0, 1.0, 1.0,
                                      1.0, 1.0, 1.0, 1.0]])
        nan = numpy.nan
        exp = [[nan, nan, 0.0, 1.0, 1.0, 0.0, nan, nan, nan]]
        assert numpy.allclose(odds_ratio, exp, equal_nan=True)

        res = _calc_position_dependent_enrichment(profiles_df, grouping_df,
                                                  reverse=True)
        comparisons, pos, pvalues, odds_ratio = res
        assert comparisons == ['group1_vs_group2']
        assert numpy.all(pos == numpy.arange(1, 10))
        assert numpy.allclose(pvalues, [[1.0, 1.0, 1.0, 1.0, 1.0,
                                         1.0, 1.0, 1.0, 1.0]])
        nan = numpy.nan
        exp = [[nan, nan, nan, numpy.inf, 1.0, 1.0, numpy.inf, nan, nan]]
        assert numpy.allclose(odds_ratio, numpy.fliplr(exp), equal_nan=True)


class MotifStatsTests(unittest.TestCase):
    def test_calc_distance_to_end(self):
        motif_profiles = numpy.array([[0, 0, 1, 0, 0, 1, 0, 0, 1],
                                      [0, 0, 0, 0, 0, 1, 0, 1, 0],
                                      [1, 0, 1, 0, 0, 0, 0, 0, 0]])
        dist = calc_distance_to_end(motif_profiles, reverse=False)
        assert numpy.all(dist == [2, 5, 0])

        dist = calc_distance_to_end(motif_profiles, reverse=True)
        assert numpy.all(dist == [0, 1, 6])

    def test_calc_cum_distrib(self):
        motif_profiles = numpy.array([[0, 0, 1, 0, 0, 1, 0, 0, 1],
                                      [0, 0, 0, 0, 0, 1, 0, 1, 0],
                                      [1, 0, 1, 0, 0, 0, 0, 0, 0]])
        exp = numpy.array([1, 1, 2, 2, 2, 3, 3, 3, 3.0]) / 3
        cum_distrib = calc_cum_distib(motif_profiles, reverse=False)
        assert numpy.allclose(cum_distrib, exp)

        exp = numpy.array([3, 3, 3, 2, 2, 2, 2, 2, 1.0]) / 3
        cum_distrib = calc_cum_distib(motif_profiles, reverse=True)
        assert numpy.allclose(cum_distrib, exp)

        motif_profiles = numpy.array([[0, 0, 1, 0, 0, 1, 0, 0, 1],
                                      [0, 0, 0, 0, 0, 0, 0, 0, numpy.nan],
                                      [1, 0, 1, 0, 0, 0, 0, 0, 0]])
        exp = numpy.array([1, 1, 2, 2, 2, 2, 2, 2, 2.0]) / 3
        cum_distrib = calc_cum_distib(motif_profiles, reverse=False)
        assert numpy.allclose(cum_distrib, exp)

    def test_calc_motif_conservation(self):
        motif_profile = [[1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                         [0, 0, 1, 1, 1, 1, 0, 0, 0, 0]]
        motif_profile = DataFrame(motif_profile)

        cons_profile = [[1.0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                        [1, 1, 0, 0, 0, 1, 1, 1, 1, 1]]
        cons_profile = DataFrame(cons_profile)
        res = calc_motif_conservation(motif_profile, cons_profile)
        assert numpy.allclose(res, [[1, 0], [0.25, 1]])

if __name__ == '__main__':
    # import sys;sys.argv = ['', 'MotifEnrichmentTests']
    unittest.main()
