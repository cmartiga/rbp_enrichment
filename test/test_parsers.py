#!/usr/bin/env python

import os
from subprocess import check_call
from tempfile import NamedTemporaryFile
import unittest

import numpy
from pysam import FastaFile, Tabixfile

from motif.databases import read_attract_database
from seq.events import (ExonCassette, RetainedIntron, calc_splice_site_scores,
                        GenomeInterval, calc_ppt_features)
from seq.parsers import (parse_vast_exon, parse_vast_intron_retention,
                         read_fasta_file, parse_vast_psi_data)
from seq.writers import write_splicing_events, write_fasta_file
from structure.io import (_capr_output_to_bedgraph, parse_capr_output,
                          _capr_output_to_wig, read_rnaz, parse_rnaz_files,
                          rnaz_data_to_df)
from utils.seq import just_seqs, fasta_to_numpy
from utils.settings import (TEST_DATA_DIR, C2_tag, I2_tag, A_tag, I1_tag,
                            C1_tag, I_tag, BIN_DIR)


class SplicingEvents(unittest.TestCase):
    def test_exon_cassette(self):
        fastafile_fpath = os.path.join(TEST_DATA_DIR, 'exon_cassette.fa')
        fastafile = FastaFile(fastafile_fpath)
        parsed_exon = {'chrom': '1', 'start': 25, 'end': 30, 'strand': '+',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 35,
                       'dw_exon_end': 45, 'up_exon_start': 5,
                       'up_exon_end': 15, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_regions_sequences(fastafile)
        assert exon_cassette.regions[C2_tag].seq == 'GGGGGGGGGG'
        assert exon_cassette.regions[I1_tag].seq == 'AAAAAAAAAA'
        assert exon_cassette.regions[I2_tag].seq == 'AAAAA'
        assert exon_cassette.regions[A_tag].seq == 'CCCCC'
        assert exon_cassette.regions[C1_tag].seq == 'TTTTTTTTTT'

        parsed_exon = {'chrom': '1', 'start': 25, 'end': 30, 'strand': '-',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 35,
                       'dw_exon_end': 45, 'up_exon_start': 5,
                       'up_exon_end': 15, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_regions_sequences(fastafile)
        assert exon_cassette.regions[C2_tag].seq == 'AAAAAAAAAA'
        assert exon_cassette.regions[I1_tag].seq == 'TTTTT'
        assert exon_cassette.regions[I2_tag].seq == 'TTTTTTTTTT'
        assert exon_cassette.regions[A_tag].seq == 'GGGGG'
        assert exon_cassette.regions[C1_tag].seq == 'CCCCCCCCCC'

    def test_retained_intron(self):
        fastafile_fpath = os.path.join(TEST_DATA_DIR, 'exon_cassette.fa')
        fastafile = FastaFile(fastafile_fpath)
        parsed_intron = {'chrom': '1', 'start': 15, 'end': 25, 'strand': '+',
                         'gene_id': 'gene1', 'phase': None,
                         'dw_exon_start': 25, 'dw_exon_end': 30,
                         'up_exon_start': 5, 'up_exon_end': 15,
                         'intron_id': 'intron1'}
        retained_intron = RetainedIntron(parsed_intron)
        retained_intron.get_regions_sequences(fastafile)
        assert retained_intron.regions[C2_tag].seq == 'CCCCC'
        assert retained_intron.regions[I_tag].seq == 'AAAAAAAAAA'
        assert retained_intron.regions[C1_tag].seq == 'TTTTTTTTTT'

        parsed_intron = {'chrom': '1', 'start': 15, 'end': 25, 'strand': '-',
                         'gene_id': 'gene1', 'phase': None,
                         'dw_exon_start': 25, 'dw_exon_end': 30,
                         'up_exon_start': 5, 'up_exon_end': 15,
                         'intron_id': 'intron1'}
        retained_intron = RetainedIntron(parsed_intron)
        retained_intron.get_regions_sequences(fastafile)
        assert retained_intron.regions[C2_tag].seq == 'AAAAAAAAAA'
        assert retained_intron.regions[I_tag].seq == 'TTTTTTTTTT'
        assert retained_intron.regions[C1_tag].seq == 'GGGGG'

    def test_parse_vasttools_exon(self):
        # Exon in the positve strand
        line = 'Cad\tMmuEX0008863\tchr5:31377844-31377894\t51\tchr5:31377549+3'
        line += '1377558,31377844-31377894,31378182\tS\t19.30\tOK,OK,OK,B1,S@6'
        line += '.56,27.44\t25.00\tVLOW,VLOW,VLOW,B1,S@5.00,15.00\t62.16\t'
        line += 'VLOW,VLOW,VLOW,B1,S@18.65,11.35\t65.38\tLOW,LOW,LOW,OK,S@'
        line += '28.11,14.89'
        items = line.strip().split()
        exon = parse_vast_exon(items)
        assert exon['start'] == 31377843
        assert exon['end'] == 31377894
        assert exon['up_exon_end'] == 31377558
        assert exon['up_exon_start'] == 31377507
        assert exon['dw_exon_end'] == 31378232
        assert exon['dw_exon_start'] == 31378181
        assert exon['strand'] == '+'

        # Exon in the negative strand
        line = 'Cad\tMmuEX0008863\tchr5:31377844-31377894\t51\tchr5:31378182,3'
        line += '1377844-31377894,31377549+31377558\tS\t19.30\tOK,OK,OK,B1,S@6'
        line += '.56,27.44\t25.00\tVLOW,VLOW,VLOW,B1,S@5.00,15.00\t62.16\t'
        line += 'VLOW,VLOW,VLOW,B1,S@18.65,11.35\t65.38\tLOW,LOW,LOW,OK,S@'
        line += '28.11,14.89'
        items = line.strip().split()
        exon = parse_vast_exon(items)
        assert exon['start'] == 31377843
        assert exon['end'] == 31377894
        assert exon['up_exon_end'] == 31377558
        assert exon['up_exon_start'] == 31377507
        assert exon['dw_exon_end'] == 31378232
        assert exon['dw_exon_start'] == 31378181
        assert exon['strand'] == '-'

        # Incorrect exon
        line = 'Cad\tMmuEX0008863\tchr5:31377844-31377894\t51\tchr5:31378182,3'
        line += '1377844-31377894,31377549+31378182\tS\t19.30\tOK,OK,OK,B1,S@6'
        line += '.56,27.44\t25.00\tVLOW,VLOW,VLOW,B1,S@5.00,15.00\t62.16\t'
        line += 'VLOW,VLOW,VLOW,B1,S@18.65,11.35\t65.38\tLOW,LOW,LOW,OK,S@'
        line += '28.11,14.89'
        items = line.strip().split()
        assert parse_vast_exon(items) is None

    def test_parse_vasttools_retained_intron(self):
        # Exon in the positve strand
        line = 'BC057079\tMmuINT0023469\tchr4:88054901-88056520\t1620\t'
        line += 'chr4:88054825-88054900=88056521-88056915:+\tIR-C'
        items = line.strip().split()
        retained_intron = parse_vast_intron_retention(items)
        assert retained_intron['start'] == 88054900
        assert retained_intron['end'] == 88056520
        assert retained_intron['up_exon_end'] == 88054900
        assert retained_intron['up_exon_start'] == 88054824
        assert retained_intron['dw_exon_end'] == 88056915
        assert retained_intron['dw_exon_start'] == 88056520
        assert retained_intron['strand'] == '+'

        # Exon in the negative strand
        line = 'Asb2\tMmuINT0019614\tchr12:104576392-104583865\t7474\t'
        line += 'chr12:104583866-104584142=104576287-104576391:-\tIR-C'
        items = line.strip().split()
        retained_intron = parse_vast_intron_retention(items)
        assert retained_intron['start'] == 104576391
        assert retained_intron['end'] == 104583866
        assert retained_intron['up_exon_end'] == 104576391
        assert retained_intron['up_exon_start'] == 104576286
        assert retained_intron['dw_exon_end'] == 104584142
        assert retained_intron['dw_exon_start'] == 104583866
        assert retained_intron['strand'] == '-'

    def test_parse_vast_psi(self):
        line = 'Cad\tMmuEX0008863\tchr5:31377844-31377894\t51\tchr5:31378182,3'
        line += '1377844-31377894,31377549+31377558\tS\t19.30\tOK,OK,OK,B1,S@6'
        line += '.56,27.44\t25.00\tVLOW,VLOW,VLOW,B1,S@5.00,15.00\t62.16\t'
        line += 'VLOW,VLOW,VLOW,B1,S@18.65,11.35\t65.38\tLOW,LOW,LOW,OK,S@'
        line += '28.11,14.89'
        items = line.split('\t')[6:]
        parsed_psi_data = parse_vast_psi_data(items)
        assert parsed_psi_data == [{'psi': 19.30, 'depth': 34.0},
                                   {'psi': 25.0, 'depth': 20.0},
                                   {'psi': 62.16, 'depth': 30.0},
                                   {'psi': 65.38, 'depth': 43.0}]

    def test_write_splicing_event(self):
        fastafile_fpath = os.path.join(TEST_DATA_DIR, 'exon_cassette.fa')
        fastafile = FastaFile(fastafile_fpath)

        # Exon cassette
        parsed_exon = {'chrom': '1', 'start': 25, 'end': 30, 'strand': '+',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 35,
                       'dw_exon_end': 45, 'up_exon_start': 5,
                       'up_exon_end': 15, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_regions_sequences(fastafile)
        expected_seqs = {C2_tag: 'GGGGGGGGGG', I1_tag: 'AAAAAAAAAA',
                         I2_tag: 'AAAAA', A_tag: 'CCCCC', C1_tag: 'TTTTTTTTTT'}

        with NamedTemporaryFile() as fhand:
            out_prefix = fhand.name
            fhand.close()
            write_splicing_events([exon_cassette], out_prefix)
            for region in exon_cassette.regions.keys():
                fpath = '{}.{}.fasta'.format(out_prefix, region)
                fhand = open(fpath)
                seq = [line.strip() for line in fhand][1]
                assert seq == expected_seqs[region]
                fhand.close()
                os.remove(fhand.name)

        # Intron Retention
        parsed_intron = {'chrom': '1', 'start': 15, 'end': 25, 'strand': '+',
                         'gene_id': 'gene1', 'phase': None,
                         'dw_exon_start': 25, 'dw_exon_end': 30,
                         'up_exon_start': 5, 'up_exon_end': 15,
                         'intron_id': 'intron1'}
        retained_intron = RetainedIntron(parsed_intron)
        retained_intron.get_regions_sequences(fastafile)
        expected_seqs = {C2_tag: 'CCCCC', I_tag: 'AAAAAAAAAA',
                         C1_tag: 'TTTTTTTTTT'}

        with NamedTemporaryFile() as fhand:
            out_prefix = fhand.name
            fhand.close()
            write_splicing_events([retained_intron], out_prefix)
            for region in retained_intron.regions.keys():
                fpath = '{}.{}.fasta'.format(out_prefix, region)
                fhand = open(fpath)
                seq = [line.strip() for line in fhand][1]
                assert seq == expected_seqs[region]
                fhand.close()
                os.remove(fhand.name)

    def test_get_clipseq_counts(self):
        clip_idx_fpath = os.path.join(TEST_DATA_DIR, 'clip.bed.gz')
        clip_idx = Tabixfile(clip_idx_fpath)

        # Exon cassette
        parsed_exon = {'chrom': '1', 'start': 25, 'end': 30, 'strand': '+',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 35,
                       'dw_exon_end': 45, 'up_exon_start': 5,
                       'up_exon_end': 15, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_tabix_counts(clip_idx)
        for region_name, region in exon_cassette.regions.items():
            if region_name in ['Upstream_exon', 'Downstream_intron']:
                assert region.counts['clip'] == 1

        # In regions that are further than the last place in bed file
        parsed_exon = {'chrom': '1', 'start': 250, 'end': 300, 'strand': '+',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 350,
                       'dw_exon_end': 450, 'up_exon_start': 50,
                       'up_exon_end': 150, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_tabix_counts(clip_idx)
        for region_name, region in exon_cassette.regions.items():
            assert region.counts == {}

        # In a chromosome that is not present in the bed file
        parsed_exon = {'chrom': '3', 'start': 250, 'end': 300, 'strand': '+',
                       'gene_id': 'gene1', 'phase': None, 'dw_exon_start': 350,
                       'dw_exon_end': 450, 'up_exon_start': 50,
                       'up_exon_end': 150, 'exon_id': 'exon2'}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.get_tabix_counts(clip_idx)
        for region_name, region in exon_cassette.regions.items():
            assert region.counts == {}

    def test_calc_splice_site_scores(self):
        class FakeSplicingEvent(object):
            def __init__(self):
                pass

        splicing_event = FakeSplicingEvent()
        splicing_event.id = 'event1'
        splicing_event.splice_sites = {'5ss': 'acggtaagt',
                                       '3ss': 'ctctactactatctatctagatc'}
        ss_scores = calc_splice_site_scores([splicing_event])
        assert numpy.allclose(ss_scores.as_matrix(), [[6.71, 11.81]])

    def test_calc_ppt_features(self):
        class FakeSplicingEvent(object):
            def __init__(self):
                pass

        region1 = GenomeInterval('chr1', 10, 40, '+')
        region1.seq = 'GATGTGCTAAGTGCTCAGCAGGATGAGAGCCACTACTGCTGTGTGGGGTGTGCGT'
        region1.seq += 'GTGCGCGTGACTGAGTAAGTTCATATGTGCATGTGTGTTAGGGAGTGGACCCAT'
        region1.seq += 'GGCTTTGTGTGTTGTTGAATGTTTGTTCTACTCCCGAGCTACACCCCAACCTCT'
        region1.seq += 'TTGGCTGTGCATTTATGATCATATGTGTTGTCTTCAG'

        region2 = GenomeInterval('chr1', 10, 40, '+')
        region2.seq = 'GCCTGGTCTACAAAGTGAGTTCCAGGACAGCCAGGGCTACACAGAGAAACCCTGT'
        region2.seq += 'CTCAAAAAACCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAGA'
        region2.seq += 'AGAAGAAGAAGAAGAAGAAGAAGAAAGAGGAGCTTGGCTCTTCAGGGTAATTGG'
        region2.seq += 'CCTTAGACTTCAGTGACTGTTTTTCTCTGGCTCCCAG'
        splicing_event = FakeSplicingEvent()
        splicing_event.id = 'event1'
        splicing_event.regions = {'upstream_intron.r': region1,
                                  'downstream_intron.r': region2}
        ppt_features = calc_ppt_features([splicing_event])
        ppt1 = ppt_features['upstream_intron.r']
        assert int(ppt1.ix[0, 'ss_dist']) == 74
        assert int(ppt1.ix[0, 'ppt_len']) == 16
        assert ppt1.ix[0, 'bp_seq'] == 'tgttgaatg'

        ppt1 = ppt_features['downstream_intron.r']
        assert int(ppt1.ix[0, 'ss_dist']) == 22
        assert int(ppt1.ix[0, 'ppt_len']) == 17
        assert ppt1.ix[0, 'bp_seq'] == 'cagtgactg'


class FastaFileTests(unittest.TestCase):
    def test_read_fasta_file(self):
        fhand = ['\n', '>seq1\n', 'ATGC\n', '>seq2\n', 'GGGG\n']
        expected = [('seq1', 'ATGC'), ('seq2', 'GGGG')]
        for seq, exp in zip(read_fasta_file(fhand), expected):
            assert seq == exp

    def test_write_fasta_file(self):
        expected = ['>seq1\n', 'ATGC\n', '>seq2\n', 'GGGG\n']
        seqs = [('seq1', 'ATGC'), ('seq2', 'GGGG')]
        with NamedTemporaryFile(mode='w') as fhand:
            write_fasta_file(seqs, fhand)
            for line, exp in zip(open(fhand.name), expected):
                assert line == exp

    def test_just_seqs(self):
        seqs = [('seq1', 'AAAAA')]
        justified_seqs = list(just_seqs(seqs, 10, just_right=False))
        assert justified_seqs[0] == ('seq1', 'AAAAA-----')
        justified_seqs = list(just_seqs(seqs, 10, just_right=True))
        assert justified_seqs[0] == ('seq1', '-----AAAAA')

        seqs = [('seq1', 'AAAAAAAAAAAAAAA')]
        justified_seqs = list(just_seqs(seqs, 10, just_right=False))
        assert justified_seqs[0] == ('seq1', 'AAAAAAAAAA')
        justified_seqs = list(just_seqs(seqs, 10, just_right=True))
        assert justified_seqs[0] == ('seq1', 'AAAAAAAAAA')

        seqs = [('seq1', 'AAAAAAAAAATTTTTTTTTT')]
        justified_seqs = list(just_seqs(seqs, 10, just_right=True))
        assert justified_seqs[0] == ('seq1', 'TTTTTTTTTT')

        # Without filling the gaps with -
        seqs = [('seq1', 'AAAAA')]
        justified_seqs = list(just_seqs(seqs, 10, just_right=False,
                                        fill_empty=False))
        assert justified_seqs[0] == ('seq1', 'AAAAA')

    def test_just_seqs_bin(self):
        with NamedTemporaryFile(mode='w') as fhand:
            fhand.write('>seq1\nAAAAA\n')
            fhand.flush()
            with NamedTemporaryFile(mode='w') as out_fhand:
                just_fasta = os.path.join(BIN_DIR, 'just_fasta.py')
                cmd = [just_fasta, fhand.name, '-o', out_fhand.name,
                       '-l', '10']
                check_call(cmd)
                for line in open(out_fhand.name):
                    assert line.strip() in ['>seq1', 'AAAAA-----']

    def test_fasta_to_numpy(self):
        seqs = [('seq1', 'AATTGG'), ('seq2', 'ATGCAT')]
        expected = numpy.array([['A', 'A', 'T', 'T', 'G', 'G'],
                                ['A', 'T', 'G', 'C', 'A', 'T']])
        seqs = fasta_to_numpy(seqs)
        assert numpy.all(seqs[1] == expected)

        seqs = [('seq1', [0, 1, 2, 1, 2, 3]), ('seq2', [0, 1, 0, 0, 1, None])]
        expected = numpy.array([[0, 1, 2, 1, 2, 3],
                                [0, 1, 0, 0, 1, numpy.nan]])
        seqs = fasta_to_numpy(seqs)
        assert numpy.allclose(seqs[1], expected, equal_nan=True)


class DatabaseParserTests(unittest.TestCase):
    def test_read_attract_database(self):
        header = 'gene_name,gene_id,organism,mutated,seq,sequencelength,'
        header += 'experiments,domains,reference,quality_score\n'
        lines = ['YBX2,id1,Mus_musculus,no,CACACCC,7,EMSA,CSD,1,0.027732**\n',
                 'YBX2,id1,Mus_musculus,no,UCCAUCU,7,EMSA,CSD,1,0.027732**\n',
                 'TLR3,id2,Mus_musculus,no,UCCAUCU,7,EMSA,CSD,2,0.027732**\n']
        with NamedTemporaryFile(mode='w') as fhand:
            fhand.writelines([header] + lines)
            fhand.flush()
            parsed_db = read_attract_database(fhand.name)
            assert parsed_db['YBX2']['seq'] == set(['CACACCC', 'TCCATCT'])
            assert parsed_db['TLR3']['seq'] == set(['TCCATCT'])

            parsed_db = read_attract_database(fhand.name, reading_option='seq')
            assert parsed_db['CACACCC']['seq'] == set(['CACACCC'])
            assert parsed_db['CACACCC']['gene_name'] == set(['YBX2'])
            assert parsed_db['TCCATCT']['seq'] == set(['TCCATCT'])
            assert parsed_db['TCCATCT']['gene_name'] == set(['YBX2', 'TLR3'])


class StructureParserTests(unittest.TestCase):
    def test_parse_capr_output(self):
        lines = ['>chr1 0 5 +length : 1',
                 'Bulge 0 4.74909e-05 0 0 0 ',
                 'Exterior 0.0497184 0.0253628 0.0251138 0.0251138 0.0251138',
                 'Hairpin 0 0.00158082 0.193904 0.974886 0.974869',
                 'Internal 0 9.54059e-06 0 0 0',
                 'Multibranch 0 0 0 0 0',
                 'Stem 0.950282 0.972999 0.780982 0 1.69417e-05']
        parsed_capr = list(parse_capr_output(lines))[0]
        exp_prof = {'Bulge': [0.0, 4.74909e-05, 0.0, 0.0, 0.0],
                    'Multibranch': [0.0, 0.0, 0.0, 0.0, 0.0],
                    'Hairpin': [0.0, 0.00158082, 0.193904, 0.974886, 0.974869],
                    'Stem': [0.950282, 0.972999, 0.780982, 0.0, 1.69417e-05],
                    'Internal': [0.0, 9.54059e-06, 0.0, 0.0, 0.0],
                    'Exterior': [0.0497184, 0.0253628, 0.0251138, 0.0251138,
                                 0.0251138]}
        exp = {'profile': exp_prof, 'start': 0, 'end': 5,
               'chrom': 'chr1', 'strand': '+'}
        assert parsed_capr == exp

    def test_capr_output_to_bedgraph(self):
        profiles = {'H': [0, 5, 6, 0.1, 0.4, 0.3],
                    'P': [1, 2, 2, 0.3, 0.1, 0.2]}
        record = {'start': 10, 'end': 20, 'chrom': 'chr1', 'strand': '+',
                  'profile': profiles}
        bg_lines = list(_capr_output_to_bedgraph([record]))[0]
        exp = {'H': ['chr1\t10\t11\t0', 'chr1\t11\t12\t5', 'chr1\t12\t13\t6',
                     'chr1\t13\t14\t0.1', 'chr1\t14\t15\t0.4',
                     'chr1\t15\t16\t0.3'],
               'P': ['chr1\t10\t11\t1', 'chr1\t11\t12\t2', 'chr1\t12\t13\t2',
                     'chr1\t13\t14\t0.3', 'chr1\t14\t15\t0.1',
                     'chr1\t15\t16\t0.2']}
        assert bg_lines == exp

        profiles = {'H': [0, 5, 6, 0.1, 0.4, 0.3],
                    'P': [1, 2, 2, 0.3, 0.1, 0.2]}
        record = {'start': 10, 'end': 20, 'chrom': 'chr1', 'strand': '-',
                  'profile': profiles}
        bg_lines = list(_capr_output_to_bedgraph([record]))[0]
        exp = {'H': ['chr1\t10\t11\t0.3', 'chr1\t11\t12\t0.4',
                     'chr1\t12\t13\t0.1', 'chr1\t13\t14\t6', 'chr1\t14\t15\t5',
                     'chr1\t15\t16\t0'],
               'P': ['chr1\t10\t11\t0.2', 'chr1\t11\t12\t0.1',
                     'chr1\t12\t13\t0.3', 'chr1\t13\t14\t2', 'chr1\t14\t15\t2',
                     'chr1\t15\t16\t1']}
        assert bg_lines == exp

    def test_capr_output_to_wig(self):
        profiles = {'H': [0, 5, 6, 0.1, 0.4, 0.3],
                    'P': [1, 2, 2, 0.3, 0.1, 0.2]}
        record = {'start': 10, 'end': 16, 'chrom': 'chr1', 'strand': '+',
                  'profile': profiles}
        lines = list(_capr_output_to_wig([record]))[0]
        exp = {'H': ['fixedStep chrom=chr1 start=10 step=1',
                     '0', '5', '6', '0.1', '0.4', '0.3'],
               'P': ['fixedStep chrom=chr1 start=10 step=1',
                     '1', '2', '2', '0.3', '0.1', '0.2']}
        assert lines == exp

        profiles = {'H': [0, 5, 6, 0.1, 0.4, 0.3],
                    'P': [1, 2, 2, 0.3, 0.1, 0.2]}
        record = {'start': 10, 'end': 16, 'chrom': 'chr1', 'strand': '-',
                  'profile': profiles}
        lines = list(_capr_output_to_wig([record]))[0]
        exp = {'H': ['fixedStep chrom=chr1 start=10 step=1',
                     '0.3', '0.4', '0.1', '6', '5', '0'],
               'P': ['fixedStep chrom=chr1 start=10 step=1',
                     '0.2', '0.1', '0.3', '2', '2', '1']}
        assert lines == exp

    def test_parse_rnaz(self):
        dirname = TEST_DATA_DIR
        fpath = os.path.join(dirname, 'event1.region1.rnaz')
        data, strc = read_rnaz(open(fpath))
        assert data == {'Energy contribution':-41.38,
                        'Background model': 'dinucleotide',
                        'Mean z-score':-6.51,
                        'Mean single sequence MFE':-76.12,
                        'Mean pairwise identity': 82.09,
                        'SVM decision value': 4.4,
                        'Consensus MFE':-40.73,
                        'Prediction': 'RNA',
                        'Covariance contribution': 0.65,
                        'WARNING': 'Out of training range. z-scores are NOT reliable.',
                        'Columns': 308.0, 'Combinations/Pair': 1.34,
                        'Sequences': 5.0,
                        'SVM RNA-class probability': 0.999789,
                        'G+C content': 0.43251,
                        'Structure conservation index': 0.54,
                        'Decision model': 'sequence based alignment quality',
                        'Shannon entropy': 0.31945,
                        'Reading direction': 'forward'}
        assert set(strc.keys()) == set(['PanTro4', 'ref', 'RheMac3',
                                        'consensus', 'Mm10', 'BosTau8'])

        for event_id, region, record in parse_rnaz_files(dirname):
            assert event_id == 'event1'
            assert region == 'region1'
            assert record[0] == data

        parsed_rnaz = parse_rnaz_files(dirname)
        rnaz_data_to_df(parsed_rnaz)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'StructureParserTests']
    unittest.main()
