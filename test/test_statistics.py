#!/usr/bin/env python

import random
import unittest

import numpy
from pandas import DataFrame

from statistics.base import EmpiricalPvalueCalculator
from statistics.chaos import dfa, mfdfa, calc_rms
from statistics.enrichment import fisher_test
from statistics.gsea import (MatrixShuffler, _calc_pheno_feat_corr,
                             calc_enrichment_score, ESnullDistribCalculator,
                             perform_pseudo_gsea, perform_gsea)


class EnrichmentTests(unittest.TestCase):

    def test_fisher_test(self):
        matrix = DataFrame()
        matrix['motif1'] = [0, 0, 0, 1, 1, 0, 0, 0, 0, 0,
                            1, 1, 0, 1, 0, 1, 1, 1, 1, 1]
        matrix['motif2'] = [0, 1, 0, 1, 1, 0, 0, 1, 0, 0,
                            0, 1, 0, 1, 1, 0, 0, 1, 0, 0]
        test = fisher_test(matrix, range(0, 10), range(10, 20))
        assert numpy.allclose(test['OR'], [0.0625, 1])
        assert numpy.allclose(test['pvalue'], [0.023014, 1])

    def test_shuffle_matrix(self):
        matrix = numpy.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        shuffle_matrix = MatrixShuffler(matrix, by_col=True,
                                        shuffle_each_row=False)
        random.seed(2)
        numpy.random.seed(1)
        shuffled = shuffle_matrix()
        assert numpy.all(shuffled == [[3, 2, 1], [6, 5, 4], [9, 8, 7]])

        shuffle_matrix = MatrixShuffler(matrix, by_col=True,
                                        shuffle_each_row=True)
        shuffled = shuffle_matrix()
        assert numpy.all(shuffled == [[3, 2, 1], [4, 5, 6], [7, 8, 9]])

        shuffle_matrix = MatrixShuffler(matrix, by_col=False,
                                        shuffle_each_row=False)
        shuffled = shuffle_matrix()
        assert numpy.all(shuffled == [[1, 2, 3], [7, 8, 9], [4, 5, 6]])

    def test_calc_pheno_feat_corr(self):
        features_matrix = DataFrame([[1, 2, 2, 3, 5, 6, 6, 4],
                                     [1, 4, 6, 1, 5, 1, 5, 1]],
                                    columns=['a'] * 4 + ['b'] * 4)
        corr = _calc_pheno_feat_corr(features_matrix.as_matrix(),
                                     phenotypes=features_matrix.columns)
        assert numpy.allclose(corr, [-0.903562, 0.0])

    def test_calc_enrichment_score(self):
        corr_array = numpy.array([0.2, 0.1, 0.7, -0.5, 0.8, -0.1, 0.15, -0.2])
        motif_matrix = DataFrame()
        motif_matrix['motif1'] = [1, 0, 1, 0, 1, 0, 0, 0]
        motif_matrix['motif2'] = [1, 0, 0, 0, 1, 1, 0, 1]
        res = calc_enrichment_score(corr_array, motif_matrix.as_matrix(),
                                    constant_inc=1)
        assert numpy.all(res[:, 0] == [1, 0.25])
        assert numpy.all(res[:, 1] == [0.375, 0.125])

        res = calc_enrichment_score(corr_array, motif_matrix.as_matrix())
        assert numpy.allclose(res[:, 0], [0.56666667, 0.20])
        assert numpy.all(res[:, 1] == [0.375, 0.125])

    def test_create_null_ES_distrib(self):
        features_numpy = numpy.array([[1, 2, 2, 3, 5, 6, 6, 4],
                                      [1, 4, 6, 1, 5, 1, 5, 1],
                                      [1, 2, 2, 1, 5, 6, 6, 5],
                                      [1, 4, 6, 2, 4, 2, 5, 1],
                                      [4, 6, 6, 5, 3, 2, 2, 1],
                                      [1, 5, 1, 5, 1, 6, 4, 1],
                                      [5, 6, 6, 5, 1, 2, 2, 1],
                                      [1, 5, 2, 4, 2, 6, 4, 1]])
        motif_numpy = numpy.array([[1, 0, 1, 0, 0, 0, 0, 1],
                                   [1, 0, 0, 0, 1, 1, 0, 1]]).transpose()
        phenotypes = ['a'] * 4 + ['b'] * 4
        random.seed(1)
        shuffle_func = MatrixShuffler(features_numpy, by_col=True,
                                      shuffle_each_row=True)
        sim_distrib = ESnullDistribCalculator(features_numpy,
                                              motif_numpy,
                                              phenotypes=phenotypes,
                                              shuffle_func=shuffle_func,
                                              shuffle_labels=True)
        assert numpy.allclose(sim_distrib(1), [0.18580725, 0.15673164])
        assert numpy.allclose(sim_distrib(1), [0.48878645, 0.07677332])

    def test_perform_gsea(self):
        test_perform_gsea()

    def test_perform_pseudo_gsea(self):
        motif_matrix = DataFrame()
        motif_matrix['motif1'] = [1, 0, 1, 0, 0, 0, 0, 1]
        motif_matrix['motif2'] = [1, 0, 0, 0, 1, 1, 0, 1]
        features_matrix = DataFrame()
        features_matrix['x'] = [20, 5, 25, 6, 12, 13, 2, 22]
        random.seed(1)
        numpy.random.seed(1)

        res = perform_pseudo_gsea(motif_matrix, features_matrix, n_iter=1000)
        assert numpy.allclose(res['ES'], [1, 0.75])
        assert numpy.all(res['rank'] == [0.375, 0.625])
        assert numpy.allclose(res['pvalue'], [0.029, 0.232])
        assert numpy.allclose(res['rho'], [20, 12])


def test_perform_gsea():
    motif_matrix = DataFrame()
    motif_matrix['motif1'] = [1, 0, 1, 0, 0, 0, 0, 1]
    motif_matrix['motif2'] = [1, 0, 0, 0, 1, 1, 0, 1]
    features_matrix = DataFrame([[1, 2, 2, 3, 5, 6, 6, 4],
                                 [1, 4, 6, 1, 5, 1, 5, 1],
                                 [1, 2, 2, 1, 5, 6, 6, 5],
                                 [1, 4, 6, 2, 4, 2, 5, 1],
                                 [4, 6, 6, 5, 3, 2, 2, 1],
                                 [1, 5, 1, 5, 1, 6, 4, 1],
                                 [5, 6, 6, 5, 1, 2, 2, 1],
                                 [1, 5, 2, 4, 2, 6, 4, 1]],
                                columns=['a'] * 4 + ['b'] * 4)
    random.seed(1)
    numpy.random.seed(1)
    res = perform_gsea(motif_matrix, features_matrix, n_iter=1000,
                       shuffle_labels=True, shuffle_each_row=True)
    assert numpy.allclose(res['ES'], [0.648198, 0.250402])
    assert numpy.all(res['rank'] == [0.625, 0.875])
    assert numpy.allclose(res['pvalue'], [0.001, 0.209])
    assert numpy.allclose(res['rho'], [0, -0.903562])

    res = perform_gsea(motif_matrix, features_matrix, n_iter=1000,
                       constant_inc=1, shuffle_labels=True,
                       shuffle_each_row=True)
    assert numpy.allclose(res['ES'], [1, 0.25])
    assert numpy.all(res['rank'] == [0.625, 0.125])
    assert numpy.allclose(res['pvalue'], [0.034, 1])
    assert numpy.allclose(res['rho'], [0, 0.970143])

    # Shuffle rows of motifs
    res = perform_gsea(motif_matrix, features_matrix, n_iter=1000,
                       constant_inc=1, shuffle_labels=False,
                       shuffle_each_row=True)
    assert numpy.allclose(res['ES'], [1, 0.25])
    assert numpy.all(res['rank'] == [0.625, 0.125])
    assert numpy.allclose(res['pvalue'], [0.034, 1])
    assert numpy.allclose(res['rho'], [0, 0.970143])

    # Using several threads. As they work in different processes the p-value
    # can change even with the same seed
    res = perform_gsea(motif_matrix, features_matrix, n_iter=1000, n_threads=2,
                       shuffle_labels=True, shuffle_each_row=True)
    assert numpy.allclose(res['ES'], [0.648198, 0.250402])
    assert numpy.all(res['rank'] == [0.625, 0.875])
    assert numpy.allclose(res['rho'], [0, -0.903562])
    assert res['pvalue'][0] < 0.01
    assert res['pvalue'][1] > 0.1


class BasicStatsTests(unittest.TestCase):
    def test_calc_empirical_pvalue(self):
        distrib = [1, 4, 2, 3, 7, 10, 2, 3, 5, 4]
        calc_empirical_pvalue = EmpiricalPvalueCalculator(distrib)
        assert numpy.allclose(calc_empirical_pvalue(5), 0.3)
        assert numpy.allclose(calc_empirical_pvalue(10), 0.1)
        assert numpy.allclose(calc_empirical_pvalue(11), 0.1)
        assert numpy.allclose(calc_empirical_pvalue(0), 1)


class ChaosTests(unittest.TestCase):
    def test_calc_rms(self):
        numpy.random.seed(0)
        values = numpy.random.normal(size=100)
        assert numpy.allclose(calc_rms(values), 1.00414682864)

    def test_dfa(self):
        numpy.random.seed(0)
        white_noise = numpy.random.normal(size=10000)
        hurst = dfa(white_noise, n_intervals=10)
        assert numpy.allclose(hurst, 0.4904328409571751)
        brownian_noise = numpy.cumsum(white_noise)
        hurst = dfa(brownian_noise, n_intervals=10)
        assert numpy.allclose(hurst, 1.4994091141198882)

    def test_mfdfa(self):
        numpy.random.seed(0)
        white_noise = numpy.random.normal(size=10000)
        fit = mfdfa(white_noise, n_intervals=10).fit_H_vs_q()
        assert numpy.allclose(fit[:2], (-0.0019192549170336584,
                                        0.49154036001601698))
        brownian_noise = numpy.cumsum(white_noise)
        fit = mfdfa(brownian_noise, n_intervals=10).fit_H_vs_q()
        assert numpy.allclose(fit[:2], (-0.0022079940768866091,
                                        1.5077804650769691))

        series_len = 10000
        my_data = numpy.random.normal(0, 0.5, series_len)
        my_data += (numpy.abs(numpy.random.normal(0, 2, series_len) *
                              numpy.sin(numpy.linspace(0, 3 * numpy.pi,
                                                       series_len))))
        my_data += numpy.sin(numpy.linspace(0, 5 * numpy.pi, series_len)) ** 2
        my_data += numpy.sin(numpy.linspace(1, 6 * numpy.pi, series_len)) ** 2
        fit = mfdfa(my_data, n_intervals=10).fit_H_vs_q()
        assert numpy.allclose(fit[:2], (0.0058094952515710657,
                                        0.97567694670778859))


if __name__ == '__main__':
    import sys;sys.argv = ['', 'ChaosTests.test_mfdfa']
    unittest.main()
