#!/usr/bin/env python

import random
from tempfile import NamedTemporaryFile
import unittest

import numpy
from pandas import DataFrame

from network.boolean import BaseNetwork
from utils.misc import group_exon_ids_by_gene
from utils.plot import (get_new_figure_and_canvas, volcano_plot,
                        volcano_plot_fisher_by_region,
                        volcano_plot_glm_by_region,
                        volcano_plot_gsea_by_region, plot_line_series,
                        plot_regions_series, plot_regions_track_values,
                        _calc_series_stat, _calc_series, _get_groups_indxs)


class PlotsTests(unittest.TestCase):
    def test_volcano(self):
        df = DataFrame()
        df['estimate'] = [0.5, 1, 2, 4, 0.25]
        df['pvalue'] = [0.001, 0.5, 0.4, 0.05, 0.9]
        df.index = ['mMotif1', 'mMotif2', 'mMotif3', 'mMotif4', 'mMotif5']
        df2 = DataFrame()
        df2['estimate'] = [0.34, 2, 1.5, 3, 0.5]
        df2['pvalue'] = [0.1, 0.6, 0.005, numpy.nan, 0.000]
        df2.index = ['mMotif1', 'mMotif2', 'mMotif3', 'mMotif4', 'mMotif5']

        with NamedTemporaryFile(suffix='.png') as fhand:
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            volcano_plot([df, df2], axes, dfs_labels=['test1', 'test2'])
            canvas.print_figure(fhand)

        with NamedTemporaryFile(suffix='.png') as fhand:
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            volcano_plot([df, df2], axes, dfs_labels=['test1', 'test2'],
                         replace_inf=True)
            canvas.print_figure(fhand)

    def test_volcano_fisher_by_region(self):
        df = DataFrame()
        df['OR'] = [0.5, 1, 2, 4, 0.25]
        df['pvalue'] = [0.001, 0.5, 0.4, 0.05, 0.9]
        df.index = ['mMotif1_exon', 'mMotif2_exon',
                    'mMotif1_intron', 'mMotif2_intron', 'mMotif3_exon']
        df2 = DataFrame()
        df2['OR'] = [0.34, 2, 1.5, 3, 0.5]
        df2['pvalue'] = [0.1, 0.6, 0.005, 0.07, 0.0001]
        df2.index = ['mMotif1_exon', 'mMotif2_exon',
                     'mMotif1_intron', 'mMotif2_intron', 'mMotif3_exon']

        with NamedTemporaryFile(suffix='.png') as fhand:
            volcano_plot_fisher_by_region([df, df2], fhand,
                                          dfs_labels=['test1', 'test2'])

    def test_volcano_glm_by_region(self):
        df = DataFrame()
        df['excluded.estimate'] = [0.5, 1, 2, 4, 0.25]
        df['excluded.pvalue'] = [0.001, 0.5, 0.4, 0.05, 0.9]
        df['included.estimate'] = [0.34, 2, 1.5, 3, 0.5]
        df['included.pvalue'] = [0.1, 0.6, 0.005, numpy.nan, 0.000]
        df.index = ['mMotif1_exon', 'mMotif2_exon',
                    'mMotif1_intron', 'mMotif2_intron', 'mMotif3_exon']

        with NamedTemporaryFile(suffix='.png') as fhand:
            try:
                volcano_plot_glm_by_region(df, fhand,
                                           params=['excluded', 'included'])
                self.fail()
            except ValueError:
                volcano_plot_glm_by_region(df, fhand,
                                           params=['excluded', 'included'],
                                           replace_inf=True)

    def test_volcano_gsea_by_region(self):
        df = DataFrame()
        df['ES'] = [0.5, 1, 2, 4, 0.25]
        df['rank'] = [0.001, 0.5, 0.1, 0.05, 0.9]
        df['rho'] = [0.9, 0.2, -0.4, -0.78, 0]
        df['pvalue'] = [0.1, 0.6, 0.005, numpy.nan, 0.000]
        df.index = ['Motif1.exon', 'Motif2.exon',
                    'Motif1.intron', 'Motif2.intron', 'Motif3.exon']

        with NamedTemporaryFile(suffix='.png') as fhand:
            try:
                volcano_plot_gsea_by_region(df, fhand,)
                self.fail()
            except ValueError:
                volcano_plot_gsea_by_region(df, fhand, replace_inf=True)

    def test_plot_line_series(self):
        x = range(10)
        ys = [[2] * 10, range(10)]
        labels = ['line1', 'line2']
        with NamedTemporaryFile(suffix='.png') as fhand:
            fig, canvas = get_new_figure_and_canvas((20, 10))
            axes = fig.add_subplot(111)
            plot_line_series(x, ys, axes, labels=labels)
            canvas.print_figure(fhand)

    def test_plot_regions_series(self):
        xs = [range(10), range(20)]
        ys_series = [[[2] * 10, range(10)], [[3] * 20, range(20)]]
        ys_series2 = [[[3] * 10, range(10)], [[2] * 20, range(20)]]
        labels = ['line1', 'line2']
        plot_labels = ['plot1', 'plot2']
        with NamedTemporaryFile(suffix='.png') as fhand:
            plot_regions_series(xs, ys_series, plot_labels=plot_labels,
                                series_labels=labels, fhand=fhand,
                                ylabel='values', ylims=(0, 25), xticks_size=2,
                                ys_series2=ys_series2)

    def test_calc_series_pvalues(self):
        numpy.random.seed(1)
        seqs_regions = [[[1, 0, 2], [2, 0, 2],
                         [1, 0, 4], [3, 0, 4],
                         [0, 0, 3], [4, 0, 3],
                         [1, 0, 2], [3, 0, 2],
                         [0, 0, 1], [2, 0, 1]]]
        grouping = DataFrame({'Group': [1, 2, 1, 2, 1, 2, 1, 2, 1, 2]})
        groups_idxs = _get_groups_indxs(grouping)
        exp_idxs = numpy.array([True, False] * 5)
        assert numpy.all(groups_idxs[0] == exp_idxs)
        assert numpy.all(groups_idxs[1] != exp_idxs)

        res = _calc_series(seqs_regions, ['region1'], groups_idxs)
        series_values, ylims = res
        assert numpy.allclose(ylims, [0, 3.36])
        assert numpy.allclose(series_values, [[0.6, 0.0, 2.4], [2.8, 0, 2.4]])

        res = _calc_series_stat(series_values, seqs_regions, ['region1'],
                                grouping, n=100)
        series_pvalues = res[0]
        expected_pvalues = [[[0.02, 1.0, 0.78000000000000003],
                             [0.020000000000000018, 1.0, 0.73999999999999999]]]
        assert numpy.allclose(series_pvalues, expected_pvalues)
        assert numpy.allclose(res[1], [0.02, 1.2])

    def test_plot_regions_track_values(self):
        seqs = [numpy.array([[0, 1, 2, 1, 2, 3], [0, 1, 0, 0, 1, 1]]),
                numpy.array([[0, 1, 2, 1, 2, 3], [0, 1, 0, 0, 1, 1]])]
        seq_names = ['seq1', 'seq2']
        region_labels = ['region1.l', 'region2.r']
        grouping = DataFrame()
        grouping['Group'] = ['Group1', 'Group2']
        grouping.index = seq_names
        with NamedTemporaryFile(mode='w') as fhand:
            plot_regions_track_values(seqs, seq_names, region_labels, grouping,
                                      fhand, ylabel='Cons score', ylims=(0, 4),
                                      xticks_size=1, calc_pvalues=True)

    def test_plot_network(self):
        numpy.random.seed(0)
        random.seed(0)
        network = BaseNetwork()
        network.generate_random_network(100, 4, degree_distrib='power-law',
                                        exponent=1)
        network.calc_topological_properties(directed=False)
        nodes_types = {node: random.choice(['a', 'b'])
                       for node in network.nodes_list}
        network.add_nodes_property(nodes_types, 'Type')
        with NamedTemporaryFile(mode='w') as fhand:
            # fhand = open('/home/cmarti/network.png', 'w')
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            network.plot(directed=False, axes=axes, layout='force_directed',
                         nodes_size='degree', nodes_color='clustering_coeff')
            canvas.print_figure(fhand)
            fhand.close()

        # Mapping color to categorical variable
        with NamedTemporaryFile(mode='w') as fhand:
            fhand = open('/home/cmarti/network.png', 'w')
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            network.plot(directed=False, axes=axes, layout='force_directed',
                         nodes_size='degree', nodes_color='Type',
                         title='Random network categorical nodes color',
                         draw_labels=True, expansion_coeff=2)
            canvas.print_figure(fhand)
            fhand.close()

    def test_hiveplot(self):

        def rand_class(x):
            return random.choice(['a', 'b', 'c'])

        n_nodes = 20
        connetivity = 2
        n_edges = int(connetivity * n_nodes)
        prop1 = numpy.random.uniform(size=n_nodes)
        prop2 = numpy.random.uniform(size=n_nodes)
        prop3 = numpy.random.normal(size=n_nodes)
        nodes_names = ['node_{}'.format(i) for i in xrange(n_nodes)]
        nodes_types = [rand_class(node) for node in nodes_names]
        edges = set()
        while(len(edges) < n_edges):
            edges.add((random.choice(nodes_names),
                       random.choice(nodes_names)))

        nodes_df = DataFrame({'prop1': prop1, 'prop2': prop2,
                              'prop3': prop3, 'type': nodes_types},
                             index=nodes_names)

        with NamedTemporaryFile(mode='w', suffix='.png') as fhand:
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            hiveplot_by_nodes_types(nodes_df, edges, sort_by='prop1',
                                    type_col='type', axes=axes)
            canvas.print_figure(fhand)
            # raw_input(fhand.name)
            fhand.close()

        with NamedTemporaryFile(mode='w', suffix='.png') as fhand:
            fig, canvas = get_new_figure_and_canvas()
            axes = fig.add_subplot(111)
            hiveplot_by_nodes_properties(nodes_df, edges, nodes_df.columns,
                                         axes=axes, edge_funct=rand_class)
            canvas.print_figure(fhand)
            # raw_input(fhand.name)
            fhand.close()


class UtilsTests(unittest.TestCase):
    def test_group_exon_ids_by_gene(self):
        lines = ['gene1\tt1\t577\t647\t1\t1\t0\texon1\t1\tMT\tMT-TF',
                 'gene1\tt1\t577\t647\t0\t1\t0\texon2\t1\tMT\tMT-TF',
                 'gene1\tt1\t577\t647\t1\t1\t0\texon3\t1\tMT\tMT-TF',
                 'gene2\tt1\t577\t647\t1\t1\t0\texon1\t1\tMT\tMT-TF']
        res = group_exon_ids_by_gene(lines)
        assert set(res['gene1'][True]['t1'].keys()) == set(['exon2'])
        assert set(res['gene1'][False]['t1'].keys()) == set(['exon1', 'exon3'])
        assert set(res['gene2'][False]['t1'].keys()) == set(['exon1'])


if __name__ == '__main__':
    import sys;sys.argv = ['', 'PlotsTests.test_plot_network']
    unittest.main()
