#!/usr/bin/env python

import os
import random
from subprocess import check_call
from tempfile import NamedTemporaryFile
import unittest

from bx.bbi.bigwig_file import BigWigFile
import numpy
from pyliftover.liftover import LiftOver
from pysam import FastaFile, Tabixfile

from motif.finder import (calc_tabix_counts_matrices, MotifInSeq,
                          calc_tabix_profiles)
from seq.events import GenomeInterval, SplicingEvent, ExonCassette
from structure.predict import calc_rnazfold_zscore
from utils.misc import calc_aa_spectra
from utils.settings import RNAMOTIF, CISBP_RNA, TEST_DATA_DIR, A_tag, C1_tag, \
    C2_tag


class FinderTests(unittest.TestCase):
    def test_motif_in_seq(self):
        # Using rnamotif database
        motif = {'seq': ['ATGCAT']}
        seq = 'CCCATGCATCCC'
        motif_in_seq = MotifInSeq(db_type=RNAMOTIF)
        assert motif_in_seq(motif, seq)
        seq = 'CCCATACATCCC'
        assert not motif_in_seq(motif, seq)

        # Using rnamotif database with 1 missmatch
        motif = {'seq': ['ATGCAT']}
        seq = 'CCCATGAATCCC'
        motif_in_seq = MotifInSeq(db_type=RNAMOTIF, max_missmatches=1)
        assert motif_in_seq(motif, seq)
        seq = 'CCCATAATTCCC'
        assert not motif_in_seq(motif, seq)

        # Using PWM with pvalues based on a distribution of scores
        motif = ('motif1', [{'G': 2.0}, {'T': 2.0}, {'A': 2.0},
                            {'G': 2.0}, {'T': 2.0}, {'A': 2.0}])
        pwm_random_scores = {'motif1': [0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 12.0]}
        motif_in_seq = MotifInSeq(db_type=CISBP_RNA, pval_cutoff=0.1,
                                  pwm_random_scores=pwm_random_scores)
        assert motif_in_seq(motif, 'CCCGTAGTACCC')
        pwm_random_scores = {'motif1': [0, 0, 0, 0, 0,
                                        0, 0, 0, 8.0, 11.0]}
        motif_in_seq = MotifInSeq(db_type=CISBP_RNA, pval_cutoff=0.1,
                                  pwm_random_scores=pwm_random_scores)
        assert not motif_in_seq(motif, 'CCCGTACCCCCCC')
        try:
            motif_in_seq = MotifInSeq(db_type=CISBP_RNA, pval_cutoff=0.05,
                                      pwm_random_scores=pwm_random_scores)
        except ValueError:
            pass

    def test_calc_clipseq_matrix(self):
        class FakeSplicingEvent(SplicingEvent):
            def __init__(self, regions):
                self.regions = regions

        region1 = GenomeInterval('chr1', 10, 20, '+')
        region1.counts = {'rbp1': 0, 'rbp2': 1}
        region1.length = 10
        region2 = GenomeInterval('chr1', 50, 60, '+')
        region2.length = 10
        region2.counts = {'rbp1': 1, 'rbp2': 0}

        splicing_event1 = FakeSplicingEvent({'region1': region1,
                                             'region2': region2})
        splicing_event1.id = 'exon_cassete1'
        splicing_event2 = FakeSplicingEvent({'region1': region2,
                                             'region2': region1})
        splicing_event2.id = 'exon_cassete2'

        splicing_events = [splicing_event1, splicing_event2]
        sel_rbps = ['rbp1', 'rbp2']
        regions, matrices = calc_tabix_counts_matrices(splicing_events,
                                                       sel_rbps)
        assert regions == ['region2', 'region1']
        assert numpy.all(matrices[0].as_matrix() == [[1, 0, 10], [0, 1, 10]])
        assert numpy.all(matrices[1].as_matrix() == [[0, 1, 10], [1, 0, 10]])

        regions, matrices = calc_tabix_counts_matrices(splicing_events,
                                                       sel_rbps,
                                                       by_region=False)
        assert regions == ['all_regions']
        assert numpy.all(matrices[0].as_matrix() == [[1, 1, 20], [1, 1, 20]])

    def test_find_motifs(self):
        motif_db = {'CTGCG': {'gene_name': set(['RBFOX1'])}}

        interval = GenomeInterval('chr1', 10, 50, '+')
        interval.seq = 'A' * 15 + 'CTGCG' + 'A' * 20
        motifs = interval.find_motifs(motif_db)
        assert motifs == {(15, 20): set(['RBFOX1'])}

        interval = GenomeInterval('chr1', 10, 50, '-')
        interval.seq = 'A' * 15 + 'CTGCG' + 'A' * 20
        motifs = interval.find_motifs(motif_db)
        assert motifs == {(15, 20): set(['RBFOX1'])}

        ref_seq = 'T' * 10 + 'A' * 15 + 'CTGCG' + 'A' * 20 + 'T' * 20
        with NamedTemporaryFile(mode='w') as fhand:
            fhand.write('>chr1\n{}\n'.format(ref_seq))
            fhand.flush()
            cmd = ['samtools', 'faidx', fhand.name]
            check_call(cmd)
            ref_fastafile = FastaFile(fhand.name)
            interval = GenomeInterval('chr1', 10, 50, '+')
            interval.get_sequence_from_fastafile(ref_fastafile)
            motifs = interval.find_motifs(motif_db)
            bed_lines = list(interval.motifs_to_bed())
            assert motifs == {(15, 20): set(['RBFOX1'])}
            assert bed_lines == ['chr1\t25\t30\t+\tRBFOX1\n']
            fhand.close()

    def test_change_coord_refs(self):
        interval = GenomeInterval('chr1', 10, 50, '+')
        assert interval.relative_to_genomic_coordinates(15) == 25
        assert interval.genomic_to_relative_coordinates(25) == 15

        interval = GenomeInterval('chr1', 10, 50, '-')
        assert interval.relative_to_genomic_coordinates(15) == 35
        assert interval.genomic_to_relative_coordinates(35) == 15

        ref_seq = 'T' * 10 + 'A' * 15 + 'CTGCG' + 'A' * 20 + 'T' * 20
        with NamedTemporaryFile(mode='w') as fhand:
            fhand.write('>chr1\n{}\n'.format(ref_seq))
            fhand.flush()
            cmd = ['samtools', 'faidx', fhand.name]
            check_call(cmd)
            ref_fastafile = FastaFile(fhand.name)
            interval = GenomeInterval('chr1', 10, 50, '+')
            interval.get_sequence_from_fastafile(ref_fastafile)
            assert interval.seq == 'A' * 15 + 'CTGCG' + 'A' * 20
            assert interval.relative_to_genomic_coordinates(15) == 25
            assert interval.genomic_to_relative_coordinates(25) == 15
            fhand.close()

        ref_seq = 'A' * 10 + 'T' * 20 + 'CGCAG' + 'T' * 15 + 'A' * 20
        with NamedTemporaryFile(mode='w') as fhand:
            fhand.write('>chr1\n{}\n'.format(ref_seq))
            fhand.flush()
            cmd = ['samtools', 'faidx', fhand.name]
            check_call(cmd)
            ref_fastafile = FastaFile(fhand.name)
            interval = GenomeInterval('chr1', 10, 50, '-')
            interval.get_sequence_from_fastafile(ref_fastafile)
            assert interval.seq == 'A' * 15 + 'CTGCG' + 'A' * 20
            assert interval.relative_to_genomic_coordinates(15) == 35
            assert interval.genomic_to_relative_coordinates(35) == 15
            fhand.close()

    def test_find_tabix_intervals(self):
        class FakeSplicingEvent(SplicingEvent):
            def __init__(self, regions):
                self.regions = regions

        regions = {'region1': GenomeInterval('chr1', 10, 30, '+'),
                   'region2': GenomeInterval('chr1', 50, 80, '-')}
        splicing_event = FakeSplicingEvent(regions)
        splicing_event.id = 'even1'

        fhand = NamedTemporaryFile(mode='w')
        fhand.write('chr1\t15\t20\t+\tRBFOX1\n')
        fhand.write('chr1\t55\t60\t-\tPTBP1\n')
        fhand.flush()

        cmd = ['bgzip', fhand.name]
        check_call(cmd)
        cmd = ['tabix', fhand.name + '.gz', '-p', 'bed']
        check_call(cmd)
        tabix_fpath = fhand.name + '.gz'
        tabix_index = Tabixfile(tabix_fpath)

        sel_rbps = ['RBFOX1', 'PTBP1']
        splicing_event.get_tabix_counts(tabix_index)
        region1 = splicing_event.regions['region1']
        region2 = splicing_event.regions['region2']
        assert region1.counts['RBFOX1'] == 1
        assert region2.counts['PTBP1'] == 1

        splicing_event.get_tabix_profile(tabix_index, sel_rbps)
        region1 = splicing_event.regions['region1']
        region2 = splicing_event.regions['region2']
        exp1_rbfox1 = numpy.zeros(20)
        exp1_rbfox1[4:9] = 1
        exp2_ptbp1 = numpy.zeros(30)
        exp2_ptbp1[19:24] = 1
        assert numpy.all(region1.profiles['RBFOX1'] == exp1_rbfox1)
        assert numpy.all(region2.profiles['PTBP1'] == exp2_ptbp1)

        regions, dfs = calc_tabix_profiles([splicing_event], 'RBFOX1')
        assert regions == ['region2', 'region1']
        assert numpy.all(dfs[1].iloc[0, 4:9] == 1)

    def test_get_bigwig(self):
        bw_fpath = os.path.join(TEST_DATA_DIR, 'phastcons.bw')
        bigwigfile = BigWigFile(open(bw_fpath))

        interval = GenomeInterval('chr1', 10, 50, '+')
        interval.get_bigwig_profile(bigwigfile, 'phastcons', expand=True)
        expected = [0.5] * 15 + [1.0] * 25
        assert numpy.allclose(interval.profiles['phastcons'], expected)

        interval = GenomeInterval('chr1', 10, 50, '-')
        interval.get_bigwig_profile(bigwigfile, 'phastcons', expand=True)
        expected = [1.0] * 25 + [0.5] * 15
        assert numpy.allclose(interval.profiles['phastcons'], expected)

    def test_exp_het(self):
        vcf_fpath = os.path.join(TEST_DATA_DIR, 'var.vcf.gz')
        vcf_index = Tabixfile(vcf_fpath)

        interval = GenomeInterval('chr1', 10, 50, '+')
        exp_het = interval.calc_exp_het(vcf_index)
        expected = [0] * 40
        expected[20] = 0.5
        assert numpy.allclose(exp_het, expected)

        interval = GenomeInterval('chr1', 11, 51, '-')
        exp_het = interval.calc_exp_het(vcf_index)
        expected = [0] * 40
        expected[21] = 0.5
        assert numpy.allclose(exp_het, expected)

    def test_liftover(self):
        liftover_fpath = os.path.join(TEST_DATA_DIR, 'chain.gz')
        interval = GenomeInterval('chr1', 20, 30, '+')
        res = interval.liftover(liftover_fpath)
        assert res == ('chr2', 21, 31, '+')

        interval = GenomeInterval('chr1', 0, 5, '+')
        res = interval.liftover(liftover_fpath)
        assert res == ('chr2', 0, 5, '+')

        interval = GenomeInterval('chr2', 0, 5, '+')
        res = interval.liftover(liftover_fpath)
        assert res == ('chr1', 97, 102, '-')

        interval = GenomeInterval('chr3', 0, 5, '+')
        res = interval.liftover(liftover_fpath)
        assert res is None

        liftover = LiftOver(liftover_fpath)
        interval = GenomeInterval('chr1', 20, 30, '+')
        res = interval.liftover(liftover)
        assert res == ('chr2', 21, 31, '+')

        interval = GenomeInterval('chr1', 0, 5, '+')
        res = interval.liftover(liftover)
        assert res == ('chr2', 0, 5, '+')

        interval = GenomeInterval('chr2', 0, 5, '+')
        res = interval.liftover(liftover)
        assert res == ('chr1', 97, 102, '-')

        interval = GenomeInterval('chr3', 0, 5, '+')
        res = interval.liftover(liftover)
        assert res is None

        liftover = {'event1': {'region1': ('chr1', 1, 10, '+')}}
        interval = GenomeInterval('chr3', 0, 5, '+')
        interval.region = 'region1'
        interval.event_id = 'event1'
        res = interval.liftover(liftover)
        assert res == ('chr1', 1, 10, '+')

        interval.event_id = 'event2'
        res = interval.liftover(liftover)
        assert res is None

    def test_find_polymorphism_changes(self):
        vcf_fpath = os.path.join(TEST_DATA_DIR, 'var.vcf.gz')
        vcf_index = Tabixfile(vcf_fpath)

        interval = GenomeInterval('1', 25, 35, '+')
        changes = interval.find_polymorphism_changes(vcf_index)
        expected = [None] * 10
        expected[5] = ('C', 'T')
        assert changes == expected

    def test_find_divergence_changes(self):
        liftover_fpath = os.path.join(TEST_DATA_DIR, 'chain.gz')
        fasta_file1 = FastaFile(os.path.join(TEST_DATA_DIR, 'especies1.fasta'))
        fasta_file2 = FastaFile(os.path.join(TEST_DATA_DIR, 'especies2.fasta'))
        liftover = LiftOver(liftover_fpath)

        interval = GenomeInterval('chr1', 0, 5, '+')
        interval.get_sequence_from_fastafile(fasta_file1)
        changes = interval.find_divergence_changes(liftover, fasta_file2,
                                                   liftover, fasta_file2)
        expected = [None] * 5
        expected[2] = ('G', 'A')
        assert changes == expected

        interval = GenomeInterval('chr2', 0, 5, '+')
        interval.get_sequence_from_fastafile(fasta_file1)
        changes = interval.find_divergence_changes(liftover, fasta_file2,
                                                   liftover, fasta_file2)
        expected = [None] * 5
        expected[1] = ('G', 'T')
        assert changes == expected

    def test_rnafold_zscore(self):
        seq = 'GCCTTGTTGGCGCAATCGGTAGCGCGTATGACTCTTAATCATAAGGTTAGGGGTTCGAGC'
        seq += 'CCCCTACAGGGCT'
        zscore = calc_rnazfold_zscore(seq)
        assert zscore < -1.5
        random.seed(1)
        zscore = calc_rnazfold_zscore(seq, use_python=True)
        assert numpy.allclose(zscore, -2.04215825132)

    def test_calc_aa_spectra(self):
        seq = 'ATGATAACACTA'
        aa_spectra = calc_aa_spectra(seq, reading_frame=0)
        assert aa_spectra == {'I': 1, 'M': 1, 'T': 1, 'L': 1}
        aa_spectra = calc_aa_spectra(seq, reading_frame=1)
        assert aa_spectra == {'_': 2, 'H': 1}
        aa_spectra = calc_aa_spectra(seq, reading_frame=2)
        assert aa_spectra == {'D': 1, 'N': 1, 'T': 1}

    def test_calc_exon_cassette_aa_spectra(self):
        A = GenomeInterval('chr1', 10, 20, '+')
        A.seq = 'ATGATAACACTA'
        C1 = GenomeInterval('chr1', 0, 5, '+')
        C1.seq = 'ATGCT'
        C2 = GenomeInterval('chr1', 25, 30, '+')
        C2.seq = 'GCTAC'
        parsed_exon = {'chrom': 'chr1', 'gene_id': None,
                       'strand': '+', 'id': 'exon1',
                       'phase': None, 'start': 10, 'end': 20}
        exon_cassette = ExonCassette(parsed_exon)
        exon_cassette.regions = {A_tag: A, C1_tag: C1, C2_tag: C2}

        exon_cassette.phase = 0
        aa_spectra = exon_cassette.calc_aa_spectra()
        assert aa_spectra == {'I': 1, 'M': 1, 'T': 1, 'L': 1}

        exon_cassette.phase = 1
        aa_spectra = exon_cassette.calc_aa_spectra()
        assert aa_spectra == {'_': 3, 'H': 1, 'L': 1}

        exon_cassette.phase = 2
        aa_spectra = exon_cassette.calc_aa_spectra()
        assert aa_spectra == {'D': 1, 'N': 1, 'T': 1, 'Y': 1, 'S': 1}

        exon_cassette.phase = None
        aa_spectra = exon_cassette.calc_aa_spectra()
        assert aa_spectra == {'D': 1, 'N': 1, 'T': 2, 'Y': 1, 'S': 1,
                              'I': 1, 'M': 1, 'L': 2, '_': 3, 'H': 1}

        aa_spectra = exon_cassette.calc_aa_spectra(count_after_stop=False)
        assert aa_spectra == {'D': 1, 'N': 1, 'T': 2, 'Y': 1, 'S': 1,
                              'I': 1, 'M': 1, 'L': 2, '_': 1}

        random.seed(1)
        aa_spectra = exon_cassette.calc_aa_spectra(count_after_stop=False,
                                                   shuffle=True)
        assert aa_spectra != {'D': 1, 'N': 1, 'T': 2, 'Y': 1, 'S': 1,
                              'I': 1, 'M': 1, 'L': 2, '_': 1}


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'FinderTests.test_calc_clipseq_matrix']
    unittest.main()
