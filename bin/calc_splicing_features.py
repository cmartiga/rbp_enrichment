#!/usr/bin/env python

import argparse

from pysam import FastaFile

from motif.matrix import write_matrix
from seq.events import (fetch_splicing_events, calc_splice_site_scores,
                        calc_ppt_features)
from utils.settings import MATS_FORMAT, EX, INT


if __name__ == '__main__':
    description = 'Makes volcano plots from the resulting matrices of the '
    description += 'enrichment analysis'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Files containing exons to analyze')
    parser.add_argument('-f', '--file_format', default=MATS_FORMAT,
                        help='Input file format (MATS)')
    parser.add_argument('-s', '--splicing_event', default=EX,
                        help='Type of splicing events (Exon_cassette)')
    parser.add_argument('-r', '--ref_index', default=None,
                        help='Genome fasta file indexed with Samtools')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-hs', '--human', default=False, action='store_true',
                        help='Use human species (def: mouse)')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    if sel_event_type not in [EX, INT]:
        msg = sel_event_type + ' type of event not supported. '
        msg += 'Only Exon_cassette or Intron_retention are allowed'
        raise ValueError(msg)
    fasta_fpath = parsed_args.ref_index
    ref_fastafile = FastaFile(fasta_fpath) if fasta_fpath else None
    out_prefix = parsed_args.output
    human = parsed_args.human
    species = 'Hsap' if human else 'Mmus'

    # Calculate splice site scores and plot if grouping is given
    splicing_events = fetch_splicing_events(in_fpath, file_format,
                                            ref_fastafile=ref_fastafile,
                                            sel_event_type=sel_event_type)
    splicing_events = list(splicing_events)
    ss_scores = calc_splice_site_scores(splicing_events, out_prefix=out_prefix)
    out_fhand = open(out_prefix + '.ss_scores.tab', 'w')
    write_matrix(ss_scores, out_fhand)

    ppt_features = calc_ppt_features(splicing_events, kword='intron',
                                     species=species)
    for region_name, features_df in ppt_features.items():
        out_fhand = open('{}.ppt.{}.tab'.format(out_prefix, region_name), 'w')
        write_matrix(features_df, out_fhand)
