#!/usr/bin/env python

import argparse
from itertools import combinations

import numpy
from pandas import read_table
import seaborn

import matplotlib.pyplot as plt
from motif.enrichment import poisson_regression
from motif.matrix import read_matrix, write_matrix
from motif.stats import calc_motifs_hamming_similarity
from statistics.enrichment import fisher_test
from utils.settings import FISHER, GLM


if __name__ == '__main__':
    description = 'Creates a clustermap based on motif profile similarity from'
    description += ' enriched motifs in certain events. It can compare event '
    description += 'or RBP motif profile using weights such as enrichment sig'
    description += 'nificance and differential expression significance'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)

    input_group = parser.add_argument_group('INPUT')
    help_msg = 'File containing motifs counts to analyze'
    input_group.add_argument('input', help=help_msg)
    input_group.add_argument('-e', '--enrichment_file', required=True,
                             help='File containing enrichment results')
    input_group.add_argument('-x', '--deg_file', default=None,
                             help='File containing DE analysis results')
    input_group.add_argument('-g', '--grouping_file', default=None,
                             help='File containing event or gene grouping')

    options_group = parser.add_argument_group('OPTIONS')
    help_msg = 'Perform analysis by regulator rather than by event'
    options_group.add_argument('-r', '--by_regulator', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Use -log(fdr) of the enrichmnet as weight for the similarity'
    help_msg += 'so that more important significantly enriched motifs are '
    help_msg += 'more important for similarity calculations. If DE analysis'
    help_msg += ' is given, it will by multiplied by -log10(padj)'
    options_group.add_argument('-w', '--use_weights', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Remove enriched elements that do not overcome this threshold'
    help_msg += '. By default all are kept'
    options_group.add_argument('-p', '--fdr_threshold', default=None,
                               type=float, help=help_msg)
    help_msg = 'Group to remove from data (No-change/No-DE)'
    options_group.add_argument('-rm', '--rm_group', default=None,
                               help=help_msg)
    help_msg = 'Combine expression and enrichment pvalues for weights'
    options_group.add_argument('-c', '--combine', default=False,
                               action='store_true', help=help_msg)

    output_group = parser.add_argument_group('OUTPUT')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fhand = parsed_args.input
    counts_df = read_matrix(in_fhand)
    enrichment_df = read_matrix(open(parsed_args.enrichment_file))
    deg_fpath = parsed_args.deg_file
    if deg_fpath is None:
        expression_df = None
    else:
        expression_df = read_matrix(open(deg_fpath))
        expression_df.index = [rowname.upper()
                               for rowname in expression_df.index]
    by_regulator = parsed_args.by_regulator
    weighted = parsed_args.use_weights
    fdr_threshold = parsed_args.fdr_threshold
    out_prefix = parsed_args.output
    grouping_fpath = parsed_args.grouping_file
    if grouping_fpath is None:
        grouping = None
    else:
        grouping = read_matrix(open(grouping_fpath))
    rm_group = parsed_args.rm_group
    if rm_group is None:
        rm_group = 'No-DE' if by_regulator else 'No-change'
    sel_events = grouping.loc[grouping.iloc[:, 0] != rm_group].index
    grouping = grouping.loc[sel_events, :]
    combine = parsed_args.combine

    # Run
    similarity = calc_motifs_hamming_similarity(enrichment_df=enrichment_df,
                                                counts_df=counts_df,
                                                padj_threshold=fdr_threshold,
                                                weighted=weighted,
                                                by_regulator=by_regulator,
                                                expression_df=expression_df,
                                                sel_events=sel_events,
                                                combine=combine)
    matrix_fpath = '{}.tab'.format(out_prefix)
    matrix_fhand = open(matrix_fpath, 'w')
    write_matrix(similarity, matrix_fhand)

    # Plot clustermap
    groups = numpy.unique(grouping.iloc[:, 0])
    colors = ['red', 'blue', 'black', 'white']
    group_color = {group: color for group, color in zip(groups, colors)}
    colors = [group_color[group] for group in grouping.ix[similarity.index, 0]]

    fig = seaborn.clustermap(similarity.as_matrix(), row_colors=colors,
                             col_colors=colors, xticklabels=False,
                             yticklabels=False)
    fig.savefig('{}.png'.format(out_prefix))
