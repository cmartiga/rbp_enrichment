#!/usr/bin/env python

import argparse

import numpy
from pandas import DataFrame

from motif.matrix import read_matrix, write_matrix


if __name__ == '__main__':
    description = 'Plots profiles for the different gruops of sequences'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', nargs=1, help='Profiles prefix')
    help_msg = 'File containing regions in profile'
    parser.add_argument('-r', '--regions', default=None, help=help_msg)
    parser.add_argument('-i', '--ids_fpath', default=None,
                        help='File containing ids to change')
    parser.add_argument('-f', '--gene_features_fpath', default=None,
                        help='File containing gene features values to compare')
    parser.add_argument('-o', '--output', required=True,
                        help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_prefix = parsed_args.input[0]
    regions = [line.strip() for line in open(parsed_args.regions)]

    ids_fpath = parsed_args.ids_fpath
    ids_df = read_matrix(open(ids_fpath))

    gene_features_fpath = parsed_args.gene_features_fpath
    gene_features_df = read_matrix(open(gene_features_fpath))

    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')

    # Run program
    profiles = []
    for region in regions:
        region_fpath = '{}.{}'.format(in_prefix, region)
        profile = read_matrix(open(region_fpath))
        exon_ids = profile.index
        profile = numpy.nanmean(profile, axis=1)
        profiles.append(profile)

    mean_values = numpy.vstack(profiles).transpose()
    mean_values = DataFrame(mean_values, index=exon_ids, columns=regions)

    common_ids = numpy.intersect1d(ids_df.index, mean_values.index)
    mean_values = mean_values.loc[common_ids, :]
    exon_gene_ids = numpy.array(ids_df.ix[mean_values.index, 0])
    gene_features_df_rownames = gene_features_df.index
    new_ids = numpy.array(list(set(exon_gene_ids).difference(set(gene_features_df_rownames))))
    print new_ids.shape, type(new_ids)
    new_df = {}
    for new_id in new_ids:
        new_df[new_id] = {col: numpy.nan for col in gene_features_df.columns}
    new_df = DataFrame.from_dict(new_df, orient='index')
    gene_features_df.append(new_df)

    for colname in gene_features_df.columns:
        values = gene_features_df.loc[exon_gene_ids, colname].as_matrix()
        mean_values[colname] = values

    write_matrix(mean_values, out_fhand)
