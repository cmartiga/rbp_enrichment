#!/usr/bin/env python

import argparse
import csv

import numpy

from motif.matrix import read_matrix, write_matrix, calc_combination_matrix, \
    test_pairwise_association


if __name__ == '__main__':

    # Create arguments
    description = 'Calculates the association of pairs of motifs '
    description += 'that appear together more often than expected by chance'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Matrix')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    help_msg = 'File containing groups information for the different ids'
    parser.add_argument('-g', '--grouping', default=None,
                        help=help_msg)
    help_msg = 'Selected group. If given then the association of motifs will '
    help_msg + 'be calculated instead'
    parser.add_argument('-sg', '--sel_group', default=None, help=help_msg)
    help_msg = 'File containing combinations of regions to look for enriched'
    help_msg += ' pairs of motifs when runnning combinations. Otherwise, all '
    help_msg += 'pairwise comparisons will be analysed'
    parser.add_argument('-cb', '--combinations_fpath', default=None,
                        help=help_msg)
    help_msg = 'Filter out combination of motifs in the same region'
    parser.add_argument('-k', '--remove_same_regions', default=False,
                        action='store_true', help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_fpath = parsed_args.output
    grouping_fpath = parsed_args.grouping
    sel_group = parsed_args.sel_group
    combinations_fpath = parsed_args.combinations_fpath
    keep_same_regions = not parsed_args.remove_same_regions
    if sel_group is not None:
        if grouping_fpath is None:
            raise ValueError('Grouping data is required to select a group')
        grouping = read_matrix(open(grouping_fpath))
    else:
        grouping = None
    if combinations_fpath is not None:
        reg_pair = [line.strip().split()
                    for line in open(combinations_fpath)]
    else:
        reg_pair = None

    # Calculate the combination matrix
    matrix = read_matrix(in_fhand)
    common_ids = numpy.intersect1d(grouping.index, matrix.index)
    matrix = matrix.loc[common_ids, :]
    grouping = grouping.loc[common_ids, :]

    selected_ids = matrix.loc[grouping.iloc[:, 0] == sel_group, :].index
    matrix = matrix.loc[selected_ids, :]
    ksp = keep_same_regions
    assoc_tests = test_pairwise_association(matrix, sel_region_pairs=reg_pair,
                                            keep_same_regions=ksp)
    out_fhand = open(out_fpath, 'w')
    fieldnames = ['id', 'present1', 'absent1', 'present2', 'absent2',
                  'pvalue', 'OR']
    writer = csv.DictWriter(out_fhand, fieldnames=fieldnames)
    writer.writeheader()
    for record in assoc_tests:
        writer.writerow(record)
    out_fhand.close()
