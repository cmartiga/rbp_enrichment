#!/usr/bin/env python

import argparse
import sys

from functions.enrichment import sets_df_to_gmt_file
from motif.matrix import read_matrix


if __name__ == '__main__':

    description = 'Transforms counts tables into GMT format for GSEA'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', default=None)
    parser.add_argument('-o', '--output', default=None)

    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath) if in_fpath else sys.stdin
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w') if in_fpath else sys.stdout

    counts_df = read_matrix(in_fhand)
    sets_df_to_gmt_file(counts_df, out_fhand)
