#!/usr/bin/env python

import argparse
from itertools import combinations

import numpy
from pandas import read_table

from motif.enrichment import poisson_regression
from motif.matrix import read_matrix, write_matrix
from statistics.enrichment import fisher_test
from utils.settings import FISHER, GLM


if __name__ == '__main__':
    description = 'Performs motif enrichment analyses of the given motifs '
    description += 'counts. Performs pairwise fisher exact tests for the '
    description += 'different groups and a GLM using Poisson distribution '
    description += 'to model the motifs counts as functions of the groups, '
    description += 'sequence length and any other given covariate'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'File containing motifs counts to analyze'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output dir')
    help_msg = 'Enrichment test to perform ({} or {})'.format(FISHER, GLM)
    parser.add_argument('-t', '--test', default=FISHER,
                        help=help_msg)
    help_msg = 'Tab separated file with additional covariates. The first '
    help_msg += ' column should contain the information about grouping'
    parser.add_argument('-g', '--covariates', required=True, help=help_msg)
    help_msg = 'The independent variable is continuous instead of categorical'
    help_msg += '. ex. Instead of groups use dPSI, log2FC'
    parser.add_argument('-n', '--continuous', default=False,
                        action='store_true', help=help_msg)
    help_msg = 'Use region length as a covariate instead of seq_length field'
    parser.add_argument('-r', '--by_region', default=False,
                        action='store_true', help=help_msg)
    parser.add_argument('-c', '--comparisons_fpath', default=None,
                        help='File containing groups to compare')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fhand = parsed_args.input
    matrix = read_matrix(in_fhand)
    enrichment_test = parsed_args.test
    out_prefix = parsed_args.output
    is_categorical = not parsed_args.continuous
    by_region = parsed_args.by_region

    covariates_df = read_table(open(parsed_args.covariates), sep='\t',
                               index_col=0)

    common_ids = numpy.intersect1d(covariates_df.index, matrix.index)
    grouping_variable = covariates_df.columns[0]
    grouping = numpy.array(covariates_df.loc[common_ids,
                                             grouping_variable])
    matrix = matrix.loc[common_ids, :]
    groups = numpy.unique(grouping)

    comparisons_fpath = parsed_args.comparisons_fpath
    if comparisons_fpath is None:
        group_pairs = list(combinations(groups, 2))
    else:
        group_pairs = [line.strip().split('\t')
                       for line in open(comparisons_fpath)]

    # Perform fisher test
    if enrichment_test == FISHER:
        if not is_categorical:
            raise ValueError('Fisher test uses grouping')
        rownames = matrix.index
        for group1, group2 in group_pairs:
            out_fpath = '{}.{}_vs_{}.tab'.format(out_prefix, group1, group2)
            out_fhand = open(out_fpath, 'w')
            rownames1 = rownames[grouping == group1]
            rownames2 = rownames[grouping == group2]
            test = fisher_test(matrix, rownames1, rownames2)
            write_matrix(test, out_fhand)

    # Perform Poisson regression
    elif enrichment_test == GLM:
        covariates_df.drop(grouping_variable, inplace=True, axis=1)
        if len(covariates_df.shape) == 1 or covariates_df.shape[1] == 0:
            covariates_df = None
        test = poisson_regression(matrix, grouping,
                                  is_categorical=is_categorical,
                                  additional_covariates=covariates_df,
                                  by_region=by_region)
        out_fhand = open(out_prefix, 'w')
        write_matrix(test, out_fhand)

    else:
        msg = 'Enrichment method not supported: {}'.format(enrichment_test)
        raise ValueError(msg)
