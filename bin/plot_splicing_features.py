#!/usr/bin/env python

import argparse

import numpy
import pandas
from pysam import FastaFile
import seaborn

from motif.matrix import write_matrix, read_matrix
from motif.stats import pairwise_mannwhitneyu
from utils.plot import get_new_figure_and_canvas, plot_ppt_features


if __name__ == '__main__':
    description = 'Makes volcano plots from the resulting matrices of the '
    description += 'enrichment analysis'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Splicing features files prefix')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    help_msg = 'File containin row ids and grouping information'
    parser.add_argument('-g', '--grouping', default=None, help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_prefix = parsed_args.input
    out_prefix = parsed_args.output
    grouping = parsed_args.grouping

    # Read splicing features data
    ss_scores = read_matrix(open(in_prefix + '.ss_scores.tab'))
    ppt_features = {}
    for region_name in ['Upstream_intron', 'Downstream_intron']:
        fpath = '{}.ppt.{}.tab'.format(in_prefix, region_name)
        ppt_features[region_name] = read_matrix(open(fpath))

    # Plot per group splicing features distributions
    grouping_df = read_matrix(grouping)
    common_ids = numpy.intersect1d(ss_scores.index, grouping_df.index)
    grouping_df = grouping_df.loc[common_ids, :]
    fig, canvas = get_new_figure_and_canvas(figsize=(10, 7))
    axes = fig.add_subplot(111)
    splice_sites = list(ss_scores.columns)
    ss_scores['Grouping'] = grouping_df.ix[common_ids, 0]
    ss_score_melted = pandas.melt(ss_scores, id_vars='Grouping')
    ss_score_melted.columns = ['Group', 'Splice site', 'Splice site score']
    axes = seaborn.boxplot(x='Splice site', y='Splice site score',
                           hue='Group', data=ss_score_melted,
                           ax=axes, showfliers=False)
    axes.set_ylim(0, 20)
    axes.set_title('Splice site strength')
    out_fhand = open(out_prefix + '.ss_score.png', 'w')
    canvas.print_figure(out_fhand)
    out_fhand.close()

    ss_scores.drop('Grouping', axis=1, inplace=True)
    test = pairwise_mannwhitneyu(ss_scores, grouping_df)
    out_fhand = open(out_prefix + '.ss_score.mannwhitneyu.tab', 'w')
    write_matrix(test, out_fhand)

    grouping_df = read_matrix(grouping)
    common_ids = numpy.intersect1d(ppt_features.values()[0].index,
                                   grouping_df.index)
    grouping_df = grouping_df.loc[common_ids, :]
#     for region_name, features_df in ppt_features.items():
#         if 'Grouping' in features_df.columns:
#             features_df.drop('Grouping', axis=1, inplace=True)
#         if 'Group' in features_df.columns:
#             features_df.drop('Group', axis=1, inplace=True)
#         if 'bp_seq' in features_df.columns:
#             features_df.drop('bp_seq', axis=1, inplace=True)
#         test = pairwise_mannwhitneyu(features_df, grouping_df.copy())
#         fpath = '{}.ppt.{}.manwhitneyu.tab'.format(out_prefix, region_name)
#         out_fhand = open(fpath, 'w')
#         write_matrix(test, out_fhand)

    out_fhand = open('{}.ppt.png'.format(out_prefix), 'w')
    for df in ppt_features.values():
        df = df.loc[common_ids, :]
    plot_ppt_features(ppt_features, grouping_df, out_fhand)
