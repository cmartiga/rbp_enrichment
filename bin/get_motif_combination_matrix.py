#!/usr/bin/env python

import argparse

from motif.matrix import read_matrix, write_matrix, calc_combination_matrix


if __name__ == '__main__':

    # Create arguments
    description = 'Calculates the presence matrix of all pairs of motifs from '
    description += 'a given matrix. It can be used to look for enrichment of'
    description += 'pairs of motifs in one group vs other or to look for motif'
    description += 's in a certain that appear together more often than '
    description += 'expected by chance'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Matrix')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    help_msg = 'Split the outputs in matrices with this number of combinations'
    help_msg += ' of motifs. Use to avoid memory problems'
    parser.add_argument('-c', '--chunk_size', default=None, type=int,
                        help=help_msg)
    help_msg = 'File containing combinations of regions to look for enriched'
    help_msg += ' pairs of motifs when runnning combinations. Otherwise, all '
    help_msg += 'pairwise comparisons will be analysed'
    parser.add_argument('-cb', '--combinations_fpath', default=None,
                        help=help_msg)
    help_msg = 'Filter out combination of motifs in the same region'
    parser.add_argument('-k', '--remove_same_regions', default=False,
                        action='store_true', help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_prefix = parsed_args.output
    chunk_size = parsed_args.chunk_size
    combinations_fpath = parsed_args.combinations_fpath
    keep_same_regions = not parsed_args.remove_same_regions
    if combinations_fpath is not None:
        reg_pair = [line.strip().split()
                    for line in open(combinations_fpath)]
    else:
        reg_pair = None

    # Calculate the combination matrix
    matrix = read_matrix(in_fhand)
    ksp = keep_same_regions
    combi_matrices = calc_combination_matrix(matrix, chunk_size=chunk_size,
                                             sel_region_pairs=reg_pair,
                                             keep_same_regions=ksp)
    for i, combi_matrix in enumerate(combi_matrices):
        if chunk_size is None:
            out_fpath = out_prefix
        else:
            out_fpath = '{}.{}'.format(out_prefix, i)
        out_fhand = open(out_fpath, 'w')
        write_matrix(combi_matrix, out_fhand)
