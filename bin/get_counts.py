#!/usr/bin/env python

import argparse

from pysam import Tabixfile

from motif.finder import calc_tabix_counts_matrices
from motif.matrix import write_matrix
from seq.events import fetch_splicing_events
from seq.genes import fetch_gene_events
from seq.parsers import parse_gtf
from utils.settings import (MATS_FORMAT, VAST_FORMAT, INTRON_WINDOW_SIZE,
                            BED_FORMAT, EVENTS_TYPES, EX, EXON_WINDOW_SIZE,
                            ALT_WINDOW_SIZE, GTF)


if __name__ == '__main__':
    description = 'Tool to generate a table with counts in any event or gene'
    description += ' region from given input file'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)

    # Input options
    input_group = parser.add_argument_group('Input options')
    help_msg = 'File containing alternative splicing events'
    input_group.add_argument('input', help=help_msg)
    help_msg = 'Input file format: {} (def), {} or {} or {} supported'
    input_group.add_argument('-f', '--format', default=MATS_FORMAT,
                             help=help_msg.format(MATS_FORMAT, VAST_FORMAT,
                                                  BED_FORMAT, GTF))
    help_msg = 'File containing the names of intervals or RBPs to find counts'
    input_group.add_argument('-r', '--interval_names_file', required=True,
                             help=help_msg)
    help_msg = 'Type of event to select {}'.format(EVENTS_TYPES)
    input_group.add_argument('-e', '--sel_event_type', default=EX,
                             help=help_msg)
    input_group.add_argument('-s', '--sorted_gtf', default=False,
                             action='store_true')

    # Database options
    database_group = parser.add_argument_group('Database options')
    help_msg = 'BED file containing motifs or binding positions indexed with'
    database_group.add_argument('-db', '--db_file', default=None,
                                help=help_msg + ' tabix.')
    help_msg = 'Col number in bed file containing RBPs names (0-indexed)'
    database_group.add_argument('-dc', '--rbps_col', default=4, type=int,
                                help=help_msg)

    # Output options
    output_group = parser.add_argument_group('Output options')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output prefix')

    # Windows group
    options_group = parser.add_argument_group('Windows options')
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    options_group.add_argument('-ew', '--exon_window_size', type=int,
                               default=EXON_WINDOW_SIZE, help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    options_group.add_argument('-iw', '--intron_window_size', type=int,
                               default=INTRON_WINDOW_SIZE, help=help_msg)
    help_msg = 'Other window size({})'.format(ALT_WINDOW_SIZE)
    options_group.add_argument('-aw', '--alternative_window_size', type=int,
                               default=ALT_WINDOW_SIZE, help=help_msg)

    # Parse arguments and check errors
    parsed_args = parser.parse_args()
    events_fpath = parsed_args.input
    file_format = parsed_args.format
    db_fpath = parsed_args.db_file
    tabix_file = Tabixfile(db_fpath)
    db_rbps_fpath = parsed_args.interval_names_file
    db_rbps_names = [line.strip() for line in open(db_rbps_fpath)]
    out_prefix = parsed_args.output
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    alt_ws = parsed_args.alternative_window_size
    sel_event_type = parsed_args.sel_event_type
    rbps_col = parsed_args.rbps_col
    is_sorted = parsed_args.sorted_gtf

    if file_format == GTF:
        gtf = parse_gtf(open(events_fpath))
        features_ws = {'exon': exon_ws, 'CDS': exon_ws, 'Intron': intron_ws}
        events = fetch_gene_events(gtf, features_ws=features_ws,
                                   is_sorted=is_sorted)
    else:
        events = fetch_splicing_events(events_fpath,
                                       file_format=file_format,
                                       sel_event_type=sel_event_type,
                                       take_windows=True,
                                       exon_window_size=exon_ws,
                                       intron_window_size=intron_ws,
                                       alternative_window_size=alt_ws)

    # Calc_motif_counts
    regions, counts_dfs = calc_tabix_counts_matrices(events, db_rbps_names,
                                                     tabix_file,
                                                     rbps_col=rbps_col)
    for region, counts_df in zip(regions, counts_dfs):
        out_fpath = '{}.{}.tab'.format(out_prefix, region)
        out_fhand = open(out_fpath, 'w')
        write_matrix(counts_df, out_fhand)
