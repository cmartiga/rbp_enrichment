#!/usr/bin/env python

from argparse import ArgumentError
import argparse

from bx.bbi.bigwig_file import BigWigFile
from pyliftover import LiftOver
from pysam import FastaFile, Tabixfile

from seq.events import fetch_splicing_events
from seq.writers import write_splicing_events
from utils.settings import MATS_FORMAT, EX, INT, BED


if __name__ == '__main__':
    description = 'Fetches sequences corresponding to exon cassettes '
    description += 'and writes them in Fasta files for each region'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Files containing exons to analyze')
    parser.add_argument('-f', '--file_format', default=MATS_FORMAT,
                        help='Input file format (MATS)')
    parser.add_argument('-s', '--splicing_event', default=EX,
                        help='Type of splicing events (Exon_cassette)')

    parser.add_argument('-r', '--ref_index', default=None,
                        help='Genome fasta file indexed with Samtools')

    help_msg = 'Path for BigWig file. Several can be given. If comma '
    help_msg += 'separated, ratio between the two will be calculated instead.'
    help_msg += ' For ChIP-seq for example'
    parser.add_argument('-bw', '--bigwig_fpaths', default=[], action='append',
                        help=help_msg)
    help_msg = 'label for BigWig file track. Several can be given'
    parser.add_argument('-bl', '--bigwig_labels', default=[], action='append',
                        help=help_msg)
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')

    parser.add_argument('-l', '--liftover', default=False, action='store_true',
                        help='Transform coordinates from mm9 to mm10')
    parser.add_argument('-lr', '--rev_liftover', default=False,
                        action='store_true',
                        help='Transform coordinates from mm10 to mm9')

    help_msg = 'Path for tabix indexed file. Several can be given'
    parser.add_argument('-ti', '--tabix_index_fpaths', default=[],
                        action='append', help=help_msg)
    parser.add_argument('-tl', '--tabix_labels', default=[], action='append',
                        help='label for tabix file. Several can be given')
    parser.add_argument('-tc', '--tabix_score_col', default=5, type=int,
                        help='Col number with score in tabix indexed file (5)')

    help_msg = 'Path for tabix indexed file. Several can be given'
    parser.add_argument('-c', '--clipseq_fpaths', default=[], action='append',
                        help=help_msg)
    help_msg = 'label for clipseq file track. Several can be given'
    parser.add_argument('-cl', '--clipseq_labels', default=[], action='append',
                        help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    if sel_event_type not in [EX, INT, BED]:
        msg = sel_event_type + ' type of event not supported. '
        msg += 'Only Exon_cassette or Intron_retention are allowed'
        raise ArgumentError(msg)
    fasta_fpath = parsed_args.ref_index
    ref_fastafile = FastaFile(fasta_fpath) if fasta_fpath else None
    bw_fpaths = parsed_args.bigwig_fpaths
    bw_labels = parsed_args.bigwig_labels
    bw_files = [[BigWigFile(open(bw)) for bw in bw_fpath.split(',')]
                for bw_fpath in bw_fpaths]
    clipseq_fpaths = parsed_args.clipseq_fpaths
    clipseq_labels = parsed_args.clipseq_labels
    clipseq_indexes = [Tabixfile(clipseq_fpath)
                       for clipseq_fpath in clipseq_fpaths]
    tabix_fpaths = parsed_args.tabix_index_fpaths
    tabix_labels = parsed_args.tabix_labels
    tabix_files = [Tabixfile(tabix_fpath) for tabix_fpath in tabix_fpaths]
    score_col = parsed_args.tabix_score_col
    out_prefix = parsed_args.output
    liftover = None
    if parsed_args.liftover:
        liftover = LiftOver('mm9', 'mm10')
        if parsed_args.rev_liftover:
            liftover = LiftOver('mm10', 'mm9')

    # Fetch and write sequences
    splicing_events = fetch_splicing_events(in_fpath, file_format,
                                            ref_fastafile=ref_fastafile,
                                            bigwigfiles=bw_files,
                                            bw_tags=bw_labels,
                                            liftover=liftover,
                                            sel_event_type=sel_event_type)
    write_splicing_events(splicing_events, out_prefix, only_id=True)
