#!/usr/bin/env python

import argparse
import csv
import itertools
import sys

import numpy

from evolution.selection import get_changes_index
from statistics.base import EmpiricalPvalueCalculator
from utils.plot import get_new_figure_and_canvas


if __name__ == '__main__':
    description = 'Finds out if changes in seq affect specific RNA binding '
    description += 'motifs from a database'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='File containing changes to analyze')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-m', '--min_af', default=None, type=float,
                        help='Select only SNPs with a given min AF ')
    parser.add_argument('-g', '--grouping', required=True,
                        help='File containing group information of events')
    parser.add_argument('-n', '--variable', default=None,
                        help='Name of feature to compare changes')
    parser.add_argument('-N', '--n_quantiles', default=1000, type=int,
                        help='Number of quantiles for QQ-plot (1000)')
    help_msg = 'Number of bootstrap for p-value calculations (200)'
    parser.add_argument('-b', '--n_bootstrap', default=200, type=int,
                        help=help_msg)
    parser.add_argument('-v', '--verbose', default=False,
                        action='store_true',)
    parser.add_argument('-i', '--ids_fpath', required=True,
                        help='File containing ids to change')

    # Parse arguments
    parsed_args = parser.parse_args()
    data_fpath = parsed_args.input
    grouping_fpath = parsed_args.grouping
    min_af = parsed_args.min_af
    out_prefix = parsed_args.output
    n_quantiles = parsed_args.n_quantiles
    variable = parsed_args.variable
    n_bootstrap = parsed_args.n_bootstrap
    verbose = parsed_args.verbose
    ids_fpath = parsed_args.ids_fpath

    # Run program
    records = csv.DictReader(open(data_fpath), delimiter='\t')
    changes_idx = get_changes_index(records, additional_change_type=variable)
    if verbose:
        sys.stderr.write('Genes index read\n')

    ids_idx = {}
    for line in open(ids_fpath):
        if 'Id' in line or line == '\n':
            continue
        items = line.strip().split()
        ids_idx[items[0]] = items[1]

    groups_idx = {}
    for i, line in enumerate(open(grouping_fpath)):
        if 'exonId' in line or line == '\n':
            continue
        event_id, group = line.strip().split()
        gene_id = ids_idx.get(event_id, None)
        if gene_id is None:
            continue

        for region, region_idx in changes_idx.get(gene_id, {}).items():
            if variable is None:
                try:
                    try:
                        groups_idx[region][group].extend(region_idx['polymorphism'])
                    except KeyError:
                        groups_idx[region][group] = region_idx['polymorphism']
                except KeyError:
                    groups_idx[region] = {group: region_idx['polymorphism']}
            else:
                for change_type in region_idx['polymorphism'].keys():
                    try:
                        try:
                            try:
                                groups_idx[region][change_type][group].extend(region_idx['polymorphism'][change_type])
                            except KeyError:
                                groups_idx[region][change_type][group] = region_idx['polymorphism'][change_type]
                        except KeyError:
                            groups_idx[region][change_type] = {group: region_idx['polymorphism'][change_type]}
                    except KeyError:
                        groups_idx[region] = {change_type: {group: region_idx['polymorphism'][change_type]}}

    percentiles = numpy.linspace(0, 100, n_quantiles)
    colors = ['blue', 'red', 'purple', 'orange', 'brown', 'green']

    if not variable:
        for region, region_idx in groups_idx.items():
            if verbose:
                sys.stderr.write('Analyzing region: {}\n'.format(region))
            out_fpath = '{}.{}.png'.format(out_prefix, region)
            groups = region_idx.keys()
            comparisons = list(itertools.combinations(groups, 2))
            n_comparisons = len(comparisons)
            fig, canvas = get_new_figure_and_canvas((7 * n_comparisons, 7))
            for i, (group1, group2) in enumerate(comparisons):
                axes = fig.add_subplot(100 + n_comparisons * 10 + 1 + i)
                axes.plot(percentiles / 100.0, percentiles / 100.0,
                          label='Expected', c='black')
                x = region_idx[group1]
                x_size = len(x)
                y = region_idx[group2]
                y_size = len(y)
                x_percentiles = numpy.percentile(x, percentiles)
                y_percentiles = numpy.percentile(y, percentiles)
                diff = numpy.sum(x_percentiles - y_percentiles)
                random_diffs = []
                for _ in xrange(n_bootstrap):
                    shuffled = numpy.append(x, y)
                    numpy.random.shuffle(shuffled)
                    x = shuffled[:x_size]
                    y = shuffled[x_size:]
                    x_perc = numpy.percentile(x, percentiles)
                    y_perc = numpy.percentile(y, percentiles)
                    axes.plot(x_perc, y_perc, alpha=0.01, c='blue')
                    random_diffs.append(numpy.sum(x_perc - y_perc))
                calc_empirical_pvalue = EmpiricalPvalueCalculator(random_diffs,
                                                                  'two-sided')
                pvalue = calc_empirical_pvalue(diff)
                axes.plot(x_percentiles, y_percentiles, c='blue',
                          label='pvalue={}'.format(pvalue))
                axes.set_ylabel(group1)
                axes.set_xlabel(group2)
                axes.legend(loc='best')
                axes.set_title('{} vs {}'.format(group1, group2))
            fig.savefig(out_fpath)
    else:
        for region, region_idx in groups_idx.items():
            if verbose:
                sys.stderr.write('Analyzing region: {}\n'.format(region))
            out_fpath = '{}.{}.png'.format(out_prefix, region)
            change_types = region_idx.keys()
            comparisons = list(itertools.combinations(change_types, 2))
            n_comparisons = len(comparisons)
            fig, canvas = get_new_figure_and_canvas((7 * n_comparisons, 7))
            for i, (change_type1, change_type2) in enumerate(comparisons):
                axes = fig.add_subplot(100 + n_comparisons * 10 + 1 + i)
                groups = region_idx[change_type1].keys()
                axes.plot(percentiles / 100.0, percentiles / 100.0,
                          label='Expected', c='black')
                for group, color in zip(groups, colors):
                    if group not in region_idx[change_type1] or group not in region_idx[change_type2]:
                        continue
                    x = region_idx[change_type1][group]
                    x_size = len(x)
                    y = region_idx[change_type2][group]
                    y_size = len(y)
                    if verbose:
                        sys.stderr.write('{}: {}\n{}: {}\n'.format(change_type1, x_size,
                                                                   change_type2, y_size))
                    x_percentiles = numpy.percentile(x, percentiles)
                    y_percentiles = numpy.percentile(y, percentiles)
                    diff = numpy.sum(x_percentiles - y_percentiles)
                    random_diffs = []
                    for _ in xrange(n_bootstrap):
                        shuffled = numpy.append(x, y)
                        numpy.random.shuffle(shuffled)
                        x = shuffled[:x_size]
                        y = shuffled[x_size:]
                        x_perc = numpy.percentile(x, percentiles)
                        y_perc = numpy.percentile(y, percentiles)
                        axes.plot(x_perc, y_perc, alpha=0.01, c=color)
                        random_diffs.append(numpy.sum(x_perc - y_perc))
                    calc_empirical_pvalue = EmpiricalPvalueCalculator(random_diffs,
                                                                      'two-sided')
                    pvalue = calc_empirical_pvalue(diff)
                    axes.plot(x_percentiles, y_percentiles, c=color,
                              label='{} pvalue={}'.format(group, pvalue))

                axes.set_ylabel(change_type2)
                axes.set_xlabel(change_type1)
                axes.legend(loc='best')
                axes.set_title('{} vs {}'.format(change_type1, change_type2))
            fig.savefig(out_fpath)
