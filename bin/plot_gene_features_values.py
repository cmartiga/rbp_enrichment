#!/usr/bin/env python

import argparse

import numpy
from pandas import melt
import seaborn

from motif.matrix import read_matrix
from utils.plot import get_new_figure_and_canvas


if __name__ == '__main__':
    description = 'Values boxplots by grouping and region type'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Gene features values file')
    parser.add_argument('-o', '--output', required=True, help='Output prefix')
    help_msg = 'File containing regions in plotting order'
    parser.add_argument('-g', '--grouping', required=True, help=help_msg)
    parser.add_argument('-y', '--ylabel', default=None)
    parser.add_argument('-i', '--ids_col', default=0, type=int)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    out_prefix = parsed_args.output
    grouping_fhand = open(parsed_args.grouping)
    grouping_df = read_matrix(grouping_fhand)
    ylabel = parsed_args.ylabel
    ids_col = parsed_args.ids_col
    features_df = read_matrix(open(in_fpath), rownames_col=ids_col)

    # Plot results
    common_ids = numpy.intersect1d(features_df.index, grouping_df.index)
    features_df = features_df.loc[common_ids, :]
    features_df['Group'] = grouping_df.ix[common_ids, 0]
    features_df = melt(features_df, 'Group')
    features_df.columns = ['Group', 'Region', ylabel]
    region_type = []
    for region in features_df['Region']:
        if region.startswith('exon'):
            region_type.append('NC-Exon')
        elif 'CDS' in region or 'exon' in region:
            region_type.append('Exon')
        else:
            region_type.append('Intron')
    features_df['Region type'] = region_type

    for region_type in numpy.unique(region_type):
        sel_data = features_df[features_df['Region type'] == region_type]
        print sel_data

        out_fpath = out_prefix + '.{}.png'.format(region_type)
        out_fhand = open(out_fpath, 'w')
        n_regions = numpy.unique(sel_data['Region']).shape[0]
        fig, canvas = get_new_figure_and_canvas((n_regions * 2, 7))
        axes = fig.add_subplot(111)
        axes = seaborn.boxplot(x='Region', y=ylabel, hue='Group',
                               data=sel_data, ax=axes, showfliers=False)
        axes.set_title(region_type)
        canvas.print_figure(out_fhand)
        out_fhand.close()
