#!/usr/bin/env python

import argparse

from motif.enrichment import perform_gsea
from motif.matrix import read_matrix, write_matrix


if __name__ == '__main__':
    description = 'Performs Gene Set Enrichment Analysis using groups of seq'
    description += 'uences with motifs as set. It ranks the sequences accordin'
    description += 'g to the correlation of a series of features, such as PSI'
    description += 'with two phenotypes with their replicates'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'File containing motifs counts to analyze'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output file')
    help_msg = 'Tab separated file with feature values to correlate with '
    help_msg += 'phenotypes for each individual'
    parser.add_argument('-f', '--features', required=True, help=help_msg)
    help_msg = 'Size of the empirical null ES distribution to generate (10000)'
    parser.add_argument('-s', '--null_size', default=10000, type=int,
                        help=help_msg)
    help_msg = 'Number of threads to calculate the empirical distribution of'
    help_msg += ' enrichment scores'
    parser.add_argument('-t', '--n_threads', default=1, type=int,
                        help=help_msg)
    parser.add_argument('-v', '--verbose', default=False, action='store_true',
                        help='Show iterations evolution')
    help_msg = 'When generating the null distribution shuffle the motif presen'
    help_msg += 'ce tags instead of phenotype labels'
    parser.add_argument('-m', '--shuffle_motifs', default=False,
                        action='store_true', help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fhand = parsed_args.input
    motif_matrix = read_matrix(in_fhand)
    out_fhand = open(parsed_args.output, 'w')
    n_iter = parsed_args.null_size
    features_matrix = read_matrix(parsed_args.features)
    features_matrix = features_matrix.loc[motif_matrix.index, :]
    n_threads = parsed_args.n_threads
    shuffle_labels = not parsed_args.shuffle_motifs
    verbose = parsed_args.verbose

    # Perform GSEA
    enrichment_df = perform_gsea(motif_matrix, features_matrix, n_iter=n_iter,
                                 verbose=verbose, n_threads=n_threads,
                                 shuffle_labels=shuffle_labels)
    write_matrix(enrichment_df, out_fhand)
