#!/usr/bin/env python

import argparse
import csv
import sys

import RNA
from pysam import FastaFile

from seq.events import fetch_splicing_events
from structure.predict import calc_rnazfold_zscore
from utils.settings import MATS_FORMAT, EX, INT, A_tag


if __name__ == '__main__':
    description = 'Makes volcano plots from the resulting matrices of the '
    description += 'enrichment analysis'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Files containing exons to analyze')
    parser.add_argument('-f', '--file_format', default=MATS_FORMAT,
                        help='Input file format (MATS)')
    parser.add_argument('-s', '--splicing_event', default=EX,
                        help='Type of splicing events (Exon_cassette)')
    parser.add_argument('-r', '--ref_index', default=None,
                        help='Genome fasta file indexed with Samtools')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-w', '--extra_length', default=30, type=int,
                        help='Window size around splice site to calculate MFE')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    if sel_event_type not in [EX, INT]:
        msg = sel_event_type + ' type of event not supported. '
        msg += 'Only Exon_cassette or Intron_retention are allowed'
        raise ValueError(msg)
    fasta_fpath = parsed_args.ref_index
    ref_fastafile = FastaFile(fasta_fpath) if fasta_fpath else None
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    extra_length = parsed_args.extra_length
    verbose = parsed_args.verbose

    # Calculate splice site scores and plot if grouping is given
    splicing_events = fetch_splicing_events(in_fpath, file_format,
                                            ref_fastafile=ref_fastafile,
                                            sel_event_type=sel_event_type)

    writer = None
    for i, splicing_event in enumerate(splicing_events):
        exon_length = splicing_event.regions[A_tag].length
        exon_id = splicing_event.id
        record = {'id': exon_id, 'length': exon_length}
        if verbose and i % 100 == 0:
            sys.stderr.write('Events analyzed: {}\n'.format(i))

        for region_name, region in splicing_event.regions.items():
            seq = region.seq
            record['{}.length'.format(region_name)] = region.length
            gc_content = float(seq.count('C') + seq.count('G'))
            if region.length != 0:
                gc_content = gc_content / region.length
            else:
                gc_content = None
            record['{}.gc_content'.format(region_name)] = gc_content

        splicing_event.get_splice_sites_seqs(extra_length=extra_length)
        for ss, seq in splicing_event.splice_sites.items():
            region_name = '{}+-{}'.format(ss, extra_length)
            gc_content = float(seq.count('C') + seq.count('G'))
            if region.length != 0:
                gc_content = gc_content / len(seq)
            else:
                gc_content = None
            record['{}.gc_content'.format(region_name)] = gc_content

            strc, mfe = RNA.fold(seq)
            record['{}.MFE'.format(region_name)] = mfe
            record['{}.strc'.format(region_name)] = strc
            mfe_zscore = calc_rnazfold_zscore(seq, mfe, shuffle=True,
                                              use_mononc_freq=True,
                                              use_python=True,
                                              n_shuffle=100)
            record['{}.MFE_zscore'.format(region_name)] = mfe_zscore

        if writer is None:
            fieldnames = ['id']
            for fieldname in record.keys():
                if fieldname != 'id':
                    fieldnames.append(fieldname)
            writer = csv.DictWriter(out_fhand, fieldnames=fieldnames,
                                    delimiter='\t')
            writer.writeheader()
        writer.writerow(record)

    out_fhand.close()
