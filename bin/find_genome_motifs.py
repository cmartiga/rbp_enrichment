#!/usr/bin/env python

import argparse
from re import compile

from bx.bbi.bigwig_file import BigWigFile
from pysam import FastaFile

from motif.databases import read_attract_database
from motif.finder import find_gene_motifs
from seq.genes import get_gene_coord
from seq.parsers import parse_gtf
from utils.settings import RNAMOTIF, CISBP_RNA, REGEXP


if __name__ == '__main__':
    description = 'Performs motif search along all genes from a given '
    description += 'and writes the output in bed files for further analyses '

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'GTF file containing annotated genes'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output BED')
    help_msg = 'Fasta file with the reference genome indexed with samtools'
    parser.add_argument('-r', '--ref_fastafile', required=True,
                        help=help_msg)
    help_msg = 'BigWig file with values to store for each interval'
    parser.add_argument('-b', '--bigwigfile', default=None, help=help_msg)
    parser.add_argument('-db', '--motif_db', required=True,
                        help='File with motif database')
    help_msg = 'Database type ({}, {} or {})'.format(RNAMOTIF, CISBP_RNA,
                                                     REGEXP)
    parser.add_argument('-dt', '--db_type', default=RNAMOTIF, help=help_msg)
    parser.add_argument('-v', '--verbose', default=False, action='store_true')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    ref_fastafile = FastaFile(parsed_args.ref_fastafile)
    motif_db_type = parsed_args.db_type
    motif_db_fpath = parsed_args.motif_db
    verbose = parsed_args.verbose
    is_regexp = False
    if motif_db_type == RNAMOTIF:
        motif_db = read_attract_database(motif_db_fpath, reading_option='seq')
    elif motif_db_type == CISBP_RNA:
        raise NotImplementedError('This option is not supported yet')
    elif motif_db_type == REGEXP:
        motif_db = {line.strip().split()[0]: compile(line.strip().split()[1])
                    for line in open(motif_db_fpath)}
        is_regexp = True
    else:
        msg = 'Database type not valid. Only {} and {} are allowed'
        raise ValueError(msg.format(RNAMOTIF, CISBP_RNA))
    bw_fpath = parsed_args.bigwigfile
    if bw_fpath is None:
        bw_file = None
    else:
        bw_file = BigWigFile(open(bw_fpath))

    # Find motifs
    gtf = parse_gtf(in_fhand)
    gene_coord = get_gene_coord(gtf)
    for bed_line in find_gene_motifs(gene_coord, ref_fastafile,
                                     motif_db_by_seq=motif_db,
                                     bw_file=bw_file, verbose=verbose,
                                     is_regexp=is_regexp):
        out_fhand.write(bed_line)
