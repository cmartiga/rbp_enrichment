#!/usr/bin/env python

import argparse
import sys

import numpy

from motif.matrix import read_matrix, write_matrix


if __name__ == '__main__':

    description = 'Transforms counts tables into GMT format for GSEA'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', default=None)
    parser.add_argument('-i', '--ids_fpath', default=None,
                        help='File containing ids to change')
    parser.add_argument('-o', '--output', default=None)

    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath) if in_fpath else sys.stdin
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w') if in_fpath else sys.stdout
    ids_fpath = parsed_args.ids_fpath
    ids_df = None if ids_fpath is None else read_matrix(open(ids_fpath))

    in_df = read_matrix(in_fhand)
    common = numpy.intersect1d(in_df.index, ids_df.index)
    in_df = in_df.loc[common, :]
    in_df.index = ids_df.ix[common, 0]
    write_matrix(in_df, out_fhand)
