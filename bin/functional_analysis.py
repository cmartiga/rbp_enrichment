#!/usr/bin/env python

import argparse
import itertools

import numpy

from functions.enrichment import fisher_enrichment_analysis
from motif.matrix import read_matrix, write_matrix
from utils.plot import plot_functional_enrichment


if __name__ == '__main__':
    description = ''
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input', help='File containing groups to analyze')
    input_group.add_argument('-s', '--sets_fpath', required=True,
                             help='GMT file with sets to check enrichment')
    input_group.add_argument('-i', '--ids_fpath', default=None,
                             help='File containing ids to change')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-f', '--fdr_threshold', default=0.01,
                               type=float,
                               help='FDR threshold to filter gene sets (0.01)')
    help_msg = 'Plot barplot for enriched categories'
    options_group.add_argument('-p', '--plot', default=False,
                               action='store_true', help=help_msg)
    options_group.add_argument('-t', '--plot_top', default=None, type=int,
                               help='Plot only the given top gene sets (all)')
    options_group.add_argument('-c', '--comparisons_fpath', default=None,
                               help='File containing groups to compare')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output files prefix')
    output_group.add_argument('-v', '--verbose', default=False,
                              action='store_true')

    # Parse arguments
    parsed_args = parser.parse_args()
    groups_fpath = parsed_args.input
    groups_df = read_matrix(open(groups_fpath))
    sets_fpath = parsed_args.sets_fpath
    out_prefix = parsed_args.output
    verbose = parsed_args.verbose
    fdr_threshold = parsed_args.fdr_threshold
    sel_top = parsed_args.plot_top
    ids_fpath = parsed_args.ids_fpath
    ids_df = None if ids_fpath is None else read_matrix(open(ids_fpath))
    make_plots = parsed_args.plot
    comparisons_fpath = parsed_args.comparisons_fpath

    groups = numpy.unique([x for x in groups_df.iloc[:, 0] if str(x) != 'nan'])
    if comparisons_fpath is None:
        group_pairs = list(itertools.combinations(groups, 2))
    else:
        group_pairs = [line.strip().split('\t')
                       for line in open(comparisons_fpath)]

    # Perform enrichment
    enrichment_results = fisher_enrichment_analysis(sets_fpath, groups_df,
                                                    ids_df=ids_df,
                                                    comparisons=group_pairs,
                                                    verbose=verbose)
    for comparison, result_df in enrichment_results.items():
        out_fpath = '{}.{}.tab'.format(out_prefix, comparison)
        out_fhand = open(out_fpath, 'w')
        write_matrix(result_df, out_fhand)

        if make_plots:
            out_fpath = '{}.{}.png'.format(out_prefix, comparison)
            out_fhand = open(out_fpath, 'w')
            plot_functional_enrichment(result_df, comparison, out_fhand,
                                       fdr_threshold=fdr_threshold,
                                       sel_top=sel_top)
