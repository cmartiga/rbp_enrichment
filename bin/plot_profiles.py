#!/usr/bin/env python

import argparse

import numpy

from motif.matrix import read_matrix
from utils.plot import plot_regions_track_values


if __name__ == '__main__':
    description = 'Plots profiles for the different gruops of sequences'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'Files prefixes containing motifs profiles to plot. If 2 or'
    help_msg += ' more are given it will plot the ratio file1/file2 '
    help_msg += '(Ex: Nucleotide diversity / Conservation)'
    parser.add_argument('input', nargs='+', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output file')
    help_msg = 'File containing regions in plotting order'
    parser.add_argument('-r', '--regions', default=None, help=help_msg)
    help_msg = 'File containing row ids and grouping information'
    parser.add_argument('-g', '--grouping', required=True, help=help_msg)
    parser.add_argument('-w', '--window_size', default=None, type=int)
    parser.add_argument('-c', '--cumulative', default=False,
                        action='store_true',
                        help='Plot cumulative motif distribution')
    help_msg = 'Permute randomly the labels to calculate position-wise pvalues'
    parser.add_argument('-p', '--calc_pvalues', default=False,
                        action='store_true', help=help_msg)
    help_msg = 'Permute randomly the labels to calculate position-wise zscores'
    parser.add_argument('-z', '--zscore', default=False,
                        action='store_true', help=help_msg)
    help_msg = 'Size of the null distribution for p-value calculations'
    parser.add_argument('-n', '--null_size', default=1000, type=int,
                        help=help_msg)
    parser.add_argument('-t', '--n_threads', default=1, type=int)
    parser.add_argument('-y', '--ylabel', help='Y-axis label', default=None)
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    help_msg = 'Multiply profiles instead of dividing'
    parser.add_argument('-m', '--multiply', default=False, action='store_true',
                        help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_prefixes = parsed_args.input
    out_fhand = open(parsed_args.output, 'w')
    regions_fpath = parsed_args.regions
    regions = [line.strip() for line in open(regions_fpath)]
    multiply = parsed_args.multiply
    if len(in_prefixes) == 1:
        dfs = [read_matrix(open('{}.{}'.format(in_prefixes[0], region)))
               for region in regions]
    else:
        if multiply:
            dfs = [read_matrix(open('{}.{}'.format(in_prefixes[0], region))) *
                   read_matrix(open('{}.{}'.format(in_prefixes[1], region)))
                   for region in regions]
        else:
            dfs = [read_matrix(open('{}.{}'.format(in_prefixes[0], region))) /
                   read_matrix(open('{}.{}'.format(in_prefixes[1], region)))
                   for region in regions]

    grouping_fhand = open(parsed_args.grouping)
    grouping_df = read_matrix(grouping_fhand)
    window_size = parsed_args.window_size
    cumulative = parsed_args.cumulative
    calc_pvalues = parsed_args.calc_pvalues
    calc_zscore = parsed_args.zscore
    null_size = parsed_args.null_size
    verbose = parsed_args.verbose
    n_threads = parsed_args.n_threads
    ylabel = parsed_args.ylabel


    # Plot results
    seq_names = dfs[2].index
    common_names = numpy.intersect1d(seq_names, grouping_df.index)
    seqs_regions = [df.loc[common_names, :].as_matrix() for df in dfs]
    grouping_df = grouping_df.loc[common_names, :]
    if ylabel is None:
        ylabel = 'Proportion of sequences with motif'
    plot_regions_track_values(seqs_regions, common_names, regions,
                              grouping=grouping_df, out_fhand=out_fhand,
                              ylabel=ylabel, window_size=window_size,
                              cumulative=cumulative, calc_pvalues=calc_pvalues,
                              null_size=null_size, verbose=verbose,
                              n_threads=n_threads, calc_zscore=calc_zscore)
