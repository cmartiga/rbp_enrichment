#!/usr/bin/env python

import argparse

from pandas import DataFrame
from pysam import Tabixfile, AlignmentFile

from motif.matrix import write_matrix
from seq.genes import fetch_gene_events
from seq.parsers import parse_gtf
from utils.misc import LogTrack


if __name__ == '__main__':
    description = 'Calculates the mean value of a BigWigFile or presence of '
    description += 'Tabix indexed intervals in the different regions of a gene'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='GTF file containing annotation')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-a', '--add_chr', default=False,
                        action='store_true', help='Add "chr" to chrom name')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-s', '--sorted_gtf', default=False,
                        action='store_true')
    parser.add_argument('-b', '--bamfiles', required=True,
                        help='Comma separated list of bamfiles')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_fpath = parsed_args.output
    add_chr = parsed_args.add_chr
    verbose = parsed_args.verbose
    is_sorted = parsed_args.sorted_gtf
    bam_fpaths = parsed_args.bamfiles.split(',')
    bamfiles = {fpath: AlignmentFile(fpath, 'rb') for fpath in bam_fpaths}
    log = LogTrack()

    # Fetch and write profiles
    gtf = parse_gtf(in_fhand)
    matrix = []
    for i, gene_event in enumerate(fetch_gene_events(gtf, add_chr=add_chr,
                                                     is_sorted=is_sorted)):
        if verbose:
            if i % 100 == 0:
                log.write('Processed genes: {}'.format(i))

        for fpath, bamfile in bamfiles.items():
            feat_values = {'gene_id': gene_event.id, 'file': fpath,
                           'gene_type': gene_event.gene_type}
            for feature, intervals in gene_event.regions.items():
                for interval in intervals:
                    n_reads = interval.get_coverage(bamfile,
                                                    strand_specific=True)
                    try:
                        feat_values[feature] += n_reads
                    except KeyError:
                        feat_values[feature] = n_reads

                    field = '{}.length'.format(feature)
                    try:
                        feat_values[field] += interval.length
                    except KeyError:
                        feat_values[field] = interval.length
            matrix.append(feat_values)

    out_fhand = open(out_fpath, 'w')
    df = DataFrame(matrix)
    write_matrix(df, out_fhand)
