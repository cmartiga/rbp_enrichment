#!/usr/bin/env python

from _functools import partial
import argparse
import csv

from pyliftover.liftover import LiftOver
from pysam import FastaFile, Tabixfile

from evolution.changes import (classify_gc_change, classify_mfe_change,
                               classify_Gquad_change)
from evolution.selection import find_polymorphism_divergence_changes, \
    compare_polymophism_divergence_changes
from seq.events import fetch_splicing_events
from seq.genes import fetch_gene_events
from seq.parsers import parse_bed_events_idx, parse_gtf
from utils.misc import run_parallel
from utils.settings import (MATS_FORMAT, EX, EXON_WINDOW_SIZE,
                            INTRON_WINDOW_SIZE)


if __name__ == '__main__':
    description = 'Makes volcano plots from the resulting matrices of the '
    description += 'enrichment analysis'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Files containing exons to analyze')
    parser.add_argument('-f', '--file_format', default=MATS_FORMAT,
                        help='Input file format (MATS)')
    parser.add_argument('-s', '--splicing_event', default=EX,
                        help='Type of splicing events (Exon_cassette)')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-vcf', '--vcf_file', required=True,
                        help='VCF file containing variation information')
    help_msg = 'File containing region names in events to analyze'
    parser.add_argument('-r', '--regions', default=None, help=help_msg)
    parser.add_argument('-n', '--n_threads', default=1, type=int,
                        help='Number of threads to use (1)')
    parser.add_argument('-maf', '--min_maf', default=0.01, type=float,
                        help='Min maf to filter out SNPs')
    help_msg = 'Do not perform divergence calculations. Only polymorphism'
    parser.add_argument('-sd', '--skip_divergence', default=False,
                        action='store_true', help=help_msg)
    parser.add_argument('-v', '--verbose', default=False, action='store_true')

    genomes_group = parser.add_argument_group('Genomes arguments')
    genomes_group.add_argument('-rg', '--ref_fastafile', default=None,
                               help='Genome fasta file indexed with Samtools')
    help_msg = 'Alternative genome fasta file indexed with Samtools'
    genomes_group.add_argument('-ag', '--alt_fastafile', default=None,
                               help=help_msg)
    help_msg = 'Outgroup genome fasta file indexed with Samtools'
    genomes_group.add_argument('-og', '--outgroup_fastafile', default=None,
                               help=help_msg)
    help_msg = 'LiftOver file forma: chain (def) or liftedover_bed'
    genomes_group.add_argument('-lf', '--liftover_format', default='chain',
                               help=help_msg)
    genomes_group.add_argument('-al', '--alt_liftover', default=None,
                               help='Alternative genome LiftOver file')
    genomes_group.add_argument('-ol', '--out_liftover', default=None,
                               help='Outgroup genome LiftOver file')

    fields_group = parser.add_argument_group('VCF fielnames options')
    help_msg = 'Minimum Allele Frequency field in VCF file (def: MAF)'
    fields_group.add_argument('-mf', '--af_field', default='MAF',
                              help=help_msg)
    help_msg = 'Variant type field in VCF file (def: TSA)'
    fields_group.add_argument('-vt', '--variant_type_field',
                              default='TSA', help=help_msg)
    parser.add_argument('-sg', '--sorted_gtf', default=False,
                        action='store_true')
    help_msg = 'Run classification of changes by stability, GC change and G-qu'
    help_msg += 'adruplex composition change'
    parser.add_argument('-c', '--characterize_changes', default=False,
                        action='store_true', help=help_msg)
    parser.add_argument('-si', '--skip_ids_fpath', default=None,
                        help='Skip ids in the given file')

    windows_group = parser.add_argument_group('Windows options')
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    windows_group.add_argument('-ew', '--exon_window_size',
                               default=EXON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    windows_group.add_argument('-iw', '--intron_window_size',
                               default=INTRON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Window to consider around changes to classify them. By default'
    help_msg += ' it will take the whole sequence'
    windows_group.add_argument('-w', '--window_size', default=None, type=int,
                               help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    regions = parsed_args.regions
    skip_div = parsed_args.skip_divergence
    if regions is not None:
        regions = [line.strip() for line in open(regions)]
    elif not skip_div:
        raise ValueError('Regions are required for divergence calculations')
    characterize_changes = parsed_args.characterize_changes
    if characterize_changes:
        classify_functs = [classify_gc_change, classify_mfe_change,
                           classify_Gquad_change]
    else:
        classify_functs = []
    n_threads = parsed_args.n_threads
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    min_maf = parsed_args.min_maf
    af_field = parsed_args.af_field
    variant_type_field = parsed_args.variant_type_field
    verbose = parsed_args.verbose
    is_sorted = parsed_args.sorted_gtf
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    window_size = parsed_args.window_size
    skip_ids_fpath = parsed_args.skip_ids_fpath
    if skip_ids_fpath:
        skip_ids = set([line.strip() for line in open(skip_ids_fpath)])
    else:
        skip_ids = None

    vcf_file = Tabixfile(parsed_args.vcf_file)
    ref_fastafile = FastaFile(parsed_args.ref_fastafile)
    if not skip_div:
        alt_fastafile = FastaFile(parsed_args.alt_fastafile)
        outgroup_fastafile = FastaFile(parsed_args.outgroup_fastafile)
        liftover_format = parsed_args.liftover_format
        alt_liftover = parsed_args.alt_liftover
        out_liftover = parsed_args.out_liftover
        if liftover_format == 'liftedover_bed':
            alt_liftover = {region: '{}.{}.bed'.format(alt_liftover, region)
                            for region in regions}
            alt_liftover = parse_bed_events_idx(alt_liftover)
            out_liftover = {region: '{}.{}.bed'.format(out_liftover, region)
                            for region in regions}
            out_liftover = parse_bed_events_idx(out_liftover)
        else:
            alt_liftover = LiftOver(alt_liftover)
            out_liftover = LiftOver(out_liftover)
    else:
        alt_fastafile, outgroup_fastafile, liftover_format = [None] * 3
        alt_liftover, out_liftover = [None] * 2

    # Run program
    if file_format == 'GTF':
        gtf = parse_gtf(open(in_fpath))
        features_ws = {'Intron': intron_ws, 'exon': exon_ws, 'CDS': exon_ws}
        events = fetch_gene_events(gtf, add_chr=False, is_sorted=is_sorted,
                                   features_ws=features_ws)
        save_seq = False
    else:
        save_seq = True
        events = fetch_splicing_events(in_fpath, file_format,
                                       ref_fastafile=ref_fastafile,
                                       take_windows=True,
                                       sel_event_type=sel_event_type,
                                       exon_window_size=exon_ws,
                                       intron_window_size=intron_ws)
    if skip_ids:
        events = (event for event in events if event.id not in skip_ids)
    if skip_div:
        funct = partial(find_polymorphism_divergence_changes,
                        vcf_index=vcf_file, liftover=alt_liftover,
                        ref_fastafile=ref_fastafile,
                        alt_fastafile=alt_fastafile,
                        outgroup_liftover=out_liftover,
                        outgroup_fastafile=outgroup_fastafile,
                        classify_changes_functions=classify_functs, verbose=True,
                        only_snps=True, only_biallelic=True, min_maf=min_maf,
                        min_lo_score=None, af_field=af_field,
                        variant_type_field=variant_type_field, skip_div=skip_div,
                        save_seq=save_seq, window_size=window_size)
    else:
        funct = partial(compare_polymophism_divergence_changes,
                        vcf_index=vcf_file, liftover=alt_liftover,
                        ref_fastafile=ref_fastafile,
                        alt_fastafile=alt_fastafile,
                        outgroup_liftover=out_liftover,
                        outgroup_fastafile=outgroup_fastafile,
                        classify_changes_functions=classify_functs,
                        only_snps=True, only_biallelic=True,
                        min_maf=min_maf, min_lo_score=None,
                        verbose=verbose, af_field=af_field,
                        variant_type_field=variant_type_field)
    records = run_parallel(funct, events, n_threads)
    writer = None

    for event_records in records:
        for record in event_records:
            if writer is None:
                writer = csv.DictWriter(out_fhand, fieldnames=record.keys(),
                                        delimiter='\t')
                writer.writeheader()
            writer.writerow(record)
    out_fhand.close()
