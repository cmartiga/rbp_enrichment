#!/usr/bin/env python

import argparse
import sys

from structure.io import write_capr

if __name__ == '__main__':

    description = 'Transforms CapR output prediction in bedGraph files for the'
    description += ' different types of structures'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', default=None, help='CapR output file')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-f', '--file_format', default='wig',
                        help='Output file format: wig (def) or bedGraph')
    parser.add_argument('-l', '--do_not_check_len', default=True,
                        action='store_false',
                        help='Avoid errors when having different lenghts')

    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath) if in_fpath else sys.stdin
    out_prefix = parsed_args.output
    file_format = parsed_args.file_format
    check_len = parsed_args.do_not_check_len

    write_capr(in_fhand, out_prefix, check_len=check_len,
               file_format=file_format)
