#!/usr/bin/env python

import argparse
import sys

from motif.databases import (read_cisbp_rna_db, read_attract_database,
                             read_pwm_background_scores)
from motif.finder import MotifInSeq, MotifCounter, calc_motif_matrix
from motif.matrix import write_matrix
from seq.parsers import read_fasta_file
from utils.settings import RNAMOTIF, CISBP_RNA


if __name__ == '__main__':
    description = 'Returns a matrix with the presence or number of motifs from'
    description += ' a database in a series of given sequences in standard '
    description += 'Fasta format. This matrices can be later used for motif '
    description += 'enrichment analysis using get_enriched_motifs.py or '
    description += 'motif_counts_diff.R to find differences between different'
    description += ' groups of sequences'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    options_group = parser.add_argument_group('Options')
    output_group = parser.add_argument_group('Output')
    help_msg = 'File containing seqs to analyze (def: stdin)'
    input_group.add_argument('input', default=None, help=help_msg)
    input_group.add_argument('-d', '--motif_db', required=True,
                             help='Path for motif database')
    help_msg = 'Use sequences as key of the database instread of RBP name'
    input_group.add_argument('-s', '--by_seq', default=False,
                             action='store_true', help=help_msg)
    output_group.add_argument('-o', '--output', default=None,
                              help='Output file (def: stdout)')
    output_group.add_argument('-n', '--seq_names', default=False,
                              action='store_true',
                              help='First field is the name of the sequence')
    help_msg = 'Max number of missmatches allowed (def: 0)'
    options_group.add_argument('-M', '--max_missmatches', default=0,
                               help=help_msg)
    help_msg = 'P-value cutoff to call a motif using PWM with the given'
    help_msg += ' background'
    options_group.add_argument('-p', '--p_value_threshold', default=0.05,
                               type=float, help=help_msg)
    help_msg = 'File containing random distribution of scores using background'
    help_msg += ' sequences to calculate p-value. Required with -p option'
    options_group.add_argument('-b', '--background', help=help_msg)
    help_msg = 'The database type: {} (def) or {}'.format(RNAMOTIF, CISBP_RNA)
    options_group.add_argument('-dt', '--db_type', default=RNAMOTIF,
                               help=help_msg)
    help_msg = 'Return a matrix with the presence/absence for each motif'
    options_group.add_argument('-pr', '--presence', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Perform multiple test correction in each seq'
    options_group.add_argument('-m', '--multiple_test', default=False,
                               action='store_true', help=help_msg)
    parser.add_argument('-lb', '--label', default=None,
                        help='Label to add to matrix columns')

    # Parse arguments and check error
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    out_fpath = parsed_args.output
    in_fhand = sys.stdin if in_fpath is None else open(in_fpath)
    out_fhand = sys.stdout if out_fpath is None else open(out_fpath, 'w')
    db_fpath = parsed_args.motif_db
    by_seq = parsed_args.by_seq
    database_type = parsed_args.db_type
    max_missmatches = parsed_args.max_missmatches
    p_value_threshold = parsed_args.p_value_threshold
    pwm_random_scores_fpath = parsed_args.background
    multiple_test = parsed_args.multiple_test
    presence = parsed_args.presence
    seq_names = parsed_args.seq_names
    if pwm_random_scores_fpath is None:
        pwm_random_scores = None
    else:
        pwm_random_scores_fhand = open(pwm_random_scores_fpath)
        pwm_random_scores = read_pwm_background_scores(pwm_random_scores_fhand)
    if database_type == RNAMOTIF:
        reading_option = 'seq' if by_seq else 'gene_name'
        parsed_db = read_attract_database(db_fpath,
                                          reading_option=reading_option)
    elif database_type == CISBP_RNA:
        parsed_db = read_cisbp_rna_db(db_fpath)
    else:
        msg = 'Database type not supported: {}'.format(database_type)
        raise ValueError()
    label = parsed_args.label

    # Calculate and write matrix
    seqs = read_fasta_file(in_fhand)
    if not presence:
        motif_function = MotifCounter(database_type, max_missmatches,
                                      p_value_threshold, pwm_random_scores,
                                      multiple_test_correction=multiple_test)
    else:
        motif_function = MotifInSeq(database_type, max_missmatches,
                                    p_value_threshold, pwm_random_scores,
                                    multiple_test_correction=multiple_test)

    matrix = calc_motif_matrix(parsed_db, seqs, motif_function, database_type)

    if label:
        matrix.columns = ['{}.{}'.format(name, label)
                          for name in matrix.columns]
    write_matrix(matrix, out_fhand)
