#!/usr/bin/env python

import argparse
import os
import sys

import numpy
import pandas
from pysam import FastaFile
from scipy.stats.stats import ttest_ind
import seaborn

import matplotlib.pyplot as plt
from motif.matrix import write_matrix, read_matrix
from seq.genes import calc_gene_splicing_efficiency_params
from seq.parsers import read_vasttools_output, vast_output_to_psi_dfs, \
    filter_psi_by_depth
from statistics.base import padjust
from utils.misc import LogTrack
from utils.plot import get_new_figure_and_canvas


if __name__ == '__main__':
    description = 'Calculates gene splicing efficiency from junction based'
    description += 'percent intron retained (PIR) from vast-tools. It calcula'
    description += 'tes the mean and standard deviation PIR per gene and '
    description += 'returns the ratio mean/sd for each gene and sample'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='File containing vast combine output')
    help_msg = 'File containing the group name for each sample. If not given'
    help_msg += ' the value for each sample will be plotted instead'
    parser.add_argument('-s', '--sample_groups', required=True,
                        help=help_msg)
    parser.add_argument('-g', '--gene_groups', default=None,
                        help='File containing the group name for each gene')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    help_msg = 'Force the calculation of all parameters. Otherwise it will lo'
    help_msg = 'ok for the file names based on the ouptut prefix for '
    help_msg += 'precalculated values'
    parser.add_argument('-f', '--force', default=False, action='store_true',
                        help=help_msg)
    parser.add_argument('-md', '--min_depth', default=10, type=int,
                        help='Min event depth to consider')
    help_msg = 'Min number of samples with required min depth. (All samples)'
    parser.add_argument('-ms', '--min_samples', default=None, type=int,
                        help=help_msg)
    help_msg = 'Min number of introns per gene to calculate gene splicing '
    help_msg += 'efficiency (2)'
    parser.add_argument('-mi', '--min_intron', default=2, type=int,
                        help=help_msg)
    parser.add_argument('-i', '--ids_fpath', default=None,
                        help='File containing ids to change')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    out_prefix = parsed_args.output
    samples_fpath = parsed_args.sample_groups
    samples_grouping = read_matrix(open(samples_fpath))
    sample_names = samples_grouping.index
    gene_groups_fpath = parsed_args.gene_groups
    if gene_groups_fpath is None:
        gene_grouping = None
    else:
        gene_grouping = read_matrix(open(gene_groups_fpath))
        if parsed_args.ids_fpath is not None:
            ids_df = read_matrix(open(parsed_args.ids_fpath))
            gene_grouping.index = ids_df.ix[gene_grouping.index, 0]
            sel_rows = [rowname != numpy.nan
                        for rowname in gene_grouping.index]
            gene_grouping = gene_grouping.loc[sel_rows, :]
    force_calculation = parsed_args.force
    min_depth = parsed_args.min_depth
    min_samples = parsed_args.min_samples
    if min_samples is None:
        min_samples = samples_grouping.shape[0]

    log = LogTrack(sys.stderr)

    # Calculate splicing efficiency parameters
    log.write('Read splicing data')
    suffixes = ['psi_table.tab', 'depth_table.tab', 'data_table.tab']
    out_fpaths = [out_prefix + '.{}'.format(suffix) for suffix in suffixes]
    for fpath in out_fpaths:
        if not os.path.exists(fpath):
            force_calculation = True
            break

    splicing_score_fpath = out_prefix + '.splicing_score.tab'
    splicing_pcorr_fpath = out_prefix + '.splicing_pcorr.tab'
    if force_calculation:
        log.write('Extract PSI and depth values')
        parsed_vast = read_vasttools_output(in_fpath)
        psi, depth, data = vast_output_to_psi_dfs(parsed_vast,
                                                  sample_names=sample_names)
        write_matrix(psi, open(out_fpaths[0], 'w'))
        write_matrix(depth, open(out_fpaths[1], 'w'))
        write_matrix(data, open(out_fpaths[2], 'w'))

        log.write('Filter events by depth')
        psi, depth, data = filter_psi_by_depth(psi, depth, data,
                                               min_depth=min_depth,
                                               min_samples=min_samples)

        log.write('Calculate gene splicing efficiency')
        res = calc_gene_splicing_efficiency_params(psi, data)
        median_psi, sd_psi, mean_pcorr = res
        score = median_psi / sd_psi

        log.write('Plot results')
        write_matrix(score, fhand=open(splicing_score_fpath, 'w'))
        write_matrix(mean_pcorr, fhand=open(splicing_pcorr_fpath, 'w'))
    else:
        log.write('PSI and depth were already calculated')
        psi = read_matrix(open(out_fpaths[0]))
        depth = read_matrix(open(out_fpaths[1]))
        data = read_matrix(open(out_fpaths[2]))

        if (not os.path.exists(splicing_score_fpath) or
                not os.path.exists(splicing_pcorr_fpath)):
            log.write('Filter events by depth')
            psi, depth, data = filter_psi_by_depth(psi, depth, data,
                                                   min_depth=min_depth,
                                                   min_samples=min_samples)

            log.write('Calculate gene splicing efficiency')
            res = calc_gene_splicing_efficiency_params(psi, data)
            median_psi, sd_psi, mean_pcorr = res
            score = median_psi / sd_psi
            score.index = psi.index

            write_matrix(score, fhand=open(splicing_score_fpath, 'w'))
            write_matrix(mean_pcorr, fhand=open(splicing_pcorr_fpath, 'w'))

        else:
            log.write('Splicing scores were already calculated')
            score = read_matrix(open(splicing_score_fpath))
            mean_pcorr = read_matrix(open(splicing_pcorr_fpath))

    groups = numpy.unique(samples_grouping.iloc[:, 0])
    if groups.shape[0] == 2:
        msg = 'Perform t-test on splicing efficiency: {} vs {}'
        log.write(msg.format(groups[0], groups[1]))
        samples1 = samples_grouping.ix[samples_grouping.iloc[:, 0] == groups[0], 0].index
        samples2 = samples_grouping.ix[samples_grouping.iloc[:, 0] == groups[1], 0].index
        result = ttest_ind(score[samples1], score[samples2], axis=1)
        diff = score[samples2].mean(axis=1) - score[samples1].mean(axis=1)
        t = result[0]
        pvalue = result[1]
        fdr = padjust(pvalue, method='fdr')
        diff_name = 'score_diff({}-{})'.format(groups[1], groups[0])
        result_df = pandas.DataFrame({'t': t, 'pvalue': pvalue, 'fdr': fdr,
                                      diff_name: diff}, index=score.index)
        out_fhand = open(out_prefix + '.ttest.tab', 'w')
        write_matrix(result_df, out_fhand)
        out_fhand.close()

    log.write('Plot results')
    score['gene'] = score.index
    score = pandas.melt(score, id_vars='gene')
    score.columns = ['gene', 'sample', 'median/sd (PIR)']
    score.index = score['gene']
    groups = numpy.array(samples_grouping.ix[score['sample'], 0])
    score['Group'] = groups
    axes = seaborn.boxplot(x='Group', y='median/sd (PIR)',
                           data=score, showfliers=False)
    axes.set_title('Splicing efficiency')
    fig = axes.get_figure()
    fig.savefig(out_prefix + '.splicing_efficiency.png')

    if gene_grouping is not None:
        groups = numpy.array(gene_grouping.ix[score.index, 0])
        score['Gene group'] = groups
        plt.figure()
        axes = seaborn.boxplot(x='Group', y='median/sd (PIR)',
                               hue='Gene group', data=score, showfliers=False)
        fig = axes.get_figure()
        fig.savefig(out_prefix + '.splicing_efficiency.gene_groups.png')

    plt.figure()
    mean_pcorr['-log10(pvalue)'] = -numpy.log10(mean_pcorr['mean_pvalue'])
    mean_pcorr['log2(n_introns)'] = numpy.log2(mean_pcorr['n_introns'])
    mean_pcorr['abs(rho)'] = -numpy.abs(mean_pcorr['mean_rho'])
    mean_pcorr = mean_pcorr.loc[numpy.isfinite(mean_pcorr['-log10(pvalue)']), :]
    mean_pcorr['-log10(pvalue)*log2(n_introns)'] = mean_pcorr['-log10(pvalue)'] * mean_pcorr['log2(n_introns)']

    p = seaborn.jointplot(x='log2(n_introns)', y='-log10(pvalue)*log2(n_introns)',
                          data=mean_pcorr)
    p.savefig(out_prefix + '.splicing_pcorr.png')
    log.write('Find genes with pairwise PIR correlation')
    corr_genes = mean_pcorr.index[mean_pcorr['-log10(pvalue)*log2(n_introns)'] > 2]
    score['correlation_group'] = 'No-correlated'
    score.loc[corr_genes, 'correlation_group'] = 'Correlated'
    fig, canvas = get_new_figure_and_canvas()
    axes = fig.add_subplot(111)
    axes = seaborn.boxplot(x='correlation_group', y='median/sd (PIR)',
                           hue='Group', ax=axes,
                           data=score, showfliers=False)
    axes.set_title('Splicing efficiency in PIR correlated genes')
    canvas.print_figure(open(out_prefix + '.splicing_efficiency.corr_pir.png', 'w'))

    score['n_introns'] = mean_pcorr.loc[score.index, 'n_introns']
    sel_rows = numpy.logical_and(numpy.isfinite(score['n_introns']),
                                 numpy.logical_not(numpy.isnan(score['n_introns'])))
    df = score.loc[sel_rows, :]
    sel_rows = numpy.logical_and(numpy.isfinite(df['median/sd (PIR)']),
                                 numpy.logical_not(numpy.isnan(df['median/sd (PIR)'])))
    df = df.loc[sel_rows, :]
    df['log2(n_introns)'] = numpy.log2(df['n_introns'])
    df['log10(median/sd (PIR))'] = numpy.log10(df['median/sd (PIR)'])
    sel_rows = numpy.logical_and(numpy.isfinite(df['log10(median/sd (PIR))']),
                                 numpy.logical_not(numpy.isnan(df['log10(median/sd (PIR))'])))
    df = df.loc[sel_rows, :]
    sel_rows = numpy.logical_and(numpy.isfinite(df['log2(n_introns)']),
                                 numpy.logical_not(numpy.isnan(df['log2(n_introns)'])))
    df = df.loc[sel_rows, :]

    p = seaborn.jointplot(x='log2(n_introns)', y='log10(median/sd (PIR))',
                          data=df, dropna=True)
    p.savefig(out_prefix + '.splicing_efficiency_vs_introns.png')
