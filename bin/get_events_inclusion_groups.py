#!/usr/bin/env python

import argparse

import numpy
import pandas

from motif.matrix import read_matrix, write_matrix
from seq.parsers import (read_vasttools_output, vast_output_to_psi_dfs,
                         filter_psi_by_depth)
from utils.settings import MIC_LENGTH


if __name__ == '__main__':
    description = 'Classifies events into Skipped, Included and No-change '
    description += ' based on vast-tools diff and combine outputs'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'File containing vast-tools diff output'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-c', '--combined_table', required=True,
                        help='Combined table containing PSI and Depth data')
    parser.add_argument('-md', '--min_depth', default=10, type=int,
                        help='Min depth to consider a splicing event (10)')
    help_msg = 'Min number of samples with min_depth to consider a splicing '
    help_msg += 'event (All samples).'
    parser.add_argument('-ms', '--min_samples', default=None, type=int,
                        help=help_msg)
    help_msg = 'Min abs difference in PSI to consider an event significant'
    parser.add_argument('-mp', '--min_dpsi', default=0.05, type=float,
                        help=help_msg)
    help_msg = 'Separate groups depending on exon size (threshold = 27nt)'
    parser.add_argument('-m', '--split_microexons', default=False,
                        action='store_true', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    vast_out_fpath = parsed_args.input
    combined_fpath = parsed_args.combined_table
    min_depth = parsed_args.min_depth
    min_samples = parsed_args.min_samples
    min_dpsi = parsed_args.min_dpsi
    split_microexons = parsed_args.split_microexons
    out_fpath = parsed_args.output

    # Select significantly changing events
    combined_fhand = open(combined_fpath)
    try:
        items = combined_fhand.next().strip().split()
        sample_names = [item for item in items[6:]
                        if item.split('-')[-1] != 'Q']
    except StopIteration:
        raise ValueError('Combined table file is empty')
    vast_combined = read_vasttools_output(combined_fhand.name)
    psi, depth, data = vast_output_to_psi_dfs(vast_combined,
                                              sample_names=sample_names)
    psi, depth, data = filter_psi_by_depth(psi, depth, data,
                                           min_depth=min_depth,
                                           min_samples=min_samples)
    selected_events = psi.index

    vast_diff = read_matrix(open(vast_out_fpath), rownames_col=1)
    commond_ids = numpy.intersect1d(vast_diff.index, selected_events)
    vast_diff = vast_diff.loc[commond_ids, :]
    data = data.loc[commond_ids, :]

    groups = numpy.full(vast_diff.shape[0], 'No-change', dtype='S20')
    sig_rows = numpy.array(vast_diff.iloc[:, -1] > 0)
    included_rows = numpy.logical_and(sig_rows,
                                      vast_diff.iloc[:, -2] > min_dpsi)
    groups[numpy.array(included_rows)] = 'Included'
    skipped_rows = numpy.logical_and(sig_rows,
                                     vast_diff.iloc[:, -2] < -min_dpsi)
    groups[numpy.array(skipped_rows)] = 'Skipped'
    if split_microexons:
        are_mic = numpy.array(data.loc[:, 'length'] <= MIC_LENGTH)
        for i, is_mic in enumerate(are_mic):
            if is_mic:
                groups[i] = 'Mic ' + groups[i]
    grouping = pandas.DataFrame({'Group': groups}, index=commond_ids)
    out_fhand = open(out_fpath, 'w')
    write_matrix(grouping, out_fhand)
