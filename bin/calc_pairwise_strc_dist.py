#!/usr/bin/env python

import argparse
import sys

from structure.distance import calc_pairwise_dist
from structure.io import parse_rnaz_files
from utils.misc import LogTrack


if __name__ == '__main__':
    description = 'Calculates pairwise distance between structres from RNAz'
    description += 'results using hamming distance from ViennaRNA package'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Folder containing RNAz result files')
    help_msg = 'Structure name to calculate pairwise distances (consensus)'
    parser.add_argument('-n', '--structure_name', default='consensus',
                        help=help_msg)
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_dir = parsed_args.input
    strc_name = parsed_args.structure_name
    out_prefix = parsed_args.output
    log = LogTrack(sys.stderr)

    # Run program
    regions_dict = {'Upstream_intron.r.250|Alternative_exon.l.50': 'UI3|A5',
                    'Upstream_exon.r.50|Upstream_intron.l.250': 'UE3|UI5',
                    'Alternative_exon.r.50|Downstream_intron.l.250': 'A3|DI5',
                    'Downstream_intron.r.250|Downstream_exon.l.50': 'DI3|DE5'}
    structures = {key: {} for key in regions_dict.values()}
    log.write('=================Reading structures=================')
    for i, (event_id, region, (data, strcs)) in enumerate(parse_rnaz_files(in_dir)):
        region = regions_dict[region]
        if i % 1000 == 0:
            log.write('Strucutres read: {}'.format(i))
        structures[region][event_id] = strcs[strc_name]

    for region, strucs in structures.items():
        log.write('=================Calculating distances=================')
        out_fpath = out_prefix + '.{}'.format(region)
        out_fhand = open(out_fpath, 'w')
        out_fhand.write('structure1\tstructure2\tdistance\n')
        for i, (key1, key2, distance) in enumerate(calc_pairwise_dist(strucs)):
            if i % 100000 == 0:
                log.write('[{}] Processed comparisons: {}'.format(region, i))
            out_fhand.write('{}\t{}\t{}\n'.format(key1, key2, distance))
        out_fhand.close()
