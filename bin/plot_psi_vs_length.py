#!/usr/bin/env python

import argparse
import sys

import numpy
import pandas
import seaborn

import matplotlib.pyplot as plt
from motif.matrix import read_matrix
from seq.parsers import (read_vasttools_output, vast_output_to_psi_dfs,
                         filter_psi_by_depth)
from utils.misc import LogTrack
from utils.settings import EX, EVENTS_INDEX, INT


if __name__ == '__main__':
    description = 'Plots the relationships between intron and exon lenght'
    description += ' and the observed differences in PSI and PIR respectively'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'File containing vast-tools diff output'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-c', '--combined_table', required=True,
                        help='Combined table containing PSI and Depth data')
    parser.add_argument('-md', '--min_depth', default=10, type=int,
                        help='Min depth to consider a splicing event (10)')
    help_msg = 'Min number of samples with min_depth to consider a splicing '
    help_msg += 'event (All samples).'
    parser.add_argument('-ms', '--min_samples', default=None, type=int,
                        help=help_msg)
    help_msg = 'List of events to take into account in the plots (None)'
    parser.add_argument('-sig', '--significant', default=None,
                        help=help_msg)
    help_msg = 'Max exon length to consider in the plot (250)'
    parser.add_argument('-ml', '--max_length', default=250, type=int,
                        help=help_msg)
    help_msg = 'Max exon length to consider in the plot (250)'
    parser.add_argument('-s', '--sel_event_type', default=EX,
                        help='Selected event type ({})'.format(EX))
    help_msg = 'Separate groups depending on exon size (threshold = 27nt)'
    parser.add_argument('-o', '--output', required=True, help='Output prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    vast_out_fpath = parsed_args.input
    combined_fpath = parsed_args.combined_table
    min_depth = parsed_args.min_depth
    min_samples = parsed_args.min_samples
    significant_fpath = parsed_args.significant
    out_prefix = parsed_args.output
    max_length = parsed_args.max_length
    sel_event_type = parsed_args.sel_event_type
    log = LogTrack(sys.stderr)

    # Select significantly changing events
    if significant_fpath is None:
        sig = None
    else:
        sig = [line.strip() for line in open(significant_fpath)]

    log.write('Read combined file')
    combined_fhand = open(combined_fpath)
    try:
        items = combined_fhand.next().strip().split()
        sample_names = [item for item in items[6:]
                        if item.split('-')[-1] != 'Q']
    except StopIteration:
        raise ValueError('Combined table file is empty')
    vast_combined = read_vasttools_output(combined_fhand.name)
    psi, depth, data = vast_output_to_psi_dfs(vast_combined,
                                              sample_names=sample_names)
    log.write('Filter data by Event depth')
    psi, depth, data = filter_psi_by_depth(psi, depth, data,
                                           min_depth=min_depth,
                                           min_samples=min_samples)
    selected_events = psi.index
    if sig is not None:
        selected_events = numpy.intersect1d(selected_events, sig)

    log.write('Read diff file')
    vast_diff = read_matrix(open(vast_out_fpath), rownames_col=1)
    commond_ids = numpy.intersect1d(vast_diff.index, selected_events)
    vast_diff = vast_diff.loc[commond_ids, :]
    data = data.loc[commond_ids, :]

    colnames = vast_diff.columns
    group1 = colnames[1]
    group2 = colnames[2]
    plt.figure()
    dpsi = numpy.array(vast_diff[group2] - vast_diff[group1])
    sel_rows = numpy.logical_not(numpy.isnan(dpsi))
    dpsi = dpsi[sel_rows]
    length = numpy.array(data.ix[sel_rows, 'length'])
    event_type = numpy.array([EVENTS_INDEX[event_type]
                              for event_type in data.ix[sel_rows, 'event_type']])
    log.write('Plot dPSI ({} - {}) vs {} Length'.format(group1, group2,
                                                        sel_event_type))
    data = pandas.DataFrame({'Length': length, 'dPSI': dpsi,
                             'Event type': event_type})
    for event_type in numpy.unique(data['Event type']):
        log.write('\t{}'.format(event_type))
        event_data = data.loc[data['Event type'] == event_type, :]
        xlabel = 'Length'
        if event_type == INT:
            xlabel = 'log10(Length)'
            event_data[xlabel] = numpy.log10(event_data['Length'])
        else:
            event_data = event_data.loc[event_data['Length'] < max_length, :]
        fig = plt.figure()
        fig = seaborn.jointplot(x=xlabel, y='dPSI', data=event_data)
        out_fpath = out_prefix + '.{}.png'.format(event_type)
        fig.savefig(out_fpath)
