#!/usr/bin/env python

import argparse


from motif.matrix import read_matrix
from utils.plot import (volcano_plot_fisher_by_region,
                        volcano_plot_glm_by_region,
                        volcano_plot_gsea_by_region)
from utils.settings import ENRICHMENT_METHODS, FISHER, GLM, GSEA


if __name__ == '__main__':
    description = 'Makes volcano plots from the resulting matrices of the '
    description += 'enrichment analysis'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'Files containing motifs enrichment results'
    parser.add_argument('input', nargs='+', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Output file')
    help_msg = 'Comma separated list of file labels or coefficient names'
    parser.add_argument('-l', '--labels', default=None, help=help_msg)
    help_msg = 'Type of enrichment analysis that was performed {}'
    help_msg = help_msg.format(ENRICHMENT_METHODS) + ' (def:{})'.format(FISHER)
    parser.add_argument('-t', '--enrichment_type', default=FISHER,
                        help=help_msg)
    help_msg = 'Replace pvalues equal 0 by the smallest to plot results'
    parser.add_argument('-r', '--replace_zeros', default=False,
                        action='store_true', help=help_msg)
    help_msg = 'Show the names only of over-represented motifs'
    parser.add_argument('-n', '--only_over', default=False,
                        action='store_true', help=help_msg)
    parser.add_argument('-rg', '--regions', default=None, help='Regions file')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpaths = parsed_args.input
    dfs = [read_matrix(open(fpath)) for fpath in in_fpaths]
    out_fhand = open(parsed_args.output, 'w')
    labels = parsed_args.labels
    enrichemnt_type = parsed_args.enrichment_type
    replace_inf = parsed_args.replace_zeros
    only_over = parsed_args.only_over
    ordered_regions = parsed_args.regions
    if ordered_regions is not None:
        ordered_regions = [line.strip() for line in open(ordered_regions)]

    # Plot results
    if enrichemnt_type == FISHER:
        labels = labels.split(',')
        if len(labels) != len(in_fpaths):
            raise ValueError('The number of files and labels must be the same')
        volcano_plot_fisher_by_region(dfs, out_fhand, labels,
                                      replace_inf=replace_inf,
                                      only_over=only_over)
#
#         merged_df = []
#         for df, label in zip(dfs, labels):
#             df['Comparison'] = label
#             merged_df.append(df)
#         merged_df = concat(merged_df)
#         regions = [rowname.split('.', 1)[-1] for rowname in merged_df.index]
#         print numpy.unique(regions)
#         ids = [rowname.split('.', 1)[0] for rowname in merged_df.index]
#         merged_df['bonferroni'] = padjust(merged_df['pvalue'])
#         show_rows = merged_df['bonferroni'] < 0.05
#         if only_over:
#             show_rows = numpy.logical_and(show_rows, merged_df['OR'] > 1)
#         merged_df['RBPs'] = ids
#         merged_df['RBPs'][show_rows] = ''
#         merged_df['Region'] = regions
#         merged_df['log2(OR)'] = numpy.log2(merged_df['OR'])
#         merged_df['-log10(p-value)'] = -numpy.log10(merged_df['pvalue'])
#
#         plot = ggplot(merged_df, aes(y='-log10(p-value)', x='log2(OR)',
#                                      color='Comparison', label='RBPs'))
#         plot = plot + geom_point(size=10) + facet_wrap('Region')
# #         plot = plot + geom_text()
#         ggsave(plot=plot, filename=out_fhand.name, width=20, height=20,
#                limitsize=False, dpi=1000)

    elif enrichemnt_type == GLM:
        if len(dfs) != 1:
            msg = 'Only one file is required when for {}'.format(GLM)
            raise ValueError(msg)
        labels = labels.split(',')
        volcano_plot_glm_by_region(dfs[0], out_fhand, params=labels,
                                   replace_inf=replace_inf)
    elif enrichemnt_type == GSEA:
        volcano_plot_gsea_by_region(dfs[0], out_fhand, replace_inf=replace_inf)
    else:
        msg = 'Only {} methods are available'.format(ENRICHMENT_METHODS)
        raise ValueError(msg)
