#!/usr/bin/env python

import argparse

import numpy as np
import pandas as pd
from seq.parsers import read_vasttools_output
from utils.misc import LogTrack


if __name__ == '__main__':
    description = 'Generates a table with inclusion and skipping reads'
    description += 'with design variables for later testing'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='vast-tools combine results')
    help_msg = 'Table containing design variables'
    parser.add_argument('-s', '--sep', default=',', help='Separator char (,)')
    parser.add_argument('-d', '--design', required=True, help=help_msg)
    help_msg = 'Min proportion of samples with alternative reads'
    help_msg += ' to consider an event as alternative (0.3)'
    parser.add_argument('-m', '--min_alt_samples', default=0.3, type=float,
                        help=help_msg)
    parser.add_argument('-o', '--output', required=True,
                        help='Output file path')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    out_fpath = parsed_args.output
    design_fpath = parsed_args.design
    sep = parsed_args.sep
    min_alt_samples = parsed_args.min_alt_samples

    # Init logger
    log = LogTrack()

    # Read data
    log.write('Load design matrix from {}'.format(design_fpath))
    design = pd.read_csv(design_fpath, index_col=0, sep=sep)
    fieldnames = design.columns
    design = design.to_dict(orient='index')

    log.write('Parse event inclusion and skipping reads: {}'.format(in_fpath))
    parsed_vast = read_vasttools_output(in_fpath)

    dfs = []
    for event_type, event in parsed_vast:
        data = []
        try:
            for sample_data in event['psi_data']:
                if sample_data['psi'] is None:
                    continue
                inclusion = int(sample_data['psi'] * sample_data['depth'] / 100.0)
                total = int(sample_data['depth'])
                record = {'event_id': event['id'], 'sample': sample_data['sample'],
                          'inclusion': inclusion, 'skipping': total - inclusion}
                try:
                    try:
                        additional_data = design[sample_data['sample']]
                    except KeyError:
                        # Remove trailing .1 added by vast-tools when paired end
                        additional_data = design[sample_data['sample'][:-2]]
                except KeyError:
                    # Sample data not found
    #                 msg = '\tData not found in design for {}'
    #                 log.write(msg.format(record['sample']))
                    continue
                record.update({field: additional_data.get(field, None)
                               for field in fieldnames})
                data.append(record)
            df = pd.DataFrame(data).set_index('sample')
            prop_alt = np.nanmean(np.logical_and(df['skipping'] > 0,
                                                 df['inclusion'] > 0))
            df['prop_alt'] = prop_alt
            dfs.append(df)
            if len(dfs) % 100 == 0:
                print len(dfs)
        except:
            continue
    data = pd.concat(dfs)
    data.to_csv(out_fpath)
