#!/usr/bin/env python

import argparse

from pysam import Tabixfile

from motif.matrix import write_matrix
from seq.genes import calc_tabix_counts_promoter, get_gene_coord
from seq.parsers import parse_gtf


if __name__ == '__main__':
    description = 'Finds overlapping intervals with gene promoters. '
    description += 'For instance TFs binding sites'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    help_msg = 'GTF file containin annotated genes'
    parser.add_argument('input', help=help_msg)
    parser.add_argument('-o', '--output', required=True, help='Count matrix')
    help_msg = 'Tabix indexed file containin TFs binding site (such as ENCODE)'
    parser.add_argument('-t', '--tabix_file', required=True, help=help_msg)
    parser.add_argument('-tn', '--tabix_file_ids', required=True,
                        help='File containing binding sites ids')
    parser.add_argument('-u', '--upstream', default=200, type=int,
                        help='Number of bases upstream the gene start site')
    parser.add_argument('-d', '--downstream', default=100, type=int,
                        help='Number of bases downstream the gene start site')
    parser.add_argument('-a', '--add_chr', default=False,
                        action='store_true', help='Add "chr" to chrom name')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    tabix_index = Tabixfile(parsed_args.tabix_file)
    tabix_index_ids = [x.strip() for x in open(parsed_args.tabix_file_ids)]
    upstream_window = parsed_args.upstream
    downstream_window = parsed_args.downstream
    add_chr = parsed_args.add_chr

    # Find binding sites overlaps
    gtf = parse_gtf(in_fhand)
    gene_coords = get_gene_coord(gtf, add_chr=add_chr)
    df = calc_tabix_counts_promoter(gene_coords, upstream_ws=upstream_window,
                                    downstream_ws=downstream_window,
                                    tabix_index=tabix_index,
                                    tf_ids=tabix_index_ids)
    write_matrix(df, out_fhand)
