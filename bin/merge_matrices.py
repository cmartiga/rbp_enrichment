#!/usr/bin/env python

import argparse

from motif.matrix import read_matrix, write_matrix, merge_matrices


if __name__ == '__main__':

    # Create arguments
    description = 'Merges different matrices with motif counts or presence '
    description += ' information for later analysis in an integrated way. '
    description += 'Label for the different files must be given since the same'
    description += ' motifs are usually analysed'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', nargs='+', help='Matrices')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    parser.add_argument('-lb', '--labels', default=None,
                        help='Comma separated list of labels for each file')
    parser.add_argument('-r', '--no_rownames', default=False,
                        action='store_true',
                        help='There are no rownames in the matrices')
    parser.add_argument('-s', '--sep', default='\t',
                        help='Separation character (tab)')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpaths = parsed_args.input
    in_fhands = [open(in_fpath) for in_fpath in in_fpaths]
    sep = parsed_args.sep
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    with_rownames = not parsed_args.no_rownames
    labels = parsed_args.labels.split(',')
    if labels is not None and len(labels) != len(in_fhands):
        msg = 'Number of labels must be equal to number of matrices'
        raise AttributeError(msg)

    # Merge motif matrices
    matrices = [read_matrix(fhand, dtype=int, sep=sep) for fhand in in_fhands]
    merged = merge_matrices(matrices, labels)
    write_matrix(merged, fhand=out_fhand)
