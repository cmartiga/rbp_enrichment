#!/usr/bin/env python

import argparse
import csv
import itertools
import sys

import numpy
import pandas

from motif.matrix import read_matrix
from statistics.base import EmpiricalPvalueCalculator
from utils.plot import get_new_figure_and_canvas


if __name__ == '__main__':
    description = 'Finds out if changes in seq affect specific RNA binding '
    description += 'motifs from a database'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='File containing changes to analyze')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-m', '--min_af', default=None, type=float,
                        help='Select only SNPs with a given min AF ')
    parser.add_argument('-g', '--grouping', required=True,
                        help='File containing group information of events')
    parser.add_argument('-n', '--variable', default=None,
                        help='Name of feature to compare changes')
    parser.add_argument('-N', '--n_quantiles', default=1000, type=int,
                        help='Number of quantiles for QQ-plot (1000)')
    help_msg = 'Number of bootstrap for p-value calculations (200)'
    parser.add_argument('-b', '--n_bootstrap', default=200, type=int,
                        help=help_msg)
    parser.add_argument('-v', '--verbose', default=False,
                        action='store_true',)

    # Parse arguments
    parsed_args = parser.parse_args()
    data_fpath = parsed_args.input
    grouping_fpath = parsed_args.grouping
    min_af = parsed_args.min_af
    out_prefix = parsed_args.output
    n_quantiles = parsed_args.n_quantiles
    variable = parsed_args.variable
    n_bootstrap = parsed_args.n_bootstrap
    verbose = parsed_args.verbose

    # Run program
    records = csv.DictReader(open(data_fpath), delimiter='\t')
    fieldnames = ['region', 'AF']
    if variable is not None:
        fieldnames.append(variable)
    data_dict = {fieldname: [] for fieldname in fieldnames}
    event_ids = []
    for record in records:
        event_ids.append(record['event_id'])
        for fieldname in fieldnames:
            data_dict[fieldname].append(record[fieldname])
    data = pandas.DataFrame(data_dict, index=event_ids)

    grouping = read_matrix(open(grouping_fpath))
    data['Group'] = grouping.ix[data.index, 0]
    if variable is None:
        variable = 'Group'
        group_fieldname = 'all'
        data[group_fieldname] = 'all'
    else:
        group_fieldname = 'Group'

    af = [float(x) if ',' not in x else float(x.split(',')[0])
          for x in data['AF']]
    data['AF'] = af
    if min_af is not None:
        data = data.loc[data['AF'] > min_af, :]

    percentiles = numpy.linspace(0, 100, n_quantiles)
    colors = ['blue', 'red', 'purple', 'orange', 'brown', 'green']

    regions = numpy.unique(data['region'])
    for region in regions:
        if verbose:
            sys.stderr.write('Analyzing region: {}\n'.format(region))
        out_fpath = '{}.{}.png'.format(out_prefix, region)
        region_df = data.loc[data['region'] == region, :]
        change_types = numpy.unique(data[variable])
        comparisons = list(itertools.combinations(change_types, 2))
        n_comparisons = len(comparisons)
        fig, canvas = get_new_figure_and_canvas((7 * n_comparisons, 7))
        for i, (change_type1, change_type2) in enumerate(comparisons):
            axes = fig.add_subplot(100 + n_comparisons * 10 + 1 + i)
            groups = numpy.unique(region_df[group_fieldname])
            axes.plot(percentiles / 100.0, percentiles / 100.0,
                      label='Expected', c='black')
            for group, color in zip(groups, colors):
                group_region_df = region_df.loc[region_df[group_fieldname] == group].copy()
                x = group_region_df.loc[group_region_df[variable] == change_type1, 'AF']
                y = group_region_df.loc[group_region_df[variable] == change_type2, 'AF']
                if verbose:
                    sys.stderr.write('{}: {}\n{}: {}\n'.format(change_type1, x.shape[0],
                                                               change_type2, y.shape[0]))
                if x.shape[0] == 0 or y.shape[0] == 0:
                    continue
                x_percentiles = numpy.percentile(x, percentiles)
                y_percentiles = numpy.percentile(y, percentiles)
                diff = numpy.sum(x_percentiles - y_percentiles)
                random_diffs = []
                for _ in xrange(n_bootstrap):
                    shuffled = group_region_df[variable].as_matrix()
                    numpy.random.shuffle(shuffled)
                    group_region_df[variable] = shuffled
                    x = group_region_df.loc[group_region_df[variable] == change_type1, 'AF']
                    y = group_region_df.loc[group_region_df[variable] == change_type2, 'AF']
                    x_perc = numpy.percentile(x, percentiles)
                    y_perc = numpy.percentile(y, percentiles)
                    axes.plot(x_perc, y_perc, alpha=0.01, c=color)
                    random_diffs.append(numpy.sum(x_perc - y_perc))
                calc_empirical_pvalue = EmpiricalPvalueCalculator(random_diffs,
                                                                  'two-sided')
                pvalue = calc_empirical_pvalue(diff)
                axes.plot(x_percentiles, y_percentiles, c=color,
                          label='{} pvalue={}'.format(group, pvalue))

            axes.set_ylabel(change_type2)
            axes.set_xlabel(change_type1)
            axes.legend(loc='best')
            axes.set_title('{} vs {}'.format(change_type1, change_type2))
        fig.savefig(out_fpath)
