#!/usr/bin/env python

import argparse

from bx.bbi.bigwig_file import BigWigFile
from pysam import FastaFile, Tabixfile

from motif.finder import calc_tabix_profiles, get_profiles, \
    _calc_regions_max_lengths
from motif.matrix import write_matrix
from seq.events import fetch_splicing_events
from seq.writers import write_profiles
from utils.settings import (MATS_FORMAT, EX, INT, EXON_WINDOW_SIZE,
                            INTRON_WINDOW_SIZE, EVENTS_TYPES)


if __name__ == '__main__':
    description = 'Fetches sequences corresponding to exon cassettes '
    description += 'and writes them in Fasta files for each region'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Files containing exons to analyze')
    parser.add_argument('-f', '--file_format', default=MATS_FORMAT,
                        help='Input file format (MATS)')
    parser.add_argument('-s', '--splicing_event', default=EX,
                        help='Type of splicing events (Exon_cassette)')
    parser.add_argument('-t', '--tabix_file', default=None,
                        help='Tabix indexed file with motifs positions')
    help_msg = 'BigWig file with genome track values (i.e. phastCons)'
    parser.add_argument('-b', '--bw_file', default=None, help=help_msg)
    help_msg = 'BigWig file with genome track values in the negative strand. '
    help_msg += 'If not given strand will not be taken into account'
    parser.add_argument('-rb', '--rev_bw_file', default=None, help=help_msg)
    help_msg = 'Tabix indexed VCF file to calculate nucleotide diversity'
    parser.add_argument('-v', '--vcf_file', default=None, help=help_msg)
    parser.add_argument('-n', '--name', required=True,
                        help='Profile name (Ex: motif name)')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    help_msg = 'Use always the starting position in BED files regardless of th'
    help_msg += 'e strand of the sequence (Only for BED files from find_genome'
    help_msg += '_motifs)'
    parser.add_argument('-u', '--use_start_pos', default=False,
                        action='store_true', help=help_msg)

    windows_group = parser.add_argument_group('Windows options')
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    windows_group.add_argument('-ew', '--exon_window_size',
                               default=EXON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    windows_group.add_argument('-iw', '--intron_window_size',
                               default=INTRON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'File containing max lengths of regions profiles'
    parser.add_argument('-l', '--region_lens', default=None, help=help_msg)

    fields_group = parser.add_argument_group('VCF fielnames options')
    help_msg = 'Minimum Allele Frequency field in VCF file (def: MAF)'
    fields_group.add_argument('-maf', '--af_field', default='MAF',
                              help=help_msg)
    help_msg = 'Variant type field in VCF file (def: TSA)'
    fields_group.add_argument('-vt', '--variant_type_field',
                              default='TSA', help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    if sel_event_type not in EVENTS_TYPES:
        msg = sel_event_type + ' type of event not supported. '
        msg += 'Only {} are allowed'.format(EVENTS_TYPES)
        raise ValueError(msg)
    tabix_fpath = parsed_args.tabix_file
    out_prefix = parsed_args.output
    use_tabix_start = parsed_args.use_start_pos
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    profile_name = parsed_args.name
    bw_fpath = parsed_args.bw_file
    rev_bw_fpath = parsed_args.rev_bw_file
    if tabix_fpath is not None:
        tabix_index = Tabixfile(tabix_fpath)
    else:
        tabix_index = None
    if bw_fpath is not None:
        bw_files = [BigWigFile(open(bw_fpath))]
        bw_tags = [profile_name]
    else:
        bw_files = []
        bw_tags = []
    if rev_bw_fpath is not None:
        rev_bw_files = [BigWigFile(open(rev_bw_fpath))]
    else:
        rev_bw_files = None

    vcf_fpath = parsed_args.vcf_file
    if vcf_fpath is not None:
        vcf_index = Tabixfile(vcf_fpath)
        profile_name = 'exp_het'
    else:
        vcf_index = None
    if bw_fpath is None and tabix_fpath is None and vcf_fpath is None:
        raise ValueError('It needs either a BigWig, tabix or vcf file')
    max_regions_lens_fpath = parsed_args.region_lens
    if max_regions_lens_fpath is None:
        max_lenghts = None
    else:
        max_lenghts = {line.strip().split()[0]: int(line.strip().split()[1])
                       for line in open(max_regions_lens_fpath)}
    af_field = parsed_args.af_field
    variant_type_field = parsed_args.variant_type_field

    # Fetch and write profiles
    splicing_events = fetch_splicing_events(in_fpath, file_format,
                                            tabix_index=tabix_index,
                                            bigwigfiles=bw_files,
                                            rev_bw_files=rev_bw_files,
                                            bw_tags=bw_tags,
                                            sel_event_type=sel_event_type,
                                            take_windows=True,
                                            use_tabix_start=use_tabix_start,
                                            exon_window_size=exon_ws,
                                            intron_window_size=intron_ws,
                                            vcf_index=vcf_index,
                                            af_field=af_field,
                                            variant_type_field=variant_type_field)
    if tabix_index is None:
        if max_lenghts is None:
            events = fetch_splicing_events(in_fpath, file_format,
                                           take_windows=True,
                                           exon_window_size=exon_ws,
                                           intron_window_size=intron_ws,
                                           af_field=af_field,
                                           variant_type_field=variant_type_field)
            max_lenghts = _calc_regions_max_lengths(events)
        profiles = get_profiles(splicing_events, profile_name,
                                max_lengths=max_lenghts)
        write_profiles(profiles, out_prefix=out_prefix)
    else:
        regions, dfs = calc_tabix_profiles(splicing_events, profile_name)
        for region, df in zip(regions, dfs):
            out_fhand = open('{}.{}'.format(out_prefix, region), 'w')
            write_matrix(df, out_fhand)
