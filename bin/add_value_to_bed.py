#!/usr/bin/env python

import argparse

from bx.bbi.bigwig_file import BigWigFile
from seq.events import GenomeInterval


if __name__ == '__main__':
    description = 'Fetches sequences corresponding to exon cassettes '
    description += 'and writes them in Fasta files for each region'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='input BED file')
    help_msg = 'BigWig file with genome track values (i.e. phastCons)'
    parser.add_argument('-b', '--bw_file', default=None, help=help_msg)
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fhand = open(parsed_args.input)
    out_fhand = open(parsed_args.output, 'w')
    bw_fpath = parsed_args.bw_file
    bw_file = BigWigFile(open(bw_fpath))

    # Parse intervals and add bigwig mean value
    for line in in_fhand:
        items = line.strip().split()
        interval = GenomeInterval(items[0], int(items[1]), int(items[2]),
                                  items[3])
        interval.get_bigwig_profile(bw_file, tag='bw', expand=True)
        items.append(str(interval.profiles['bw'].mean()))
        out_fhand.write('\t'.join(items) + '\n')
