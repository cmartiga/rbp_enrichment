#!/usr/bin/env python

import argparse

from bx.bbi.bigwig_file import BigWigFile
from pysam import Tabixfile

from motif.matrix import write_matrix
from seq.genes import calc_gene_features_values
from seq.parsers import parse_gtf
from utils.settings import EXON_WINDOW_SIZE, INTRON_WINDOW_SIZE


if __name__ == '__main__':
    description = 'Calculates the mean value of a BigWigFile or presence of '
    description += 'Tabix indexed intervals in the different regions of a gene'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='GTF file containing annotation')
    parser.add_argument('-t', '--tabix_file', default=None,
                        help='Tabix indexed file with motifs positions')
    help_msg = 'BigWig file with genome track values (i.e. phastCons)'
    parser.add_argument('-b', '--bw_file', default=None, help=help_msg)
    help_msg = 'BigWig file with genome track values in the negative strand. '
    help_msg += 'If not given strand will not be taken into account'
    parser.add_argument('-rb', '--rev_bw_file', default=None, help=help_msg)
    help_msg = 'Tabix indexed VCF file to calculate nucleotide diversity'
    parser.add_argument('-vcf', '--vcf_file', default=None, help=help_msg)
    parser.add_argument('-n', '--tag', required=True,
                        help='Profile name (Ex: motif name)')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-a', '--add_chr', default=False,
                        action='store_true', help='Add "chr" to chrom name')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-s', '--sorted_gtf', default=False,
                        action='store_true')

    windows_group = parser.add_argument_group('Windows options')
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    windows_group.add_argument('-ew', '--exon_window_size',
                               default=EXON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    windows_group.add_argument('-iw', '--intron_window_size',
                               default=INTRON_WINDOW_SIZE, type=int,
                               help=help_msg)

    fields_group = parser.add_argument_group('VCF fielnames options')
    help_msg = 'Minimum Allele Frequency field in VCF file (def: MAF)'
    fields_group.add_argument('-maf', '--af_field', default='MAF',
                              help=help_msg)
    help_msg = 'Variant type field in VCF file (def: TSA)'
    fields_group.add_argument('-vt', '--variant_type_field',
                              default='TSA', help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath)
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    tag = parsed_args.tag
    add_chr = parsed_args.add_chr
    verbose = parsed_args.verbose
    is_sorted = parsed_args.sorted_gtf
    af_field = parsed_args.af_field
    variant_type_field = parsed_args.variant_type_field

    tabix_fpath = parsed_args.tabix_file
    if tabix_fpath is not None:
        tabix_index = Tabixfile(tabix_fpath)
    else:
        tabix_index = None

    bw_fpath = parsed_args.bw_file
    rev_bw_fpath = parsed_args.rev_bw_file
    if bw_fpath is not None:
        bw_file = BigWigFile(open(bw_fpath))
    else:
        bw_file = None
    if rev_bw_fpath is not None:
        rev_bw_file = BigWigFile(open(rev_bw_fpath))
    else:
        rev_bw_file = None

    vcf_fpath = parsed_args.vcf_file
    if vcf_fpath is not None:
        vcf_index = Tabixfile(vcf_fpath)
    else:
        vcf_index = None

    # Fetch and write profiles
    gtf = parse_gtf(in_fhand)
    features_ws = {'Intron': intron_ws, 'exon': exon_ws, 'CDS': exon_ws}
    df = calc_gene_features_values(gtf, add_chr=add_chr,
                                   tabix_index=tabix_index,
                                   bw_file=bw_file, rev_bw_file=rev_bw_file,
                                   tag=tag, features_ws=features_ws,
                                   vcf_index=vcf_index, verbose=verbose,
                                   is_sorted=is_sorted, af_field=af_field,
                                   variant_type_field=variant_type_field)
    write_matrix(df, out_fhand)
