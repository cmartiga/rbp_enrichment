#!/usr/bin/env python

import itertools
import os
from sys import stderr

import numpy
from pandas import concat
from pysam import Tabixfile

from motif.enrichment import poisson_regression
from motif.finder import calc_tabix_counts_matrices
from motif.matrix import (read_matrix, write_matrix, merge_matrices,
                          calc_combination_matrix)
from seq.events import fetch_splicing_events
from statistics.enrichment import fisher_test
from statistics.gsea import perform_gsea
from utils.bin_utils import add_read_events, add_grouping_arg, \
    add_windows_options, init_parser, add_output_arg
from utils.misc import LogTrack, run_parallel
from utils.plot import (volcano_plot_fisher_by_region,
                        volcano_plot_glm_by_region,
                        volcano_plot_gsea_by_region)
from utils.settings import BED_FORMAT


def get_fpath_region(fpath):
    items = fpath.split('.')
    for i, item in enumerate(items):
        if '_intron' in item or '_exon' in item:
            if items[i + 1] != 'l' and items[i + 1] != 'r':
                return None
            return '.'.join(items[i:i + 2])
    raise ValueError('No region recognized in {}'.format(fpath))


def select_common_events(covariates_fpath, exons_fpath, col_id=1):
    covariates_ids = set([line.split()[0] for line in open(covariates_fpath)])
    exons_ids = set([line.split()[col_id] for line in open(exons_fpath)])
    common_ids = covariates_ids.intersection(exons_ids)

    new_cov_fpath = covariates_fpath + '.sel'
    new_cov_fhand = open(new_cov_fpath, 'w')
    for line in open(covariates_fpath):
        if line.split()[0] in common_ids:
            new_cov_fhand.write(line)
    new_exons_fpath = exons_fpath + '.sel'
    new_exons_fhand = open(new_exons_fpath, 'w')
    for i, line in enumerate(open(exons_fpath)):
        if i == 0:
            new_exons_fhand.write(line)
        elif line.split()[col_id] in common_ids:
            new_exons_fhand.write(line)
    new_exons_fhand.flush()
    new_cov_fhand.flush()
    return new_cov_fpath, new_exons_fpath


if __name__ == '__main__':
    description = 'Return significantly enriched motifs from a database in two'
    description += ' sets of sequences. It uses fisher test two tail to check '
    description += 'for motif and performs Bonferroni and FDR corrections'

    # Create arguments
    parser = init_parser(description)
    parser, input_group, options_group, output_group = parser

    # Input options
    add_read_events(input_group)
    add_grouping_arg(input_group)

    help_msg = 'Comma separated list of files containing the names of '
    help_msg += 'intervals or RBPs to find counts for each database'
    input_group.add_argument('-r', '--interval_names_files', required=True,
                             help=help_msg)

    # Database options
    database_group = parser.add_argument_group('Database options')
    help_msg = 'Comma separated list of BED files containing motifs or binding'
    help_msg += ' positions indexed with tabix.'
    database_group.add_argument('-db', '--dbs_files', default=None,
                                help=help_msg)
    help_msg = 'Comma separated list of labels for the different databases'
    database_group.add_argument('-dl', '--dbs_labels', default=None,
                                help=help_msg)
    help_msg = 'Col number in bed file containing RBPs names (0-indexed)'
    database_group.add_argument('-dc', '--rbps_col', default=4, type=int,
                                help=help_msg)

    # Output options
    add_output_arg(output_group, output_type='Directory')
    output_group.add_argument('-l', '--logfile', default=None,
                              help='File to write the log (def: stderr)')

    # Options group
    help_msg = 'Run GLM poisson test for enrichment also'
    options_group.add_argument('-rg', '--run_glm', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Calculate matrices for all pairwise combinations and perform'
    help_msg += ' enrichment analyses'
    options_group.add_argument('-rc', '--run_combinations', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'File containing combinations of regions to look for enriched'
    help_msg += ' pairs of motifs when runnning combinations. Otherwise, all '
    help_msg += 'pairwise comparisons will be analysed'
    options_group.add_argument('-cb', '--combinations_fpath', default=None,
                               help=help_msg)
    options_group.add_argument('-c', '--comparisons_fpath', default=None,
                               help='File containing groups to compare')
    options_group.add_argument('-mp', '--min_phastcons', default=None,
                               type=float,
                               help='Min phastCons to consider a motif (None)')
    help_msg = 'Number of threads to calculate the empirical distribution of'
    help_msg += ' enrichment scores or for combination of motifs'
    options_group.add_argument('-t', '--n_threads', default=1, type=int,
                               help=help_msg)
    help_msg = 'Show only the names of over-represented motifs in v-plots'
    options_group.add_argument('--only_over', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Use full regions instead of windows at the ends'
    options_group.add_argument('--full_length', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Use only exonic regions for enrichment analysis'
    options_group.add_argument('--only_exons', default=False,
                               action='store_true', help=help_msg)
    add_windows_options(options_group)

    # GSEA group
    gsea_group = parser.add_argument_group('GSEA group')
    help_msg = 'Perform GSEA on the motifs using the correlation between pheno'
    help_msg += 'types and a series of features in this file'
    gsea_group.add_argument('-a', '--features_fpath', default=None,
                            help=help_msg)
    help_msg = 'Size of the empirical null ES distribution to generate (10000)'
    gsea_group.add_argument('-s', '--null_size', default=10000, type=int,
                            help=help_msg)
    help_msg = 'When generating the null distribution shuffle the motif presen'
    help_msg += 'ce tags instead of phenotype labels'
    gsea_group.add_argument('-m', '--shuffle_motifs', default=False,
                            action='store_true', help=help_msg)

    # Parse arguments and check errors
    parsed_args = parser.parse_args()
    events_fpath = parsed_args.input
    file_format = parsed_args.format
    groups_fpath = parsed_args.groups_fpath
    features_fpath = parsed_args.features_fpath
    if features_fpath is None:
        features_df = None
    else:
        features_df = read_matrix(open(features_fpath))
    dbs_fpaths = parsed_args.dbs_files.split(',')
    dbs_labels = parsed_args.dbs_labels.split(',')
    dbs_rbps_fpaths = parsed_args.interval_names_files.split(',')
    dbs_rbps_names = [[line.strip() for line in open(fpath)]
                      for fpath in dbs_rbps_fpaths]
    out_dir = parsed_args.output
    log_fpath = parsed_args.logfile
    log = LogTrack(open(log_fpath, 'w') if log_fpath else stderr)

    run_glm = parsed_args.run_glm
    run_combinations = parsed_args.run_combinations
    min_phastcons = parsed_args.min_phastcons
    if min_phastcons is not None:
        cols_filters = {5: lambda x: float(x) > min_phastcons}
    else:
        cols_filters = None
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    alternative_ws = parsed_args.alternative_window_size
    n_iter = parsed_args.null_size
    n_threads = parsed_args.n_threads
    shuffle_labels = not parsed_args.shuffle_motifs
    sel_event_type = parsed_args.sel_event_type
    comparisons_fpath = parsed_args.comparisons_fpath
    combinations_fpath = parsed_args.combinations_fpath
    only_over = parsed_args.only_over
    take_windows = not parsed_args.full_length
    only_exons = parsed_args.only_exons
    rbps_col = parsed_args.rbps_col
    if combinations_fpath is not None:
        reg_pair = [line.strip().split()
                    for line in open(combinations_fpath)]
    else:
        reg_pair = None

    # Create output directories
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    matrices_dir = os.path.join(out_dir, 'matrices')
    if not os.path.exists(matrices_dir):
        os.mkdir(matrices_dir)
    enrichment_dir = os.path.join(out_dir, 'enrichment')
    if not os.path.exists(enrichment_dir):
        os.mkdir(enrichment_dir)

    # Select common ids from the files
    col_id = 4 if file_format == BED_FORMAT else 1
    if file_format == 'counts':
        col_id = 0
    groups_fpath, events_fpath = select_common_events(groups_fpath,
                                                      events_fpath, col_id)

    for db_tabix_fpath, db_label, rbps in zip(dbs_fpaths, dbs_labels,
                                              dbs_rbps_names):
        log.write('=== Analysis for {} database ==='.format(db_label))
        if file_format == 'counts':
            log.write('Loading motif counts'.format(db_label))
            merged_counts_df = read_matrix(open(events_fpath))

        else:
            # Fetch splicing events
            log.write('Fetch splicing events and motifs')
            tabix_file = Tabixfile(db_tabix_fpath)
            alt_ws = alternative_ws
            events = fetch_splicing_events(events_fpath,
                                           file_format=file_format,
                                           sel_event_type=sel_event_type,
                                           take_windows=take_windows,
                                           exon_window_size=exon_ws,
                                           intron_window_size=intron_ws,
                                           alternative_window_size=alt_ws,
                                           only_exons=only_exons)

            # Calc_motif_counts
            log.write('Calculate counts matrices')
            regions, counts_dfs = calc_tabix_counts_matrices(events, rbps,
                                                             tabix_file,
                                                             cols_filters,
                                                             rbps_col,
                                                             same_strand=True)
            # Merge counts matrices
            log.write('Merge counts matrices')
            merged_counts_df = merge_matrices(counts_dfs, regions)
            fname = '{}.counts'.format(db_label)
            matrix_fpath = os.path.join(matrices_dir, fname)
            matrix_fhand = open(matrix_fpath, 'w')
            write_matrix(merged_counts_df, matrix_fhand)
            merged_counts_df = read_matrix(open(matrix_fpath))

        # Fisher test enrichment
        log.write('Perform motif enrichment analysis: fisher test')
        grouping_df = read_matrix(open(groups_fpath))
        grouping_variable = grouping_df.columns[0]
        grouping = numpy.array(grouping_df.loc[merged_counts_df.index,
                                               grouping_variable])
        groups = numpy.unique(grouping)
        groups = [x for x in groups if str(x) != 'nan']

        if comparisons_fpath is None:
            group_pairs = list(itertools.combinations(groups, 2))
        else:
            group_pairs = [line.strip().split('\t')
                           for line in open(comparisons_fpath)]

        rownames = merged_counts_df.index
        dfs_to_plot = []
        plot_labels = []
        for group1, group2 in group_pairs:
            out_fname = '{}.{}_vs_{}.tab'.format(db_label, group1, group2)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            rownames1 = rownames[grouping == group1]
            rownames2 = rownames[grouping == group2]
            alternative = 'greater' if only_over else'two-sided'
            test = fisher_test(merged_counts_df, rownames1, rownames2,
                               alternative=alternative)
            write_matrix(test, out_fhand)
            dfs_to_plot.append(test)
            plot_labels.append('{}_vs_{}'.format(group1, group2))

        out_fname = '{}.fisher.png'.format(db_label)
        out_fpath = os.path.join(enrichment_dir, out_fname)
        out_fhand = open(out_fpath, 'w')
        try:
            volcano_plot_fisher_by_region(dfs_to_plot, out_fhand, plot_labels,
                                          replace_inf=True,
                                          only_over=only_over)
        except ValueError:
            print 'Unable to plot {}'.format(out_fname)

        # GLM poisson test enrichment
        if run_glm:
            log.write('Perform motif enrichment analysis: GLM poisson')
            test = poisson_regression(merged_counts_df, grouping,
                                      is_categorical=True)
            out_fname = '{}.glm_poisson.tab'.format(db_label)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            write_matrix(test, out_fhand)

            out_fname = '{}.glm_poisson.png'.format(db_label)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            labels = set()
            for colname in test.colnames:
                if 'intercept' in colname or 'seq_length' in colname:
                    continue
                labels.add(colname.split('.')[0])
            volcano_plot_glm_by_region(test, out_fhand, params=list(labels),
                                       replace_inf=True)

        # GSEA
        if features_df is not None:
            log.write('Perform motif enrichment analysis: GSEA')
            features_df = features_df.loc[merged_counts_df.index, :]
            test = perform_gsea(merged_counts_df, features_df, n_iter=n_iter,
                                n_threads=n_threads,
                                shuffle_labels=shuffle_labels)
            out_fname = '{}.gsea.tab'.format(db_label)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            write_matrix(test, out_fhand)

            out_fname = '{}.gsea.png'.format(db_label)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            volcano_plot_gsea_by_region(test, out_fhand, replace_inf=True)

        # Combinations of motifs pairs
        if run_combinations:
            tmp_dir = os.path.join(out_dir, 'tmp')
            if not os.path.exists(tmp_dir):
                os.mkdir(tmp_dir)
            log.write('Perform motif pair enrichment analysis: fisher')
            combi_matrices = calc_combination_matrix(merged_counts_df,
                                                     chunk_size=1000,
                                                     sel_region_pairs=reg_pair)
            results = {}

            def run_enrichment_test(combi_matrix):
                res = []
                for group1, group2 in group_pairs:
                    rownames1 = rownames[grouping == group1]
                    rownames2 = rownames[grouping == group2]
                    test = fisher_test(combi_matrix, rownames1, rownames2)
                    res.append((group1, group2, test))
                return res

            tests = run_parallel(run_enrichment_test, combi_matrices,
                                 n_threads)

            for combi_tests in tests:
                for group1, group2, test in combi_tests:
                    try:
                        results[(group1, group2)].append(test)
                    except KeyError:
                        results[(group1, group2)] = [test]

            combi_tests = []
            combi_labels = []
            for key, dfs in results.items():
                merged_combi = concat(dfs, axis=0)
                out_fname = '{}.combi.{}_vs_{}.tab'.format(db_label, key[0],
                                                           key[1])
                out_fpath = os.path.join(enrichment_dir, out_fname)
                out_fhand = open(out_fpath, 'w')
                write_matrix(merged_combi, out_fhand)
                combi_tests.append(merged_combi)
                combi_labels.append('{}_vs_{}'.format(group1, group2))

            out_fname = '{}.combi.fisher.png'.format(db_label)
            out_fpath = os.path.join(enrichment_dir, out_fname)
            out_fhand = open(out_fpath, 'w')
            volcano_plot_fisher_by_region(combi_tests, out_fhand, combi_labels,
                                          replace_inf=True,
                                          only_over=only_over)
