#!/usr/bin/env python

import argparse

import numpy
import pandas
import seaborn

from motif.matrix import read_matrix, write_matrix
from statistics.base import padjust


if __name__ == '__main__':
    description = 'Plots heatmap for the enriched categories in different '
    description += 'enrichment analyses perfomed separately'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input', help='Config file')
    options_group = parser.add_argument_group('Options')
    help_msg = 'FDR threshold to filter gene sets (0.01)'
    options_group.add_argument('-f', '--fdr_threshold', default=0.01,
                               type=float, help=help_msg)
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output files prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    config_fpath = parsed_args.input
    fdr_threshold = parsed_args.fdr_threshold
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')

    # Take categories that are significantly enriched in any comparison
    kept_categories = set()
    dfs_dict = {}
    for line in open(config_fpath):
        if line == '\n':
            continue
        items = line.strip().split('\t')
        label = items[0]
        df = []
        for fpath in items[1:]:
            print fpath
            df.append(read_matrix(fpath))
        df = pandas.concat(df, axis=1) if len(df) > 1 else df[0]
        df['fdr'] = padjust(df['pvalue'], method='fdr')
        sel_rows = df['fdr'] < fdr_threshold
        for category in df.loc[sel_rows, :].index:
            kept_categories.add(category)
        dfs_dict[label] = df

    # Select FDR values of these categories for plotting
    kept_categories = list(kept_categories)
    fdrs = []
    labels = []
    for label, df in dfs_dict.items():
        fdrs.append(numpy.array(df.loc[kept_categories, 'fdr']))
        labels.append(label)
    fdrs = numpy.vstack(fdrs).transpose()
    fdrs = pandas.DataFrame(fdrs, columns=labels, index=kept_categories)
    fdrs = -numpy.log10(fdrs)
    write_matrix(fdrs, out_fhand)
    print 'Selected {} categories'.format(len(kept_categories))
