#!/usr/bin/env python

import argparse
import sys

from seq.parsers import read_fasta_file
from seq.writers import write_fasta_file
from utils.seq import just_seqs


if __name__ == '__main__':

    description = 'Justifies sequences either to the right or left. Blanks are'
    description += ' filled with "-" characters'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', default=None,
                        help='Fasta file')
    parser.add_argument('-o', '--output', default=None,
                        help='Output file')
    parser.add_argument('-r', '--just_right', default=False,
                        action='store_true',
                        help='Justify sequences to the right (def:false)')
    help_msg = 'Length of fragments to select (def: find max len)'
    parser.add_argument('-l', '--length', default=None, type=int,
                        help=help_msg)
    parser.add_argument('-x', '--no_fill', default=True, action='store_false',
                        help='Do not add "-" when the sequence is over')

    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    in_fhand = open(in_fpath) if in_fpath else sys.stdin
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w') if out_fpath else sys.stdout
    just_right = parsed_args.just_right
    length = parsed_args.length
    fill_empty = parsed_args.no_fill

    if length is None:
        if in_fpath is None:
            msg = 'It cannot find the max length if sequences are not in a '
            msg += 'file'
            raise ValueError(msg)
        length = 0
        for name, seq in read_fasta_file(in_fhand):
            seq_len = len(seq)
            if seq_len > length:
                length = seq_len
        in_fhand = open(in_fpath)

    seqs = read_fasta_file(in_fhand)
    justified_seqs = just_seqs(seqs, length=length, just_right=just_right,
                               fill_empty=fill_empty)
    write_fasta_file(justified_seqs, out_fhand)
