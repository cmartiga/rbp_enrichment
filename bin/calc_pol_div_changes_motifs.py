#!/usr/bin/env python

import argparse
import csv

from pysam import FastaFile, Tabixfile

from motif.databases import read_attract_database
from motif.finder import MotifCounter
from utils.misc import run_parallel
from utils.settings import CISBP_RNA, RNAMOTIF, REGEXP


if __name__ == '__main__':
    description = 'Finds out if changes in seq affect specific RNA binding '
    description += 'motifs from a database'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='File containing changes to analyze')
    parser.add_argument('-o', '--output', required=True,
                        help='Output files prefix')
    parser.add_argument('-db', '--motif_db', required=True,
                        help='File with motif database')
    help_msg = 'Database type ({}, {} or {})'.format(RNAMOTIF, CISBP_RNA,
                                                     REGEXP)
    parser.add_argument('-dt', '--db_type', default=RNAMOTIF, help=help_msg)
    help_msg = 'Use sequences as key of the database instread of RBP name'
    parser.add_argument('-s', '--by_seq', default=False, action='store_true',
                        help=help_msg)
    parser.add_argument('-n', '--n_threads', default=1, type=int,
                        help='Number of threads to use (1)')
    help_msg = 'Comma separeted list of otif names to evaluate. If not given '
    help_msg += 'all database motifs will be scanned'
    parser.add_argument('-m', '--motifs', default=None, help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    reader = csv.DictReader(open(in_fpath), delimiter='\t')
    motif_db_fpath = parsed_args.motif_db
    motif_db_type = parsed_args.db_type
    n_threads = parsed_args.n_threads
    by_seq = parsed_args.by_seq
    sel_motifs = parsed_args.motifs
    out_fpath = parsed_args.output
    out_fhand = open(out_fpath, 'w')
    reading_option = 'seq' if by_seq else 'gene_name'

    if motif_db_type == RNAMOTIF:
        motif_db = read_attract_database(motif_db_fpath,
                                         reading_option=reading_option)
    elif motif_db_type == CISBP_RNA:
        raise NotImplementedError('This option is not supported yet')
    elif motif_db_type == REGEXP:
        motif_db = {line.strip().split()[0]: compile(line.strip().split()[1])
                    for line in open(motif_db_fpath)}
        is_regexp = True
    else:
        msg = 'Database type not valid. Only {} and {} are allowed'
        raise ValueError(msg.format(RNAMOTIF, CISBP_RNA))

    if sel_motifs is not None:
        sel_motifs = sel_motifs.split(',')
        motif_db = {key: value for key, value in motif_db.items()
                    if key in sel_motifs}
    count_motifs = MotifCounter(motif_db_type)

    # Run program

    def add_motifs_counts(record):
        for motif_name, motif in motif_db.items():
            seq = record['seq']
            prev_counts = count_motifs(motif, seq)
            change_seq = list(seq)
            change = record['change'].split("'")
            change_seq[int(record['pos'])] = change[3]
            change_seq = ''.join(change_seq)
            changed_counts = count_motifs(motif, change_seq)
            if changed_counts > prev_counts:
                record[motif_name] = '{}+'.format(motif_name)
            elif changed_counts < prev_counts:
                record[motif_name] = '{}-'.format(motif_name)
            else:
                record[motif_name] = '{}='.format(motif_name)
        return record

    new_records = run_parallel(add_motifs_counts, reader, n_threads)

    header = False
    for record in new_records:
        if not header:
            writer = csv.DictWriter(out_fhand, fieldnames=record.keys(),
                                    delimiter='\t')
            writer.writeheader()
            header = True
        writer.writerow(record)
    out_fhand.close()
