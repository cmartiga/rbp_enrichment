#!/usr/bin/env python

import argparse
import gzip

from pysam import Fastafile

from motif.graphprot import train_database
from seq.events import get_gene_coord
from seq.parsers import parse_gtf


if __name__ == '__main__':
    description = 'Creates models using GraphProt for different RBPs from '
    description += 'a bedfile containing the binding sites. It will create a '
    description += 'randomized set of intervals in the same genes in which a '
    description += 'binding site of certain protein was detected as negative '
    description += 'set for training the model'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input', help='BED file containing binding sites')
    help_msg = 'GTF containin annotation for the genes'
    input_group.add_argument('-g', '--gtf', required=True, help=help_msg)
    input_group.add_argument('-c', '--chrom_sizes', required=True,
                             help='File containing chromosome sizes')
    input_group.add_argument('-r', '--ref_fastafile', required=True,
                             help='Samtools indexed genome fasta file')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-n', '--n_shuffle', type=int, default=10000,
                               help='Number of intervals to generate (10000)')
    options_group.add_argument('-s', '--expand_size', type=int, default=150,
                               help='N bases to expand bindig sites (150)')
    options_group.add_argument('-v', '--verbose', action='store_true',
                               default=False)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--out_prefix', required=True,
                              help='Prefix for output files: GraphProt models')

    # Parse arguments
    parsed_args = parser.parse_args()
    bed_fpath = parsed_args.input
    if bed_fpath.split('.')[-1] == 'gz':
        bed_fhand = gzip.open(bed_fpath)
    else:
        bed_fhand = open(bed_fpath)

    gtf_fpath = parsed_args.gtf
    gtf_records = parse_gtf(open(gtf_fpath))
    gene_coords = get_gene_coord(gtf_records, add_chr=True)

    chrom_sizes_fpath = parsed_args.chrom_sizes
    ref_fastafile = Fastafile(parsed_args.ref_fastafile)
    n_shuffle = parsed_args.n_shuffle
    expand_size = parsed_args.expand_size
    verbose = parsed_args.verbose
    out_prefix = parsed_args.out_prefix

    # Run program
    train_database(bed_fhand, gene_coords, chrom_sizes_fpath=chrom_sizes_fpath,
                   ref_fastafile=ref_fastafile, n_shuffle=n_shuffle,
                   verbose=verbose, out_prefix=out_prefix,
                   expand_size=expand_size)
