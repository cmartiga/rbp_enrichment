#!/usr/bin/env python

import argparse
import os
import sys

import numpy
from pysam import Tabixfile

from motif.enrichment import calc_pos_dependent_enrichment_regions
from motif.finder import calc_tabix_profiles
from motif.matrix import read_matrix, write_matrix
from motif.stats import calc_distances_to_ends_df, pairwise_mannwhitneyu, \
    calc_motif_conservation
from seq.events import fetch_splicing_events
from utils.plot import (plot_regions_track_values,
                        plot_distance_to_ss_bloxplots,
                        plot_regions_pos_dependent_enrichment,
                        plot_motif_ss_score, plot_motif_conservation,
                        plot_ppt_features, plot_bp_centered_profiles)
from utils.settings import (MATS_FORMAT, EX, EXON_WINDOW_SIZE,
                            INTRON_WINDOW_SIZE, INT, EVENTS_TYPES,
                            ALT_WINDOW_SIZE, BED)
from utils.misc import LogTrack


if __name__ == '__main__':
    description = 'Performs deepet analysis of a single motif in the observed'
    description += ' splicing events. Performs motif profiles, position depend'
    description += 'ent enrichment analysis, distance to the splice sites and '
    description += 'association with splice site scores in the different '
    description += 'groups'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)

    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input', help='Files containing exons to analyze')
    input_group.add_argument('-f', '--file_format', default=MATS_FORMAT,
                             help='Input file format (MATS)')
    input_group.add_argument('-s', '--splicing_event', default=EX,
                             help='Type of events {}'.format(EVENTS_TYPES))
    input_group.add_argument('-t', '--tabix_file', required=True,
                             help='Tabix indexed file with motifs positions')
    help_msg = 'Profile values to check if there are differences between motif'
    help_msg += 's and no motifs in the different groups (i.e. phastCons)'
    input_group.add_argument('-c', '--profile_prefix', default=None,
                             help=help_msg)
    input_group.add_argument('-r', '--rbp_name', required=True,
                             help='RBP name to profile')
    help_msg = 'File containing row ids and grouping information'
    input_group.add_argument('-g', '--grouping', required=True, help=help_msg)
    help_msg = 'File with splice site scores for splicing events. If given '
    help_msg += 'association between RBP ocurrence and splice sites will be '
    help_msg += 'shown for the different groups'
    input_group.add_argument('-ss', '--splice_site_scores', default=None,
                             help=help_msg)
    help_msg = 'Files prefix with PTT features values predicted by SVM_BP'
    input_group.add_argument('-ppt', '--ppt_features', default=None,
                             help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output dir')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-mp', '--min_phastcons', default=None,
                               type=float,
                               help='Min phastCons to consider a motif (None)')
    help_msg = 'Use always the starting position in BED files regardless of th'
    help_msg += 'e strand of the sequence (Only for BED files from find_genome'
    help_msg += '_motifs)'
    options_group.add_argument('-u', '--use_start_pos', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Exon window size({})'.format(EXON_WINDOW_SIZE)
    options_group.add_argument('-ew', '--exon_window_size',
                               default=EXON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Intron window size({})'.format(INTRON_WINDOW_SIZE)
    options_group.add_argument('-iw', '--intron_window_size',
                               default=INTRON_WINDOW_SIZE, type=int,
                               help=help_msg)
    help_msg = 'Other window size({})'.format(ALT_WINDOW_SIZE)
    options_group.add_argument('-aw', '--alternative_window_size', type=int,
                               default=ALT_WINDOW_SIZE, help=help_msg)

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    file_format = parsed_args.file_format
    sel_event_type = parsed_args.splicing_event
    if sel_event_type not in [EX, INT, BED]:
        msg = sel_event_type + ' type of event not supported. '
        msg += 'Only Exon_cassette or Intron_retention are allowed'
        raise ValueError(msg)
    tabix_fpath = parsed_args.tabix_file
    tabix_index = Tabixfile(tabix_fpath)
    out_dir = parsed_args.output
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    use_tabix_start = parsed_args.use_start_pos
    intron_ws = parsed_args.intron_window_size
    exon_ws = parsed_args.exon_window_size
    alt_ws = parsed_args.alternative_window_size
    sel_rbp = parsed_args.rbp_name
    ss_scores_fpath = parsed_args.splice_site_scores
    if ss_scores_fpath is not None:
        ss_scores = read_matrix(open(ss_scores_fpath))
    else:
        ss_scores = None
    grouping_fpath = parsed_args.grouping
    grouping_df = read_matrix(open(grouping_fpath))
    profile_prefix = parsed_args.profile_prefix
    ppt_prefix = parsed_args.ppt_features
    if ppt_prefix is not None:
        ppt_features = {}
        regions = ['Upstream_intron', 'Downstream_intron']
        for region in regions:
            fpath = '{}.{}.tab'.format(ppt_prefix, region)
            ppt_features[region] = read_matrix(open(fpath))
    else:
        ppt_features = {}
    min_phastcons = parsed_args.min_phastcons
    if min_phastcons is not None:
        cols_filters = {5: lambda x: float(x) > min_phastcons}
    else:
        cols_filters = None

    log = LogTrack(sys.stderr)
    if sel_event_type == EX:
        sorted_regions = ['Upstream_exon.r.{}'.format(exon_ws),
                          'Upstream_intron.l.{}'.format(intron_ws),
                          'Upstream_intron.r.{}'.format(intron_ws),
                          'Alternative_exon.l.{}'.format(exon_ws),
                          'Alternative_exon.r.{}'.format(exon_ws),
                          'Downstream_intron.l.{}'.format(intron_ws),
                          'Downstream_intron.r.{}'.format(intron_ws),
                          'Downstream_exon.l.{}'.format(exon_ws)]
    elif sel_event_type == BED:
        sorted_regions = ['interval.l.{}'.format(alt_ws),
                          'interval.r.{}'.format(alt_ws)]
    else:
        raise NotImplementedError('Other events are not allowed yet')

    if file_format != 'profile':
        # Fetch splicing events and store them in memory
        splicing_events = fetch_splicing_events(in_fpath, file_format,
                                                tabix_index=tabix_index,
                                                sel_event_type=sel_event_type,
                                                take_windows=True,
                                                use_tabix_start=use_tabix_start,
                                                exon_window_size=exon_ws,
                                                intron_window_size=intron_ws,
                                                alternative_window_size=alt_ws,
                                                tabix_cols_filter=cols_filters)

        # Calculate RBP profiles
        log.write('Calculate {} profile'.format(sel_rbp))
        regions, profiles_dfs = calc_tabix_profiles(splicing_events, sel_rbp)
        profiles_index = {}
        for region, df in zip(regions, profiles_dfs):
            profiles_index[region] = df
            profile_fpath = os.path.join(out_dir, 'profile.{}'.format(region))
            profile_fhand = open(profile_fpath, 'w')
            write_matrix(df, profile_fhand)
    else:
        log.write('Loading {} profile'.format(sel_rbp))
        profiles_index = {}
        profiles_dfs = []
        regions = sorted_regions
        for region in sorted_regions:
            profile_fpath = os.path.join(in_fpath, 'profile.{}'.format(region))
            profile_fhand = open(profile_fpath)
            profiles_index[region] = read_matrix(profile_fhand)
            profiles_dfs.append(profiles_index[region])

    # Plot motif or binding site profile
    log.write('Plot profile')
    events_ids = profiles_dfs[0].index
    profiles_numpy = [profiles_index[region].as_matrix()
                      for region in sorted_regions]
    grouping = grouping_df.loc[events_ids, :]

    profiles_fpath = os.path.join(out_dir, 'profiles.png')
    profiles_fhand = open(profiles_fpath, 'w')
    ylabel = 'Proportion of sequences with {}'.format(sel_rbp)
    plot_regions_track_values(profiles_numpy, events_ids, sorted_regions,
                              grouping=grouping, out_fhand=profiles_fhand,
                              ylabel=ylabel, cumulative=False)

    # Plot motif or binding site cumulative profile
    log.write('Plot cumulative profile')
    profiles_fpath = os.path.join(out_dir, 'profiles.cum.png')
    profiles_fhand = open(profiles_fpath, 'w')
    ylabel = 'Proportion of sequences with {}'.format(sel_rbp)
    plot_regions_track_values(profiles_numpy, events_ids, sorted_regions,
                              grouping=grouping, out_fhand=profiles_fhand,
                              ylabel=ylabel, cumulative=True)

    # Plot position dependent enrichment profile
    log.write('Plot position dependent enrichment profile')
    profiles_fpath = os.path.join(out_dir, 'profiles.enrichment.png')
    profiles_fhand = open(profiles_fpath, 'w')
    profiles_dfs = [profiles_index[region] for region in sorted_regions]
    enrichments = calc_pos_dependent_enrichment_regions(profiles_dfs,
                                                        sorted_regions,
                                                        grouping_df)
    plot_regions_pos_dependent_enrichment(enrichments, sorted_regions,
                                          profiles_fhand, plot_or=False)
    profiles_fhand.flush()

    # Calculate distance from the first motif to the splice site
    log.write('Calculate distance from the first ocurrence to splice site')
    distances_fpath = os.path.join(out_dir, 'distances_to_ss.tab')
    distances_fhand = open(distances_fpath, 'w')
    distances_df = calc_distances_to_ends_df(profiles_dfs, sorted_regions)
    write_matrix(distances_df, distances_fhand)
    distances_fhand.close()

    # Perform Mann-Whitney U test for differences in distances between groups
    log.write('Testing differences in distances to splice site')
    d_test_fpath = os.path.join(out_dir, 'distances_to_ss.mannwhitneyu.tab')
    d_test_fhand = open(d_test_fpath, 'w')
    tests = pairwise_mannwhitneyu(distances_df, grouping)
    write_matrix(tests, d_test_fhand)
    d_test_fhand.close()

    # Plot distances to splice sites boxplots
    log.write('Plot distances to splice sites')
    dist_plot_fpath = os.path.join(out_dir, 'distances_to_ss.png')
    dist_plot_fhand = open(dist_plot_fpath, 'w')
    ylabel = 'Distance to end' if sel_event_type == BED else None
    plot_distance_to_ss_bloxplots(distances_df, grouping, dist_plot_fhand,
                                  ylabel=ylabel)
    dist_plot_fhand.close()

    # Plot association between motif and splice site scores
    log.write('Plot motif presence association with splice site scores')
    if ss_scores is not None:
        for splice_site in ss_scores.columns:
            fname = 'motif.ss_scores.{}.png'.format(splice_site)
            motif_ss_fpath = os.path.join(out_dir, fname)
            motif_ss_fhand = open(motif_ss_fpath, 'w')
            plot_motif_ss_score(profiles_dfs, sorted_regions, ss_scores,
                                grouping, splice_site, motif_ss_fhand)

    # Plot association between motif and PPT features
    log.write('Plot motif presence association with PPT features')
    for region, ppt_features_df in ppt_features.items():
        for motif_region in sorted_regions:
            profile = profiles_index[motif_region]
            common_ids = numpy.intersect1d(profile.index,
                                           ppt_features_df.index)
            df = ppt_features_df.loc[common_ids, :]
            profile = profile.loc[common_ids, :]
            fname = 'ppt_features_{}.{}.png'.format(region, motif_region)
            fpath = os.path.join(out_dir, fname)
            out_fhand = open(fpath, 'w')
            plot_ppt_features({region: df}, grouping, out_fhand,
                              profile.as_matrix())

    # Plot mean conservation of motifs and non motifs along regions and groups
    log.write('Plot motif presence association with conservation scores')
    if profile_prefix is not None:
        num_motifs_fpath = os.path.join(out_dir, 'motif_bigwig.png')
        num_motifs_fhand = open(num_motifs_fpath, 'w')
        motif_num_dfs = []
        for region in sorted_regions:
            fpath = profile_prefix + '.{}'.format(region)
            num_profile_df = read_matrix(open(fpath))
            motif_profile_df = profiles_index[region]
            motif_num_df = calc_motif_conservation(motif_profile_df,
                                                   num_profile_df)
            motif_num_dfs.append(motif_num_df)
        plot_motif_conservation(motif_num_dfs, regions, grouping,
                                num_motifs_fhand)

    # Plot profiles centered at the BP
    if ppt_features:
        profiles_fpath = os.path.join(out_dir, 'profiles.BP.png')
        profiles_fhand = open(profiles_fpath, 'w')

        bp_dists = {key: numpy.array(value.loc[events_ids, 'ss_dist'])
                    for key, value in ppt_features.items()}
        ylabel = 'Proportion of sequences with {}'.format(sel_rbp)
        plot_bp_centered_profiles(profiles_numpy, events_ids, sorted_regions,
                                  grouping=grouping, out_fhand=profiles_fhand,
                                  bp_dists=bp_dists, len_left=120,
                                  len_right=60, ylabel=ylabel,
                                  cumulative=False)

        profiles_fpath = os.path.join(out_dir, 'profiles.BP.cum.png')
        profiles_fhand = open(profiles_fpath, 'w')
        bp_dists = {key: numpy.array(value.loc[events_ids, 'ss_dist'])
                    for key, value in ppt_features.items()}
        plot_bp_centered_profiles(profiles_numpy, events_ids, sorted_regions,
                                  grouping=grouping, out_fhand=profiles_fhand,
                                  bp_dists=bp_dists, len_left=120,
                                  len_right=60, ylabel=ylabel, cumulative=True)
