#include <Python.h>
#include "zscore.h"

int dangles = 1;


static PyObject* calc_mfe_zscore(PyObject *self, PyObject *args)
{
    const char *seq;
    double mfe;
    int type;
    int avoid_shuffle = 0;
    char warning_string = 'c';
    double zscore;

    if (!PyArg_ParseTuple(args, "sdi", &seq, &mfe, &type))
        return NULL;

    zscore = mfe_zscore(seq, mfe, &type, avoid_shuffle, &warning_string);
    return Py_BuildValue("d", zscore);
}

static PyMethodDef ZscoreMethods[] = {
    {"calc_mfe_zscore",  calc_mfe_zscore, METH_VARARGS, "Calculates MFE zscore of a sequence."},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initzscore(void)
{
    (void) Py_InitModule("zscore", ZscoreMethods);
}

