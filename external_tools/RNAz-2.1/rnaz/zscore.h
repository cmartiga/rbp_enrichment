/*********************************************************************
 *                                                                   *
 *                              zscore.h                             *
 *                                                                   *
 *	Compute a z-score to assess significance of a predicted MFE  *
 *                                                                   *
 *	          c Stefan Washietl, Ivo L Hofacker                  *
 *                                                                   *
 *	   $Id: zscore.h,v 1.3 2004/09/19 13:31:42 wash Exp $        *
 *                                                                   *
 *********************************************************************/

#ifndef _ZSCORE_H_
#define _ZSCORE_H_

    extern void regression_svm_init();

    extern void regression_svm_free();

    extern double mfe_zscore(const char *seq, double mfe, int *type, int avoid_shuffle, char* warning_string);

#endif


