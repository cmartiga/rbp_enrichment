#include <string>
#include <stdlib.h>
#include <iostream>

using namespace std;

void CapR(int constraint,string input_file,string output_file,string rwidth_file,string position_file);

int main(int argc,char* argv[]){
	if(argc != 6){
		cout << "the number of argument is invalid" << endl;
		return(0);
	}

	string input_file = argv[1];
	string output_file = argv[2];
	int constraint =  atoi(argv[3]);
	string access_length_file = argv[4];
	string position_file = argv[5];
	CapR(constraint,input_file,output_file, access_length_file,position_file);

	return(0);
}