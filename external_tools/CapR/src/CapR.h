#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include <fstream>
#include "energy_const.h"
#include "pair_mat.h"
#include "energy_par.h"

using namespace std;

int MIN(int a,int b);
int MAX(int a,int b);

double exphairpin[31];
double exptetra[30];
double expmismatchH[7][5][5];
double expmismatchI[7][5][5];
double expstack[7][7];
double expbulge[31];
double expTermAU;
double expint11[8][8][5][5];
double expint21[8][8][5][5][5];
double expint22[8][8][5][5][5][5];
double expinternal[31];
double expMLclosing;
double expMLintern;
double expdangle5[8][5];
double expdangle3[8][5];
double expninio[MAXLOOP+1];

static int temperature = 37;
static double kT = (temperature+K0)*GASCONST;
static double lxc37=107.856; /* parameter for logarithmic loop energy extrapolation*/

class CapR_model{
	public:
	string _str_sequence;
	vector<int> _sequence;
	vector<int> _position;
	int _width;
	int _length;
	int _constraint;

	vector<double> _Alpha_outer;
	vector<vector<double> > _Alpha_stem;
	vector<vector<double> > _Alpha_stemend;
	vector<vector<double> > _Alpha_multi;
	vector<vector<double> > _Alpha_multibif;
	vector<vector<double> > _Alpha_multi1;
	vector<vector<double> > _Alpha_multi2;

	vector<double> _Beta_outer;
	vector<vector<double> > _Beta_stem;
	vector<vector<double> > _Beta_stemend;
	vector<vector<double> > _Beta_multi;
	vector<vector<double> > _Beta_multibif;
	vector<vector<double> > _Beta_multi1;
	vector<vector<double> > _Beta_multi2;

	vector<double> Hairpin;
	vector<double> Bulge;
	vector<double> Internal;
	vector<double> Multi;
	vector<double> Outer;
	vector<double> Stem;

	CapR_model(){
		_width = 0;
		_length = 0;
		_constraint = 0;
	}

	void Allclear(){
		_str_sequence = "";

		for(int i = 0; i <= _length;i++){
			_Alpha_stem[i].clear();
			_Alpha_stemend[i].clear();
			_Alpha_multi[i].clear();
			_Alpha_multibif[i].clear();
			_Alpha_multi1[i].clear();
			_Alpha_multi2[i].clear();
			_Beta_stem[i].clear();
			_Beta_stemend[i].clear();
			_Beta_multi[i].clear();
			_Beta_multibif[i].clear();
			_Beta_multi1[i].clear();
			_Beta_multi2[i].clear();
		}

		_sequence.clear();
		_position.clear();

		_Alpha_outer.clear();
		_Alpha_stem.clear();
		_Alpha_stemend.clear();
		_Alpha_multi.clear();
		_Alpha_multibif.clear();
		_Alpha_multi1.clear();
		_Alpha_multi2.clear();

		_Beta_outer.clear();
		_Beta_stem.clear();
		_Beta_stemend.clear();
		_Beta_multi.clear();
		_Beta_multibif.clear();
		_Beta_multi1.clear();
		_Beta_multi2.clear();
	
		Hairpin.clear();
		Bulge.clear();
		Internal.clear();
		Multi.clear();
		Outer.clear();
		Stem.clear();

		_width = 0;
		_length = 0;
		_constraint = 0;

	}

	void Calc_inside(){
		for (int j =TURN+1; j <= _length; j++){
			for (int i=j-TURN; i >= max(0,j-_constraint-1); i--){
				//Alpha_stem
				int type = BP_pair[_sequence[i+1]][_sequence[j]];
				int type2 = BP_pair[_sequence[i+2]][_sequence[j-1]];
				bool flag = 0;

				double temp = 0;
				if (type != 0) {
					type2 = rtype[type2];
					if(_Alpha_stem[i+1][j-i-2] != -INF){
						if(type2 != 0){
							//Stem��Stem
							temp = _Alpha_stem[i+1][j-i-2]+ log(ExpLoopEnergy(type, type2,i+1,j,i+2,j-1));
						}
						flag = 1;
					}

					if(_Alpha_stemend[i+1][j-i-2] != -INF){
						//Stem��StemEnd
						if(flag == 1){
							temp = logsumexp(temp,_Alpha_stemend[i+1][j-i-2]);
						}else{
							temp = _Alpha_stemend[i+1][j-i-2];
						}
						//sum
						flag = 1;
					}

					if(flag == 0){
						_Alpha_stem[i][j-i] = -INF;
					}else{
						_Alpha_stem[i][j-i] = temp;
						flag = 0;
					}

				}else{
					_Alpha_stem[i][j-i] = -INF;
				}

				//Alpha_multiBif
				temp = 0;
				flag = 0;
				for (int k=i+1; k<=j-1; k++){
					if(_Alpha_multi1[i][k-i] != -INF && _Alpha_multi2[k][j-k] != -INF){
						if(flag == 0){
							temp = _Alpha_multi1[i][k-i]+_Alpha_multi2[k][j-k];
							flag = 1;
						}else{
							temp = logsumexp(temp,_Alpha_multi1[i][k-i]+_Alpha_multi2[k][j-k]);
						} 
					}
				}
				if(flag == 0){
					_Alpha_multibif[i][j-i] = -INF;
				}else{
					_Alpha_multibif[i][j-i] = temp;
				}

				//Alpha_multi2
				temp = 0; flag = 0; 
				if (type != 0) {
					if(_Alpha_stem[i][j-i] != -INF){
						temp = _Alpha_stem[i][j-i]+log(expMLintern);
						if (i>0) temp += log(expdangle5[type][_sequence[i]]);
						if (j<_length) temp += log(expdangle3[type][_sequence[j+1]]);
						if( j == _length && type>2){
							temp += log(expTermAU);
						}
						flag = 1;
					}
				}
				if(_Alpha_multi2[i][j-i-1] != -INF){
					_Alpha_multi2[i][j-i] = _Alpha_multi2[i][j-i-1];
					if(flag == 1){
						_Alpha_multi2[i][j-i] = logsumexp(temp,_Alpha_multi2[i][j-i]);
					}
				}else{
					if(flag == 1){
						_Alpha_multi2[i][j-i] = temp;
					}else{
						_Alpha_multi2[i][j-i] = -INF;
					}
				}

				//Alpha_multi1
				if(_Alpha_multi2[i][j-i] != -INF && _Alpha_multibif[i][j-i] != -INF){
					_Alpha_multi1[i][j-i] = logsumexp(_Alpha_multi2[i][j-i],_Alpha_multibif[i][j-i]);
				}else if(_Alpha_multi2[i][j-i] == -INF){
					_Alpha_multi1[i][j-i] = _Alpha_multibif[i][j-i];
				}else if(_Alpha_multibif[i][j-i] == -INF){
					_Alpha_multi1[i][j-i] = _Alpha_multi2[i][j-i];
				}else{
					_Alpha_multi1[i][j-i] = -INF;
				}

				//Alpha_multi
				flag = 0;
				if(_Alpha_multi[i+1][j-i-1] != -INF){
					_Alpha_multi[i][j-i] = _Alpha_multi[i+1][j-i-1];
					flag = 1;
				}

				if(flag == 1){
					if(_Alpha_multibif[i][j-i] != -INF){
						_Alpha_multi[i][j-i] = logsumexp(_Alpha_multi[i][j-i],_Alpha_multibif[i][j-i]);
					}
				}else{
					if(_Alpha_multibif[i][j-i] != -INF){
						_Alpha_multi[i][j-i] = _Alpha_multibif[i][j-i];
					}else{
						_Alpha_multi[i][j-i] = -INF;
					}
				}

				//Alpha_stemend
				if(j != _length){
					temp = 0;
					type = BP_pair[_sequence[i]][_sequence[j+1]];
					if (type!=0) {
						//StemEnd��sn
						temp = log(ExpHairpinEnergy(type, i,j+1));
	
						//StemEnd��sm_Stem_sn
						for (int p =i; p <= MIN(i+MAXLOOP,j-TURN-2); p++) {
							int u1 = p-i;
							for (int q=MAX(p+TURN+2,j-MAXLOOP+u1); q<=j; q++) {
								type2 = BP_pair[_sequence[p+1]][_sequence[q]];
								if(_Alpha_stem[p][q-p] != -INF){
									if (type2 != 0 && !(p == i && q == j)) {
										type2 = rtype[type2];
										temp = logsumexp(temp,_Alpha_stem[p][q-p]+log(ExpLoopEnergy(type, type2,i,j+1,p+1,q))); 
									}
								}
							}
						}

						//StemEnd��Multi
						int tt = rtype[type];
						temp = logsumexp(temp,_Alpha_multi[i][j-i]+log(expMLclosing)+log(expMLintern)+log(expdangle3[tt][_sequence[i+1]])+log(expdangle5[tt][_sequence[j]]));

						//sum
						_Alpha_stemend[i][j-i] = temp;
					}else{
						_Alpha_stemend[i][j-i] = -INF;
					}
				}
			}
		}

		//Alpha_Outer
		for(int i = 1;i <= _length;i++){
			double temp = _Alpha_outer[i-1];
			for(int p = max(0,i-_constraint-1); p <i;p++){
				if(_Alpha_stem[p][i-p] != -INF){
					double ao = _Alpha_stem[p][i-p];
					int type = BP_pair[_sequence[p+1]][_sequence[i]];
					if (type != 0) {
						if (p>0) ao += log(expdangle5[type][_sequence[p]]);
						if (i<_length) ao += log(expdangle3[type][_sequence[i+1]]);
						if( i == _length && type>2){
							ao += log(expTermAU);
						}
					}
					temp = logsumexp(temp,ao+_Alpha_outer[p]);
				}
			}
			_Alpha_outer[i] = temp;
		}
	};

	void Calc_outside(){
		//Beta_outer
		for(int i = _length-1;i >= 0;i--){
			double temp = _Beta_outer[i+1];
			for(int p = i+1; p <= min(i+_constraint+1,_length);p++){
				if(_Alpha_stem[i][p-i] != -INF){
					double bo = _Alpha_stem[i][p-i];
					int type = BP_pair[_sequence[i+1]][_sequence[p]];
					if (type != 0) {
						if (i>0) bo += log(expdangle5[type][_sequence[i]]);
						if (p<_length) bo += log(expdangle3[type][_sequence[p+1]]);
						if( p == _length && type>2){
							bo += log(expTermAU);
						}
					}
					temp = logsumexp(temp,bo+_Beta_outer[p]);
				}
			}
			_Beta_outer[i] = temp;	
		}

		for (int q=_length; q>=TURN+1; q--) {
			for (int p=max(0,q-_constraint-1); p<= q-TURN; p++) {
				double temp = 0;
				int type = 0;
				int type2 = 0;
				bool flag = 0;
				
				if(p != 0 && q != _length){
					//Beta_stemend
					if(q-p >= _constraint){
						_Beta_stemend[p][q-p] = -INF;
					}else{
						_Beta_stemend[p][q-p] = _Beta_stem[p-1][q-p+2];
					}

					//Beta_Multi
					flag = 0;
					if(q-p+1 <= _constraint+1){
						if(_Beta_multi[p-1][q-p+1] != -INF){
							temp = _Beta_multi[p-1][q-p+1];
							flag = 1;
						}
					}

					type = BP_pair[_sequence[p]][_sequence[q+1]];
					int tt = rtype[type];
					if(flag == 1){
						if(_Beta_stemend[p][q-p] != -INF){
							temp = logsumexp(temp,_Beta_stemend[p][q-p]+log(expMLclosing)+log(expMLintern)+ log(expdangle3[tt][_sequence[p+1]])+log(expdangle5[tt][_sequence[q]]));
						}
					}else{
						if(_Beta_stemend[p][q-p] != -INF){
							temp = _Beta_stemend[p][q-p]+log(expMLclosing)+log(expMLintern)+log(expdangle3[tt][_sequence[p+1]])+log(expdangle5[tt][_sequence[q]]);
						}else{
							temp = -INF;
						}
					}
					_Beta_multi[p][q-p] = temp;

					//Beta_Multi1
					temp = 0; flag = 0;
					for(int k = q+1 ; k<= min(_length,p+_constraint);k++){
						if(_Beta_multibif[p][k-p] != -INF && _Alpha_multi2[q][k-q] != -INF){
							if(flag == 0){
								temp = _Beta_multibif[p][k-p]+_Alpha_multi2[q][k-q];
								flag = 1;
							}else{
								temp = logsumexp(temp,_Beta_multibif[p][k-p]+_Alpha_multi2[q][k-q]);
							}
						}
					}
					if(flag == 1){
						_Beta_multi1[p][q-p] = temp;
					}else{
						_Beta_multi1[p][q-p] = -INF;
					}

					//Beta_Multi2
					temp = 0; flag = 0;
					if(_Beta_multi1[p][q-p] != -INF){
						temp = _Beta_multi1[p][q-p];
						flag = 1;
					}
					if(q-p <= _constraint){
						if(_Beta_multi2[p][q-p+1] != -INF){
							if(flag == 1){
								temp = logsumexp(temp,_Beta_multi2[p][q-p+1]);
							}else{
								temp = _Beta_multi2[p][q-p+1];
							}
							flag = 1;
						}
					}
					
					for(int k = max(0,q-_constraint); k < p ;k++){
						if(_Beta_multibif[k][q-k] != -INF && _Alpha_multi1[k][p-k] != -INF){
							if(flag == 0){
								temp = _Beta_multibif[k][q-k]+_Alpha_multi1[k][p-k];
								flag = 1;
							}else{
								temp = logsumexp(temp,_Beta_multibif[k][q-k]+_Alpha_multi1[k][p-k]);
							}
						}
					}
					if(flag == 0){
						_Beta_multi2[p][q-p] = -INF;
					}else{
						_Beta_multi2[p][q-p] = temp;
					}

					//Beta_multibif
					if(_Beta_multi1[p][q-p] != -INF && _Beta_multi[p][q-p] != -INF){
						_Beta_multibif[p][q-p] = logsumexp(_Beta_multi1[p][q-p],_Beta_multi[p][q-p]);
					}else if(_Beta_multi[p][q-p] == -INF){
						_Beta_multibif[p][q-p] = _Beta_multi1[p][q-p];
					}else if(_Beta_multi1[p][q-p] == -INF){
						_Beta_multibif[p][q-p] = _Beta_multi[p][q-p];
					}else{
						_Beta_multibif[p][q-p] = -INF;
					}

				}

				//Beta_stem
				type2 = BP_pair[_sequence[p+1]][_sequence[q]];
				if(type2 != 0){
					temp = _Alpha_outer[p]+_Beta_outer[q];
					if (p>0) temp += log(expdangle5[type2][_sequence[p]]);
					if (q<_length) temp += log(expdangle3[type2][_sequence[q+1]]);
					if( q == _length && type2>2){
						temp += log(expTermAU);
					}

					type2 = rtype[type2];
					for (int i=MAX(1,p-MAXLOOP); i<=p; i++){
						for (int j=q; j<=MIN(q+ MAXLOOP -p+i,_length-1); j++) {
							type = BP_pair[_sequence[i]][_sequence[j+1]];
							if (type != 0 && !(i == p && j == q)) {
								if(j-i <= _constraint+1 && _Beta_stemend[i][j-i] != -INF){
									temp = logsumexp(temp,_Beta_stemend[i][j-i]+log(ExpLoopEnergy(type,type2,i,j+1,p+1,q)));
								}
							}
						}
					}

					if(p != 0 && q != _length){
						type = BP_pair[_sequence[p]][_sequence[q+1]];
						if(type != 0){
							if(q-p+2 <= _constraint+1 && _Beta_stem[p-1][q-p+2] != -INF){
								temp = logsumexp(temp,_Beta_stem[p-1][q-p+2]+log(ExpLoopEnergy(type,type2,p,q+1,p+1,q)));
							}
						}
					}
					_Beta_stem[p][q-p] = temp;

					if(_Beta_multi2[p][q-p] != -INF){
						temp = _Beta_multi2[p][q-p]+log(expMLintern);
						type2 = rtype[type2];
						if (p>0) temp += log(expdangle5[type2][_sequence[p]]);
						if (q<_length) temp += log(expdangle3[type2][_sequence[q+1]]);
						if( q == _length && type2>2){
							temp += log(expTermAU);
						}
						_Beta_stem[p][q-p] = logsumexp(temp,_Beta_stem[p][q-p]);
					}
				}else{
					_Beta_stem[p][q-p] = -INF;
				}
			}
		}
	};

	double logsumexp(double x,double y){
		if(x>y){
			return((x + log(exp(y-x) + 1.0)));
		}else{
			return((y + log(exp(x-y) + 1.0)));
		}
	}

	void Initiallize(int constraint,string &temp_line,vector<int> p_position){
		_constraint = constraint;

		for(int i = 0;i<p_position.size();i++){
			_position.push_back(p_position[i]);
		}

		Set_sequence(temp_line);

		Init_inside();
		Init_outside();
		Init_ener_param();
	};

	void Init_inside(){
		_Alpha_outer.resize(_length+1);
		_Alpha_stem.resize(_length+1,vector<double>(_constraint+2));
		_Alpha_stemend.resize(_length+1,vector<double>(_constraint+2));
		_Alpha_multi.resize(_length+1,vector<double>(_constraint+2));
		_Alpha_multibif.resize(_length+1,vector<double>(_constraint+2));
		_Alpha_multi1.resize(_length+1,vector<double>(_constraint+2));
		_Alpha_multi2.resize(_length+1,vector<double>(_constraint+2));

		for(int i = 0; i <= _length;i++){
			for(int j = 0; j <=_constraint+1  ;j++){
				_Alpha_stem[i][j] = -INF;
				_Alpha_stemend[i][j] = -INF;
				_Alpha_multi[i][j] = -INF;
				_Alpha_multibif[i][j] = -INF;
				_Alpha_multi1[i][j] = -INF;
				_Alpha_multi2[i][j] = -INF;
			}
		}
		for(int i = 0; i <= _length;i++){
			_Alpha_outer[i] = 0;
		}
	}

	void Init_outside(){
		_Beta_outer.resize(_length+1);
		_Beta_stem.resize(_length+1,vector<double>(_constraint+2));
		_Beta_stemend.resize(_length+1,vector<double>(_constraint+2));
		_Beta_multi.resize(_length+1,vector<double>(_constraint+2));
		_Beta_multibif.resize(_length+1,vector<double>(_constraint+2));
		_Beta_multi1.resize(_length+1,vector<double>(_constraint+2));
		_Beta_multi2.resize(_length+1,vector<double>(_constraint+2));

		for(int i = 0; i <= _length;i++){
			for(int j = 0; j <= _constraint+1  ;j++){
				_Beta_stem[i][j] = -INF;
				_Beta_stemend[i][j] = -INF;
				_Beta_multi[i][j] = -INF;
				_Beta_multibif[i][j] = -INF;
				_Beta_multi1[i][j] = -INF;
				_Beta_multi2[i][j] = -INF;
			}
		}
		for(int i = 0; i <= _length;i++){
			_Beta_outer[i] = 0;
		}
	}


	void Set_sequence(string sequence){
		_length = sequence.length();
		_str_sequence = "$" + sequence;
		_sequence.resize(_length+1);
		for(int i = 0;i < _length;i++){
			if(sequence[i] == 'A'){
				_sequence[i+1] = 1;
			}else if(sequence[i] == 'C'){
				_sequence[i+1] = 2;
			}else if(sequence[i] == 'G'){
				_sequence[i+1] = 3;
			}else{
				_sequence[i+1] = 4;
			}
		}
	}

	void Init_ener_param(){
		for (int i=0; i<=30; i++) {
			exphairpin[i] = exp( - hairpin37[i]*10./kT);
		}
		for (int i=0; i<30; i++) {
			exptetra[i] = exp( -tetra_energy37[i]*10./kT);
		}
		for (int i=0; i< 7; i++){
			for (int j=0; j<5; j++){
				for (int k=0; k<5; k++) {
					expmismatchI[i][j][k] = exp(-mismatchI37[i][j][k]*10.0/kT);
					expmismatchH[i][j][k] = exp(-mismatchH37[i][j][k]*10.0/kT);
				}
			}
		}
		for (int i=0; i<7; i++){
			for (int j=0; j<7; j++) {
				expstack[i][j] = exp( -stack37[i][j]*10./kT);
			}
		}
		for (int i=0; i<=30; i++) {
			expbulge[i] = exp( - bulge37[i]*10./kT); 
		}
		expTermAU = exp(-TerminalAU*10/kT);
		
		for (int i=0; i<=7; i++){
			for (int j=0; j<=7; j++){
				for (int k=0; k<5; k++){
					for (int l=0; l<5; l++) {
						expint11[i][j][k][l] = exp(-int11_37[i][j][k][l]*10./kT);
					}
				}
			}
		}
		for (int i=0; i<=7; i++){
			for (int j=0; j<=7; j++){
				for (int k=0; k<5; k++){
					for(int l=0; l<5; l++) {
						for (int m=0; m<5; m++) {
							expint21[i][j][k][l][m] = exp(-int21_37[i][j][k][l][m]*10./kT);
						}
					}
				}
			}
		}
		for (int i=0; i<=7; i++){
			for (int j=0; j<=7; j++){
				for (int k=0; k<5; k++){
					for (int l=0; l<5; l++){
						for (int m=0; m<5; m++){
							for (int n=0; n<5; n++){
								expint22[i][j][k][l][m][n] = exp(-int22_37[i][j][k][l][m][n]*10./kT);
							}
						}
					}
				}
			}
		}
		for (int i=0; i<=30; i++) {
			expinternal[i] = exp( -internal_loop37[i]*10./kT);
		}
		expMLclosing = exp( -ML_closing37*10/kT);
		expMLintern = exp( -ML_intern37*10./kT);
		
		for (int i=0; i<=7; i++){
			for (int j=0; j<=4; j++) {
				expdangle5[i][j] = exp(-dangle5_37[i][j]*10./kT); //SMOOTH
				expdangle3[i][j] = exp(-dangle3_37[i][j]*10./kT); //SMOOTH
				if (i>2){
					expdangle3[i][j] *= expTermAU;
				}
			}
		}
		for (int i=0; i<=MAXLOOP; i++){
			expninio[i]=exp(-MIN(MAX_NINIO,i*F_ninio37)*10/kT);
		}
	}

	double ExpHairpinEnergy(int type, int i, int j) {
		int d = j-i-1;
		string sub_seq = _str_sequence.substr(i,d+2);
		double q = 0; 
		if(d <= 30){
			q = exphairpin[d];
		}else{
			q = exphairpin[30] * exp(- lxc37*log( d/30.) *10./kT);
		}
		/*if (d==4){
			int tel = Tetraloops.find(sub_seq);
			if (tel != string::npos){
				q *= exptetra[tel/7];
			}
		}else */if(d!= 3){
			q *= expmismatchH[type][_sequence[i+1]][_sequence[j-1]];
		}else{
			if(type >2){
				q *= expTermAU;
			}
		}
		return q;
	}

	double ExpLoopEnergy(int type, int type2,int i,int j,int p,int q){
		double z=0;
		int u1 = p-i-1;
		int u2 = j-q-1;
		
		if ((u1==0) && (u2==0)){
			z = expstack[type][type2];
		}else{
			if ((u1==0)||(u2==0)) {
				int u;
				if(u1 == 0){
					u = u2;
				}else{
					u = u1;
				}

				if(u <= 30){
					z = expbulge[u];
				}else{
					z = expbulge[30] * exp(- lxc37*log( u/30.) *10./kT);
				}

				if (u == 1){
					z *= expstack[type][type2];
				}else {
					if (type>2){
						z *= expTermAU;
					}
					if (type2>2){
						z *= expTermAU;
					}
				}
			}else{     
				if (u1+u2==2) {
					z = expint11[type][type2][_sequence[i+1]][_sequence[j-1]];
				}else if ((u1==1) && (u2==2)){
					z = expint21[type][type2][_sequence[i+1]][_sequence[q+1]][_sequence[j-1]];
				}else if ((u1==2) && (u2==1)){
					z = expint21[type2][type][_sequence[q+1]][_sequence[i+1]][_sequence[p-1]];
				}else if ((u1==2) && (u2==2)){
					z = expint22[type][type2][_sequence[i+1]][_sequence[p-1]][_sequence[q+1]][_sequence[j-1]];
				}else{
					z = expinternal[u1+u2]*expmismatchI[type][_sequence[i+1]][_sequence[j-1]]*expmismatchI[type2][_sequence[q+1]][_sequence[p-1]];
					z *= expninio[abs(u1-u2)];
				}
			}
		}
		return z;
	}

	void Set_width(int w){
		_width = w;
	}

	void Calc_state_prob(vector<int> region_width, string name,ofstream &ofs){
		ofs << name;
		for(int size = 0; size< region_width.size(); size++){
			_width = region_width[size];
			Hairpin.resize(_position.size(),0);
			Bulge.resize(_length-_width+1,0);
			Internal.resize(_length-_width+1,0);
			Outer.resize(_position.size(),0);
			Multi.resize(_position.size(),0);
			Stem.resize(_position.size(),0);

			int k = 0;
			Calc_Bulge_prob();
			Calc_Internal_prob();
			
			for(int i = 0;i<_position.size();i++){
				Hairpin[i] = Calc_Hairpin_prob(_position[i]);
				Outer[i] = Calc_Outer_prob(_position[i]);
				Multi[i] = Calc_Multi_prob(_position[i]);
				Stem[i] = Calc_Stem_prob(_position[i]);
			}

			ofs << "length : " << _width << endl;
			ofs << "Bulge ";
			for(int i = 0; i <_position.size() ;i++){
				if(_position[i]+_width-1 > _length || _position[i] < 1){
					ofs << "-1 ";
				}else{
					ofs << Bulge[_position[i]-1] << " ";
				}
			}
			ofs << endl;
			ofs << "Exterior ";
			for(int i = 0; i <  Outer.size() ;i++){
				ofs << Outer[i] << " ";
			}
			ofs << endl;
			ofs << "Hairpin ";
			for(int i = 0; i < Hairpin.size() ;i++){
				ofs << Hairpin[i] << " ";
			}
			ofs << endl;
			ofs << "Internal ";
			for(int i = 0; i < _position.size() ;i++){
				if(_position[i]+_width-1 > _length || _position[i] < 1){
					ofs << "-1 ";
				}else{
					ofs << Internal[_position[i]-1] << " ";
				}
			}
			ofs << endl;
			ofs << "Multibranch ";
			for(int i = 0; i < Multi.size() ;i++){
				ofs << Multi[i] << " ";
			}
			ofs << endl;
			ofs << "Stem ";
			for(int i = 0; i < Stem.size() ;i++){
				ofs << Stem[i] << " ";
			}
			ofs << endl;
			Clear_state_prob();
		}
		ofs << endl;
	}

	void Clear_state_prob(){
		Stem.clear();
		Bulge.clear();
		Hairpin.clear();
		Internal.clear();
		Multi.clear();
		Outer.clear();
	}

	double Calc_Stem_prob(int x){
		if(x+_width-1 > _length || x < 1){
			return(-1);
		}
		double probability = 0;
		double temp = 0;
		double temp2 = 0;
		int type = 0;
		int type2 = 0;
		bool flag = 0;
		bool flag2 = 0;

		for(int i = 1;(i+_width <= x-TURN) && (x+_width-1 <= _length);i++){
			temp = 0;
			temp2 = 0;
			if(x+_width-1-i>_constraint){
				temp = 0;
			}else if(_Beta_stem[i-1][x+_width-i] != -INF){
				temp = _Beta_stem[i-1][x+_width-i];

				for(int j = 0; j < _width-1;j++){
					type = BP_pair[_sequence[i+j]][_sequence[x+_width-1-j]];
					type2 = rtype[BP_pair[_sequence[i+j+1]][_sequence[x+_width-j-2]]];
					if(type == 0 || type2 == 0){
						flag = 1;
						break;
					}
					temp += log(ExpLoopEnergy(type,type2,i+j,x+_width-1-j,i+j+1,x+_width-j-2));
				}

				if(flag == 0){
					type = BP_pair[_sequence[i+_width-1]][_sequence[x]];
					type2 = rtype[BP_pair[_sequence[i+_width]][_sequence[x-1]]];
					if(_Alpha_stemend[i+_width-1][x-i-_width] != -INF){
						temp2 = temp+_Alpha_stemend[i+_width-1][x-i-_width];
						flag2 = 1;
					}
					
					if(_Alpha_stem[i+_width-1][x-i-_width] != -INF){
						if(flag2 == 1){
							temp2 = logsumexp(temp2,temp+_Alpha_stem[i+_width-1][x-i-_width] + log(ExpLoopEnergy(type,type2,i+_width-1,x,i+_width,x-1)));
						}else{
							temp2 = temp+_Alpha_stem[i+_width-1][x-i-_width] + log(ExpLoopEnergy(type,type2,i+_width-1,x,i+_width,x-1));
						}
					}
				}
			}
			if(temp2 != 0){
				temp2 = exp(temp2-_Alpha_outer[_length]);
			}

			probability += temp2;
			flag = 0;
			flag2 = 0;
		}

		for(int i = x+_width+TURN;i+_width-1<=_length;i++){
			temp = 0;
			temp2 = 0;
			if(i+_width-1-x>_constraint){
				temp = 0;
			}else if(_Beta_stem[x-1][i+_width-x] != -INF){
				temp = _Beta_stem[x-1][i+_width-x];
				for(int j = 0; j < _width-1;j++){
					type = BP_pair[_sequence[x+j]][_sequence[i+_width-1-j]];
					type2 = rtype[BP_pair[_sequence[x+j+1]][_sequence[i+_width-j-2]]];
					if(type == 0 || type2 == 0){
						flag = 1;
						break;
					}
					temp += log(ExpLoopEnergy(type,type2,x+j,i+_width-1-j,x+j+1,i+_width-j-2));
				}

				if(flag == 0){
					type = BP_pair[_sequence[x+_width-1]][_sequence[i]];
					type2 = rtype[BP_pair[_sequence[x+_width]][_sequence[i-1]]];
					if(_Alpha_stemend[x+_width-1][i-x-_width] != -INF){
						temp2 = temp+_Alpha_stemend[x+_width-1][i-x-_width];
						flag2 = 1;
					}
					
					if(_Alpha_stem[x+_width-1][i-x-_width] != -INF){
						if(flag2 == 1){
							temp2 = logsumexp(temp2,temp+_Alpha_stem[x+_width-1][i-x-_width] + log(ExpLoopEnergy(type,type2,x+_width-1,i,x+_width,i-1)));
						}else{
							temp2 = temp+_Alpha_stem[x+_width-1][i-x-_width]+ log(ExpLoopEnergy(type,type2,x+_width-1,i,x+_width,i-1));
						}
					}
				}
			}
			if(temp2 != 0){
				temp2 = exp(temp2-_Alpha_outer[_length]);
			}

			probability += temp2;
			flag = 0;
			flag2 = 0;
		}
		return(probability);
	}

	double Calc_Outer_prob(int x){
		if(x+_width-1 > _length || x < 1){
			return(-1);
		}
		double probability = 0;
		probability = exp(_Alpha_outer[x-1]+_Beta_outer[x+_width-1]-_Alpha_outer[_length]);
		return(probability);
	}

	double Calc_Multi_prob(int x){
		if(x+_width-1 > _length || x < 1){
			return(-1);
		}
		double probability = 0;
		double temp = 0;
		bool flag = 0;
		for(int i = x+_width-1; i<=min(x+_constraint,_length);i++){
			if(_Beta_multi[x-1][i-x+1] != -INF && _Alpha_multi[x+_width-1][i-x-_width+1] != -INF){
				if(flag == 0){
					temp = _Beta_multi[x-1][i-x+1] + _Alpha_multi[x+_width-1][i-x-_width+1];
					flag = 1;
				}else{
					temp = logsumexp(temp,_Beta_multi[x-1][i-x+1]+ _Alpha_multi[x+_width-1][i-x-_width+1]);
				}
			}
		}
		
		for(int i = max(0,x+_width-1-_constraint); i<x;i++){
			if(_Beta_multi2[i][x+_width-1-i] != -INF && _Alpha_multi2[i][x-i-1] != -INF){
				if(flag == 0){
					temp = _Beta_multi2[i][x+_width-1-i] + _Alpha_multi2[i][x-i-1];
					flag = 1;
				}else{
					temp = logsumexp(temp,_Beta_multi2[i][x+_width-1-i] + _Alpha_multi2[i][x-i-1]);
				}
			}
		}
		if(flag == 1){
			probability = exp(temp-_Alpha_outer[_length]);
		}
		return(probability);
	}

	double Calc_Hairpin_prob(int x){
		if(x+_width-1 > _length || x < 1){
			return(-1);
		}
		double probability = 0;
		double temp = 0;
		int type = 0;

		for(int i = max(1,x-_constraint);i<x ;i++){
			for(int j = x+_width; j<=min(i+_constraint,_length);j++){
				type = BP_pair[_sequence[i]][_sequence[j]];
				if(_Beta_stemend[i][j-i-1] != -INF){
					temp = _Beta_stemend[i][j-i-1] + log(ExpHairpinEnergy(type, i,j));
					probability += exp(temp-_Alpha_outer[_length]);
				}
			}
		}
		return(probability);
	}

	void Calc_Bulge_prob(){
		
		double probability = 0;
		double temp = 0;
		int type = 0;
		int type2 = 0;
		bool flag = 0;

		for(int i = 1; i<_length;i++){
			for(int j = i+1; j<=min(i+_constraint,_length);j++){
				type = BP_pair[_sequence[i]][_sequence[j]];
				if (type!=0) {
					for (int p = i+1; p <= MIN(i+MAXLOOP+1,j-1); p++) {
						int u1 = p-i-1;
						int q = j-1;
						type2 = BP_pair[_sequence[p]][_sequence[q]];
						if (type2 != 0 && !(p == i+1 && q == j-1)) {
							type2 = rtype[type2];
							if(_Beta_stemend[i][j-i-1] != -INF && _Alpha_stem[p-1][q-p+1] != -INF){
								temp = _Beta_stemend[i][j-i-1] + log(ExpLoopEnergy(type, type2,i,j,p,q))+_Alpha_stem[p-1][q-p+1];
								probability = exp(temp-_Alpha_outer[_length]);

								for(int k = i+1; k <= p-_width;k++){
									Bulge[k-1] += probability;
								}
							}
						}

					}

					int p = i+1;
					int u1 = p-i-1;
					for (int q=MAX(p+TURN+1,j-MAXLOOP+u1-1); q<j; q++) {
						type2 = BP_pair[_sequence[p]][_sequence[q]];
						if (type2 != 0 && !(p == i+1 && q == j-1)) {
							type2 = rtype[type2];
							if(_Beta_stemend[i][j-i-1] != -INF && _Alpha_stem[p-1][q-p+1] != -INF){
								temp = _Beta_stemend[i][j-i-1] + log(ExpLoopEnergy(type, type2,i,j,p,q))+_Alpha_stem[p-1][q-p+1];
								probability = exp(temp-_Alpha_outer[_length]);

								for(int k = q+1; k <= j-_width;k++){
									Bulge[k-1] += probability;
								}
							}
						}
					}
				}
			}
		}
	}

	void Calc_Internal_prob(){

		double probability = 0;
		double temp = 0;
		int type = 0;
		int type2 = 0;
		bool flag = 0;

		for(int i = 1; i<_length;i++){
			for(int j = i+1; j<=min(i+_constraint,_length);j++){
				type = BP_pair[_sequence[i]][_sequence[j]];
				if (type!=0) {
					for (int p =i+2; p <= MIN(i+MAXLOOP+1,j-1); p++) {
						int u1 = p-i-1;
						for (int q=MAX(p+TURN+1,j-MAXLOOP+u1-1); q<j-1; q++) {
							type2 = BP_pair[_sequence[p]][_sequence[q]];
							if (type2 != 0 && !(p == i+1 && q == j-1)) {
								type2 = rtype[type2];
								if(_Beta_stemend[i][j-i-1] != -INF && _Alpha_stem[p-1][q-p+1] != -INF){
									temp = _Beta_stemend[i][j-i-1] + log(ExpLoopEnergy(type, type2,i,j,p,q))+_Alpha_stem[p-1][q-p+1];
									probability = exp(temp-_Alpha_outer[_length]);

									for(int k = i+1; k <= p-_width;k++){
										Internal[k-1] += probability;
									}

									for(int k = q+1; k <= j-_width;k++){
										Internal[k-1] += probability;
									}
								} 
							}
						}
					}
				}
			}
		}
	}
};