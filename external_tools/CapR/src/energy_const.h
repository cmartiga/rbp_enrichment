/*
 * energy_const.h
 *
 *   Extracted from RNAfold program of Vienna package (version 1.8.5)
 *   the author of the original code: Ivo L Hofacker
 *
 *   modified to the present form by Tsukasa Fukunaga
 */

#ifndef _ENERGY_CONST
#define _ENERGY_CONST

#define GASCONST 1.98717  /* in [cal/K] */
#define K0  273.15
#define INF 1000000
#define TURN 3 
#define MAXLOOP 30

#endif