#include "CapR.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <algorithm>

void CapR(int constraint,string input_file,string output_file,string rwidth_file,string position_file){
	ifstream fp;
	fp.open(rwidth_file.c_str(),ios::in);
	if (!fp){
		cout << "length file open error" <<endl;
		return;
	}

	char buf[500000];
	vector<int> region_width; region_width.reserve(10);
	fp.getline(buf,500000);
	char* tp;
	tp = strtok(buf, " " );
	region_width.push_back(atoi(tp));
	while(tp != NULL){
		tp = strtok( NULL," " );
		if ( tp != NULL ){
			region_width.push_back(atoi(tp));
		}
    }
	fp.close();

	ifstream pfp;
	fp.open(position_file.c_str(),ios::in);
	if (!pfp){
		cout << "Position File open error!" <<endl;
		return;
	}
	
	char pbuf[500000];
	vector<int> position; position.reserve(10);
	fp.getline(pbuf,500000);
	tp = strtok(pbuf, " " );
	position.push_back(atoi(tp));
	while(tp != NULL){
		tp = strtok( NULL," " );
		if ( tp != NULL ){
			position.push_back(atoi(tp));
		}
    }

	ifstream ifp;
	string buffer;
	ifp.open(input_file.c_str(),ios::in);
	if (!ifp){
		cout << "input file open error" <<endl;
		return;
	}
	getline(ifp,buffer);
	string name = buffer;
	string temp_line = "";
	string new_name = "";
	CapR_model st_model;
	ofstream ofs(output_file.c_str());
	while(getline(ifp,buffer)){
		if(buffer[0] == '>'){
			new_name = buffer;
			st_model.Initiallize(constraint,temp_line,position);
			st_model.Calc_inside();
			st_model.Calc_outside();
			st_model.Calc_state_prob(region_width,name,ofs);
			st_model.Allclear();
			name = new_name;
			temp_line = "";
		}else{
			if(buffer[buffer.size()-1] == '\r' || buf[buffer.size()-1] == '\r\n' || buf[buffer.size()-1] == '\n'){
				buffer.erase(buffer.size()-1,1);
			}
			temp_line = temp_line+buffer;
		}
	}
	ifp.close();
	
	st_model.Initiallize(constraint,temp_line,position);
	st_model.Calc_inside();
	st_model.Calc_outside();
	st_model.Calc_state_prob(region_width,name,ofs);

	ofs.close();

}