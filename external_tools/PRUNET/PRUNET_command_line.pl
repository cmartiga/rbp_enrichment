#!/usr/bin/perl


#Pending: warnings from Translator.pm, asynchronous, for big networks: saving hashes in hard-disk, more attractors, optimize logic functions too, defined logic functions, multivariate, SBML.
#use strict;
use warnings;
use Getopt::Long;

use FindBin;                     # locate this script
use lib "$FindBin::RealBin";

use PRUNET_multivariate;
use boolean_functions_generator;
use Translator;

#user interface
my $maximum_iterations = 150;
my $population = 50;
my $selection = 15;
my $elitism = 5;
my $optimization = 'univariate';
my $updating_scheme = 'synchronous';#ASYNCHRONOUS IS NOT WORKING YET
my $network_type = 'fixed';
my $output_dir = 'prunet_out';



sub begins_with{
    return substr($_[0], 0, length($_[1])) eq $_[1];
}

# Initialize input arrays
my @network_view_content;
my @booleanized_phe_basal;
my @booleanized_phe;
my @fixed_interactions_1_content;
my @fixed_interactions_2_content;

my $lines_block = "";
my $line;
while(<STDIN>) {
    $line = $_;
    if (begins_with($line, "#")){
        chomp($line);
        $lines_block = $line;
    }
    else{
        if($lines_block eq "# PKN"){
            push @network_view_content, $line;
        }
        elsif($lines_block eq "# phen1"){
            push @booleanized_phe_basal, $line;            
        }
        elsif($lines_block eq "# phen2"){
            push @booleanized_phe, $line;            
        }
        elsif($lines_block eq "# fixed"){
            push @fixed_interactions_1_content, $line;            
            push @fixed_interactions_2_content, $line;
        }
        elsif($lines_block eq "# max_iterations"){
            chomp($line);
            $maximum_iterations = $line;
        }
        elsif($lines_block eq "# selection"){
            chomp($line);
            $selection = $line;
        }
        elsif($lines_block eq "# elitism"){
            chomp($line);
            $elitism = $line;
        }
        elsif($lines_block eq "# population_size"){
            chomp($line);
            $population = $line;
        }
    }
}
close STDIN;

my %CIRCUITS = ();

# Run program
if($network_type eq 'fixed'){
    my ($edges,$populations_1,$scores_1) = PRUNET_multivariate::PRUNET(
	\@network_view_content,
        \@booleanized_phe_basal,
	\@booleanized_phe,
	\@fixed_interactions_1_content,
	\@fixed_interactions_2_content,
	\@updating_sequence,
	$maximum_iterations,
	$population,
	$selection,
	$elitism,
	$network_type,
	$optimization,
	$updating_scheme,
    \%CIRCUITS,
    $output_dir);	

    my %edge_frequences = %{$edges};
    my @population_of_networks_network_1 = @{$populations_1};
    my @scores_of_population_of_networks_network_1 = @{$scores_1};

    my @temporal;
    for (my $i=0;$i<scalar @population_of_networks_network_1;$i++){
        print ">> Network $i: $scores_of_population_of_networks_network_1[$i]\n";
        @temporal = keys %{$population_of_networks_network_1[$i]};
        foreach my $temporal_line(@temporal){
            print $temporal_line;
        }
    }
    
}
elsif($network_type eq 'variable'){
    my ($results,$popul_1,$scor_1,$popul_2,$scor_2) = PRUNET_multivariate::PRUNET(
	\@network_view_content,
	\@booleanized_phe_basal,
	\@booleanized_phe,
	\@fixed_interactions_1_content,
	\@fixed_interactions_2_content,
	\@updating_sequence,
	$maximum_iterations,
	$population,
	$selection,
	$elitism,
	$network_type,
	$optimization,
	$updating_scheme,
        \%CIRCUITS,
        $output_dir);	
    my @results = @{$results};
		
    my %edge_frequences_network_1 = %{$results[0]};
    my @population_of_networks_network_1 = @{$popul_1};
    my @scores_of_population_of_networks_network_1 = @{$scor_1};
    my %edge_frequences_network_2 = %{$results[1]};
    my @population_of_networks_network_2 = @{$popul_2};
    my @scores_of_population_of_networks_network_2 = @{$scor_2};

}
else {}


