#!/usr/bin/env python

from distutils.core import setup, Extension
import os

from utils.settings import EXT_DIR


lib_dir = os.path.join(EXT_DIR, 'RNAz-2.1', 'rnaz')

module1 = Extension('zscore', depends=['zscore.h', 'utils.h', 'svm.h',
                                       'fold.h', 'fold_vars.h', 'params.h',
                                       'energy_par.h', 'read_epars.h',
                                       'svm_helper.h', 'rnaz_utils.h'],
                    library_dirs=[lib_dir],
                    include_dirs=[lib_dir],
                    libraries=['zscore', 'utils', 'svm', 'fold', 'fold_vars',
                               'params', 'energy_par', 'read_epars',
                               'svm_helper', 'rnaz_utils'],
                    sources=[os.path.join(lib_dir, 'zscoremodule.c')])

description = 'Module for calculating MFE zscores of RNA sequences from '
description += 'RNAfold predictions'
setup(name='RNAz zscore package', version='1.0', description=description,
      ext_modules=[module1])
