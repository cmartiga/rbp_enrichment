# /usr/bin/env python

import itertools
from subprocess import check_call
from tempfile import NamedTemporaryFile

import numpy
from pandas import DataFrame, concat

from statistics.enrichment import fisher_test
from utils.misc import take_chunks
from utils.settings import GSEA_FPATH


def get_data_file(df):
    data_fhand = NamedTemporaryFile('w', suffix='.txt')
    df.to_csv(data_fhand, sep='\t')
    data_fhand.flush()
    return data_fhand.name


def get_cls_file(grouping):
    cls_fhand = NamedTemporaryFile('w', suffix='.cls')
    classes = list(set(grouping))
    if len(classes) != 2:
        msg = 'Only 2 phenotypes are allowed'
        raise ValueError(msg)
    lines = '{} {} 1\n'.format(len(grouping), len(classes))
    lines += '# {}\n{}\n'.format(' '.join(classes), ' '.join(grouping))
    cls_fhand.write(lines)
    cls_fhand.flush()
    return cls_fhand.name


def sets_df_to_gmt_file(sets_df, out_fhand=None, sep='\t'):
    ids = numpy.array(sets_df.index)
    if out_fhand is None:
        out_fhand = NamedTemporaryFile('w')
    lines = ''
    for set_name in sets_df.columns:
        iset = list(ids[numpy.array(sets_df[set_name] > 0)])
        lines += '{}{}{}\n'.format(set_name, sep, sep.join(iset))
    out_fhand.write(lines)
    out_fhand.flush()
    return out_fhand.name


def _parse_gmt_line(line):
    try:
        items = line.strip().split('\t')
        set_name = items[0]
        if items[1].startswith('http'):
            set_ids = items[2:]
        else:
            set_ids = items[1:]
        set_ids = set([set_id.upper() for set_id in set_ids])
        return set_name, set_ids
    except IndexError:
        return None


def gmt_file_to_sets_df(gmt_lines, ids):
    presence = []
    set_names = []
    for line in gmt_lines:
        parsed = _parse_gmt_line(line)
        if parsed is None:
            continue
        set_name, set_ids = parsed
        set_names.append(set_name)
        presence.append(numpy.array([1 if x in set_ids else 0 for x in ids]))
    presence = DataFrame(numpy.vstack(presence).transpose(), index=ids,
                         columns=set_names)
    return presence


def gene_set_enrichment_analysis(data_fpath, gmt_fpath, cls_fpath,
                                 out_dir, n_permutation=1000,
                                 permutation_type='phenotype', collapse=False,
                                 mode='Max_probe', normalize=True,
                                 weighted_score=True, metric='Signal2Noise',
                                 order='descending', sort_type='real',
                                 seed=None, max_set_size=1000,
                                 min_set_size=15, max_mem=6000):
    for line in open(cls_fpath):
        if line.startswith('#'):
            classes = line.strip().split()[1:]
            if len(classes) != 2:
                raise ValueError('Only 2 phenotype classes are allowed')
            comparison = '#{}_versus_{}'.format(classes[0], classes[1])
    collapse = 'true' if collapse else 'false'
    normalize = 'meandiv' if normalize else 'None'
    scoring = 'weighted' if weighted_score else 'classic'
    seed = seed if seed is not None else 'timestamp'
    label = comparison[1:]

    cmd = ['java', '-cp', GSEA_FPATH, '-Xmx{}m'.format(max_mem),
           'xtools.gsea.Gsea', '-res', data_fpath, '-cls',
           cls_fpath + comparison, '-gmx', gmt_fpath,
           '-collapse', collapse, '-mode', mode, '-norm', normalize,
           '-nperm', str(n_permutation), '-permute', permutation_type,
           '-rnd_type', 'no_balance', '-scoring_scheme', scoring,
           '-rpt_label', label, '-metric', metric, '-sort', sort_type,
           '-order', order, '-include_only_symbols', 'true', '-make_sets',
           'true', '-median', 'false', '-num', '100', '-plot_top_x', '20',
           '-rnd_seed', seed, '-save_rnd_lists', 'false', '-set_max',
           str(max_set_size), '-set_min', str(min_set_size), '-zip_report',
           'false', '-out', out_dir, '-gui', 'false']
    check_call(cmd)


def event_id_to_gene_id(grouping_df, event_gene_ids_df):
    groups = grouping_df.copy()
    groups.index = event_gene_ids_df.ix[groups.index, 0]
    return groups


def _get_event_gene_id_dict(event_gene_id_df):
    gene_id_idx = {}
    for row in event_gene_id_df.as_matrix():
        gene_id = row[0]
        event_id = row[1]
        try:
            gene_id_idx[gene_id].append(event_id)
        except KeyError:
            gene_id_idx[gene_id] = [event_id]
    return gene_id_idx


def sets_event_id_to_gene_id(sets_gmt_fpath, event_gene_id_df, out_fhand=None):
    gene_id_idx = _get_event_gene_id_dict(event_gene_id_df)
    if out_fhand is None:
        out_fhand = NamedTemporaryFile(mode='w', suffix='.gmt')
    for line in open(sets_gmt_fpath):
        set_name, set_ids = _parse_gmt_line(line)
        new_items = [set_name]
        for set_id in set_ids:
            try:
                new_items.extend(gene_id_idx[set_id])
            except KeyError:
                continue
        if len(new_items) > 1:
            out_fhand.write('\t'.join(new_items) + '\n')
    out_fhand.flush()
    return out_fhand.name


def fisher_enrichment_analysis(sets_gmt_fpath, grouping_df, chunk_size=100,
                               ids_df=None, comparisons=None, verbose=False):
    grouping = numpy.array(grouping_df.iloc[:, 0])
    rownames = grouping_df.index
    if ids_df is not None:
        sel_ids = numpy.unique(ids_df.ix[rownames, 0])
        sel_ids = numpy.array([sel_id for sel_id in sel_ids
                               if type(sel_id) == str])
    else:
        sel_ids = rownames
    sel_ids = numpy.unique([sel_id.upper() for sel_id in sel_ids])

    results = {}
    if comparisons is None:
        groups = numpy.unique(grouping)
        comparisons = itertools.combinations(groups, 2)
    for group1, group2 in comparisons:
        comparison = '{}_vs_{}.tab'.format(group1, group2)
        if verbose:
            print 'Comparing {} vs {}'.format(group1, group2)
        rownames1 = rownames[grouping == group1]
        rownames1 = numpy.array([x for x in rownames1 if x != numpy.nan])
        rownames2 = rownames[grouping == group2]
        rownames2 = numpy.array([x for x in rownames2 if x != numpy.nan])
        if ids_df is not None:
            rownames1 = ids_df.ix[rownames1, 0]
            rownames2 = ids_df.ix[rownames2, 0]
        rownames1 = numpy.unique([str(x).upper() for x in rownames1])
        rownames2 = numpy.unique([str(x).upper() for x in rownames2])
        tests = []
        fhand = open(sets_gmt_fpath)
        for i, chunk_lines in enumerate(take_chunks(fhand,
                                                    chunk_size=chunk_size)):
            if verbose:
                print 'Chunk', i
            chunk_df = gmt_file_to_sets_df(chunk_lines, sel_ids)
            tests.append(fisher_test(chunk_df, rownames1, rownames2,
                                     alternative='greater'))
        results[comparison] = concat(tests, axis=0)
    return results
