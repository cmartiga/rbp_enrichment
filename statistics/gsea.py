#!/usr/bin/env python
from itertools import permutations
from multiprocessing import Pool
import random
import time

import numpy
from pandas import DataFrame

from motif.matrix import rows_are_uniform, cols_are_uniform
from statistics.base import EmpiricalPvalueCalculator, padjust
from utils.misc import run_parallel


class MatrixShuffler(object):
    def __init__(self, matrix, by_col=True, shuffle_each_row=False):
        if not isinstance(matrix, numpy.ndarray):
            raise ValueError('It must be a numpy array')
        if by_col:
            self.idx_permutations = list(permutations(range(matrix.shape[1])))
        self.matrix = matrix
        self.shuffle_each_row = shuffle_each_row
        self.by_col = by_col

    def __call__(self):
        if self.by_col:
            if self.shuffle_each_row:
                new_matrix = []
                for row in self.matrix:
                    idxs = numpy.array(random.choice(self.idx_permutations))
                    new_matrix.append(row[idxs])
                return numpy.vstack(new_matrix)
            else:
                idxs = numpy.array(random.choice(self.idx_permutations))
                return self.matrix[:, idxs]
        else:
            return numpy.random.permutation(self.matrix)


def _calc_pheno_feat_corr(features_matrix, phenotypes):
    pheno = phenotypes
    pheno_classes = numpy.unique([p.split('.')[0] for p in pheno])
    if len(pheno_classes) != 2:
        msg = 'Correlation can only be calculated with two phenotypes'
        raise ValueError(msg)
    pheno1 = pheno_classes[0]
    pheno = [p.split('.')[0] == pheno1 for p in pheno]

    return numpy.array([numpy.corrcoef(row, pheno)[1, 0]
                        for row in features_matrix])


def calc_enrichment_score(corr_array, motif_numpy, constant_inc=None):
    sorted_idxs = numpy.fliplr([numpy.argsort(corr_array)])[0]
    sorted_corr = corr_array[sorted_idxs]
    motif_matrix = motif_numpy[sorted_idxs, :]

    enrichment_mask = motif_matrix == 0
    enrichment_mask_not = numpy.logical_not(enrichment_mask)
    sorted_matrix = numpy.vstack([sorted_corr] *
                                 motif_matrix.shape[1]).transpose()

    sorted_matrix = sorted_matrix.astype(numpy.float)
    assert sorted_matrix.shape == enrichment_mask.shape

    if constant_inc is not None:
        sorted_matrix[enrichment_mask_not] = constant_inc
    else:
        abs_corr = numpy.absolute(sorted_matrix[enrichment_mask_not])
        sorted_matrix[enrichment_mask_not] = abs_corr

    sorted_matrix[enrichment_mask] = 0
    total = numpy.sum(sorted_matrix == 0, axis=0)
    for i in range(sorted_matrix.shape[1]):
        decrease = -numpy.sum(sorted_matrix[:, i]) / total[i]
        sorted_matrix[enrichment_mask[:, i], i] = decrease

    walk = numpy.absolute(numpy.cumsum(sorted_matrix, axis=0))
    rank = (numpy.argmax(walk, axis=0) + 1) / float(walk.shape[0])
    enrichment_score = numpy.max(walk, axis=0) / (walk.shape[0] - total)
    return numpy.vstack([enrichment_score, rank]).transpose()


class ESnullDistribCalculator(object):
    def __init__(self, features_numpy, motif_numpy, phenotypes, shuffle_func,
                 shuffle_labels=True, constant_inc=None, verbose=False):
        self.features_numpy = features_numpy
        self.motif_numpy = motif_numpy
        self.phenotypes = phenotypes
        self.shuffle_func = shuffle_func
        self.constant_inc = constant_inc
        self.verbose = verbose
        self.shuffle_labels = shuffle_labels

    def __call__(self, iteration_number):
        if self.verbose:
            print '\tIteration {} : {}'.format(iteration_number, time.ctime())
        if self.shuffle_labels:
            features_numpy = self.shuffle_func()
            motif_numpy = self.motif_numpy
            i_corr_mat = _calc_pheno_feat_corr(features_numpy,
                                               phenotypes=self.phenotypes)
        else:
            features_numpy = self.features_numpy
            motif_numpy = self.shuffle_func()
            if hasattr(self, 'corr_array'):
                i_corr_mat = self.corr_array
            else:
                i_corr_mat = _calc_pheno_feat_corr(features_numpy,
                                                   phenotypes=self.phenotypes)
                self.corr_array = i_corr_mat

        es = calc_enrichment_score(i_corr_mat, motif_numpy,
                                   constant_inc=self.constant_inc)[:, 0]
        return es


def perform_gsea(motif_matrix, features_matrix, n_iter=1000,
                 constant_inc=None, verbose=False, n_threads=1,
                 shuffle_labels=True, shuffle_each_row=False):

    if numpy.any(motif_matrix.index != features_matrix.index):
        raise ValueError('Ids from the two matrices do not match')

    motif_numpy = motif_matrix.as_matrix()
    features_numpy = features_matrix.as_matrix()

    # Remove sequences with constant feature values
    uniform = rows_are_uniform(features_numpy)
    variable = numpy.logical_not(uniform)
    features_numpy = features_numpy[variable, :]
    motif_numpy = motif_numpy[variable, :]

    # Remove motifs that are not present in any sequence or all of them
    motif_numpy[motif_numpy > 0] = 1
    with_motif = numpy.logical_not(cols_are_uniform(motif_numpy))
    motif_numpy = motif_numpy[:, with_motif]
    motifs = motif_matrix.columns[with_motif]

    # Calculate enrichment score
    phenotypes = features_matrix.columns
    corr = _calc_pheno_feat_corr(features_numpy, phenotypes=phenotypes)
    enrichment_matrix = calc_enrichment_score(corr, motif_numpy,
                                              constant_inc=constant_inc)

    # Create a random distribution of enrichment scores
    if shuffle_labels:
        shuffle_matrix = MatrixShuffler(features_numpy, by_col=True,
                                        shuffle_each_row=shuffle_each_row)
    else:
        shuffle_matrix = MatrixShuffler(motif_numpy, by_col=False)

    if verbose:
        print 'Starting iterations'
    calc_null_distribs = ESnullDistribCalculator(features_numpy=features_numpy,
                                                 motif_numpy=motif_numpy,
                                                 phenotypes=phenotypes,
                                                 shuffle_func=shuffle_matrix,
                                                 constant_inc=constant_inc,
                                                 verbose=verbose,
                                                 shuffle_labels=shuffle_labels)
    null_enrichment_scores = list(run_parallel(calc_null_distribs,
                                               range(n_iter), n_threads))
    null_enrichment_scores = numpy.vstack(null_enrichment_scores).transpose()

    # Calculate pvalues
    pvalues = []
    for enrichment_row, distrib_row in zip(enrichment_matrix,
                                           null_enrichment_scores):
        calc_empirical_pvalue = EmpiricalPvalueCalculator(distrib_row,
                                                          'greater')
        pval = calc_empirical_pvalue(enrichment_row[0])
        pvalues.append(pval)

    # Prepare results data frame
    res = DataFrame(enrichment_matrix, index=motifs,
                    columns=['ES', 'rank'])
    rho_idxs = numpy.round(enrichment_matrix[:, 1] *
                           features_numpy.shape[0], 0).astype(int) - 1
    res['rho'] = numpy.fliplr([numpy.sort(corr)])[0][rho_idxs]
    res['pvalue'] = pvalues
    res['fdr'] = padjust(pvalues, method='fdr')
    return res


def perform_pseudo_gsea(motif_matrix, features_matrix, n_iter=1000,
                        verbose=False, n_threads=1):
    if features_matrix.shape[1] != 1:
        msg = 'The number of columns of the features matrix should be one. '
        msg += 'Values will be sorted according to its value'
        raise ValueError(msg)

    if numpy.any(motif_matrix.index != features_matrix.index):
        raise ValueError('Ids from the two matrices do not match')

    motif_numpy = motif_matrix.as_matrix()
    features_numpy = features_matrix[features_matrix.columns[0]].as_matrix()

    # Remove motifs that are not present in any sequence or all of them
    motif_numpy[motif_numpy > 0] = 1
    with_motif = numpy.logical_not(cols_are_uniform(motif_numpy))
    motif_numpy = motif_numpy[:, with_motif]
    motifs = motif_matrix.columns[with_motif]

    # Calculate enrichment score
    enrichment_matrix = calc_enrichment_score(features_numpy, motif_numpy,
                                              constant_inc=1)

    # Create a random distribution of enrichment scores
    shuffle_matrix = MatrixShuffler(motif_numpy, by_col=False)
    if verbose:
        print 'Starting iterations'
    calc_null_distribs = ESnullDistribCalculator(features_numpy=features_numpy,
                                                 motif_numpy=motif_numpy,
                                                 phenotypes=None,
                                                 shuffle_func=shuffle_matrix,
                                                 constant_inc=1,
                                                 verbose=verbose,
                                                 shuffle_labels=False)
    calc_null_distribs.corr_array = features_numpy
    if n_threads == 1:
        map_distribs = map
    else:
        workers = Pool(processes=n_threads)
        map_distribs = workers.map
    null_enrichment_scores = list(map_distribs(calc_null_distribs,
                                               range(n_iter)))
    null_enrichment_scores = numpy.vstack(null_enrichment_scores).transpose()

    # Calculate pvalues
    pvalues = []
    for enrichment_row, distrib_row in zip(enrichment_matrix,
                                           null_enrichment_scores):
        calc_empirical_pvalue = EmpiricalPvalueCalculator(distrib_row,
                                                          'greater')
        pval = calc_empirical_pvalue(enrichment_row[0])
        pvalues.append(pval)

    # Prepare results data frame
    res = DataFrame(enrichment_matrix, index=motifs,
                    columns=['ES', 'rank'])
    rho_idxs = numpy.round(enrichment_matrix[:, 1] *
                           features_numpy.shape[0], 0).astype(int) - 1
    res['rho'] = numpy.fliplr([numpy.sort(features_numpy)])[0][rho_idxs]
    res['pvalue'] = pvalues
    res['fdr'] = padjust(pvalues, method='fdr')
    return res
