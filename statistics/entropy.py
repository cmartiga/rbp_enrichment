#!/usr/bin/env python

import numpy


def calc_elements_counts(values):
    values = numpy.array(values)
    counts = []
    for unique_value in numpy.unique(values):
        counts.append(numpy.sum(values == unique_value))
    return numpy.array(counts)


def _calc_optimum_lambda(counts):
    X = float(counts.shape[0])
    m = counts.sum()
    lambda_num = X * (m ** 2 - numpy.sum(counts * counts))
    lambda_denom = (m - 1) * (X * numpy.sum(counts * counts) - m ** 2)
    return lambda_num / lambda_denom


def _shrink_correction(counts):
    lambda_factor = _calc_optimum_lambda(counts)
    probabilities = lambda_factor / counts.shape[0]
    probabilities = probabilities + (1 - lambda_factor) * counts / counts.sum()
    return probabilities


def _miller_madow_correction(counts):
    probabilities = counts / float(counts.sum())
    entropy = -numpy.sum(probabilities * numpy.log2(probabilities))
    return entropy + (counts.shape[0] - 1) / (2 * counts.sum())


def calc_H(values, values2=None, estimator='empirical'):
    if values2 is not None:
        values = numpy.array(['{}.{}'.format(x, y)
                              for x, y in zip(values, values2)])
    counts = calc_elements_counts(values)
    if estimator == 'empirical':
        probabilities = counts / float(counts.sum())
    elif estimator == 'shrink':
        probabilities = _shrink_correction(counts)
    elif estimator == 'mm':
        return _miller_madow_correction(counts)
    else:
        raise ValueError('Estimator {} not supported'.format(estimator))
    entropy = -numpy.sum(probabilities * numpy.log2(probabilities))
    return entropy


def calc_MI(values1, values2, estimator='empirical', h1=None, h2=None,
            h12=None):
    if h1 is None:
        h1 = calc_H(values1, estimator=estimator)
    if h2 is None:
        h2 = calc_H(values2, estimator=estimator)
    if h12 is None:
        h12 = calc_H(values1, values2, estimator=estimator)
    return h1 + h2 - h12
