#!/usr/bin/env python

import numpy
import rpy2.robjects as R

from utils.settings import SUPPORTED_CORRECTION_METHODS


class EmpiricalPvalueCalculator(object):
    '''The two sided test takes the minimun of the 2 possible one-sided
       p-values multiplied by 2'''

    def __init__(self, distrib, alternative='greater'):
        self.distrib = numpy.sort(distrib)
        self.total = float(self.distrib.shape[0])
        self.alternative = alternative
        allowed = ['greater', 'lower', 'two-sided']
        if alternative not in allowed:
            msg = 'The only alternative hipothesis allowed are {}'
            raise ValueError(msg.format(allowed))

    def __call__(self, value):
        idx = numpy.searchsorted(self.distrib, value)
        if self.distrib[0] == self.distrib[-1]:
            return 1.0
        if self.alternative == 'greater':
            if idx == self.total:
                idx = self.total - 1
            return 1 - idx / self.total
        elif self.alternative == 'lower':
            if idx == 0:
                idx = 1
            return idx / self.total
        else:
            if idx == self.total:
                idx = self.total - 1
            greater_p = 1 - idx / self.total
            if idx == 0:
                idx = 1
            lower_p = idx / self.total
            return min([greater_p, lower_p]) * 2


def padjust(p_values, method='bonferroni'):
    supported_methods = SUPPORTED_CORRECTION_METHODS
    if method in supported_methods:
        return R.r['p.adjust'](R.FloatVector(p_values), method=method)
    else:
        msg = 'Correction method not implemented: {}'.format(method)
        raise ValueError(msg)


def calc_autocovariance(values, lag):
    values = numpy.array(values)
    N = values.shape[0]
    mean = numpy.mean(values)
    autocovariance = numpy.sum([(values[k] - mean) * (values[k + lag] - mean)
                                for k in range(lag, N - lag)]) / (N - 2 * lag)
    return autocovariance


def calc_autocorrelation(values, lag):
    return calc_autocovariance(values, lag) / calc_autocovariance(values, 0)
