#!/usr/bin/env python

import numpy
import matplotlib.pyplot as plt
from utils.misc import AppendableDict
from utils.plot import get_new_figure_and_canvas


def calc_rms(values, values2=None, polynomial_order=1, q_order=2):
    if not isinstance(values, type(numpy.array)):
        values = numpy.array(values)
    if polynomial_order == 0:
        pred = values.mean()
    else:
        if values2 is None:
            values2 = numpy.arange(values.shape[0])
        elif not isinstance(values2, numpy.array):
            values2 = numpy.array(values2)
        coefficients = numpy.polyfit(values2, values, polynomial_order)[::-1]
        pred = numpy.zeros(values.shape)
        for power, coeff in enumerate(coefficients):
            pred += coeff * (values2 ** power)
    if q_order == 0:
        return numpy.exp(0.5 * numpy.mean(numpy.log((values - pred) ** 2)))
    else:
        return (((values - pred) ** q_order).mean()) ** (1.0 / q_order)


def dfa(time_series, n_intervals, polynomial_order=1,
        bimodal_response=False, times=None, plot=False):
    q_order = 2
    time_series = numpy.cumsum(time_series - time_series.mean())
    rms_series = []

    max_size_log = numpy.log10(time_series.shape[0] / 5)
    interval_sizes_log = numpy.linspace(1, max_size_log, n_intervals)
    interval_sizes = (10 ** interval_sizes_log).astype(int)

    for n in interval_sizes:
        n_rms = []
        for i in range(0, time_series.shape[0] - n, n):
            interval = time_series[i: i + n]
            rms = calc_rms(interval, values2=times,
                           polynomial_order=polynomial_order,
                           q_order=2)
            n_rms.append(rms)
        n_rms = numpy.array(n_rms)
        if q_order == 0:
            n_rms = numpy.exp(0.5 * numpy.mean(numpy.log(n_rms ** 2)))
        else:
            n_rms = (((n_rms) ** q_order).mean()) ** (1.0 / q_order)
        rms_series.append(n_rms)

    if bimodal_response:
        slopes1 = []
        slopes2 = []
        for i in xrange(3, len(rms_series) - 3):
            slope = numpy.polyfit(numpy.log(interval_sizes[:i]),
                                  numpy.log(rms_series[:i]), degree=1)[0]
            slopes1.append(slope)

            slope = numpy.polyfit(numpy.log(interval_sizes[i:]),
                                  numpy.log(rms_series[i:]), degree=1)[0]
            slopes2.append(slope)
        slopes = numpy.vstack([slopes1, slopes2]).transpose()
        slopes = slopes[numpy.argmax(numpy.abs(slopes[0] - slopes[1])), :]
    else:
        if plot:
            fig = plt.figure()
            axes = fig.add_subplot(121)
            axes.plot(numpy.arange(time_series.shape[0]), time_series)
            axes = fig.add_subplot(122)
            axes.plot(numpy.log(interval_sizes), numpy.log(rms_series))
            fig.savefig('/tmp/temp_plot_dfa.png')
            plt.close(fig)

        slope = numpy.polyfit(numpy.log(interval_sizes), numpy.log(rms_series),
                              1)[0]
        slopes = [slope]

    return slopes


class MFDFAresult():
    def __init__(self, time_series, rms_series, interval_sizes, min_rms=None):
        self.rms_series = rms_series
        self.interval_sizes = interval_sizes
        self.time_series = time_series
        self.min_rms = min_rms

        hurst_coeffs = []
        q_orders = rms_series.keys()
        for q in q_orders:
            rms = numpy.array(rms_series[q])
            scales = numpy.array(interval_sizes)
            if min_rms is not None:
                sel_idxs = rms > min_rms
                rms = rms[sel_idxs]
                scales = scales[sel_idxs]
            hurst = numpy.polyfit(numpy.log(scales), numpy.log(rms), 1)[0]
            hurst_coeffs.append(hurst)
        self.hurst_coeffs = numpy.array(hurst_coeffs)
        self.q_orders = numpy.array(q_orders)

    @property
    def mass_exponent_tq(self):
        return self.hurst_coeffs * self.q_orders - 1

    @property
    def singularity_exponent_hq(self):
        return numpy.diff(self.mass_exponent_tq) / numpy.diff(self.q_orders)

    @property
    def singularity_dimension_Dq(self):
        return (self.q_orders[:-1] * self.singularity_exponent_hq -
                self.mass_exponent_tq[:-1])

    @property
    def multifractal_spectra_width(self):
        hq = self.singularity_exponent_hq
        return hq.max() - hq.min()

    def plot_series(self, axes):
        y = numpy.diff(self.time_series)
        axes.plot(numpy.arange(self.time_series.shape[0] - 1), y, 'b',
                  label='Original')
        axes.set_ylim(y.min() - 7, y.max() + 7)
        axes.legend(loc='upper left')
        axes.set_ylabel('System state')
        axes = axes.twinx()
        axes.plot(numpy.arange(self.time_series.shape[0]), self.time_series,
                  'r', label='Integrated')
        axes.legend(loc='upper right')
        axes.set_xlabel('Time units')
        axes.set_title('Time series')
        return axes

    def plot_RMS_vs_interval_size(self, axes, fit=True):
        for q, rms in self.rms_series.items():
            if q % 2 == 0:
                continue
            x = numpy.log(self.interval_sizes)
            y = numpy.log(rms)
            if self.min_rms is not None:
                sel_idxs = y > numpy.log(self.min_rms)
                x = x[sel_idxs]
                y = y[sel_idxs]
            axes.plot(x, y, 'o', label='q = {}'.format(q), lw=1.5, mew=1.5)
            axes.legend(loc=0)
            if fit:
                slope, intercept = numpy.polyfit(x, y, 1)
                pred = intercept + slope * x
                axes.plot(x, pred)

        axes.set_ylabel('log(RMS)')
        axes.set_xlabel('log(Interval size)')
        axes.set_title('RMS vs Interval size')
        return axes

    def plot_Hq_vs_q(self, axes, fit=True):
        axes.plot(self.q_orders, self.hurst_coeffs, 'o')
        if fit:
            slope, intercept = numpy.polyfit(self.q_orders,
                                             self.hurst_coeffs, 1)
            pred = intercept + slope * self.q_orders
            axes.plot(self.q_orders, pred)
        axes.set_ylabel('Hurst exponent')
        axes.set_xlabel('q-Order')
        axes.set_title('q-Hurst exponent vs q-Order')
        return axes

    def plot_multifractal_spectra(self, axes):
        x = self.singularity_exponent_hq
        y = self.singularity_dimension_Dq
        axes.plot(x, y, 'o')
        axes.set_ylabel('Singularity dimension (Dq)')
        axes.set_xlabel('Singularity exponent (hq)')
        axes.set_title('Multifractal spectra')
        return axes

    def plot(self, fhand):
        fig, canvas = get_new_figure_and_canvas(figsize=(16, 12))
        axes = fig.add_subplot(221)
        self.plot_series(axes)
        axes = fig.add_subplot(222)
        self.plot_RMS_vs_interval_size(axes, fit=True)
        axes = fig.add_subplot(223)
        self.plot_Hq_vs_q(axes, fit=True)
        axes = fig.add_subplot(224)
        self.plot_multifractal_spectra(axes)
        canvas.print_figure(fhand)
        fhand.flush()

    def fit_H_vs_q(self):
        slope, intercept = numpy.polyfit(self.q_orders, self.hurst_coeffs, 1)
        self.Hq_slope = slope
        self.Hq_intercept = intercept
        pred = intercept + slope * self.q_orders

        sstot = ((self.hurst_coeffs - self.hurst_coeffs.mean()) ** 2).sum()
        ssres = ((self.hurst_coeffs - pred) ** 2).sum()
        r2 = 1 - ssres / float(sstot)

        return slope, intercept, r2


def mfdfa(time_series, n_intervals, polynomial_order=1, times=None,
          min_rms=None):
    q_orders = numpy.arange(-5, 6)
    # TODO: Guess why I do not get good calculations for q = 0
    q_orders = q_orders[q_orders != 0]

    time_series = numpy.cumsum(time_series - time_series.mean())
    rms_series = AppendableDict()

    max_size_log = numpy.log10(time_series.shape[0] / 5)
    interval_sizes_log = numpy.linspace(1, max_size_log,
                                        n_intervals)
    interval_sizes = (10 ** interval_sizes_log).astype(int)

    for n in interval_sizes:
        n_rms = []
        for i in range(0, time_series.shape[0] - n, n):
            interval = time_series[i: i + n]
            rms = calc_rms(interval, values2=times,
                           polynomial_order=polynomial_order,
                           q_order=2)
            n_rms.append(rms)
        n_rms = numpy.array(n_rms)

        for q_order in q_orders:
            if q_order == 0:
                qn_rms = numpy.exp(0.5 * numpy.mean(numpy.log(n_rms ** 2)))
                msg = 'Calculation for q = 0 is not implemented yet'
                raise NotImplementedError(msg)
            else:
                qn_rms = (((n_rms) ** q_order).mean()) ** (1.0 / q_order)
            rms_series.append(q_order, qn_rms)

    return MFDFAresult(time_series, rms_series, interval_sizes,
                       min_rms=min_rms)


def calc_correlation_dimension(distance_matrix, n_values=30):
    '''Definition and nomenclature from
       https://en.wikipedia.org/wiki/Correlation_dimension'''

    start = numpy.log10(numpy.min(distance_matrix))
    stop = numpy.log10(numpy.max(distance_matrix))
    epsilon_values = 10 ** numpy.linspace(start, stop, n_values)
    corr_integral = [(distance_matrix <= e).mean() for e in epsilon_values]
    slope = numpy.polyfit(numpy.log(epsilon_values),
                          numpy.log(corr_integral), 1)[0]
    return slope
