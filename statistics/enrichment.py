#!/usr/bin/env python

import numpy
from pandas import DataFrame
from scipy.stats.stats import fisher_exact


class ContingencyTable(object):
    def __init__(self, array1, array2):
        if array1.shape != array2.shape:
            msg = 'Arrays must be the same size to calculate a '
            msg += 'contingency table'
            raise ValueError(msg)

        present1 = array1 > 0
        absent1 = numpy.logical_not(present1)
        present2 = array2 > 0
        absent2 = numpy.logical_not(present2)

        self.table = [[numpy.logical_and(present1, present2).sum(),
                       numpy.logical_and(present1, absent2).sum()],
                      [numpy.logical_and(absent1, present2).sum(),
                       numpy.logical_and(absent1, absent2).sum()]]

    def calc_odds_ratio(self):
        table = self.table
        odds_ratio = ((table[0][0] * table[1][1]) /
                      float(table[0][1] * table[1][0]))
        return odds_ratio

    def fisher_test(self, two_sided=True):
        if two_sided:
            alternative = 'two-sided'
        else:
            alternative = 'greater'
        self.odds_ratio, self.pvalue = fisher_exact(self.table, alternative)


def fisher_test(matrix, rownames1, rownames2, alternative='two-sided'):
    rownames1 = numpy.intersect1d(rownames1, matrix.index)
    rownames2 = numpy.intersect1d(rownames2, matrix.index)

    matrix1 = matrix.loc[rownames1, :]
    present1 = numpy.sum(matrix1 > 0, axis=0)
    absent1 = matrix1.shape[0] - present1

    matrix2 = matrix.loc[rownames2, :]
    present2 = numpy.sum(matrix2 > 0, axis=0)
    absent2 = matrix2.shape[0] - present2

    counts_matrix = numpy.vstack([present1, absent1, present2, absent2])
    colnames = ['present1', 'absent1', 'present2', 'absent2']
    counts_matrix = DataFrame(counts_matrix.transpose(),
                              columns=colnames, index=matrix.columns)
    pvals = []
    odds_ratios = []
    for i in range(counts_matrix.shape[0]):
        contingency_table = [counts_matrix.iloc[i, 0:2],
                             counts_matrix.iloc[i, 2:4]]
        oddsratio, pvalue = fisher_exact(contingency_table,
                                         alternative=alternative)
        pvals.append(pvalue)
        odds_ratios.append(oddsratio)
    counts_matrix['pvalue'] = pvals
    counts_matrix['OR'] = odds_ratios

    return counts_matrix
